from zspack import femsol, mshgen
from mpi4py import MPI
import numpy as np
import sys

femsol.Initialize(InitPetsc=True)

nx, ny, nz = 0, 0, 0
CpType = 0
try:
    nx = int(str(sys.argv[1]))
    ny = int(str(sys.argv[2]))
    nz = int(str(sys.argv[3]))
    CpType = int(str(sys.argv[4]))
except (IndexError, ValueError):
    raise ValueError("Il faut un nombre !")

repeat=(nx,ny,nz)
nodes_nbr=(3,3,3)

maillage, sd_coords = mshgen.CreateParallelCuboid(
                                start=(0,0,0),
                                stop=(1e3,1e3,1e3), # mm
                                num=nodes_nbr,
                                split=None,
                                repeat=repeat,
                                coords=True,
                                grp1=("F","Dx+"),
                                grp5=("N","Dx-")
                            )

InterfaceMapping, GlbIntrfcNodesNbr = mshgen.GetParallelInterfaceMapping(
                                                                num=nodes_nbr,
                                                                repeat=repeat,
                                                                Type="Dual"
                                                            )

LocIntrfcNodes, GlbIntrfcNodes = InterfaceMapping

modele = femsol.CreateModel(maillage, "3D")

material = femsol.CreateMaterial(
                                   type="isotrope",
                                   young= 210, # GPA
                                   poisson=0.3,
                                   gho=2.33,
                                   alpha=50,
                                   beta=1e-7
                                )

behavior   = femsol.CreateBehavior(
                                    maillage,
                                    All=material
                                  )

impdof = femsol.CreateBC( 
                        modele, 
                        grp5=(
                                ("DZ", 0.),
                                ("DY", 0.),
                                ("DX", 0.)
                            )
                        )

impeff = femsol.CreateLoad(modele)

impeff.ApplyFaceLoad(
                    grp1=(
                            ("Tx", 7.4), # GPA
                        )
                    )

StiffnessMatrix, GeneEffort = femsol.Assemble(
                                            modele,
                                            behavior,
                                            ImposedDof=impdof,
                                            ImposedEfforts=impeff,
                                            StiffnessMatrix=True,
                                            StiffnessIntegType="Complete",
                                            GeneralizedEffort=True,
                                            GeneEfrtIntegType="Complete",
                                            StorageType="Sparse"
                                        )

LocDofsNumbering = StiffnessMatrix.GetDofNumbering()
LocalMatrix = StiffnessMatrix.GetValues(StorageType="Sparse")
LocalRhs = GeneEffort.GetValues(Copy=True)
GeneEffPartIdx, GeneEffPartVal = StiffnessMatrix.GetGeneEffPart()
LocalRhs[GeneEffPartIdx] += GeneEffPartVal

InterfaceNumbering = femsol.CreateInterfaceNumbering(
                                                GlbIntrfcNodesNbr, 
                                                LocIntrfcNodes, 
                                                GlbIntrfcNodes, 
                                                LocDofsNumbering, 
                                                Type="Dual"
                                            )

# InterfaceNumbering.PrintInfo()

CpTypes = [None, 'kernel', 'geneo']

FetiSolver = femsol.CreateFetiSolver(
                                InterfaceNumbering, 
                                LocalMatrix, 
                                LocalRhs,
                                PrecondType='lumped',
                                CoarseSpace=CpTypes[CpType]
                            )

FetiSolver.execute(mtype="CG", residual_history=True)

# dspfield_C = FetiSolver.solveLocalProblem(
#     StiffnessMatrix.GetImposedDof())

# ResObject = femsol.CreateResults(modele)
# ResObject.AddResult(dspfield_C)
# ResObject.Export("test_feti")






























