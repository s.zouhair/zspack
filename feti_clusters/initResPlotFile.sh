#!/bin/bash

initResPlotFile () {
    echo "import matplotlib.pyplot as plt" >> $1
    echo "import numpy as np" >> $1
    echo  >> $1
    echo "plt.figure(dpi=1200)" >> $1
    echo >> $1
    echo "shape = ($2, $3)" >> $1
    echo >> $1 
    echo "def getIndex(ProcsNbr, shape=None, dim=None):" >> $1
    echo "    assert(len(shape) == 2 and dim in [1, 2, 3])" >> $1
    echo "    Index = int(round(pow(ProcsNbr, 1 / dim))) - 2" >> $1
    echo "    assert(pow(Index + 2, dim) == ProcsNbr)" >> $1
    echo "    result = np.unravel_index(Index, shape)" >> $1
    echo "    if shape[0] == 1: result = result[1]," >> $1
    echo "    elif shape[1] == 1: result = result[0]," >> $1
    echo "    return result" >> $1
    echo >> $1
    echo "def getAxis(axis, ProcsNbr, shape=shape, dim=$4):" >> $1
    echo "    Index = getIndex(ProcsNbr, shape=shape, dim=dim)" >> $1
    echo "    try:" >> $1
    echo "        r, c = Index" >> $1
    echo "        return axis[r, c]" >> $1
    echo "    except ValueError:" >> $1
    echo "        return axis if shape[0] == 1 and \\" >> $1
     echo "           shape[1] == 1 else axis[Index[0]]" >> $1
    echo >> $1
    echo "figure, axis = plt.subplots($2, $3, figsize=(10 * $3, 10 * $2), constrained_layout=True)" >> $1
    echo "figure.suptitle('Residual at each iteration', fontsize=24)" >> $1
    echo "figure.supxlabel('iterations', fontsize=16)" >> $1
    echo "figure.supylabel('residual', fontsize=16)" >> $1
    echo >> $1
}

finalizeResPlotFile () {
    echo >> $1
    echo "plt.savefig(\"$2\")" >> $1
}