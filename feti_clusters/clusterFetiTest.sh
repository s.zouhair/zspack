#!/usr/bin/bash
#SBATCH --job-name="result"
#SBATCH --mem-per-cpu=1024
#SBATCH --nodes=3
#SBATCH --ntasks-per-node=42
#SBATCH --partition=cn
#SBATCH --time=04:00:00
#SBATCH --wckey=P10WB:ASTER
#SBATCH --error="/home/j65779/result_stderr.txt"
#SBATCH --output="/home/j65779/result_output.txt"
#SBATCH --exclusive

source /home/j65779/.myCronosEnv.sh

if [ $# -lt 5 ]; then
    echo "error: missing test parameters"
    echo "should be used: ./MaqJob file dim size pltrows pltcols"; exit
elif [ $# -gt 5 ]; then
    echo "error: too many parameters"
    echo "should be used: ./MaqJob file dim size pltrows pltcols"; exit
elif ! [ -f "$1" ]; then
    echo "$1 doesn't exists"; exit
elif [ $3 -lt 2 ]; then
    echo "size must be at least 2"; exit
fi

if [ $2 = 1 ]; then
    Param="\$i 1 1"
elif [ $2 = 2 ]; then
    Param="\$i \$i 1"
elif [ $2 = 3 ]; then
    Param="\$i \$i \$i"
else
    echo "dim must be either 1, 2 or 3"; exit
fi

MyHome=/home/j65779
Prefix=$(date +"%d_%m_%Y_%I_%M_%p")
PdfResFile=$MyHome/TestRes$2D_$Prefix.pdf
PlotFile=$MyHome/resplot_$Prefix.py
rm $PlotFile 2> /dev/null
touch $PlotFile


source $MyHome/dev/Maquette/feti_clusters/initResPlotFile.sh
initResPlotFile $PlotFile $4 $5 $2

export ZHRSMR_FETI_ITERATIONS_SAMPLE_NBR=20


for i in $(eval echo {2..$3})
do
    ProcsNbr=$(($i**$2))
    for cp in {0..2}
    do
        $(eval echo "mpiexec -n $ProcsNbr python3 $1 $Param $cp") >> $PlotFile
    done
done

finalizeResPlotFile $PlotFile $PdfResFile


