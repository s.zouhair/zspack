from . import fem_solver as femsol
from . import mesh_generator as mshgen

del globals()["fem_solver"], globals()["mesh_generator"]