"""
This module implements a mesh generator for a parametric rectangular
parallelepiped geometry (cuboid).

"""

from mpi4py import MPI
import numpy as np

from .... import fem_solver

create_mesh = fem_solver.CreateMesh
is_parallel_study = fem_solver.IsParallelStudy

################################## MESH CREATION ##################################

def create_parallel_cuboid(start, stop, num, split, repeat, comm=None, coords=False, **Groups):
    """
    Creates a parallel mesh for a cuboid geometry (rectangular parallelepiped).

    Parameters
    ----------
        start, stop, num, split : 'Tuples of 3 integers'
            See create_cuboid for more details.

        repeat : 'tuple'
            The number of subdomains along each axis (nx, ny, nz).
        
        Comm : 'MPI.Comm'
            The MPI communicator. If None, MPI.COMM_WORLD is used.
        
        Groups : 'dict'
            A list of predefined groups of nodes/faces to add to the mesh.
            See create_cuboid for more details.
        
        coords : 'bool'
            use to choose if the coordinates of the subdomain within 
            subdomains grid should be returned.

    """

    if not is_parallel_study():
        raise ValueError("Parallel cuboid mesh "
            "cannot be used for sequential studies")

    comm = MPI.COMM_WORLD if comm is None else comm
    comm_size = comm.Get_size()
    comm_rank = comm.Get_rank()

    x_sd_nbr, y_sd_nbr, z_sd_nbr = repeat

    if comm_size != x_sd_nbr * y_sd_nbr * z_sd_nbr:
        raise ValueError("The communicator size "
            "isn't compatible with subdomains number")
    
    sd_coord = {
        "x" : comm_rank % x_sd_nbr,
        "y" : (comm_rank % (x_sd_nbr * y_sd_nbr)) // x_sd_nbr,
        "z" : comm_rank // (x_sd_nbr * y_sd_nbr)
    }

    delta = {
        "x" : (stop[0] - start[0]) / x_sd_nbr,
        "y" : (stop[1] - start[1]) / y_sd_nbr,
        "z" : (stop[2] - start[2]) / z_sd_nbr
    }

    local_start = (
        start[0] + delta["x"] * sd_coord["x"],
        start[1] + delta["y"] * sd_coord["y"],
        start[2] + delta["z"] * sd_coord["z"]
    )

    local_stop = (
        local_start[0] + delta["x"],
        local_start[1] + delta["y"],
        local_start[2] + delta["z"]
    )

    """
    print(
        "The subdomain number", comm_rank,
        "\tcoord : ({}, {}, {})".format(*list(sd_coord.values())),
        "\tstart : ({}, {}, {})".format(*local_start),
        "\tstop  : ({}, {}, {})".format(*local_stop)
    )
    """

    to_delete = []
    for group_name, (_, group_code) in Groups.items():
        wrong_group = group_code == "Dx-" and sd_coord["x"] != 0 \
            or group_code == "Dx+" and sd_coord["x"] != x_sd_nbr - 1 \
            or group_code == "Dy-" and sd_coord["y"] != 0 \
            or group_code == "Dy+" and sd_coord["y"] != y_sd_nbr - 1 \
            or group_code == "Dz-" and sd_coord["z"] != 0 \
            or group_code == "Dz+" and sd_coord["z"] != z_sd_nbr - 1
        if wrong_group: to_delete.append(group_name)
    
    for group_name in to_delete:
        del Groups[group_name]


    seq_cuboid = create_cuboid(local_start, \
        local_stop, num, split, **Groups)

    if not coords:
        result = seq_cuboid
    elif isinstance(seq_cuboid, tuple):
        result = *seq_cuboid, sd_coord
    else: result = seq_cuboid, sd_coord

    return result

def create_cuboid(start, stop, num, split, **Groups):
    """
    Creates a mesh for a cuboid geometry (rectangular parallelepiped).
                                        
                     _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _  N2
                    /|                             /|
                   / |                            / |
                  /  |                           /  |
                 /_ _|_ _ _ _ _ _ _ _ _ _ _ _ _ /   |
                |    |                         |    |
                |    |                         |    |
                |    |                         |    |
                |    |                         |    |
                |    | _ _ _ _ _ _ _ _ _ _ _ _ | _ _|
                |   /                          |   /
                |  /                           |  /
                | /                            | /
                |/_ _ _ _ _ _ _ _ _ _ _ _ _ _ _|/ 
             N1 

    Parameters
    ----------
    start : 'Tuple of 3 integers'
        The coordinates of the node N1 (x1, y1, z1).

    stop : 'Tuple of 3 integers'
        The coordinates of the node N2 (x2, y2, z2).

        Nb : Preferably choose N1 and N2 such that :
            
                x2 > x1, y2 > y1 and z2 > z1.

    num : 'Tuple of 3 integers'
        The number of nodes along each axis (Nx, Ny, Nz).

    split : 'Tuple of 3 integers'
        If not None, it's the number of blocks along each axis (Bx, By, Bz).
        The mesh elements are splitted into Bx x By x Bz blocks grid. for each
        block an elements group is created and added to the mesh. This parameter
        is useful to assign more than one material to the mesh (because it creates
        the necessary elements groups).

    Groups : 'dict'
        A list of predefined groups of nodes/faces to add to the mesh. Here's
        how a group is declared :

                              group_name = (group_type, group_code)
        
            - group_name : the name of the group, it must not contains 
                           the character "." or start with the string 
                           "Node".
            
            - group_type : the type of the group, a single character.
                           'N' for a nodes group and 'F' for a faces
                           group (edges groups are not implemented yet).
            
            - group_code : a string representing the face of the cuboid,
                           for which the group will be created (where the
                           group's nodes / faces belong). Six values are
                           possible :

                           "Dx-" : the perpendicular face to the x axis,
                                   and which contains the node N1.

                           "Dx+" : the perpendicular face to the x axis,
                                   and which contains the node N2.
                           
                           "Dy-" : the perpendicular face to the y axis,
                                   and which contains the node N1.
                           
                           "Dy+" : the perpendicular face to the y axis,
                                   and which contains the node N2.
                           
                           "Dz-" : the perpendicular face to the z axis,
                                   and which contains the node N1.
                           
                           "Dz+" : the perpendicular face to the z axis,
                                   and which contains the node N2.

        Examples :

            - group_1 = ("N", "Dx+")

            - group_2 = ("F", "Dx+")

            - group_3 = ("N", "Dz-")

    Return
    ------
        split is None :
            the created mesh object is returned. It doesn't contain
            any elements group.

        split is not None :
            A tuple is returned. It's first element is the mesh object,
            the second is a 3D list that contains the names of elements
            groups, created by partitioning the mesh's elements into a
            blocks grid according to the parameter split. To get the name
            of a given block from the list, the same indices of the block
            within the blocks grid must be used (bx, by, bz). These
            indices can also be used to obtain the block name directly
            by formatting the string below :

                            "GRP_x{bx}_y{by}_z{bz}"

            Nb : it is recommended to use the returned list, as the string
            above may eventually change.
            
    """
    assert(all(_CheckEntryFloat_(x) for x in [start, stop]))
    assert(_CheckEntryInt_(num))
    if split is not None:   assert(_CheckEntryInt_(split))

    # Calculation of nodes number
    NodesNbr  = num[0] * num[1] * num[2]

    # Calculation of elements number
    ElmntsNbr = (num[0] - 1) * (num[1] - 1) * (num[2] - 1)

    # Calculation of coordinates array
    Coordinates  = _CalcCoordinates_(start, stop, num, NodesNbr)

    # Calculation of connectivity array
    Connectivity = _CalcConnectivity_(num, ElmntsNbr)

    mesh_grps = {}

    for grp_name, (grp_type, grp_tcode) in Groups.items():
        mesh_grps[grp_name] = grp_type, _GetIntrfc_(
            grp_type, grp_tcode, num, as_array=False)

    if split:
        assert(all( (x - 1) % y == 0  for x, y in zip(num, split)))
        elmnts = [[['' for k in range(split[2])] for j in range(split[1])] 
                                                    for i in range(split[0]) ]
        for x, y, z in np.ndindex(split):
            grp_name, grp_list  = _GetElmntsBlock_((x, y, z), split, num)
            mesh_grps[grp_name] = 'E', grp_list
            elmnts[x][y][z]     = grp_name

    NewMesh = create_mesh(NodesNbr, ElmntsNbr, 
        3, Coordinates, Connectivity, **mesh_grps)

    if split is not None:
        rslt = NewMesh, elmnts
    else:
        rslt = NewMesh

    return rslt

    # return  elmnts, mesh_grps, Coordinates, Connectivity

def _GetElmntsBlock_(coord, split, num):
    blk_size = tuple((n - 1) // s for n, s in zip(num, split))

    x_coord = np.tile(np.arange(blk_size[0]) 
                           + blk_size[0] * coord[0], blk_size[1] * blk_size[2])
    y_coord = np.tile((np.arange(blk_size[1]) 
                    + blk_size[1] * coord[1]).repeat(blk_size[0]), blk_size[2])
    z_coord = (np.arange(blk_size[2]) 
                    + blk_size[2] * coord[2]).repeat(blk_size[0] * blk_size[1])

    rslt = x_coord + y_coord * (num[0] - 1) \
                                        + z_coord * (num[0] - 1) * (num[1] - 1)

    blk_name = "GRP_x%d_y%d_z%d" % (coord[0], coord[1], coord[2])

    return blk_name, list(rslt)

def _CalcCoordinates_(start, stop, num, NodesNbr):
    # Filling the coordinates 2D array
    x_coord, y_coord, z_coord = [np.linspace(st, sp, num=n) 
                                        for st, sp, n in zip(start, stop, num)]
    
    Coordinates      = np.empty((NodesNbr, 3), dtype=float)
    Coordinates[:,0] = np.tile(x_coord, num[1] * num[2])
    Coordinates[:,1] = np.tile(y_coord.repeat(num[0]), num[2])
    Coordinates[:,2] = z_coord.repeat(num[0] * num[1])
    del x_coord, y_coord, z_coord
    
    return Coordinates

def _CalcConnectivity_(num, ElmntsNbr):
    # Filling the connectivity 2D array
    x_idxs, y_idxs, z_idxs = [np.arange(n) for n in num]

    # Calculation of indices x, y and z for all nodes. Please note that the 
    # vectors *_idxs have the same dimension : (num[0]-1)*(num[1]-1)*(num[2]-1)
    x_idxs = np.tile(np.arange(num[0] - 1), (num[1] - 1) * (num[2] - 1))
    y_idxs = np.tile(np.arange(num[1] - 1).repeat(num[0] - 1), num[2] - 1)
    z_idxs = np.arange(num[2] - 1).repeat((num[0] - 1) * (num[1] - 1))

    temp_vec_1 = x_idxs + (y_idxs * num[0]) + (z_idxs * num[0] * num[1])
    del x_idxs, y_idxs, z_idxs

    offset = np.array([0                        , 1,
                       num[0] + 1               , num[0],
                       num[0] * num[1]          , num[0] * (num[1] + 1),
                       num[0] * (num[1] + 1) + 1, num[0] * num[1] + 1])

    Connectivity       = np.empty((ElmntsNbr, 9), dtype=int)
    Connectivity[:,0]  = 8 # 8 noeuds par hexaèdre
    Connectivity[:,1:] = np.add(temp_vec_1[:,np.newaxis], offset, 
                                                        out=Connectivity[:,1:])

    return Connectivity


def _CheckEntryInt_(entry):
    assert(isinstance(entry, tuple) and len(entry) == 3)
    assert(all(isinstance(x, int) for x in entry))
    return True

def _CheckEntryFloat_(entry):
    assert(isinstance(entry, tuple) and len(entry) == 3)
    assert(all(isinstance(x, (float, int)) for x in entry))
    return True

def _GetIntrfc_(GroupType, Face, num, as_array=True):
    if GroupType == 'N':
        return _GetIntrfcNodes_(Face, num, as_array=as_array)
    else:
        assert(GroupType == 'F')
        return _GetIntrfcElmnts_(Face, num, as_array=as_array)

def _GetIntrfcNodes_(Face, num, as_array=True):
    rslt = _GetIntrfcNodesAs2D_(Face, num).flatten()
    return rslt if as_array else list(rslt)

def _GetIntrfcElmnts_(Face, num, as_array=True):
    temp = _GetIntrfcNodesAs2D_(Face, num)
    nrow, ncol = temp.shape[0] - 1, temp.shape[1] - 1
    irow = (np.arange(nrow)[:,np.newaxis] 
                                + np.array([0, 0, 1, 1])).repeat(ncol, axis=0)
    icol = np.tile(np.arange(ncol)[:,np.newaxis] 
                                          + np.array([0, 1, 1, 0]), (nrow, 1))
    rslt = temp[irow, icol]
    return rslt if as_array else list(map(tuple, rslt))

def _GetIntrfcNodesAs2D_(Face, num):
    assert(isinstance(Face, str) and len(Face) == 3)
    assert(Face.endswith("+") or Face.endswith("-"))
    
    if Face.startswith("Dx"):
        nodes = np.arange(num[1], dtype=int) * num[0]
        steps = np.arange(num[2], dtype=int) * num[0] * num[1]
        rslt = nodes[:,np.newaxis] + steps
        if Face.endswith("+"):
            rslt += num[0] - 1
    elif Face.startswith("Dy"):
        nodes = np.arange(num[0], dtype=int)
        steps = np.arange(num[2], dtype=int) * num[0] * num[1]
        rslt = nodes[:,np.newaxis] + steps
        if Face.endswith("+"):
            rslt += num[0] * (num[1] - 1)
    else:
        assert(Face.startswith("Dz"))
        nodes = np.arange(num[0], dtype=int)
        steps = np.arange(num[1], dtype=int) * num[0]
        rslt = nodes[:,np.newaxis] + steps
        if Face.endswith("+"):
            rslt += num[0] * num[1] * (num[2] - 1)

    return rslt.transpose()

def nodes_idx__from_coord(start, stop, num, x_coord, y_coord, z_coord, srtdIdx=False):
    assert(all(_CheckEntryInt_(x) for x in [start, stop, num]))
    x_step = (stop[0] - start[0]) / (num[0] - 1)
    y_step = (stop[1] - start[1]) / (num[1] - 1)
    z_step = (stop[2] - start[2]) / (num[2] - 1)

    x_idx   = np.rint((x_coord - start[0]) / x_step).astype(np.int32)
    y_idx   = np.rint((y_coord - start[1]) / y_step).astype(np.int32)
    z_idx   = np.rint((z_coord - start[2]) / z_step).astype(np.int32)

    rslt = x_idx + y_idx * num[0] + z_idx * num[0] * num[1]

    sortedIdxs = np.argsort(rslt)
    NodesNbr   = num[0] * num[1] * num[2]
    if not np.all(rslt[sortedIdxs] == np.arange(NodesNbr)):
        raise Exception("Implementation error !!")

    return rslt if not srtdIdx else rslt, sortedIdxs

if __name__ == "__main__":
    num   = (5,4,6)
    start = (0,0,0)
    stop  = (8,9,5)
    split = (2,3,5)

    maillage, elmnt = create_cuboid(start, stop, num, split, 
                                            grp1=("N","Dx-"), grp2=("F","Dx+"))
    
    coord = maillage.Coordinates
    conec = maillage.Connectivity


    for idx, row in enumerate(coord):
        print("Nodes %d" % idx, row)

    print("")
    print("")

    for idx, row in enumerate(conec):
        print("Elmnt %d" % idx, row)

    print("")
    print("")
    
    print("Dx- : ")
    print("Nodes : ", _GetIntrfcNodes_("Dx-", num=num))
    print("Elmnt : ", _GetIntrfcElmnts_("Dx-", num=num))

    print("")

    print("Dx+ : ")
    print("Nodes : ", _GetIntrfcNodes_("Dx+", num=num))
    print("Elmnt : ", _GetIntrfcElmnts_("Dx+", num=num))

    print("")
    print("")

    print("Dy- : ")
    print("Nodes : ", _GetIntrfcNodes_("Dy-", num=num))
    print("Elmnt : ", _GetIntrfcElmnts_("Dy-", num=num))

    print("")

    print("Dy+ : ")
    print("Nodes : ", _GetIntrfcNodes_("Dy+", num=num))
    print("Elmnt : ", _GetIntrfcElmnts_("Dy+", num=num))

    print("")
    print("")

    print("Dz- : ")
    print("Nodes : ", _GetIntrfcNodes_("Dz-", num=num))
    print("Elmnt : ", _GetIntrfcElmnts_("Dz-", num=num))

    print("")


    print("Dz+ : ")
    print("Nodes : ", _GetIntrfcNodes_("Dz+", num=num))
    print("Elmnt : ", _GetIntrfcElmnts_("Dz+", num=num))