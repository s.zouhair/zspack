from mpi4py import MPI
import numpy as np

from .... import fem_solver
is_parallel_study = fem_solver.IsParallelStudy

def get_parralel_interface_global_to_local_mapping( \
    num=None, repeat=None, Type=None, comm=None):
    """
    Creates a parallel mesh for a cuboid geometry (rectangular parallelepiped).

    Parameters
    ----------
        num, repeat : 'Tuples of 3 integers'
            See get_interface_global_to_local_mapping for more details.
        
        Type : 'str'
            See get_interface_global_to_local_mapping for more details.

        Comm : MPI.Comm
            The MPI communicator. If None, MPI.COMM_WORLD is used.

    Return
    ------
        A tuple is returned. It's first element is a tuple of two arrays that 
        describes the local/global mapping of the subdomain interface. The 
        second element of the returned tuple is the number of nodes of the 
        interface.

                       ->   ((arr_1, arr_2), interface_nodes_nbr)
        
        with :
            
            arr_1 : the local numbering of subdomain nodes that belongs to the
            interface (redundant if Type == "Dual").
            
            arr_2 : the numbering of subdomain nodes that belong to the interface  
            whithin the global interface nodes.

    """

    if not is_parallel_study():
        raise ValueError("Parallel cuboid partitionning "
            "cannot be used for sequential studies")

    comm = MPI.COMM_WORLD if comm is None else comm
    comm_size = comm.Get_size()
    comm_rank = comm.Get_rank()

    x_sd_nbr, y_sd_nbr, z_sd_nbr = repeat

    if comm_size != x_sd_nbr * y_sd_nbr * z_sd_nbr:
        raise ValueError("The communicator size "
            "isn't compatible with subdomains number")

    sd_coord = (
        comm_rank % x_sd_nbr,
        (comm_rank % (x_sd_nbr * y_sd_nbr)) // x_sd_nbr,
        comm_rank // (x_sd_nbr * y_sd_nbr)
    )

    return get_interface_global_to_local_mapping( \
        num=num, repeat=repeat, OnlySd=sd_coord, Type=Type)

def get_interface_global_to_local_mapping(num=None, repeat=None, OnlySd=None, Type=None):
    """
    Partition a cuboid and return the local/global mapping of the interfaces
    of its subdomains. The cuboid is partitioned as a 3D grid of subdomains 
    which are also cuboids (see the parameter repeat). The discretization of 
    subdomains is uniform and is defined by the parameter num.

    Parameters
    ----------
        num : 'tuple'
            The number of nodes along each axis (Nx, Ny, Nz) for a subdomain.
        
        repeat : 'tuple'
            The number of subdomains along each axis (nx, ny, nz).

        Type : 'str'
            The method's type. Two options are possible, either "Primal" for 
            primal domain decomposition methods or "Dual" for dual domain
            decomposition methods.

    Return
    ------
        A tuple is returned. It's first element is a 3D list that contains the 
        local/global mapping of subdomains interfaces. For a given subdomain, 
        the mapping is a tuple of two arrays. The second element of the returned 
        tuple is the number of nodes of the interface. If Type == "Dual" and 
        since the dual description of the interface is redundant, a multiple node 
        is counted once for each pair of subdomains to which it belongs (e.g. a 
        node shared by 4 subdomains is counted 6 times). 

                       ->   (mapping_list, interface_nodes_nbr)
        
        Example : let's consider a subdomain of coordinates (sd_x, sd_y, sd_z) 
        within the 3D grid of subdomains. To access its mapping, just use :

                    arr_1, arr_2 = mapping_list[sd_z][sd_y][sd_x]
            
            arr_1 : the local numbering of subdomain nodes that belongs to the
            interface (redundant if Type == "Dual").
            
            arr_2 : the numbering of subdomain nodes that belong to the interface  
            whithin the global interface nodes.

    """
    assert(all(_CheckEntry_(x) for x in [num, repeat]))
    assert(Type in ["Dual", "Primal"])

    num_map = {
        'x' : num[0],
        'y' : num[1],
        'z' : num[2]
    }

    plans_map = {
        'x' : (repeat[0] - 1, 'z', 'y'),
        'y' : (repeat[1] - 1, 'z', 'x'),
        'z' : (repeat[2] - 1, 'y', 'x')
    }

    edges_map = {
        'x' : ((plans_map['z'][0], plans_map['y'][0]), plans_map['x'][0] + 1),
        'y' : ((plans_map['z'][0], plans_map['x'][0]), plans_map['y'][0] + 1),
        'z' : ((plans_map['y'][0], plans_map['x'][0]), plans_map['z'][0] + 1),
    }

    nodes_shape = (plans_map['z'][0], plans_map['y'][0], plans_map['x'][0])

    start_idx = 0
    x_plans, start_idx = _GetPlans_(plans_map, 'x', num_map, start_idx)
    y_plans, start_idx = _GetPlans_(plans_map, 'y', num_map, start_idx)
    z_plans, start_idx = _GetPlans_(plans_map, 'z', num_map, start_idx)
    plans = [x_plans, y_plans, z_plans]

    x_edges, start_idx = _GetEdges_(edges_map, 'x', num_map, start_idx, Type)
    y_edges, start_idx = _GetEdges_(edges_map, 'y', num_map, start_idx, Type)
    z_edges, start_idx = _GetEdges_(edges_map, 'z', num_map, start_idx, Type)
    edges = [x_edges, y_edges, z_edges]

    nodes, start_idx = _GetNodes_(nodes_shape, start_idx, Type)

    result = []

    if OnlySd is None:
        for z_idx in range(repeat[2]):
            result.append([])
            for y_idx in range(repeat[1]):
                result[z_idx].append([])
                for x_idx in range(repeat[0]):
                    result[z_idx][y_idx].append(Get_SD_G_To_L_Int_Nmbrg(x_idx,
                            y_idx, z_idx, repeat, num, plans, edges, nodes, Type))
    else:
        assert(_CheckEntry_(OnlySd))
        x_idx, y_idx, z_idx = OnlySd
        result = Get_SD_G_To_L_Int_Nmbrg(x_idx, y_idx, 
            z_idx, repeat, num, plans, edges, nodes, Type)

    total_intrcfc_nodes = start_idx

    return result, total_intrcfc_nodes

def Get_SD_G_To_L_Int_Nmbrg(x_idx, y_idx, z_idx, repeat, num, plans, edges, nodes, intrfc_type):

    plans_info, edges_info, nodes_info = _InitDicts_(x_idx, y_idx, z_idx, repeat)

    have_plans, plans_map = plans_info
    have_edges, edges_map, edges_dual_nmbrng = edges_info
    have_nodes, nodes_map, nodes_dual_nmbrng = nodes_info

    local_indices  = ()
    intrfc_indices = ()

    for axe in ['x', 'y', 'z']:
        for sign in ['-', '+']:
            plan_name = "D{}{}".format(axe, sign)
            if have_plans[plan_name]:
                local_indices  += _GetSubPlanIndices_(plan_name, plans_map, num),
                intrfc_indices += _GetSubPlanIntrfcIndices_(x_idx, y_idx, z_idx, plan_name, plans),

    for axe in ['x', 'y', 'z']:
        for nbr in range(4):
            edge_name = "D{}{}".format(axe, nbr)
            if have_edges[edge_name]:
                local_indices  += _GetSubEdgeIndices_(edge_name, edges_map, num, intrfc_type),
                intrfc_indices += _GetSubEdgeIntrfcIndices_(x_idx, y_idx, z_idx, edge_name, edges,
                                                                    intrfc_type, edges_dual_nmbrng),

    for nbr in range(8):
        node_name = "N{}".format(nbr)
        if have_nodes[node_name]:
            local_indices  += _GetSubNodeIndice_(node_name, nodes_map, num, intrfc_type),
            intrfc_indices += _GetSubNodeIntrfcIndices_(x_idx, y_idx, z_idx, node_name, nodes,
                                                                    intrfc_type, nodes_dual_nmbrng),

    return np.concatenate(local_indices, axis=None), np.concatenate(intrfc_indices, axis=None)



def _GetSubNodeIndice_(node_name, nodes_map, num, intrfc_type):
    x_idx, y_idx, z_idx = nodes_map[node_name]
    
    z_stride = num[0] * num[1] * (num[2] - 1)
    y_stride = num[0] * (num[1] - 1)
    x_stride = num[0] - 1

    node_idx = z_idx * z_stride + y_idx * y_stride + x_idx * x_stride
    shape = 1 if intrfc_type == "Primal" else 7

    return np.full(shape, node_idx, dtype=int)

def _GetSubNodeIntrfcIndices_(x_idx, y_idx, z_idx, node_name, nodes,
                                        intrfc_type, nodes_dual_nmbrng):

    if node_name.endswith("0"):
        x_coord = x_idx - 1
        y_coord = y_idx - 1
        z_coord = z_idx - 1
    elif node_name.endswith("1"):
        x_coord = x_idx
        y_coord = y_idx - 1
        z_coord = z_idx - 1
    elif node_name.endswith("2"):
        x_coord = x_idx - 1
        y_coord = y_idx
        z_coord = z_idx - 1
    elif node_name.endswith("3"):
        x_coord = x_idx
        y_coord = y_idx
        z_coord = z_idx - 1
    elif node_name.endswith("4"):
        x_coord = x_idx - 1
        y_coord = y_idx - 1
        z_coord = z_idx
    elif node_name.endswith("5"):
        x_coord = x_idx
        y_coord = y_idx - 1
        z_coord = z_idx
    elif node_name.endswith("6"):
        x_coord = x_idx - 1
        y_coord = y_idx
        z_coord = z_idx
    else:
        x_coord = x_idx
        y_coord = y_idx
        z_coord = z_idx

    node_idx = nodes[z_coord, y_coord, x_coord]

    if intrfc_type == "Dual":
        indices = nodes_dual_nmbrng[node_name[1]]
        result = node_idx[indices]
    else:
        result = np.array([node_idx])

    return result

def _GetSubEdgeIntrfcIndices_(SD_x_idx, SD_y_idx, SD_z_idx, edge_name, edges,
                                                intrfc_type, edges_dual_nmbrng):

    if edge_name.startswith("Dx"):
        edge_type = 0
        edge_nbr  = SD_x_idx
        if edge_name.endswith("0"):
            row_idx = SD_z_idx - 1
            col_idx = SD_y_idx - 1
        elif edge_name.endswith("1"):
            row_idx = SD_z_idx - 1
            col_idx = SD_y_idx
        elif edge_name.endswith("2"):
            row_idx = SD_z_idx
            col_idx = SD_y_idx - 1
        else:
            row_idx = SD_z_idx
            col_idx = SD_y_idx
    elif edge_name.startswith("Dy"):
        edge_type = 1
        edge_nbr  = SD_y_idx
        if edge_name.endswith("0"):
            row_idx = SD_z_idx - 1
            col_idx = SD_x_idx - 1
        elif edge_name.endswith("1"):
            row_idx = SD_z_idx - 1
            col_idx = SD_x_idx
        elif edge_name.endswith("2"):
            row_idx = SD_z_idx
            col_idx = SD_x_idx - 1
        else:
            row_idx = SD_z_idx
            col_idx = SD_x_idx
    else:
        edge_type = 2
        edge_nbr  = SD_z_idx
        if edge_name.endswith("0"):
            row_idx = SD_y_idx - 1
            col_idx = SD_x_idx - 1
        elif edge_name.endswith("1"):
            row_idx = SD_y_idx - 1
            col_idx = SD_x_idx
        elif edge_name.endswith("2"):
            row_idx = SD_y_idx
            col_idx = SD_x_idx - 1
        else:
            row_idx = SD_y_idx
            col_idx = SD_x_idx

    start_idx, stop_idx = edges[edge_type][row_idx][col_idx][edge_nbr]

    result = np.arange(start_idx, stop_idx, dtype=int)

    if intrfc_type == "Dual":
        bloc_size = result.size // 6
        indices = ((edges_dual_nmbrng[edge_name[2]] * bloc_size)[:, None] + \
                                    np.arange(bloc_size, dtype=int)).flatten()
        result = result[indices]

    return result

def _GetSubPlanIntrfcIndices_(SD_x_idx, SD_y_idx, SD_z_idx, plan_name, plans):
    
    if plan_name.startswith("Dx"):
        plan_type = 0
        row_idx   = SD_z_idx
        col_idx   = SD_y_idx
        if plan_name.endswith("+"):
            plan_nbr = SD_x_idx
        else:
            plan_nbr = SD_x_idx - 1
    elif plan_name.startswith("Dy"):
        plan_type = 1
        row_idx   = SD_z_idx
        col_idx   = SD_x_idx
        if plan_name.endswith("+"):
            plan_nbr = SD_y_idx
        else:
            plan_nbr = SD_y_idx - 1
    else:
        plan_type = 2
        row_idx   = SD_y_idx
        col_idx   = SD_x_idx
        if plan_name.endswith("+"):
            plan_nbr = SD_z_idx
        else:
            plan_nbr = SD_z_idx - 1

    start_idx, stop_idx = plans[plan_type][plan_nbr][row_idx, col_idx]

    return np.arange(start_idx, stop_idx, dtype=int)

def _GetSubPlanIndices_(plan_name, plans_map, num):
    down_edge, up_edge, left_edge, right_edge = plans_map[plan_name]

    plan_indices = _GetPlanIndices_(plan_name, num)
    rows_nbr, cols_nbr = plan_indices.shape

    first_row = 1 if down_edge else 0
    last_row  = rows_nbr - 1 if up_edge else rows_nbr

    first_col = 1 if left_edge else 0
    last_col  = cols_nbr - 1 if right_edge else cols_nbr

    return plan_indices[first_row:last_row, first_col:last_col].flatten()

def _GetPlanIndices_(plan_name, num):
    assert(isinstance(plan_name, str) and len(plan_name) == 3)
    assert(plan_name[:2] in ["Dx", "Dy", "Dz"])
    assert(plan_name[2] in ["+", "-"])

    if plan_name.startswith("Dx"):
        cols_nbr = num[1]
        rows_nbr = num[2]
        cols_stride = num[0]
        rows_stride = num[0] * num[1]
        if plan_name.endswith("-"):
            start_node = 0
        else:
            start_node = num[0] - 1
    elif plan_name.startswith("Dy"):
        cols_nbr = num[0]
        rows_nbr = num[2]
        cols_stride = 1
        rows_stride = num[0] * num[1]
        if plan_name.endswith("-"):
            start_node = 0
        else:
            start_node = num[0] * (num[1] - 1)
    else:
        cols_nbr = num[0]
        rows_nbr = num[1]
        cols_stride = 1
        rows_stride = num[0]
        if plan_name.endswith("-"):
            start_node = 0
        else:
            start_node = num[0] * num[1] * (num[2] - 1)

    temp_cols = np.arange(cols_nbr, dtype=int) * cols_stride
    temp_rows = np.arange(rows_nbr, dtype=int) * rows_stride

    return temp_rows[:, None] + temp_cols + start_node

def _GetSubEdgeIndices_(edge_name, edges_map, num, intrfc_type):
    first_node, second_node = edges_map[edge_name]

    edge_indices = _GetEdgeIndices_(edge_name, num)
    edge_size = edge_indices.size

    first_idx = 1 if first_node else 0
    last_idx  = edge_size - 1 if second_node else edge_size

    result = edge_indices[first_idx:last_idx]

    return result if intrfc_type == "Primal" else np.tile(result, 3)

def _GetEdgeIndices_(edge_name, num):
    assert(isinstance(edge_name, str) and len(edge_name) == 3)
    assert(edge_name[:2] in ["Dx", "Dy", "Dz"])
    assert(edge_name[2] in ["0", "1", "2", "3"])

    if edge_name.startswith("Dx"):
        nodes_nbr = num[0]
        nodes_stride = 1
        if edge_name.endswith("0"):
            start_node = 0
        elif edge_name.endswith("1"):
            start_node = num[0] * (num[1] - 1)
        elif edge_name.endswith("2"):
            start_node = num[0] * num[1] * (num[2] - 1)
        else:
            start_node = num[0] * (num[1] * num[2] - 1)

    elif edge_name.startswith("Dy"):
        nodes_nbr = num[1]
        nodes_stride = num[0]
        if edge_name.endswith("0"):
            start_node = 0
        elif edge_name.endswith("1"):
            start_node = num[0] - 1
        elif edge_name.endswith("2"):
            start_node = num[0] * num[1] * (num[2] - 1)
        else:
            start_node = num[0] * num[1] * (num[2] - 1) + num[0] - 1
    else:
        nodes_nbr = num[2]
        nodes_stride = num[0] * num[1]
        if edge_name.endswith("0"):
            start_node = 0
        elif edge_name.endswith("1"):
            start_node = num[0] - 1
        elif edge_name.endswith("2"):
            start_node = num[0] * (num[1] - 1)
        else:
            start_node = num[0] * num[1] - 1
    
    return np.arange(nodes_nbr, dtype=int) * nodes_stride + start_node

def _InitDicts_(x_idx, y_idx, z_idx, repeat):

    assert(x_idx < repeat[0])
    assert(y_idx < repeat[1])
    assert(z_idx < repeat[2])

    have_plans = {
        "Dx+" : x_idx < repeat[0] - 1,
        "Dx-" : x_idx > 0,
        
        "Dy+" : y_idx < repeat[1] - 1,
        "Dy-" : y_idx > 0,
        
        "Dz+" : z_idx < repeat[2] - 1,
        "Dz-" : z_idx > 0,
    }

    have_edges = {
        "Dx0" : have_plans["Dy-"] and have_plans["Dz-"],
        "Dx1" : have_plans["Dy+"] and have_plans["Dz-"],
        "Dx2" : have_plans["Dy-"] and have_plans["Dz+"],
        "Dx3" : have_plans["Dy+"] and have_plans["Dz+"],

        "Dy0" : have_plans["Dx-"] and have_plans["Dz-"],
        "Dy1" : have_plans["Dx+"] and have_plans["Dz-"],
        "Dy2" : have_plans["Dx-"] and have_plans["Dz+"],
        "Dy3" : have_plans["Dx+"] and have_plans["Dz+"],

        "Dz0" : have_plans["Dx-"] and have_plans["Dy-"],
        "Dz1" : have_plans["Dx+"] and have_plans["Dy-"],
        "Dz2" : have_plans["Dx-"] and have_plans["Dy+"],
        "Dz3" : have_plans["Dx+"] and have_plans["Dy+"],
    }

    have_nodes = {
        "N0" : have_edges["Dx0"] and have_edges["Dy0"],
        "N1" : have_edges["Dx0"] and have_edges["Dy1"],
        "N2" : have_edges["Dx1"] and have_edges["Dy0"],
        "N3" : have_edges["Dx1"] and have_edges["Dy1"],
        "N4" : have_edges["Dx2"] and have_edges["Dy2"],
        "N5" : have_edges["Dx2"] and have_edges["Dy3"],
        "N6" : have_edges["Dx3"] and have_edges["Dy2"],
        "N7" : have_edges["Dx3"] and have_edges["Dy3"],
    }

    plans_map = {
        "Dx+" : (have_edges["Dy1"], have_edges["Dy3"], have_edges["Dz1"], have_edges["Dz3"]),
        "Dx-" : (have_edges["Dy0"], have_edges["Dy2"], have_edges["Dz0"], have_edges["Dz2"]),
        
        "Dy+" : (have_edges["Dx1"], have_edges["Dx3"], have_edges["Dz2"], have_edges["Dz3"]),        
        "Dy-" : (have_edges["Dx0"], have_edges["Dx2"], have_edges["Dz0"], have_edges["Dz1"]),
        
        "Dz+" : (have_edges["Dx2"], have_edges["Dx3"], have_edges["Dy2"], have_edges["Dy3"]),
        "Dz-" : (have_edges["Dx0"], have_edges["Dx1"], have_edges["Dy0"], have_edges["Dy1"]),
    }

    edges_map = {
        "Dx0" : (have_nodes["N0"], have_nodes["N1"]),
        "Dx1" : (have_nodes["N2"], have_nodes["N3"]),
        "Dx2" : (have_nodes["N4"], have_nodes["N5"]),
        "Dx3" : (have_nodes["N6"], have_nodes["N7"]),

        "Dy0" : (have_nodes["N0"], have_nodes["N2"]),
        "Dy1" : (have_nodes["N1"], have_nodes["N3"]),
        "Dy2" : (have_nodes["N4"], have_nodes["N6"]),
        "Dy3" : (have_nodes["N5"], have_nodes["N7"]),

        "Dz0" : (have_nodes["N0"], have_nodes["N4"]),
        "Dz1" : (have_nodes["N1"], have_nodes["N5"]),
        "Dz2" : (have_nodes["N2"], have_nodes["N6"]),
        "Dz3" : (have_nodes["N3"], have_nodes["N7"]),
    }

    nodes_map = {
        "N0" : (0, 0, 0),
        "N1" : (1, 0, 0),
        "N2" : (0, 1, 0),
        "N3" : (1, 1, 0),
        "N4" : (0, 0, 1),
        "N5" : (1, 0, 1),
        "N6" : (0, 1, 1),
        "N7" : (1, 1, 1),
    }

    edges_dual_nmbrng = {
        "0" : np.array([1, 3, 4]),
        "1" : np.array([1, 2, 5]),
        "2" : np.array([0, 3, 5]),
        "3" : np.array([0, 2, 4])
    }

    nodes_dual_nmbrng = {
        "0" : np.array([ 3,  7, 11, 14, 18, 22, 24]),
        "1" : np.array([ 3,  6, 10, 12, 19, 23, 27]),        
        "2" : np.array([ 2,  7,  9, 15, 16, 23, 26]),
        "3" : np.array([ 2,  6,  8, 13, 17, 22, 25]),
        "4" : np.array([ 1,  5, 11, 15, 19, 20, 25]),
        "5" : np.array([ 1,  4, 10, 13, 18, 21, 26]),
        "6" : np.array([ 0,  5,  9, 14, 17, 21, 27]),
        "7" : np.array([ 0,  4,  8, 12, 16, 20, 24]),
    }

    plans_info = have_plans, plans_map
    edges_info = have_edges, edges_map, edges_dual_nmbrng
    nodes_info = have_nodes, nodes_map, nodes_dual_nmbrng

    return plans_info, edges_info, nodes_info

def _GetNodes_(nodes_shape, start_idx, intrfc_type):
    nx, ny, nz = nodes_shape

    intrfc_is_primal = intrfc_type == "Primal"

    nodes_nbr  = nx * ny * nz * (1 if intrfc_is_primal else 28)
    nodes_shape += () if intrfc_is_primal else (28,)

    nodes = None
    if nodes_nbr:
        stop_idx = start_idx + nodes_nbr
        nodes = np.arange(start_idx, stop_idx, dtype=int).reshape(nodes_shape)
        start_idx = stop_idx
    return nodes, start_idx

def _GetEdges_(edges_map, edges_type, num_map, start_idx, intrfc_type):
    assert(edges_type in ['x', 'y', 'z'])
    edges_shape, sub_edges_nbr = edges_map[edges_type]
    rows_nbr, cols_nbr = edges_shape
    edges = None
    if rows_nbr * cols_nbr:
        edges = [[np.full((sub_edges_nbr, 2), -1) for _ in range(cols_nbr)]
                                                     for _ in range(rows_nbr)]
        sub_edges_shape = num_map[edges_type]
        sub_edges_size  = _SetSubEdgesSize_(sub_edges_nbr, sub_edges_shape)
        mult_coef = 1 if intrfc_type == "Primal" else 6
        for i in range(rows_nbr):
            for j in range(cols_nbr):
                for idx in range(sub_edges_nbr):
                    edges[i][j][idx] = start_idx, start_idx + \
                                            mult_coef * sub_edges_size[idx]
                    start_idx += mult_coef * sub_edges_size[idx]
    return edges, start_idx

def _SetSubEdgesSize_(sub_edges_nbr, sub_edges_shape):
    sub_edges_size = np.zeros(sub_edges_nbr, dtype=int)
    for i in range(sub_edges_nbr):
        nodes_nbr = sub_edges_shape
        if i > 0:
            nodes_nbr -= 1
        if i < sub_edges_nbr - 1:
            nodes_nbr -= 1
        sub_edges_size[i] = nodes_nbr
    return sub_edges_size

def _GetPlans_(plans_map, plans_type, num_map, start_idx):
    assert(plans_type in ['x', 'y', 'z'])
    plans_nbr, rows_type, cols_type = plans_map[plans_type]
    plans = None
    if plans_nbr:
        rows_nbr = plans_map[rows_type][0] + 1
        cols_nbr = plans_map[cols_type][0] + 1
        sub_plans_shape = num_map[rows_type], num_map[cols_type]
        sub_plans_size = _SetSubPlansSize_(rows_nbr, cols_nbr, sub_plans_shape)
        plans = [np.full((rows_nbr, cols_nbr, 2), -1) for _ in range(plans_nbr)]
        for plan in plans:
            for i in range(rows_nbr):
                for j in range(cols_nbr):
                    plan[i,j] = start_idx, start_idx + sub_plans_size[i,j]
                    start_idx += sub_plans_size[i,j]
    return plans, start_idx

def _SetSubPlansSize_(rows_nbr, cols_nbr, SubPlansShape):
    sub_plans_size = np.zeros((rows_nbr, cols_nbr), dtype=int)

    for i in range(rows_nbr):
        for j in range(cols_nbr):
            nodes_per_row, nodes_per_col = SubPlansShape

            if i > 0 :
                nodes_per_row -= 1
            if j > 0 :
                nodes_per_col -= 1
            
            if i < rows_nbr - 1 : 
                nodes_per_row -= 1
            if j < cols_nbr - 1 :
                nodes_per_col -= 1
            
            sub_plans_size[i,j] = nodes_per_row * nodes_per_col

    return sub_plans_size

def _CheckEntry_(entry):
    assert(isinstance(entry, tuple) and len(entry) == 3)
    assert(all(isinstance(x, int) for x in entry))
    return True

if __name__ == "__main__":

    test_results_loc = {
        "Primal" : {
            "x1_y1_z1" : np.array([
                    15, 18, 27, 30, 39, 42, 17, 20, 29, 32, 
                    41, 44, 13, 25, 37, 22, 34, 46,  4,  7, 
                    52, 55,  1, 10, 49, 58,  3,  6,  5,  8, 
                    51, 54, 53, 56, 12, 24, 36, 14, 26, 38, 
                    21, 33, 45, 23, 35, 47,  0,  2,  9, 11, 
                    48, 50, 57, 59
                ]),

            "x2_y1_z1" : np.array([
                    15, 18, 27, 30, 39, 42, 13, 14, 25, 26,
                    37, 38, 22, 23, 34, 35, 46, 47,  4,  5,
                    7,  8, 52, 53, 55, 56,  1,  2, 10, 11, 
                    49, 50, 58, 59,  3,  6, 51, 54, 12, 24,
                    36, 21, 33, 45,  0,  9, 48, 57
                ]),

            "x2_y1_z2" : np.array([
                    15, 18, 27, 30, 39, 42, 51, 54, 13, 14,
                    25, 26, 37, 38, 49, 50, 22, 23, 34, 35,
                    46, 47, 58, 59,  4,  5,  7,  8,  1,  2,
                    10, 11,  3,  6, 12, 24, 36, 48, 21, 33,
                    45, 57,  0,  9
                ]),

            "x2_y2_z2" : np.array([
                    15, 18, 21, 27, 30, 33, 39, 42, 45, 51,
                    54, 57, 13, 14, 25, 26, 37, 38, 49, 50,
                    4,  5,  7,  8, 10, 11,  1,  2,  3,  6,
                    9, 12, 24, 36, 48, 0
                ])
        },

        "Dual" : {
            "x1_y1_z1" : np.array([
                    15, 18, 27, 30, 39, 42, 17, 20, 29, 32,
                    41, 44, 13, 25, 37, 22, 34, 46,  4,  7,
                    52, 55,  1,  1,  1, 10, 10, 10, 49, 49, 
                    49, 58, 58, 58,  3,  6,  3,  6,  3,  6,
                    5,  8,  5,  8,  5,  8, 51, 54, 51, 54,
                    51, 54, 53, 56, 53, 56, 53, 56, 12, 24, 
                    36, 12, 24, 36, 12, 24, 36, 14, 26, 38, 
                    14, 26, 38, 14, 26, 38, 21, 33, 45, 21, 
                    33, 45, 21, 33, 45, 23, 35, 47, 23, 35, 
                    47, 23, 35, 47,  0,  0,  0,  0,  0,  0,
                    0,  2,  2,  2,  2,  2,  2,  2,  9,  9,
                    9,  9,  9,  9,  9, 11, 11, 11, 11, 11, 
                    11, 11, 48, 48, 48, 48, 48, 48, 48, 50, 
                    50, 50, 50, 50, 50, 50, 57, 57, 57, 57,
                    57, 57, 57, 59, 59, 59, 59, 59, 59, 59
                ]),

            "x2_y1_z1" : np.array([
                    15, 18, 27, 30, 39, 42, 13, 14, 25, 26,
                    37, 38, 22, 23, 34, 35, 46, 47,  4,  5,
                    7,  8, 52, 53, 55, 56,  1,  2,  1,  2,
                    1,  2, 10, 11, 10, 11, 10, 11, 49, 50,
                    49, 50, 49, 50, 58, 59, 58, 59, 58, 59,
                    3,  6,  3,  6,  3,  6, 51, 54, 51, 54,
                    51, 54, 12, 24, 36, 12, 24, 36, 12, 24,
                    36, 21, 33, 45, 21, 33, 45, 21, 33, 45,
                    0,  0,  0,  0,  0,  0,  0,  9,  9,  9,
                    9,  9,  9,  9, 48, 48, 48, 48, 48, 48,
                    48, 57, 57, 57, 57, 57, 57, 57
                ]),

            "x2_y1_z2" : np.array([
                    15, 18, 27, 30, 39, 42, 51, 54, 13, 14,
                    25, 26, 37, 38, 49, 50, 22, 23, 34, 35,
                    46, 47, 58, 59,  4,  5,  7,  8,  1,  2,
                    1,  2,  1,  2, 10, 11, 10, 11, 10, 11,
                    3,  6,  3,  6,  3,  6, 12, 24, 36, 48,
                    12, 24, 36, 48, 12, 24, 36, 48, 21, 33,
                    45, 57, 21, 33, 45, 57, 21, 33, 45, 57,
                    0,  0,  0,  0,  0,  0,  0,  9,  9,  9,
                    9,  9,  9,  9
                ]),

            "x2_y2_z2" : np.array([
                    15, 18, 21, 27, 30, 33, 39, 42, 45, 51,
                    54, 57, 13, 14, 25, 26, 37, 38, 49, 50,
                    4,  5,  7,  8, 10, 11,  1,  2,  1,  2,
                    1,  2,  3,  6,  9,  3,  6,  9,  3,  6,
                    9, 12, 24, 36, 48, 12, 24, 36, 48, 12,
                    24, 36, 48, 0,  0,  0,  0,  0,  0,  0
                ])
        }
    }

    test_results_int = {
        "Primal" : {

            "x1_y1_z1" : (
                    np.arange( 41,  47, dtype=int), 
                    np.arange(129, 135, dtype=int), 
                    np.arange(202, 205, dtype=int), 
                    np.arange(257, 260, dtype=int), 
                    np.arange(305, 307, dtype=int), 
                    np.arange(345, 347, dtype=int), 
                    np.arange(368, 369, dtype=int), 
                    np.arange(373, 374, dtype=int), 
                    np.arange(378, 379, dtype=int), 
                    np.arange(383, 384, dtype=int), 
                    np.arange(389, 391, dtype=int), 
                    np.arange(397, 399, dtype=int), 
                    np.arange(405, 407, dtype=int), 
                    np.arange(413, 415, dtype=int), 
                    np.arange(422, 425, dtype=int), 
                    np.arange(433, 436, dtype=int), 
                    np.arange(444, 447, dtype=int), 
                    np.arange(455, 458, dtype=int), 
                    np.array([462, 463, 464, 465, 466, 467, 468, 469])
                ),

            "x2_y1_z1" : (
                    np.arange(129, 135, dtype=int),
                    np.arange(205, 211, dtype=int),
                    np.arange(260, 266, dtype=int),
                    np.arange(307, 311, dtype=int),
                    np.arange(347, 351, dtype=int),
                    np.arange(369, 371, dtype=int), 
                    np.arange(374, 376, dtype=int), 
                    np.arange(379, 381, dtype=int), 
                    np.arange(384, 386, dtype=int),
                    np.arange(397, 399, dtype=int), 
                    np.arange(413, 415, dtype=int),
                    np.arange(433, 436, dtype=int), 
                    np.arange(455, 458, dtype=int), 
                    np.array([463, 465, 467, 469])
                ),

            "x2_y1_z2" : (
                    np.arange(156, 164, dtype=int),
                    np.arange(223, 231, dtype=int),
                    np.arange(278, 286, dtype=int),
                    np.arange(347, 351, dtype=int),
                    np.arange(379, 381, dtype=int), 
                    np.arange(384, 386, dtype=int),
                    np.arange(413, 415, dtype=int),
                    np.arange(436, 440, dtype=int), 
                    np.arange(458, 462, dtype=int),
                    np.array([467, 469])
                ),

            "x2_y2_z2" : (
                    np.arange(164, 176, dtype=int),
                    np.arange(278, 286, dtype=int),
                    np.arange(360, 366, dtype=int),
                    np.arange(384, 386, dtype=int),
                    np.arange(415, 418, dtype=int),
                    np.arange(458, 462, dtype=int),
                    np.array([469])
                )
        },

        "Dual" : {
            "x1_y1_z1" : (
                    np.arange( 41,  47, dtype=int),
                    np.arange(129, 135, dtype=int),
                    np.arange(202, 205, dtype=int),
                    np.arange(257, 260, dtype=int),
                    np.arange(305, 307, dtype=int),
                    np.arange(345, 347, dtype=int),
                    np.arange(379, 380, dtype=int),
                    np.arange(381, 382, dtype=int),
                    np.arange(382, 383, dtype=int),
                    np.arange(409, 410, dtype=int),
                    np.arange(410, 411, dtype=int),
                    np.arange(413, 414, dtype=int),
                    np.arange(438, 439, dtype=int),
                    np.arange(441, 442, dtype=int),
                    np.arange(443, 444, dtype=int),
                    np.arange(468, 469, dtype=int),
                    np.arange(470, 471, dtype=int),
                    np.arange(472, 473, dtype=int),
                    np.arange(506, 508, dtype=int),
                    np.arange(510, 512, dtype=int),
                    np.arange(512, 514, dtype=int),
                    np.arange(554, 556, dtype=int),
                    np.arange(556, 558, dtype=int),
                    np.arange(562, 564, dtype=int),
                    np.arange(600, 602, dtype=int),
                    np.arange(606, 608, dtype=int),
                    np.arange(610, 612, dtype=int),
                    np.arange(648, 650, dtype=int),
                    np.arange(652, 654, dtype=int),
                    np.arange(656, 658, dtype=int),
                    np.arange(705, 708, dtype=int),
                    np.arange(711, 714, dtype=int),
                    np.arange(714, 717, dtype=int),
                    np.arange(771, 774, dtype=int),
                    np.arange(774, 777, dtype=int),
                    np.arange(783, 786, dtype=int),
                    np.arange(834, 837, dtype=int),
                    np.arange(843, 846, dtype=int),
                    np.arange(849, 852, dtype=int),
                    np.arange(900, 903, dtype=int),
                    np.arange(906, 909, dtype=int),
                    np.arange(912, 915, dtype=int),
                    np.array([945, 949, 953, 956, 960, 964, 966]),
                    np.array([973, 976, 980, 982, 989, 993, 997]),
                    np.array([1000, 1005, 1007, 1013, 1014, 1021, 1024]),
                    np.array([1028, 1032, 1034, 1039, 1043, 1048, 1051]),
                    np.array([1055, 1059, 1065, 1069, 1073, 1074, 1079]),
                    np.array([1083, 1086, 1092, 1095, 1100, 1103, 1108]),
                    np.array([1110, 1115, 1119, 1124, 1127, 1131, 1137]),
                    np.array([1138, 1142, 1146, 1150, 1154, 1158, 1162])
                ),

            "x2_y1_z1" : (
                    np.arange(129, 135, dtype=int),
                    np.arange(205, 211, dtype=int),
                    np.arange(260, 266, dtype=int),
                    np.arange(307, 311, dtype=int),
                    np.arange(347, 351, dtype=int),
                    np.arange(386, 388, dtype=int),
                    np.arange(390, 392, dtype=int),
                    np.arange(392, 394, dtype=int),
                    np.arange(416, 418, dtype=int),
                    np.arange(418, 420, dtype=int),
                    np.arange(424, 426, dtype=int),
                    np.arange(444, 446, dtype=int),
                    np.arange(450, 452, dtype=int),
                    np.arange(454, 456, dtype=int),
                    np.arange(474, 476, dtype=int),
                    np.arange(478, 480, dtype=int),
                    np.arange(482, 484, dtype=int),
                    np.arange(554, 556, dtype=int),
                    np.arange(558, 560, dtype=int),
                    np.arange(560, 562, dtype=int),
                    np.arange(648, 650, dtype=int),
                    np.arange(654, 656, dtype=int),
                    np.arange(658, 660, dtype=int),
                    np.arange(771, 774, dtype=int),
                    np.arange(777, 780, dtype=int),
                    np.arange(780, 783, dtype=int),
                    np.arange(900, 903, dtype=int),
                    np.arange(909, 912, dtype=int),
                    np.arange(915, 918, dtype=int),
                    np.array([973, 977, 981, 984, 988, 992, 994]),
                    np.array([1028, 1033, 1035, 1041, 1042, 1049, 1052]),
                    np.array([1083, 1087, 1093, 1097, 1101, 1102, 1107]),
                    np.array([1138, 1143, 1147, 1152, 1155, 1159, 1165])
                ),

            "x2_y1_z2" : (
                    np.arange(156, 164, dtype=int),
                    np.arange(223, 231, dtype=int),
                    np.arange(278, 286, dtype=int),
                    np.arange(347, 351, dtype=int),
                    np.arange(446, 448, dtype=int),
                    np.arange(450, 452, dtype=int),
                    np.arange(452, 454, dtype=int),
                    np.arange(476, 478, dtype=int),
                    np.arange(478, 480, dtype=int),
                    np.arange(484, 486, dtype=int),
                    np.arange(650, 652, dtype=int),
                    np.arange(654, 656, dtype=int),
                    np.arange(656, 658, dtype=int),
                    np.arange(790, 794, dtype=int),
                    np.arange(798, 802, dtype=int),
                    np.arange(802, 806, dtype=int),
                    np.arange(918, 922, dtype=int),
                    np.arange(930, 934, dtype=int),
                    np.arange(938, 942, dtype=int),
                    np.array([1085, 1089, 1093, 1096, 1100, 1104, 1106]),
                    np.array([1140, 1145, 1147, 1153, 1154, 1161, 1164])
                ),

            "x2_y2_z2" : (
                    np.arange(164, 176, dtype=int),
                    np.arange(278, 286, dtype=int),
                    np.arange(360, 366, dtype=int),
                    np.arange(476, 478, dtype=int),
                    np.arange(480, 482, dtype=int),
                    np.arange(482, 484, dtype=int),
                    np.arange(663, 666, dtype=int),
                    np.arange(669, 672, dtype=int),
                    np.arange(672, 675, dtype=int),
                    np.arange(922, 926, dtype=int),
                    np.arange(930, 934, dtype=int),
                    np.arange(934, 938, dtype=int),
                    np.array([1141, 1145, 1149, 1152, 1156, 1160, 1162])
                )
        }
    }


    num     = (3,4,5)
    repeat  = (3,3,3)

    sub_domains_coords = (
        (1, 1, 1), 
        (2, 1, 1),
        (2, 1, 2),
        (2, 2 ,2)
    )

    result = {
        "Primal" : get_interface_global_to_local_mapping(num, repeat, Type="Primal"),
        "Dual"   : get_interface_global_to_local_mapping(num, repeat, Type="Dual")
    }

    for sd_coords in sub_domains_coords:
        x_idx, y_idx, z_idx = sd_coords
        sub_domain_name = "x{}_y{}_z{}".format(x_idx, y_idx, z_idx)
        print("The sub-domain {} :".format(sub_domain_name))
        for intrfc_type in ["Primal", "Dual"]:
            print("\t{} interface :".format(intrfc_type))

            loc_ind_ref = test_results_loc[intrfc_type][sub_domain_name]
            int_ind_ref = np.concatenate(test_results_int[intrfc_type][sub_domain_name], axis=None)

            sd_result = result[intrfc_type][0][z_idx][y_idx][x_idx]

            print("\t\tLocal interface indices are correct ?  ->\t{}".format(np.all(sd_result[0] == loc_ind_ref)))
            print("\t\tGlobal interface indices are correct ? ->\t{}".format(np.all(sd_result[1] == int_ind_ref)))
        print("")
    
    # Test de l'option "OnlySd=XXX"
    res_refe, nods_nbr_refe = get_interface_global_to_local_mapping(num, repeat, Type="Dual")
    for idx in np.ndindex(repeat):
        sd_res = res_refe[idx[2]][idx[1]][idx[0]]
        res_temp, nods_nbr_temp = get_interface_global_to_local_mapping(num, repeat, OnlySd=idx, Type="Dual")
        print("The subdomain", idx, ":")
        print("\tThe number of nodes :", nods_nbr_temp == nods_nbr_refe)
        print("\tFirst element of the tuple :", np.all(sd_res[0] == res_temp[0]))
        print("\tSecond element of the tuple :", np.all(sd_res[1] == res_temp[1]))
        print("\n\n")

    def intrfc_pb_gb_size(intrfc_nodes_nbr, blocks_nbr=1, dtype="float"):
        bytes_nbr = 8 if dtype == "float" else 16
        dofs_nbr = intrfc_nodes_nbr * 3 * blocks_nbr
        mat_bytes_nbr = (dofs_nbr / 1024) * (dofs_nbr / 1024) * bytes_nbr
        return mat_bytes_nbr / 1024

    repeat_1   = (1,3,3)
    num_1 = (10, 10, 10)
    _, p_int_size = get_interface_global_to_local_mapping(num_1, repeat_1, Type="Primal")
    print(p_int_size)
    print("The size of the mCRE primal interface problem is : {} Gb.".format(intrfc_pb_gb_size(p_int_size, blocks_nbr=3, dtype="complex")))
    _, d_int_size = get_interface_global_to_local_mapping(num_1, repeat_1, Type="Dual")
    print(d_int_size)
    print("The size of the mCRE dual interface problem is : {} Gb.".format(intrfc_pb_gb_size(d_int_size, blocks_nbr=3, dtype="complex")))
    