"""
This module implements the BoundCond (Boundary conditions) and BoundLoad
(Boundary loads) classes.

"""

import numpy as np

from .. import model
from .. import pkgutils as utils

class BoundCond:
    """
    Boundary conditions class.

    Parameters
    ----------
    StudyModel : 'model object'
        The model to which boundary conditions will be applied.
    
    Groups : 'dict'
        A list of BC that will be applied. Here's how a new BC is declared :

                    group_name = ((DOF_1, V_1), ..., (DOF_n, V_n))

            - group_name : the name of the nodes group that will support the
                           new BC (the group should be already defined in the
                           mesh).
            
            - DOF_i      : The name of degree of freedom to be imposed.

            - V_i        : The value of DOF_i.
        
        Examples :
            - group_1 = (('DX', 5),)
            - group_2 = (('DZ', .5), ('DY', 3.))
            - group_3 = (('DZ', 4.7), ('DX', 2.), ('DY', 0.0))

    Attributes
    ----------
    StudyModel : 'model object'
        The underlying model to the boundary conditions object.
    
    ImposedValues : 'np.ndarray'
        An array where the values of the imposed degrees of freedom are stored.
        It's indexed by the object DofNumbering (for imposed DOFs).
    
    DofNumbering : 'DofNumbering object'
        The underlying DOF numbering object. Created when the first boundary 
        condition is added, and updated when other BC are applied.

    """

    def __init__(self, StudyModel, **Groups):
        assert(isinstance(StudyModel, model.model))
        self.StudyModel = StudyModel
        self.ImposedValues = np.array([], dtype=float)
        self.DofNumbering  = model.DofNumbering(StudyModel)
        self.AddImposedDof(**Groups)
    
    def AddImposedDof(self, **Groups):
        """
        Used to add new boundary conditions. Imposed values are inserted
        into the array 'BoundCond.ImposedValues', their indices within this
        array are used to update the DOF numbering object (BoundCond.DofNumbering),
        by adding them into the array DofNumbering.DofOrdering (check the class
        DofNumbering for more informations).

        """
        Studymesh = self.StudyModel.Studymesh
        for Group, GroupList in Groups.items():
            assert(isinstance(GroupList, tuple))
            assert(all((isinstance(x, tuple) and len(x) == 2) for x in GroupList))
            if utils.IsParallelStudy() and not Studymesh.ContainsGroup(Group):
                continue
            if Studymesh.GetGroupsType(Group) != "Nodes":
                raise ValueError("%s isn't a nodes group" % Group)
            ImposedDofList = []
            ImposedValList = []
            ProcessedDofs  = []
            for ImposedDofName, ImposedDofValue in GroupList:
                if ImposedDofName in ProcessedDofs:
                    raise ValueError("The DOF '%s' is repeated twice for the same nodes group (%s)!" % (ImposedDofName, Group))
                ProcessedDofs.append(ImposedDofName)
                ImposedDofList.append(self.StudyModel.GetModelDofOrder(ImposedDofName))
                ImposedValList.append(float(ImposedDofValue))
            ImposedValuesPositions = self.GetValuesPositions(ImposedValList)
            ImposedDofNodesList    = Studymesh.GetGroupItems(Group, GroupType="Nodes")
            ImposedDofList         = np.array(ImposedDofList, dtype=int)
            self.DofNumbering.UpdateNumbering(ImposedDofNodesList, ImposedDofList, ImposedValuesPositions)

    def GetValuesPositions(self, ValuesList):
        """
        Adds new values to the array BoundCond.ImposedValues, and returns
        the indices where they were inserted. If some values of the list 
        ValuesList exists already in BoundCond.ImposedValues, they aren't 
        reinserted (to avoid redundancies). This function should not be used.
        It is only used within AddImposedDof(...) under strict assumptions.
        
        """
        ImpoValArrSize = self.ImposedValues.size
        ValuesPosition = np.zeros(len(ValuesList))
        for CurrValIdx, value in enumerate(ValuesList):
            a = np.nonzero(self.ImposedValues==value)[0]
            if a.size:
                assert(a.size == 1)
                ValuesPosition[CurrValIdx] = a[0]
            else:
                self.ImposedValues = np.append(self.ImposedValues, value)
                ValuesPosition[CurrValIdx] = ImpoValArrSize
                ImpoValArrSize += 1
        return ValuesPosition

    def GetImpDofsVals(self, ValuesPosition):
        """
        Returns an array of BoundCond.ImposedValues's elements whose
        indices are given by the array ValuesPosition.
        
        """
        assert(self.ContainsImpDofs())
        assert(isinstance(ValuesPosition, np.ndarray) and ValuesPosition.ndim == 1)
        assert(np.amax(ValuesPosition) < self.ImposedValues.size)
        return self.ImposedValues[ValuesPosition]

    def GetModel(self):
        """Returns the underlying model to the boundary conditions object."""
        return self.StudyModel

    def GetDofNumbering(self):
        """
        Returns the underlying DofNumbering object to the boundary 
        conditions object.
        
        """
        return self.DofNumbering
    
    def ContainsImpDofs(self):
        return self.ImposedValues.size != 0

class BoundLoad:
    """
    Boundary loads class

    Parameters
    ----------
    StudyModel : 'model object'
        The model to which Boundary loads will be applied.

    Attributes
    ----------
    StudyModel : 'model object'
        The underlying model to the boundary conditions object.

    FacesEfforts : 'dict'
        A dictionary that stores boundary imposed faces efforts (loads).
        Its keys are faces groups (must be already defined in the mesh),
        and their corresponding values are arrays that contains the efforts
        vector (surface load).
    
    EdgesEfforts : 'dict'
        A dictionary that stores boundary imposed edges efforts (loads).
        Its keys are edges groups (must be already defined in the mesh),
        and their corresponding values are arrays that contains the efforts
        vector (line load).
    
    NodesEfforts : 'dict'
        A dictionary that stores boundary imposed nodes efforts (loads).
        Its keys are nodes groups (must be already defined in the mesh),
        and their corresponding values are arrays that contains the efforts
        vector (point load).

    """

    def __init__(self, StudyModel):
        assert(isinstance(StudyModel, model.model))
        self.StudyModel = StudyModel
        self.FacesEfforts = None
        self.EdgesEfforts = None
        self.NodesEfforts = None

    def ApplyFaceLoad(self, **Groups):
        """
        Adds new faces loads to Boundary loads object.

        Parameters
        ----------
            Groups : 'dict'
                A list of faces loads that will be applied. Here's how a new load
                is declared :

                          group_name = ((COMP_1, V_1), ..., (COMP_n, V_n))

                    - group_name : the name of the faces group that will support
                                   the load (the group should be already defined
                                   in the mesh).
            
                    - COMP_i     : The name of the component to be applied.

                    - V_i        : The value of COMP_i.

                Examples (3D modeling case):
                    - group_1 = (('Tx', 3.),)
                    - group_2 = (('Tz', 2.5), ('Ty', 5))
                    - group_3 = (('Tz', .7), ('Tx', 1.3), ('Ty', 4.0))

        """
        if not self.StudyModel.GetModelMesh().HasFaces():
            raise ValueError("The model doesn't support Faces efforts !!")
        return self.ApplyLoad("Faces", **Groups)

    def ApplyEdgeLoad(self, **Groups):
        """
        Adds new edges loads to Boundary loads object.

        Parameters
        ----------
            Groups : 'dict'
                A list of edges loads that will be applied. Here's how a new load
                is declared :

                          group_name = ((COMP_1, V_1), ..., (COMP_n, V_n))

                    - group_name : the name of the edges group that will support
                                   the load (the group should be already defined
                                   in the mesh).
            
                    - COMP_i     : The name of the component to be applied.

                    - V_i        : The value of COMP_i.

                Examples (3D modeling case):
                    - group_1 = (('Lx', 3.),)
                    - group_2 = (('Lz', 2.5), ('Ly', 5))
                    - group_3 = (('Lz', .7), ('Lx', 1.3), ('Ly', 4.0))
                
                Nb: Not implemented yet !!!

        """
        if not self.StudyModel.GetModelMesh().HasEdges():
            raise ValueError("The model doesn't support Edges efforts !!")
        return self.ApplyLoad("Edges", **Groups)

    def ApplyNodeLoad(self, **Groups):
        """
        Adds new nodes loads to Boundary loads object.

        Parameters
        ----------
            Groups : 'dict'
                A list of nodes loads that will be applied. Here's how a new load
                is declared :

                          group_name = ((COMP_1, V_1), ..., (COMP_n, V_n))

                    - group_name : the name of the nodes group that will support
                                   the load (the group should be already defined
                                   in the mesh).
            
                    - COMP_i     : The name of the component to be applied.

                    - V_i        : The value of COMP_i.

                Examples (3D modeling case):
                    - group_1 = (('Fx', 3.),)
                    - group_2 = (('Fz', 2.5), ('Fy', 5))
                    - group_3 = (('Fz', .7), ('Fx', 1.3), ('Fy', 4.0))

                Nb : Not implemented yet !!!

        """
        return self.ApplyLoad("Nodes", **Groups)

    def ApplyLoad(self, Type, **Groups):
        """
        It should not be used. It is only used within the functions 
        ApplyFaceLoad(...), ApplyEdgeLoad(...) and AddNodeLoad(...) under
        strict assumptions.
        
        """
        EffortsType = "%sEfforts" % Type
        Studymesh = self.StudyModel.Studymesh
        if not getattr(self, EffortsType):
            setattr(self, EffortsType, {})
        EffortsName = "%dD.%s" % (self.StudyModel.GetModelDimension(), EffortsType)
        EffortsArraySize = utils.GetEffortsArraySize(EffortsName)
        for Group, GroupList in Groups.items():
            assert(isinstance(GroupList, tuple))
            assert(all((isinstance(x, tuple) and len(x) == 2) for x in GroupList))
            if utils.IsParallelStudy() and not Studymesh.ContainsGroup(Group):
                continue
            if Studymesh.GetGroupsType(Group) != Type:
                raise ValueError("%s isn't a %s group" % (Group, Type))
            EffortsArray = self.GetEffortsArray(EffortsName, GroupList, EffortsArraySize, Group)
            if np.all(EffortsArray == 0):
                raise ValueError("It is useless to apply zero efforts to the %s group %s." % (Type, Group))
            elif Group in getattr(self, EffortsType):
                getattr(self, EffortsType)[Group] += EffortsArray
            else:
                getattr(self, EffortsType)[Group] = EffortsArray

    def GetEffortsArray(self, EffortType, ImposedComponents, EffortsArraySize, GroupName):
        """
        Computes the efforts vector when an new load is added, this vector
        is then used by ApplyLoad(...) that inserts it in one of the available
        dictionaries (according to the effort's type). This function should
        not be used. It is only used within the function ApplyLoad(...) under
        strict assumptions.
        
        """
        EffortsArray = np.zeros(EffortsArraySize, dtype=float)
        ProcessedComponents = []
        for ImposedCompName, ImposedCompValue in ImposedComponents:
            if ImposedCompName in ProcessedComponents:
                raise ValueError("The component '%s' is repeated twice for the same group (%s)!" % (ImposedCompName, GroupName))
            ProcessedComponents.append(ImposedCompName)
            ComponentIdx = utils.GetEffortComponentOrder(EffortType, ImposedCompName)
            EffortsArray[ComponentIdx] = ImposedCompValue
        return EffortsArray

    def GetFacesEfforts(self):
        """
        Returns faces loads.
        
        Return
        ------
            A tuple of two lists is returned :
            
                            (list_1, list_2)

                - list_1 : a list that contains the names of all the faces
                           groups where loads are applied.

                - list_2 : a list that contains efforts vectors (the order
                           of the first list is respected).

        """
        assert(self.HasFacesEfforts())
        return [*self.FacesEfforts.keys()], [*self.FacesEfforts.values()]

    def GetEdgesEfforts(self):
        """
        Returns edges loads.
        
        Return
        ------
            A tuple of two lists is returned :
            
                            (list_1, list_2)

                - list_1 : a list that contains the names of all the edges
                           groups where loads are applied.

                - list_2 : a list that contains efforts vectors (the order
                           of the first list is respected).

        """
        assert(self.HasEdgesEfforts())
        return [*self.EdgesEfforts.keys()], [*self.EdgesEfforts.values()]

    def GetNodesEfforts(self):
        """
        Returns nodes loads.
        
        Return
        ------
            A tuple of two lists is returned :
            
                            (list_1, list_2)

                - list_1 : a list that contains the names of all the nodes
                           groups where loads are applied.

                - list_2 : a list that contains efforts vectors (the order
                           of the first list is respected).

        """
        assert(self.HasNodesEfforts())
        return [*self.NodesEfforts.keys()], [*self.NodesEfforts.values()]

    def HasFacesEfforts(self):
        """Checks whether a BoundLoad object contains faces loads"""
        return not not self.FacesEfforts

    def HasEdgesEfforts(self):
        """Checks whether a BoundLoad object contains edges loads"""
        return not not self.EdgesEfforts

    def HasNodesEfforts(self):
        """Checks whether a BoundLoad object contains nodes loads"""
        return not not self.NodesEfforts

    def GetModel(self):
        """Returns the underlying model to the BoundLoad object."""
        return self.StudyModel
