"""
This module implements the linear dynamic operator.

"""

from scipy.linalg import solve
from scipy.sparse.linalg import spsolve

from .. import pkgutils as utils
from .. import pkgdatastr as datastr

def SolveLDYN(StiffMatrix=None, MassMatrix=None, 
    DampMatrix=None, GeneEfforts=None, Freq=None, StorageType=None):
    """
    Solves a linear dynamic problem and returns the resulting displacement
    field (complex valued).

    Parameters
    ----------
        StiffMatrix : 'Matrix object'
            The stiffness matrix object.

        MassMatrix : 'Matrix object'
            The mass matrix object.

        DampMatrix : 'Matrix object'
            The damping matrix object.
        
        GeneEfforts : 'Vector object'
            The generalized efforts vector object.

        Freq : 'float'
            The frequency.

        StorageType : 'str'
            The storage type to be used for the problem matrix, two options
            are possible :
            
                - "Dense"  : the whole matrix is stored.
                - "Sparse" : only non-zero entries of the matrix are stored
                             as a scipy sparse matrix (bsr_matrix)
        
        Nb : FE matrices and vectors must have the same underlying DOF's
        numbering object.

    Return
    ------
       A vector object containing the displacement field is returned
       (complex valued).

    """
    DampExist = DampMatrix is not None
    assert(isinstance(StiffMatrix, datastr.Matrix))
    assert(isinstance(MassMatrix, datastr.Matrix))
    assert(isinstance(DampMatrix, datastr.Matrix) or not DampExist)
    assert(isinstance(GeneEfforts, datastr.Vector))
    assert(StorageType in ["Dense", "Sparse"])
    assert(isinstance(Freq, float))

    if StiffMatrix.GetType() != "Stiffness":
        raise ValueError("The first matrix isn't a stiffness matrix !")
    elif MassMatrix.GetType() != "Mass":
        raise ValueError("The second matrix isn't a mass matrix !")
    elif DampExist and DampMatrix.GetType() != "Damping":
        raise ValueError("The third matrix isn't a damping matrix !")
    elif GeneEfforts.GetType() != "GeneEfforts":
        raise ValueError("The vector isn't a generalized efforts vector !")

    checkDofNumbering(StiffMatrix, MassMatrix, DampMatrix, GeneEfforts)
    DofNumbering = StiffMatrix.GetDofNumbering()
    ImposedDof   = StiffMatrix.GetImposedDof()

    mat = getLDYNMat(StiffMatrix, MassMatrix, DampMatrix, Freq, StorageType)
    rhs = getLDYNRhs(StiffMatrix, MassMatrix, DampMatrix, GeneEfforts, Freq)

    if StorageType == "Dense":
        rhs = solve(mat, rhs, overwrite_a=True, overwrite_b=True, check_finite=True)
    else:
        rhs = spsolve(mat.tocsr(), rhs, use_umfpack=True)

    result = datastr.Vector(DofNumbering, ImposedDof, VectorType="DofsField", ValuesBuff=rhs)

    return result

def getLDYNMat(StiffMatrix, MassMatrix, DampMatrix, Freq, StorageType):
    """
    xxx.

    """
    K = StiffMatrix.GetValues(StorageType=StorageType)
    M = MassMatrix.GetValues(StorageType=StorageType)
    
    Omg = utils.GetOmega(Freq)
    OmgSqrd = pow(Omg, 2)

    Result = K - OmgSqrd * M

    if DampMatrix is not None:
        C = DampMatrix.GetValues(StorageType=StorageType)
        Result = Result + 1j * Omg * C

    return Result

def getLDYNRhs(StiffMatrix, MassMatrix, DampMatrix, GeneEfforts, Freq):
    """
    xxx.

    """
    rhs = GeneEfforts.GetValues()
    if DampMatrix is not None:
        rhs = rhs.astype(complex)

    Omg = utils.GetOmega(Freq)
    OmgSqrd = pow(Omg, 2)

    StiffGeP, StiffGeV = StiffMatrix.GetGeneEffPart()
    rhs[StiffGeP] += StiffGeV

    MassGeP, MassGeV   = MassMatrix.GetGeneEffPart()
    rhs[MassGeP] += - OmgSqrd * MassGeV

    if DampMatrix is not None:
        DampGeP, DampGeV   = DampMatrix.GetGeneEffPart()
        rhs[DampGeP] += 1j * Omg * DampGeV

    return rhs

def checkDofNumbering(StiffMat, MassMat, DampMat, GeneEfforts):
    """
    Checks whether the FE matrices and the generalized efforts 
    vector have the same DofNumbering object.

    """
    a = StiffMat.GetDofNumbering()
    b = MassMat.GetDofNumbering()
    c = a if DampMat is None else DampMat.GetDofNumbering()
    d = GeneEfforts.GetDofNumbering()
    if not (a is b is c is d):
        raise ValueError("FE matrices and the"
            " generalized efforts vector are not "
            "defined on the same Dofs numbering")