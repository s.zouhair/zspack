"""
This module implements the linear elasticity operator.

"""
import numpy as np
from scipy.linalg import solve
from scipy.sparse.linalg import spsolve

from .. import pkgutils as utils
from .. import pkgdatastr as datastr

def SolveLE(StiffMatrix=None, GeneEfforts=None, \
    StorageType=None, ExtraBC=None, ExtraLoad=None):
    """
    Solves a linear static problem and returns the resulting displacement
    field.

    Parameters
    ----------
        StiffMatrix : 'Matrix object'
            The stiffness matrix object.
        
        GeneEfforts : 'Vector object'
            The generalized efforts vector object.

        StorageType : 'str'
            The storage type to be used for the problem matrix, two options
            are possible :
            
                - "Dense"  : the whole matrix is stored.
                - "Sparse" : only non-zero entries of the matrix are stored
                             as a scipy sparse matrix (bsr_matrix)
        
        ExtraBC : 'tuple or list'
            A tuple / list that contains additional degrees of freedom to be 
            imposed and their corresponding values

                            (dofs_indices, dofs_vals)
            
                - dofs_indices : an array containing additional degrees of 
                  freedom to be imposed.

                - dofs_indices : an array containing the values of additional 
                  degrees of freedom to be imposed.

        ExtraLoad : 'tuple or list'
            A tuple / list containing additional nodal forces to be applied 
            and their corresponding values.

                            (dofs_indices, dofs_vals)
            
                - dofs_indices : an array containing degrees of freedom where 
                  the additional nodal forces will be applied.

                - dofs_indices : an array containing the values of additional 
                  nodal forces to be applied.
        
        Nb : StiffMatrix and GeneEfforts must have the same underlying DOF's
        numbering object.

    Return
    ------
       A vector object containing the displacement field is returned.

    """
    assert(isinstance(StiffMatrix, datastr.Matrix))
    assert(isinstance(GeneEfforts, datastr.Vector))
    assert(StorageType in ["Dense", "Sparse"])

    if StiffMatrix.GetType() != "Stiffness":
        raise ValueError("The matrix isn't a stiffness matrix !")
    elif GeneEfforts.GetType() != "GeneEfforts":
        raise ValueError("The vector isn't a generalized efforts vector !")
    elif StiffMatrix.GetDofNumbering() is not GeneEfforts.GetDofNumbering():
        raise ValueError("The stiffness matrix and the generalized efforts" \
                         "vector are not defined on the same Dofs numbering")

    DofNumbering = StiffMatrix.GetDofNumbering()
    FreeDofsNbr  = DofNumbering.GetFreeDofNbr()
    
    if ExtraBC is not None:
        __CheckExtras__(ExtraBC, FreeDofsNbr)
    if ExtraLoad is not None:
        __CheckExtras__(ExtraLoad, FreeDofsNbr)

    mat, rhs, i_dofs_indices = __Get_Matrix_And_RHS__(StiffMatrix, \
            GeneEfforts, FreeDofsNbr, StorageType, ExtraBC, ExtraLoad)

    if StorageType == "Dense":
        rhs = solve(mat, rhs, sym_pos=True, \
            overwrite_b=True, check_finite=True)
    else:
        rhs = spsolve(mat.tocsr(), rhs, use_umfpack=True)
    
    if ExtraBC is not None:
        extra_bc_indices, extra_bc_vals = ExtraBC
        temp_vec = np.zeros(FreeDofsNbr, dtype=rhs.dtype)
        temp_vec[i_dofs_indices] = rhs
        temp_vec[extra_bc_indices] = extra_bc_vals
        rhs = temp_vec

    ImposedDof = StiffMatrix.GetImposedDof()
    result = datastr.Vector(DofNumbering, ImposedDof, \
                VectorType="DofsField", ValuesBuff=rhs)

    return result

def __Get_Matrix_And_RHS__(stiff_mat, gene_efrt, free_dofs_nbr, \
                                    storage_type, extra_bc, extra_load):

    mat = stiff_mat.GetValues(StorageType=storage_type)
    rhs = gene_efrt.GetValues(Copy=True)
    gep, gev = stiff_mat.GetGeneEffPart()
    rhs[gep] += gev

    if extra_load is not None:
        extra_load_indices, extra_load_vals = extra_load
        rhs[extra_load_indices] += extra_load_vals

    if extra_bc is None:
        res_mat = mat
        res_rhs = rhs
        i_dofs_indices = 0
    else:
        extra_bc_indices, extra_bc_vals = extra_bc
        temp_vec = np.full(free_dofs_nbr, True)
        temp_vec[extra_bc_indices] = False
        i_dofs_indices = np.nonzero(temp_vec)[0]
        mat_ii, mat_ib = utils.GetMatRows(mat, i_dofs_indices, \
                        Split=True, SplitOrdering=extra_bc_indices)
        res_mat = mat_ii
        res_rhs = rhs[i_dofs_indices] - mat_ib.dot(extra_bc_vals)
    return res_mat, res_rhs, i_dofs_indices

def __CheckExtras__(Extra, dofs_nbr):
    assert(isinstance(Extra, (tuple, list)) and len(Extra) == 2)
    indices, vals = Extra
    assert(isinstance(indices, np.ndarray) and indices.ndim == 1)
    assert(isinstance(vals, np.ndarray) and vals.ndim == 1)
    assert(indices.size == vals.size)
    assert(np.all(indices < dofs_nbr))

