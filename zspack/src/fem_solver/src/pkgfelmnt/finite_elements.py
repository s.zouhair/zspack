"""
This module implements the FiniteElement (Finite Element) class , and all
the methods that allow interacting with it.

"""

import numpy as np
import timeit

from .. import mesh
from .. import pkgutils as utils
from . import fe_cata

class FiniteElement:
    """
    Boundary conditions class

    Parameters
    ----------
    Dimension : 'int'
        The dimension of the finite element.

    NodesNbr : 'int'
        The nodes number of the finite element.

    Modeling : 'str'
        The modeling of the finite element. It must be defined in the
        catalogue, and it must support elements whose dimension and
        nodes number are given respectively by the parameters Dimension
        and NodesNbr.

    Attributes
    ----------
    NodesNbr : 'int'
        The nodes number of the finite element.

    Dimension : 'int'
        The dimension of the finite element.

    Modeling : 'str'
        The modeling of the finite element.

    Type : 'str'
        The type of the finite element. It describes its geometrie.

        Examples :

            - For a tetrahedron (4 nodes 3D element) : 
                -> Type = "3D.4N"
            
            - For an hexahedron (8 nodes 3D element) :
                -> Type = "3D.8N"
    
    BoundaryType : 'str'
        The type of the finite element's boundary. It describes the
        geometrie of its boundary finite element.

        Examples :

            - For a tetrahedron (4 nodes 3D element) :
                -> BoundaryType = "2D.3N"
            
            - For an hexahedron (8 nodes 3D element) :
                -> BoundaryType = "2D.4N"

    BoundarySize : 'int'
        The nodes number of the finite element boundary.

        Examples :

            - For a tetrahedron (4 nodes 3D element) :
                -> BoundarySize = 3
            
            - For an hexahedron (8 nodes 3D element) :
                -> BoundarySize = 4

    ShapeFunctions : 'np.ndarray'
        A one dimensional array that contains the list of the finite element
        shape functions.

        Examples :

            - For a tetrahedron (4 nodes 3D element) :
              
                -> ShapeFunctions = [N_1(*x), N_2(*x), N_3(*x), N_4(*x)]
              
              Nb : x is a tuple of 3 floats (the coordinates in the
              reference element).

    ShapeFctsGrads : 'np.ndarray'
        An array that contains the gradient of shape functions (1D if
        Dimension == 1, 2D otherwise). If shape functions are linear, its
        elements are scalars (float), otherwise they're functions.

        Nb : see the catalogue for examples.

    """

    def __init__(self, Dimension, NodesNbr, Modeling):
        assert(isinstance(NodesNbr, int) and NodesNbr > 0)
        assert(Dimension in [1,2,3])
        assert(isinstance(Modeling, str))
        if Dimension != 3:
            raise ValueError("Please note that boundary is not implemented yet for " \
                "%d dimensional elements, you cannot apply boundary loads" % Dimension)
        # On vérifie si la modélisation demandé existe dans le catalogue FEcata
        if Modeling not in fe_cata.Modeling:
            raise ValueError("There is no modeling called %s" % (Modeling))
        self.NodesNbr  = NodesNbr
        self.Dimension = Dimension
        self.Modeling = Modeling
        self.Type = utils.GetElmntType(Dimension, NodesNbr)
        # On vérifie si les fonctions de forme (et leurs gradients) existe
        # dans le catalogue FEcata. Si c'est le cas, on vérifie aussi si 
        # l'élément appelé "self.Type" supporte la modélisation appellée
        # "Modeling"
        if self.Type not in fe_cata.ShapeFcts:
            raise ValueError("The %d-dimensional %d nodes element isn't implemented"
                                                               % (Dimension, NodesNbr))
        elif self.Type not in fe_cata.Modeling[Modeling][0]:
            raise ValueError("Modeling %s doesn't support %dD elements with %d nodes"
                                                    % (Modeling, Dimension, NodesNbr))
        self.BoundaryType = fe_cata.BoundaryType[self.Type][1]
        assert(self.BoundaryType != "NotImplemented")
        self.BoundarySize = self.GetBoundarysList().shape[1]
        # On récupére les vecteurs contenant la liste des fcts de forme et leurs gradients
        self.ShapeFunctions = fe_cata.ShapeFcts[self.Type]
        assert(self.ShapeFunctions.shape == (NodesNbr,))
        self.ShapeFctsGrads = fe_cata.ShapeFctsGrad[self.Type]
        if Dimension != 1:
            assert(self.ShapeFctsGrads.shape == (NodesNbr, Dimension))
        else:
            assert(self.ShapeFctsGrads.shape == (NodesNbr,))

    # La fonction ci-dessous calcul la matrice d'interpolation qui permet d'évaluer le vecteur de ddls 
    # dans un point de l'élément (différent des noeuds) dont l'antécédent dans l'élément de référence 
    # a les coordonées a =(a1,...,ad). Si Ve est le vecteur de ddl de l'élément, et V(a) est le vecteur 
    # de ddls au point qu'on vient de citer, alors la fonction ci-dessous renvoie une matrice [N(a)] 
    # tel que :                            V(a) = [N(a)] * Ve
    def GetElmntInterpolationMat(self, a):
        """
        Returns the interpolation matrix of the finite element. this matrix
        allows evaluating the unknowns DOFs at any point x of the finite element,
        using the element's DOF vector, and the parameter a that represents the
        image of x in the reference element.

        Examples (tetrahedron with "3D" Modeling) :
             _                                                             _
            |   N_1(*a)     0        0     ...  N_4(*a)     0        0      |
            |      0     N_1(*a)     0     ...     0     N_4(*a)     0      |
            |_     0        0     N_1(*a)  ...     0        0     N_4(*a)  _|

            Such that N_1, ..., N_4 are the shape functions of the tetrahedron.

        """
        assert(isinstance(a, np.ndarray))
        assert(len(a) == self.Dimension)
        DofPerNode = utils.GetModelingDofNbr(self.Modeling)
        return utils.ConmputeN(self.ShapeFunctions, a, DofPerNode)

    def GetBoundaryInterpolationMat(self, a):
        """
        Returns the interpolation matrix of the boundary finite element
        (assuming that it supports the same DOFs as the element). This
        matrix allows evaluating the unknowns DOFs at any point x of the
        finite element's boundary, using the element's DOF vector (restricted
        over the desired part of the boundary), and the parameter a that
        represents the image of x in the reference element (of the boundary
        finite element).

        Examples (tetrahedron with "3D" Modeling) :
             _                                                             _
            |   M_1(*a)     0        0     ...  M_3(*a)     0        0      |
            |      0     M_1(*a)     0     ...     0     M_3(*a)     0      |
            |_     0        0     M_1(*a)  ...     0        0     M_3(*a)  _|

            Such that M_1, ..., M_3 are shape functions of the boundary
            finite element, whose geometry is a triangle (since the element
            is a tetrahedron), thus only three shape functions are needed.

        """
        assert(isinstance(a, np.ndarray))
        assert(self.Dimension !=1 and len(a) == self.Dimension - 1)
        BoundaryShapeFunctions = fe_cata.ShapeFcts[self.BoundaryType]
        DofPerNode = utils.GetModelingDofNbr(self.Modeling)
        return utils.ConmputeN(BoundaryShapeFunctions, a, DofPerNode)

    # La fct ci-dessous calcul les gradients des fcts de forme dans l'élément de référence au 
    # point "a" = (a[1],..,a[Dimension]). Pour les éléments dont le gradient des fcts de formes 
    # est constant (il est indépendant du point d'évaluation "a"), elle le renvoie directement.
    def ComputeElmntRefGrad(self, a=None):
        """
        Returns the shape functions gradient evaluated at a point of the
        reference element, whose coordinates are given by the parameter a.

        Return
        ------
            - Shape functions are linear : the parameter a is ignored and
              the array FiniteElement.ShapeFctsGrads is returned immediatly
              (since its elements are scalars).

            - Shape functions aren't linear : the parameter a must be an array
              containing coordinates of the point (whithin the reference element)
              where the gradient will be evaluated. Unlike the first case, the 
              elements of FiniteElement.ShapeFctsGrads are functions this time,
              that's why the parameter a in required. Once evaluated, the 
              gradient's array is returned.

        """
        if (self.ShapeFctsGrads.dtype == float):
            return self.ShapeFctsGrads
        assert ((a is not None) and isinstance(a, np.ndarray) and a.shape == (self.Dimension,))
        assert(self.ShapeFctsGrads.dtype == object)
        result = np.zeros(self.ShapeFctsGrads.shape)
        for idx in np.ndindex(result.shape):
            result[idx] = self.ShapeFctsGrads[idx](*a)
        return result
    
    # La fonction cidessous renvoie un tuple de deux np.ndarray (trois si ReturnInterpMats est True).
    # Le premier contient les poids des points de gauss, le deuxième contient les Gradients des fonctions 
    # de formes de l'élément calculé pour chacun des points de gauss. Si ReturnInterpMats est true le 
    # troisième np.ndarray contient les matrices d'interpolations évaluées aux points de gausse.
    def GetElmntGradAndWghtAtGauss(self, CalcType, IntegOrder, ReturnInterpMats=False):
        """
        Returns the shape functions gradients evaluated at Gauss points,
        their corresponding weights are also returned. The integration scheme
        used (and consequently the used Gauss points) is determined according
        to the parameters CalcType, and IntegOrder (see GetElmntIntegScheme(...)
        for more details).

        Return
        ------
            A tuple of two arrays is returned if ReturnInterpMats is False :
            
                                (arr_1, arr_2)

                - arr_1 : one dimensional array that contains Gauss weights.

                - arr_2 : an array whose elements are the computed shape
                          functions gradients at Gauss points.
                          
            A tuple of three arrays is returned if ReturnInterpMats is False :
            
                                (arr_1, arr_2, arr_3)

                - arr_3 : an array whose elements are the computed interpolation
                          matrices at Gauss points.

        Example
        -------
            Let's assume that the element is a tetrahedron, and that the integration
            scheme leads to 4 Gauss points whose coordinates (in the reference 
            element) and weights are respectively given by g_i and w_i , with
            i in range(4) :

                - arr_1 : arr_1.shape = (4,)
                          arr_1[i]    = w_i

                - arr_2 : arr_2.shape = (4, 4, 3)
                          arr_2[i]    = ComputeElmntRefGrad(g_i)

                - arr_3 : arr_2.shape = (4, 3, 12)
                          arr_2[i]    = GetElmntInterpolationMat(g_i)
                        
            Nb : of course a 3D modelisation is used (3 DOFs per node).

        """
        GaussPtsAndWeights = self.GetElmntIntegScheme(CalcType, IntegOrder)
        assert(GaussPtsAndWeights.shape[1] == self.Dimension + 1)
        GaussWeights = GaussPtsAndWeights[:,0]
        GradsList    = [self.ComputeElmntRefGrad(Row[1:]) for Row in GaussPtsAndWeights]
        GaussGrads   = np.array(GradsList)
        if ReturnInterpMats:
            InterpMatsList    = [self.GetElmntInterpolationMat(Row[1:]) for Row in GaussPtsAndWeights]
            GaussInterpMats   = np.array(InterpMatsList)
            return GaussWeights, GaussGrads, GaussInterpMats
        else:
            return GaussWeights, GaussGrads

    def ComputeBoundaryRefGrad(self, a=None):
        """
        Returns the shape functions gradient of the boundary finite element.
        The gradient is evaluated at a point of the boundary reference element,
        whose coordinates are given by the parameter a.

        Return
        ------
            - Boundary FE shape functions are linear : the parameter a is ignored
              and the gradient's array is returned immediatly (since its elements
              are scalars).

            - Boundary FE shape functions aren't linear : the parameter a must be
              an array containing coordinates of the point (whithin the boundary
              reference element) where the gradient will be evaluated. Unlike the
              first case, the elements of gradient's array are functions this time,
              that's why the parameter a in required. Once evaluated, the 
              gradient's array is returned.

        """
        BoundaryShapeFctsGrads = fe_cata.ShapeFctsGrad[self.BoundaryType]
        if self.Dimension == 3:
            assert(BoundaryShapeFctsGrads.shape == (self.BoundarySize, 2))
        elif self.Dimension == 2:
            assert(BoundaryShapeFctsGrads.shape == (self.BoundarySize,))
        if (BoundaryShapeFctsGrads.dtype == float):
            return BoundaryShapeFctsGrads
        assert((a is not None) and isinstance(a, np.ndarray) and a.shape == (self.Dimension - 1,))
        assert(BoundaryShapeFctsGrads.dtype == object)
        result = np.zeros(BoundaryShapeFctsGrads.shape)
        for idx in np.ndindex(result.shape):
            result[idx] = BoundaryShapeFctsGrads[idx](*a)
        return result

    def GetBoundaryGradAndWghtAtGauss(self, IntegOrder):
        """
        Returns shape functions gradients of the boundary finite element.
        Gradients are evaluated at Gauss points, their corresponding weights
        are also returned. The integration scheme used (and consequently the
        used Gauss points) is determined according to the parameter IntegOrder
        (see GetBoundaryIntegScheme(...) for more details).

        Return
        ------
            A tuple of two arrays is returned :
            
                                (arr_1, arr_2)

                - arr_1 : one dimensional array that contains Gauss weights.

                - arr_2 : an array whose elements are the computed shape
                          functions gradients (of the boundary finite 
                          element) at Gauss points.

        Example
        -------
            Let's assume that the element is a tetrahedron (thus its boundary is
            a triangle), and that the integration scheme leads to 3 Gauss points
            whose coordinates (in the boundary reference element) and weights
            are respectively given by g_i and w_i , with i in range(3) :

                - arr_1 : arr_1.shape = (3,)
                          arr_1[i]    = w_i

                - arr_2 : arr_2.shape = (3, 3, 2)
                          arr_2[i]    = ComputeBoundaryRefGrad(g_i)

        """
        GaussPtsAndWeights = self.GetBoundaryIntegScheme(IntegOrder)
        assert(GaussPtsAndWeights.shape[1] == self.Dimension)
        GaussWeights = GaussPtsAndWeights[:,0]
        GradsList    = [self.ComputeBoundaryRefGrad(Row[1:]) for Row in GaussPtsAndWeights]
        GaussGrads   = np.array(GradsList)
        return GaussWeights, GaussGrads

    def GetBoundaryInterpFctAtGauss(self, IntegOrder):
        """
        Returns interpolation matrices of the boundary finite element,
        evaluated at Gauss points. The integration scheme used (and
        consequently the used Gauss points) is determined according to
        the parameter IntegOrder (see GetBoundaryIntegScheme(...) for more
        details).

        Return
        ------
            A single arrays is returned (arr).

        Example
        -------
            Let's assume that the element is a tetrahedron (thus its boundary is
            a triangle), and that the integration scheme leads to 3 Gauss points
            whose coordinates (in the boundary reference element) are given by
            g_i, with i in range(3) :

                - arr.shape = (3, 3, 9)
                - arr[i]    = GetBoundaryInterpolationMat(g_i)

            Nb : A 3D modelisation is considered (3 DOFs per node).

        """
        GaussPtsAndWeights = self.GetBoundaryIntegScheme(IntegOrder)
        assert(GaussPtsAndWeights.shape[1] == self.Dimension)
        InterpFctList    = [self.GetBoundaryInterpolationMat(Row[1:]) for Row in GaussPtsAndWeights]
        GaussInterpFct = np.array(InterpFctList)
        return GaussInterpFct

    def GetElmntIntegScheme(self, CalcType, IntegOrder):
        """
        Returns the element Gauss points coordinates, and their 
        corresponding weights. Gauss points are determined according to
        the parameters CalcType and IntegOrder. CalcType represents the
        calculation type (ie : Stiffness or Mass), and IntegOrder is the
        integration scheme order (ie : Complete or Reduced).

        Return
        ------
            A 2D array is returned, each row correspond to a Gauss point.
            The first column contains the weights, and the remaining 
            columns contains coordinates.

        """
        assert(CalcType in ["Stiffness", "Mass"])
        assert(IntegOrder in ["Complete", "Reduced"])
        CalcName = self.GetType() + "." + CalcType.upper()
        return utils.GetGaussPointsAndWeights(CalcName, IntegOrder)

    def GetBoundaryIntegScheme(self, IntegOrder):
        """
        Returns the boundary element Gauss points coordinates, and their 
        corresponding weights. Gauss points are determined according to the
        parameter IntegOrder that represents the integration scheme order 
        (ie : Complete or Reduced).

        Return
        ------
            A 2D array is returned, each row correspond to a Gauss point.
            The first column contains the weights, and the remaining 
            columns contains coordinates.

        """
        assert(IntegOrder in ["Complete", "Reduced"])
        CalcName = self.BoundaryType + ".STIFFNESS"
        return utils.GetGaussPointsAndWeights(CalcName, IntegOrder)

    def GetBoundarysList(self):
        """
        Returns an array that contains the boundary parts of the finite
        element. Each row contains nodes indices (using the local numbering
        of the element) that defines a given part.

        Example
        -------
            A tetrahedron's boundary consists of 4 triangles (4 parts). Each
            triangle is defined by its 3 nodes. The returned array in this
            case is :

                           np.array([[1, 2, 4],
                                     [1, 3, 2],
                                     [2, 3, 4],
                                     [1, 4, 3]])

            The first triangle consists of the first, second and fourth node
            of the tetrahedron.

        """
        BoundaryGeom = self.GetBoundaryGeom()
        if BoundaryGeom == "Faces":
            assert(self.Type in fe_cata.Faces)
            return fe_cata.Faces[self.Type]
        else:
            assert(BoundaryGeom == "Edges" and self.Type in fe_cata.Edges)
            return fe_cata.Edges[self.Type]

    def GetBoundaryGeom(self):
        """
        Returns the boundary geometry of the finite element. Two values
        are possible :

            - "Faces" : for 3D elements.

            - "Edges" : for 2D elements.

        """
        return fe_cata.BoundaryType[self.Type][0]

    def GetType(self):
        """Returns the type of the finite element."""
        return self.Type

    def GetFacesType(self):
        """Returns the feces type of the finite element (only for 3D elements)."""
        assert(self.HasFaces())
        return utils.GetFaceNameByElmntType(self.Type)

    def GetEdgesType(self):
        """Returns the edges type of the finite element (only for 3D elements)."""
        assert(self.HasEdges())
        return utils.GetEdgeNameByElmntType(self.Type)

    def GetNodesNbr(self):
        """Returns the nodes number of the finite element."""
        return self.NodesNbr
    
    def GetFaceNodesNbr(self):
        """
        Returns the face's nodes number of the finite element (only for 3D 
        elements).
        
        """
        assert(self.HasFaces())
        return self.BoundarySize

    def HasFaces(self):
        """
        Checks whether the finite element has faces (True for 3D
        elements, False otherwise)
        
        """
        return self.Dimension == 3

    def HasEdges(self):
        """
        Checks whether the finite element has edges (False for 1D
        elements, True otherwise)
        
        """
        return self.Dimension in [2, 3]

# Cette fonction permet de calculer le jacobien dans l'élément réel (l'élément numéro ElmntIdx 
# (numérotation à 0) dans le maillage StudyMesh), d'un point dont l'antécédent à l'élément de 
# référence est "a", ShapeFctsGrad correspond dans ce cas au gradient des fcts de forme évalué 
# dans a par FiniteElement.ComputeElmntRefGrad(a). Le dernier paramètre "inv" est un bool qui permet 
# de spécifier qu'on veut l'inverse du jacobien en sortie, il est par défaut à False
# Remarque : On passe ShapeFctsGrad en argument et non pas a, pour ne pas devoir calculer 
# ShapeFctsGrad pour tout les élément de meme type (puisque ShapeFctsGrad dépend que du point 
# d'évaluation dans l'élément de référence (Généralement un point de gausse), on profite de cela 
# pour la calculer une seule fois pour tt les élément du meme type)
def ComputeElmntJacobian(StudyMesh, ElmntIdx, ShapeFctsGrad, OnlyDet=False, inv=False):
    """
    Computes the jacobian at a point "x" of the element of index ElmntIdx
    in the mesh StudyMesh. "x" is implicitly passed by the parameter 
    ShapeFctsGrad that representes the gradient of shape functions (returned
    by FiniteElement.ComputeElmntRefGrad(a)). The gradient is evaluated at "a",
    the antecedent of "x" within the reference element (most likely a Gauss point).

    Nb : ShapeFctsGrad  is passed instead of "a", to avoid evaluating it for
    all the elements of the same type.

    Return
    ------
        A tuple is returned. Its first element is either the Jacobian matrix
        (inv=False), or its inverse (inv=True). The second element is the 
        determinant of the jacobian matrix. If OnlyDet is True, only the jacobian
        matrix determinant is returned.

    """
    ElmntCoord  = StudyMesh.GetElmntCoord(ElmntIdx)
    result      = np.matmul(ElmntCoord, ShapeFctsGrad)
    determinant = np.linalg.det(result)

    return determinant if OnlyDet else (result if not inv else np.linalg.inv(result), determinant)

# Cette fonction calcul le jacobian de la face d'un élément 3D (Utilisé pour le calcul du second membre). 
# La face doit etre connue par le maillage par son indice FaceIdx, Le gradient des fonctions de forme de 
# la face "ShapeFctsGrad" doit etre calculé avant l'appel a cet fonction, son calcul doit etre fait par la
# fonction FiniteElement.ComputeBoundaryRefGrad(a) (Le paramètre a est optionnel si le gradient est cte 
# dans l'élément de référence). La valeur renoyée est un float. Si la face compte n noeud de coordonées x(k), 
# chacune liée à une fonction forme Nk(a) (tq a = (a1, a2)) La fonction ci-dessus calcul:
#
#     || Sigma(k : 0 -> n){(dNk/da1)(a) * x(k)} .PV. Sigma(k : 0 -> n){(dNk/da2)(a) * x(k)} ||_2
#
# .PV. : Produit vectoriel ; ShapeFctsGrad[i,j] = dNi/daj)(a)
def ComputeFaceJacobian(StudyMesh, FaceIdx, ShapeFctsGrad):
    """
    Computes the jacobian at a point "x" of the face of index FaceIdx in the mesh
    StudyMesh (StudyMesh must be 3 dimensional). "x" is implicitly passed by the 
    parameter ShapeFctsGrad that representes the shape functions gradient of the 
    boundary finite element (returned by FiniteElement.ComputeBoundaryRefGrad(a)).    
    The gradient is evaluated at "a", the antecedent of "x" within the boundary 
    reference element (most likely a Gauss point).

    Nb : ShapeFctsGrad  is passed instead of "a", to avoid evaluating it for
    all the faces of the same type.

    Return
    ------
        A scalar is returned (the jacobian of the face).

    """
    FaceCoord  = StudyMesh.GetFaceCoord(FaceIdx)
    TempMatrix = np.matmul(FaceCoord, ShapeFctsGrad)
    result     = np.cross(TempMatrix[:,0], TempMatrix[:,1])
    return np.linalg.norm(result)

def ComputeEdgeJacobian(StudyMesh, FaceIdx, ShapeFctsGrad, inv=False):
    """Not implemented yet."""
    pass

# Cette fonction renvoie la matrice Be évalué au point a de l'élément de référence où ShapeFctsGrad 
# est évalué (Pour des éléments TriDimensionnel). Be est la matrice qui fait la liason entre les 
# composantes indépendantes du tenseur de déformation et les déplacements nodaux (éléments finis 
# isoparamétriques). ElmntIdx est l'élément ou l'on veut évaluer Be, et ShapeFctsGrad représente 
# implicitement le point de cet élément où Be doir etre calculer. ShapeFctsGrad est expliquée dans 
# la decription de la fonction ComputeElmntJacobian().
# La fonction ci-dessous va etre appelée au niveau du calcul élémentaire, meme si c possible de calculer 
# DofNbr a partir des autres paramètres "StudyMesh, ElmntIdx, ShapeFctsGrad", on le passe comme mm 
# en argument pour éviter de le calculer pour chaque élément (les éléments de mm type bien sur).
def __Compute_BE_3D__(StudyMesh, ElmntIdx, ShapeFctsGrad, DofNbr, BE):
    """
    It should not be used. It is only used within the function
    Compute_BE_2D_3D(...) under strict assumptions (for more details
    see Compute_BE_2D_3D(...)).
    
    """
    assert(isinstance(StudyMesh, mesh.mesh) and StudyMesh.Dimension == 3)
    assert(isinstance(ElmntIdx, (int, np.integer)) and 0 <= ElmntIdx < StudyMesh.ElmntsNbr)
    ElmntNodesNbr = StudyMesh.GetElmntNodesNbr(ElmntIdx)
    # On teste le nombre de ddls. En 3D, il 3 ddls par noeuds
    assert(DofNbr == 3 * ElmntNodesNbr)
    assert(isinstance(ShapeFctsGrad, np.ndarray) and ShapeFctsGrad.shape == (ElmntNodesNbr, 3))
    JacobianInv, determinant = ComputeElmntJacobian(StudyMesh, ElmntIdx, ShapeFctsGrad, inv=True)
    # On crée la matrice GN dont les éléments vont servir au remplissage de la matrice Be
    GN = np.matmul(ShapeFctsGrad, JacobianInv)
    assert(GN.shape == ShapeFctsGrad.shape)
    # On crée maintenant la matrice Be avant de la remplir (en 3D elle compte 6 lignes parce qu'il y a 6 
    # composantes indépendantes dans chacun des tenseurs de déformation et de contraintes)
    BE[:,:] = 0.0
    # On note {Ve} le vecteur de ddls de l'élément, chaque noeud en compte 3 ("DX","DY","DZ") et c'est 
    # dans cette ordre qu'ils sont stockés dans {Ve} noeud après l'autre. La matrice Be relie contrainte 
    # et déformation : (Seuls les éléments non nuis de Be seront calculés)
    # Epsi_xx = (d(Ux) / dx) = Be[0,:] * {Ve}. Les éléments non nuls de Be[0,:] sont :
    BE[0,0::3] = GN[:,0]
    # Epsi_yy = (d(Uy) / dy) = Be[1,:] * {Ve}. Les éléments non nuls de Be[1,:] sont :
    BE[1,1::3] = GN[:,1]
    # Epsi_zz = (d(Uz) / dz) = Be[2,:] * {Ve}. Les éléments non nuls de Be[2,:] sont :
    BE[2,2::3] = GN[:,2]
    # 2 * Epsi_xy = (d(Ux) / dy) + (d(Uy) / dx) = Be[3,:] * {Ve}. Les éléments non nuls de Be[3,:] sont :
    BE[3,0::3] = GN[:,1]
    BE[3,1::3] = GN[:,0]
    # 2 * Epsi_xz = (d(Ux) / dz) + (d(Uz) / dx) = Be[4,:] * {Ve}. Les éléments non nuls de Be[4,:] sont :
    BE[4,0::3] = GN[:,2]
    BE[4,2::3] = GN[:,0]
    # 2 * Epsi_yz = (d(Uy) / dz) + (d(Uz) / dy) = Be[5,:] * {Ve}. Les éléments non nuls de Be[5,:] sont :
    BE[5,1::3] = GN[:,2]
    BE[5,2::3] = GN[:,1]
    return BE, determinant

def __Compute_BE_2D__(StudyMesh, ElmntIdx, ShapeFctsGrad, DofNbr, BE):
    """
    It should not be used. It is only used within the function
    Compute_BE_2D_3D(...) under strict assumptions (for more details
    see Compute_BE_2D_3D(...)).
    
    """
    assert(isinstance(StudyMesh, mesh.mesh) and StudyMesh.Dimension == 2)
    assert(isinstance(ElmntIdx, (int, np.integer)) and 0 <= ElmntIdx < StudyMesh.ElmntsNbr)
    ElmntNodesNbr = StudyMesh.GetElmntNodesNbr(ElmntIdx)
    # On teste le nombre de ddls. En 2D, il 2 ddls par noeuds
    assert(DofNbr == 2 * ElmntNodesNbr)
    assert(isinstance(ShapeFctsGrad, np.ndarray) and ShapeFctsGrad.shape == (ElmntNodesNbr, 2))
    JacobianInv, determinant = ComputeElmntJacobian(StudyMesh, ElmntIdx, ShapeFctsGrad, inv=True)
    # Maintenant j'ai l'inverse de la matrice jacobienne, ainsi que DN = ShapeFctsGrad, il faut savoire 
    # maintenant le nombre de composante indépendantes de la déformation pour pouvoir calculer la matrice 
    # Be. C'est la que je me ss arreté.
    # On crée la matrice GN dont les éléments vont servir au remplissage de la matrice Be
    GN = np.matmul(ShapeFctsGrad, JacobianInv)
    assert(GN.shape == ShapeFctsGrad.shape)
    # On crée maintenant la matrice Be avant de la remplir (en 2D elle compte 3 lignes parce qu'il y a 3 
    # composantes indépendantes dans chacun des tenseurs de déformation et de contraintes)
    BE[:,:] = 0.0
    # On note {Ve} le vecteur de ddls de l'élément, chaque noeud en compte 2 ("DX","DY") et c'est 
    # dans cette ordre qu'ils sont stockés dans {Ve} noeud après l'autre. La matrice Be relie contrainte 
    # et déformation : (Seuls les éléments non nuis de Be seront calculés)
    # Epsi_xx = (d(Ux) / dx) = Be[0,:] * {Ve}. Les éléments non nuls de Be[0,:] sont :
    BE[0,0::2] = GN[:,0]
    # Epsi_yy = (d(Uy) / dy) = Be[1,:] * {Ve}. Les éléments non nuls de Be[1,:] sont :
    BE[1,1::2] = GN[:,1]
    # 2 * Epsi_xy = (d(Ux) / dy) + (d(Uy) / dx) = Be[3,:] * {Ve}. Les éléments non nuls de Be[2,:] sont :
    BE[2,0::2] = GN[:,1]
    BE[2,1::2] = GN[:,0]
    return BE, determinant

# Le role de cette fonction dans un calcul élémentaire est de récupérer la fonction qui permet de calculer Be 
# pour la modélisation choisie durant l'étude.
def Compute_BE_2D_3D(Modeling, StudyMesh, ElmntIdx, ShapeFctsGrad, DofNbr, BE):
    """
    Computes the matrix Be (see the Be section) at a point "x" of the element
    of index ElmntIdx in the mesh StudyMesh. "x" is implicitly passed by the
    parameter ShapeFctsGrad that representes the gradient of shape functions
    (returned by FiniteElement.ComputeElmntRefGrad(a)). The gradient is evaluated
    at "a", the antecedent of "x" within the reference element (most likely a Gauss
    point). DofNbr is the total number of the element's DOFs, and BE is an array
    where the matrix will be stored (to avoid repetitive reallocations).

    Nb : ShapeFctsGrad is passed instead of "a", to avoid evaluating it for
    all the elements of the same type.

    Be
    --
        A matrix that connects the DOFs vector of an isoparametric finite element
        to its corresponding strain vector (a vector that contains strain tensor's
        components).

    """
    assert(isinstance(Modeling, str))
    # On vérifie si la modélisation demandé existe dans le catalogue FEcata
    if Modeling not in fe_cata.Modeling:
        raise ValueError("There is no modeling called %s" % (Modeling))
    assert(Modeling in ["2D", "3D"])
    if Modeling == "3D":
        return __Compute_BE_3D__(StudyMesh, ElmntIdx, ShapeFctsGrad, DofNbr, BE)
    elif Modeling == "2D":
        return __Compute_BE_2D__(StudyMesh, ElmntIdx, ShapeFctsGrad, DofNbr, BE)

#-------------------------------------------------------------------------------------------------------


if __name__ == "__main__":
    fe = FiniteElement(3,8,"3D")
    print(fe.NodesNbr, fe.Dimension)


