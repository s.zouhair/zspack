"""This module implements the finite elements catalogue."""

import numpy as np
import vtk

from .. import num_integration

# Un dictionnaire contenant la liste de fonctions de forme, accessible par une 
# chaine de caractères indiquant le type de l'élément (Dimension + nombre de 
# noeuds). Ex : ShapeFcts["3D.4N"] renvoie les fonctions de forme du thétrahèdre
ShapeFcts = {}

# Un dictionnaire contenant les dérivées partielles des fonctions de forme accessible 
# de la meme manière que la liste des fonctions de forme
ShapeFctsGrad = {}

# Un dictionnaire contenant le type de frontière (bord) de chaque élément finies
BoundaryType = {}

# Un dictionnaire contenant les faces de chaque type d'élément. Chaque face est déterminée 
# par la liste des numéros locaux des noeuds qui la composent (Bien sur seul les éléments 
# tridimentionnel sont concernée). Les numéros des noeuds commence à 1 dans ce dictionnaire.
Faces = {}

# Un dictionnaire contenant les arêtes de chaque type d'élément. Chaque arête est déterminée 
# par la liste des numéros locaux des deux noeuds qui la composent (Bien sur seul les éléments 
# tri et bidimentionnel sont concernée).
Edges = {}

# ddddddddddddd
CompleteIntegration = {}

# ddddddddddddd
ReducedIntegration = {}

# VTK cells types
VtkCellType = {
                    "3D.8N" : vtk.VTK_HEXAHEDRON,
                    "3D.4N" : vtk.VTK_TETRA,
                    "2D.3N" : vtk.VTK_TRIANGLE,
                    "2D.4N" : vtk.VTK_QUAD
              }

# Un dictionnaire qui lie la taille de la face d'un élément (Le nombre de noeuds) au nom 
# de ce dernier. Ex: self.ElmntNameByFaceSize['3D.3'] = '3D.4N'
ElmntNameByFaceName  = {
                            "3D.4N.F" : "3D.8N",
                            "3D.3N.F" : "3D.4N"
                       }

# Le role du dictionnaire ci-dessous est l'inverse du dictionnaire ci-dessus
FaceNameByElmntName  = {
                            "3D.8N" : "3D.4N.F",
                            "3D.4N" : "3D.3N.F",
                            "2D.3N" : "machin1", # A supprimer
                            "2D.4N" : "machin2"  # A supprimer
                       }

#ddddddddddddddd
EdgeNameByElmntName  = {
                            "3D.8N" : "3D.2N.E",
                            "3D.4N" : "3D.2N.E",
                            "2D.3N" : "2D.2N.E", # A supprimer
                            "2D.4N" : "2D.2N.E"  # A supprimer
                       }

# Un dictionnaire contenant les types de modélisation mis en place dans le code, accessible 
# par une chaine de caractères (le nom de la modélisation). Ex : Modeling["3D"] renvoie une 
# liste de deux tuples. Le 1er tuple contient une liste des ddls supportés par la modélisation 
# "3D" ("DX", "DY", "DZ"), le 2ème tuple contient la liste des éléments support de cette 
# modélisation ("3D.8N" : Hexaèdre)
Modeling =  {
                "3D" :  [
                            ("3D.8N", "3D.4N"),
                            ("DX", "DY", "DZ"), 
                            ("Epsi_xx", "Epsi_yy", "Epsi_zz", "Epsi_xy", "Epsi_xz", "Epsi_yz"), 
                            ("Sigm_xx", "Sigm_yy", "Sigm_zz", "Sigm_xy", "Sigm_xz", "Sigm_yz")
                        ]
            }

# Hexaèdre / 8 Noeuds -----------------------------------------------------------------------------
ShapeFcts["3D.8N"] = np.array([ lambda a1, a2, a3 :  0.125 * (1 - a1) * (1 - a2) * (1 - a3),
                                lambda a1, a2, a3 :  0.125 * (1 + a1) * (1 - a2) * (1 - a3),
                                lambda a1, a2, a3 :  0.125 * (1 + a1) * (1 + a2) * (1 - a3),
                                lambda a1, a2, a3 :  0.125 * (1 - a1) * (1 + a2) * (1 - a3),
                                lambda a1, a2, a3 :  0.125 * (1 - a1) * (1 - a2) * (1 + a3),
                                lambda a1, a2, a3 :  0.125 * (1 - a1) * (1 + a2) * (1 + a3),
                                lambda a1, a2, a3 :  0.125 * (1 + a1) * (1 + a2) * (1 + a3),
                                lambda a1, a2, a3 :  0.125 * (1 + a1) * (1 - a2) * (1 + a3)])

ShapeFctsGrad["3D.8N"] = np.array([[lambda a1, a2, a3 : -0.125 * (1 - a2) * (1 - a3)  ,  lambda a1, a2, a3 : -0.125 * (1 - a1) * (1 - a3)  ,  lambda a1, a2, a3 : -0.125 * (1 - a1) * (1 - a2)],
                                   [lambda a1, a2, a3 :  0.125 * (1 - a2) * (1 - a3)  ,  lambda a1, a2, a3 : -0.125 * (1 + a1) * (1 - a3)  ,  lambda a1, a2, a3 : -0.125 * (1 + a1) * (1 - a2)],
                                   [lambda a1, a2, a3 :  0.125 * (1 + a2) * (1 - a3)  ,  lambda a1, a2, a3 :  0.125 * (1 + a1) * (1 - a3)  ,  lambda a1, a2, a3 : -0.125 * (1 + a1) * (1 + a2)],
                                   [lambda a1, a2, a3 : -0.125 * (1 + a2) * (1 - a3)  ,  lambda a1, a2, a3 :  0.125 * (1 - a1) * (1 - a3)  ,  lambda a1, a2, a3 : -0.125 * (1 - a1) * (1 + a2)],
                                   [lambda a1, a2, a3 : -0.125 * (1 - a2) * (1 + a3)  ,  lambda a1, a2, a3 : -0.125 * (1 - a1) * (1 + a3)  ,  lambda a1, a2, a3 :  0.125 * (1 - a1) * (1 - a2)],
                                   [lambda a1, a2, a3 : -0.125 * (1 + a2) * (1 + a3)  ,  lambda a1, a2, a3 :  0.125 * (1 - a1) * (1 + a3)  ,  lambda a1, a2, a3 :  0.125 * (1 - a1) * (1 + a2)],
                                   [lambda a1, a2, a3 :  0.125 * (1 + a2) * (1 + a3)  ,  lambda a1, a2, a3 :  0.125 * (1 + a1) * (1 + a3)  ,  lambda a1, a2, a3 :  0.125 * (1 + a1) * (1 + a2)],
                                   [lambda a1, a2, a3 :  0.125 * (1 - a2) * (1 + a3)  ,  lambda a1, a2, a3 : -0.125 * (1 + a1) * (1 + a3)  ,  lambda a1, a2, a3 :  0.125 * (1 + a1) * (1 - a2)]])

BoundaryType["3D.8N"] = ("Faces", "2D.4N")

Faces["3D.8N"] = np.array([[1, 5, 8, 2],
                           [3, 7, 6, 4],
                           [2, 8, 7, 3],
                           [1, 4, 6, 5],
                           [1, 2, 3, 4],
                           [5, 6, 7, 8]], dtype=int)

Edges["3D.8N"] = np.array([[1, 2],
                           [2, 3],
                           [3, 4],
                           [4, 1],
                           [5, 6],
                           [6, 7],
                           [7, 8],
                           [8, 5],
                           [1, 5],
                           [2, 8],
                           [3, 7],
                           [4, 6]], dtype=int)

CompleteIntegration["3D.8N.STIFFNESS"] = "Hexaèdre.P2"
ReducedIntegration["3D.8N.STIFFNESS"]  = "Hexaèdre.P1"

CompleteIntegration["3D.8N.MASS"] = "Hexaèdre.P4"
ReducedIntegration["3D.8N.MASS"]  = "Hexaèdre.P2"

# Tétraèdre / 4 Noeuds ----------------------------------------------------------------------------
ShapeFcts["3D.4N"] = np.array([ lambda a1, a2, a3 :  a1,
                                lambda a1, a2, a3 :  a2,
                                lambda a1, a2, a3 :  a3,
                                lambda a1, a2, a3 :  1 - a1 - a2 - a3])

ShapeFctsGrad["3D.4N"] = np.array([[ 1.  ,   0.  ,  0.],
                                   [ 0.  ,   1.  ,  0.],
                                   [ 0.  ,   0.  ,  1.],
                                   [-1.  ,  -1.  , -1.]], dtype=float)

BoundaryType["3D.4N"] = ("Faces", "2D.3N")

Faces["3D.4N"] = np.array([[1, 2, 4],
                           [1, 3, 2],
                           [2, 3, 4],
                           [1, 4, 3]], dtype=int)

Edges["3D.4N"] = np.array([[1, 2],
                           [2, 3],
                           [3, 1],
                           [1, 4],
                           [2, 4],
                           [3, 4]], dtype=int)

CompleteIntegration["3D.4N.STIFFNESS"] = "Tétraèdre.P1"
ReducedIntegration["3D.4N.STIFFNESS"]  = "Tétraèdre.P0"

CompleteIntegration["3D.4N.MASS"] = "Tétraèdre.P2"
ReducedIntegration["3D.4N.MASS"]  = "Tétraèdre.P1"

# Quadrangle / 4 Noeuds ---------------------------------------------------------------------------
ShapeFcts["2D.4N"] = np.array([ lambda a1, a2 :  0.25 * (1 - a1) * (1 - a2),
                                lambda a1, a2 :  0.25 * (1 + a1) * (1 - a2),
                                lambda a1, a2 :  0.25 * (1 + a1) * (1 + a2),
                                lambda a1, a2 :  0.25 * (1 - a1) * (1 + a2)])

ShapeFctsGrad["2D.4N"] = np.array([[lambda a1, a2 :  -0.25 * (1 - a2)  ,  lambda a1, a2 :  -0.25 * (1 - a1)],
                                   [lambda a1, a2 :   0.25 * (1 - a2)  ,  lambda a1, a2 :  -0.25 * (1 + a1)],
                                   [lambda a1, a2 :   0.25 * (1 + a2)  ,  lambda a1, a2 :   0.25 * (1 + a1)],
                                   [lambda a1, a2 :  -0.25 * (1 + a2)  ,  lambda a1, a2 :   0.25 * (1 - a1)]])

BoundaryType["2D.4N"] = ("Edges", "NotImplemented")

Edges["2D.4N"] = np.array([[1, 2],
                           [2, 3],
                           [3, 4],
                           [4, 1]], dtype=int)

CompleteIntegration["2D.4N.STIFFNESS"] = "Quadrangle.P2"
ReducedIntegration["2D.4N.STIFFNESS"]  = "Quadrangle.P1"

CompleteIntegration["2D.4N.MASS"] = "Quadrangle.P4"
ReducedIntegration["2D.4N.MASS"]  = "Quadrangle.P3"

# Triangle / 3 Noeuds -----------------------------------------------------------------------------
ShapeFcts["2D.3N"] = np.array([ lambda a1, a2 :  a1,
                                lambda a1, a2 :  a2,
                                lambda a1, a2 :  1 - a1 - a2])

ShapeFctsGrad["2D.3N"] = np.array([[ 1.  ,   0.],
                                   [ 0.  ,   1.],
                                   [-1.  ,  -1.]], dtype=float)

BoundaryType["2D.3N"] = ("Edges", "NotImplemented")

Edges["2D.3N"] = np.array([[1, 2],
                           [2, 3],
                           [3, 1]], dtype=int)

CompleteIntegration["2D.3N.STIFFNESS"] = "Triangle.P1"
ReducedIntegration["2D.3N.STIFFNESS"]  = "Triangle.P0"

CompleteIntegration["2D.3N.MASS"] = "Triangle.P2.V1"
ReducedIntegration["2D.3N.MASS"]  = "Triangle.P1"

#--------------------------------------------------------------------------------------------------

if __name__ == "__main__":
    assert(len(ShapeFcts) == len(ShapeFctsGrad))
    for key in ShapeFcts:
        assert(key in ShapeFctsGrad)
        assert(key in BoundaryType)
        assert(key in Faces or key in Edges)
        assert(type(ShapeFcts[key])     == np.ndarray)
        assert(type(ShapeFctsGrad[key]) == np.ndarray)
    for key in CompleteIntegration.values():
        assert(key in num_integration.GaussianQuad)
    for key in ReducedIntegration.values():
        assert(key in num_integration.GaussianQuad)

        #assert(type(Faces[key]) == np.ndarray)
    # print(ShapeFctsGrad["3D.8N"].shape)
    # l = np.array([0,0,0])
    # print(ShapeFctsGrad["3D.8N"][7,0](*l))