"""
This module implements the Matrix class, and several
utility functions.

"""

import numpy as np
from scipy.sparse import bsr_matrix, vstack, hstack

from ..pkgbndary import boundary
from ..pkgutils import GetElmntTypeDimAndNodesNbr, GetSubMatrix
from ..pkgmatbhv import constitutive_relation
from ..pkgasmbly import assembly
from .. import model

class Matrix:
    """
    Matrix class.

    Parameters
    ----------
        DofNumbering : 'DofNumbering object'
            The DOF's numbering object.

        ImposedDof : 'BoundCond object'
            The boundary conditions object.

        StorageType : 'str'
            The storage type to be used, three values are possible :
            
                - "Symmetric" : only the upper triangular part is stored
                                as a one-dimensional array

                - "NonSymmetric" : the whole matrix is stored.

                - "Sparse" : Only non-zero elements are stored.

        MatrixType : 'str',optional
            The matrix type. Three values are possible :

                - "Stiffness" : for a stiffness matrix.

                - "Damping"   : for a damping matrix.

                - "Mass"      : for a mass matrix.
        
        ElmntsMats : 'bool', optional
            If True, elementary matrices computed during assembly process
            are also stored.

    Attributes
    ----------
        DofNumbering : 'DofNumbering object'
            The DOF's numbering object of the matrix.

        ImposedDof : 'BoundCond object'
            The boundary conditions object of the matrix.

        Behaviour : 'behavior object'
            The behavior object (describes the material field).

        StorageType : 'str'
            The storage type of the matrix.

        Type : 'str'
            The matrix type.

        Size : 'int'
            The DOF's number of the matrix.

        Value : 'np.ndarray'
            The matrix's values array. For sparse matrices, it's
            a simple np.ndarray during assembly process (like other 
            storage types), by the end of this process it is transformed
            to a scipy sparse matrix (bsr_matrix).
        
        RowsIndices : 'np.ndarray'
            Available only for sparse storage. Used to store
            rows indices of elements stored in Value. Deleted
            and set to None once the assembly process terminated.

        ColsIndices : 'np.ndarray'
            Available only for sparse storage. Used to store
            columns indices of elements stored in Value. Deleted
            and set to None once the assembly process terminated.
        
        SprsCurrIdx : 'int'
            Available only for sparse storage. Used during the
            assembly process to keep track of the index where
            the next elementry matrix will be stored within the
            arrays Value, RowsIndices and ColsIndices.

        StoreElmntryMats : 'bool'
            Determines whether elementary matrices are stored or not.

        ElemntryMatsVals : 'np.ndarray'
            An array where elementary matrices of elements are stored.
            Only their upper triangular parts are stored as 1D arrays.
            This array is one dimensional.

        ElemntryMatsPtr : 'np.ndarray'
            A 2d array with shape (ElmntsNbr, 2). Each row correspond to an
            element, the first and second columns are respectively the start
            and stop indices within ElemntryMatsVals where the elementary
            matrix is stored.

        ElemntryMatsCurrIdx : 'int'
            An integer that keeps track of the position within ElemntryMatsVals
            where the next elementary matrix will be stored during a FE assembly
            process.

        TempGeneEfforts : 'np.ndarray'
            An array used during the matrix assembly to store its contributions
            to the generalized efforts vector. After the assembly operation,
            it's deleted.

        GeneEffortsVal : 'np.ndarray'
            An array used to store non-zero values of TempGeneEfforts
            after the matrix assemby.
        
        GeneEffortsPos : 'np.ndarray'
            An array used to store the indices of non-zero values of
            TempGeneEfforts after the matrix assembly.

    """

    def __init__(self, DofNumbering, ImposedDof, MatrixType=None, StorageType="Symmetric", ElmntsMats=True):
        assert(isinstance(DofNumbering, model.DofNumbering))
        assert(DofNumbering.UpdatingFinalized)
        assert(StorageType in ["Symmetric", "NonSymmetric", "Sparse"])
        assert(MatrixType in ["Stiffness", "Mass", "Damping"])
        assert(ElmntsMats in [True, False])
        if ImposedDof is not None:
            assert(isinstance(ImposedDof, boundary.BoundCond))
            assert(ImposedDof.GetDofNumbering() is DofNumbering)
        elif DofNumbering.GetImposedDofNbr() != 0:
            raise ValueError("The object DofNumbering contains imposed DOFs, \
                        therefore the parameter ImposedDof must be specified")

        self.DofNumbering = DofNumbering
        self.ImposedDof = ImposedDof
        self.Behavior = None
        self.StorageType = StorageType
        self.Type = MatrixType
        self.Size = DofNumbering.GetFreeDofNbr()
        self.IsAssembled = False

        # An array where the matrix values are stored
        self.Value = None

        # Objects related to sparse matrices storage
        self.RowsIndices = None
        self.ColsIndices = None
        self.SprsCurrIdx = None

        # Objects related to elementary matrices
        self.StoreElmntryMats = ElmntsMats
        self.ElemntryMatsVals = None
        self.ElemntryMatsPtr  = None
        self.ElemntryMatsCurrIdx = None

        # Don't change the order of the calls bellow
        self.InitElmntsMatsBuff()
        self.InitStorageBuffs()

        # Les variables ci dessous servent à stocké les valeurs temporaire des 
        # contributions des matrice de rigidités/Mass et Ammortissement au second
        # membre (Le vecteur des efforts généralisés)
        self.TempGeneEfforts = None
        self.GeneEffortsVal  = None
        self.GeneEffortsPos  = None

    def InitElmntsMatsBuff(self):
        """
        Initialize the elementary matrices objects.

        """
        BuffTotalSize = 0
        StudyModel = self.DofNumbering.GetModel()
        StudyMesh  = StudyModel.GetModelMesh()
        ElmntsType = StudyModel.GetModelElmntsTypes()
        DofsNbrPerNode = self.DofNumbering.GetDofNbrPerNode()
        for ElmntType in ElmntsType:
            ElmntsNbr = StudyMesh.GetNumberof(ElmntType)
            _, NodesPerElmnt = GetElmntTypeDimAndNodesNbr(ElmntType)
            BuffTotalSize += ElmntsNbr * GetTriPartSize(NodesPerElmnt * DofsNbrPerNode)
        self.ElemntryMatsVals = np.empty(BuffTotalSize, dtype=float)
        self.ElemntryMatsPtr = np.empty((StudyMesh.GetNumberof("Elmnts"), 2), dtype=int)
        self.ElemntryMatsCurrIdx = 0

    def InitStorageBuffs(self):
        """
        Initialize the storage buffers.

        """
        if self.StorageType == "Symmetric":
            ValeShape = GetTriPartSize(self.Size),
        elif self.StorageType == "NonSymmetric":
            ValeShape = self.Size, self.Size
        else:
            # Le ValeShape ci-dessous est plus grand que ce
            # qui est nécessaire, il est tronqué plus tard.
            ValeShape = self.ElemntryMatsVals.size * 2,

        self.Value = np.zeros(ValeShape, dtype=float)
        if self.StorageType == "Sparse":
            self.RowsIndices = np.zeros(ValeShape, dtype=int)
            self.ColsIndices = np.zeros(ValeShape, dtype=int)
            self.SprsCurrIdx = 0

    def DelElmntsMatsBuff(self):
        """
        Delete the elementary matrices objects.

        """
        self.StoreElmntryMats = False
        del self.ElemntryMatsVals
        self.ElemntryMatsVals = None
        del self.ElemntryMatsPtr
        self.ElemntryMatsPtr  = None
        self.ElemntryMatsCurrIdx = 0

    def SetBehavior(self, behavior):
        """Sets the behavior used to assemble the matrix"""
        assert(isinstance(behavior, constitutive_relation.behavior))
        self.Behavior = behavior

    def SetElmntryMatVal(self, ElmntryMat, ElmntIdx):
        """
        Store the elementary matrix for the element number
        ElmntIdx. It must be used only within an assembly
        operator (ex : Assemble_STIFFMAT, Assemble_MASS, ...).

        """
        assert(0 <= ElmntIdx < self.DofNumbering.GetElmntsNbr())
        assert(not self.IsAssembled)
        assert(ElmntryMat.ndim == 2)
        TempIdxs = np.triu_indices_from(ElmntryMat)
        TriPartsize = GetTriPartSize(ElmntryMat.shape[0])
        StartIdx = self.ElemntryMatsCurrIdx
        EndIdx = self.ElemntryMatsCurrIdx + TriPartsize
        self.ElemntryMatsVals[StartIdx:EndIdx] = ElmntryMat[TempIdxs]
        self.ElemntryMatsPtr[ElmntIdx, 0] = StartIdx
        self.ElemntryMatsPtr[ElmntIdx, 1] = EndIdx
        self.ElemntryMatsCurrIdx = EndIdx

    def GetElmntryMatVal(self, ElmntIdx, ImpDofs=None, As2DArray=None):
        """
        Returns the elementry matrix of element ElmntIdx. The
        parameter ImpDofs determines whether imposed DOFs rows 
        and columns must be eliminated or not ( False means that
        they must be eliminated). If As2DArray is False 
        only the upper triangular part of the matrix is returned 
        as 1D vector, otherwise the whole matrix is returned.

        """
        assert(0 <= ElmntIdx < self.DofNumbering.GetElmntsNbr())
        assert(isinstance(ImpDofs, bool) and isinstance(As2DArray, bool))
        if self.IsAssembled and not self.StoreElmntryMats:
            raise ValueError("Elementary matrices aren't stored.")

        start, end   = self.ElemntryMatsPtr[ElmntIdx]
        SymElmntMat  = self.ElemntryMatsVals[start:end]
        DofNumbering = self.DofNumbering

        if ImpDofs or not DofNumbering.HasImposedDof(ElmntIdx=ElmntIdx):
            ElmntMat = GetSymAsDense(SymElmntMat) if As2DArray else SymElmntMat
        else:
            DenseElmntMat  = GetSymAsDense(SymElmntMat)
            FreeDofsNum, _ = DofNumbering.GetElmntNumbring(ElmntIdx, HasImposedDof=True)
            _, FreeDofsLocalNumbering  = FreeDofsNum
            FreeDofsSubMatrix = GetSubMatrix(DenseElmntMat, RowsIdx=FreeDofsLocalNumbering, IsFeAsmbly=True)
            if As2DArray:
                ElmntMat = FreeDofsSubMatrix
            else:
                TempIdxs = np.triu_indices_from(FreeDofsSubMatrix)
                ElmntMat = FreeDofsSubMatrix[TempIdxs]

        return ElmntMat

    def Assemble(self):
        assert(not self.IsAssembled)
        DofNumbering = self.DofNumbering
        ImposedDof   = self.ImposedDof
        if ImposedDof is not None:
            self.InitGeneEfforts()
        ElmntsNbr = self.DofNumbering.GetElmntsNbr()
        for ElmntIdx in range(ElmntsNbr):
            ElmntMat = self.GetElmntryMatVal(ElmntIdx, ImpDofs=True, As2DArray=True)
            if ImposedDof is not None and DofNumbering.HasImposedDof(ElmntIdx=ElmntIdx):
                FreeDofsNum, ImpoDofsNum = DofNumbering.GetElmntNumbring(ElmntIdx, HasImposedDof=True)
                FreeDofsGlobalNumbering, FreeDofsLocalNumbering  = FreeDofsNum
                ImposedValuePosition, ImposedDofsLocalNumbering = ImpoDofsNum
                FreeDofsSubMatrix    = GetSubMatrix(ElmntMat, RowsIdx=FreeDofsLocalNumbering, IsFeAsmbly=True)
                ImposedDofsSubMatrix = GetSubMatrix(ElmntMat, RowsIdx=FreeDofsLocalNumbering, ColsIdx=ImposedDofsLocalNumbering, IsFeAsmbly=True)
                ImposedDofsValues    = ImposedDof.GetImpDofsVals(ImposedValuePosition)
                ValuesToStore        = -np.matmul(ImposedDofsSubMatrix, ImposedDofsValues)
                assert(ValuesToStore.size == FreeDofsGlobalNumbering.size)
                self.SetValues(FreeDofsSubMatrix, FreeDofsGlobalNumbering)
                self.StoreGeneEfforts(ValuesToStore, FreeDofsGlobalNumbering)
            else:
                GlobalNumbering = DofNumbering.GetElmntNumbring(ElmntIdx, HasImposedDof=False)
                self.SetValues(ElmntMat, GlobalNumbering)
        if not self.StoreElmntryMats:
            self.DelElmntsMatsBuff()
        self.FinalizeGeneEfforts()
        if self.StorageType == "Sparse":
            self.FinalizeSprsAssembly()
        self.IsAssembled = True

    def SetValues(self, Values, Indices):
        """
        Sets values of a diagonal block of the matrix. The bloc is defined 
        by its rows/columns indices stored in the 1D array Indices (rows
        and columns indices are the same since the bloc is diagonal).

        Parameters
        ----------
            Values : 'np.ndarray'
                A two-dimensional array containing values of the block.

            Indices : 'np.ndarray'
                An array that contains rows/columns indices.
            
        NB : Please note that only blocks of the diagonal can be modified.

        """
        assert(isinstance(Indices, np.ndarray) and Indices.ndim == 1)
        assert(isinstance(Values, np.ndarray))
        assert(Values.shape == (Indices.size, Indices.size))
        assert(not self.IsAssembled)

        if self.StorageType == "Sparse":
            return self.__SetSprsValues__(Values, Indices)
        elif self.StorageType == "Symmetric":
            return self.__SetSymValues__(Values, Indices)
        else:
            return self.__SetNsymValues__(Values, Indices)

    def __SetSymValues__(self, Values, Indices):
        """
        It should not be used. It is only used within the functions 
        SetValues(...) under strict assumptions.

        """
        assert(self.StorageType == "Symmetric")
        TempIdxs  = np.triu_indices_from(Values)
        SrtdIdxs  = np.argsort(Indices)
        TriUpIdxs = SrtdIdxs[TempIdxs[0]], SrtdIdxs[TempIdxs[1]]
        ValueIdxs = self.__GetSymIdxs__(Indices[SrtdIdxs])
        self.Value[ValueIdxs] += Values[TriUpIdxs]

    def __SetNsymValues__(self, Values, Indices):
        """
        It should not be used. It is only used within the functions 
        SetValues(...) under strict assumptions.

        """
        assert(self.StorageType == "NonSymmetric")
        self.Value[Indices[:,np.newaxis], Indices] += Values

    def __SetSprsValues__(self, Values, Indices):
        """
        It should not be used. It is only used within the functions 
        SetValues(...) under strict assumptions.

        """
        assert(self.StorageType == "Sparse")
        start = self.SprsCurrIdx
        end   = start + Values.size
        self.Value[start:end] = Values.flat
        self.RowsIndices[start:end] = Indices.repeat(Indices.size)
        self.ColsIndices[start:end] = np.tile(Indices, Indices.size)
        self.SprsCurrIdx = end

    def __GetSymIdxs__(self, SortedIdxs):
        """
        It should not be used. It is only used within the functions 
        __SetSymValues__(...) under strict assumptions.
        
        """
        IdxsNbr = SortedIdxs.size
        result  = np.zeros(GetTriPartSize(IdxsNbr), dtype=int)
        CurrIdx, NextIdx = 0, IdxsNbr
        for Idx, RowIdx in enumerate(SortedIdxs):
            RowStart = GetRowStartIdx(RowIdx, self.Size)
            result[CurrIdx:NextIdx] = SortedIdxs[Idx:] - SortedIdxs[Idx] + RowStart
            CurrIdx, NextIdx = NextIdx, NextIdx + (IdxsNbr - (Idx + 1))
        return result

    def FinalizeSprsAssembly(self):
        """
        Called at the end of assembly process if sparse storage
        is used. It creates the sparse matrix object

        """
        data = self.Value[:self.SprsCurrIdx].copy()
        del self.Value
        self.Value = None
        row  = self.RowsIndices[:self.SprsCurrIdx].copy()
        del self.RowsIndices
        self.RowsIndices = None
        col  = self.ColsIndices[:self.SprsCurrIdx].copy()
        del self.ColsIndices
        self.ColsIndices = None
        self.Value = bsr_matrix((data, (row, col)), shape=(self.Size, self.Size), dtype=float)

    def GetValues(self, StorageType=None):
        """
        Returns the matrix values array. The parameter StorageType
        is used to specify the storage type that must be used. Only two
        storage types are implemented for now :

            - "Dense"  : a 2D array is returned (The full matrix).
            - "Sparse" : a scipy sparse matrix is returned (bsr_matrix)

        """
        assert(self.IsAssembled and StorageType in ["Dense", "Sparse"])
        if StorageType == "Dense":
            return self.__GetDenseVal__()
        else:
            return self.__GetSparseVal__()

    def __GetDenseVal__(self):
        """
        It should not be used (use GetValues instead). It is only used
        within the functions GetValues(...) under strict assumptions.
        
        """

        if self.StorageType == "Symmetric":
            return GetSymAsDense(self.Value)
        elif self.StorageType == "NonSymmetric":
            return self.Value
        else:
            return self.Value.toarray()

    def __GetSparseVal__(self):
        """
        It should not be used (use GetValues instead). It is only used
        within the functions GetValues(...) under strict assumptions.
        
        """

        if self.StorageType == "Symmetric":
            return bsr_matrix(GetSymAsDense(self.Value))
        elif self.StorageType == "NonSymmetric":
            return bsr_matrix(self.Value)
        else:
            return self.Value

    def InitGeneEfforts(self):
        """
        Initialize the attribute TempGeneEfforts at the beginning of
        the matrix assembly.
        
        """
        if self.TempGeneEfforts == "Finalized":
            raise ValueError("GeneEfforts is already Finalized !")
        elif self.TempGeneEfforts is not None:
            raise ValueError("GeneEfforts is already initialized !")
        else:
            self.TempGeneEfforts = np.zeros(self.Size, dtype=float)

    def StoreGeneEfforts(self, GeneEffortsValues, GeneEffortsPos):
        """Sets values of TempGeneEfforts during the matrix assembly."""
        assert(self.TempGeneEfforts is not None)
        self.TempGeneEfforts[GeneEffortsPos] += GeneEffortsValues
    
    def FinalizeGeneEfforts(self):
        """
        Called at the end of the matrix assembly, to delete TempGeneEfforts
        and store its non-zero values and their positions respectively in the 
        arrays GeneEffortsVal and GeneEffortsPos.
        
        """
        assert(self.GeneEffortsVal is None)
        assert(self.GeneEffortsPos is None)
        
        if self.TempGeneEfforts is not None:
            assert(isinstance(self.TempGeneEfforts, np.ndarray))
            assert(self.ImposedDof is not None)
            Mask = self.TempGeneEfforts != 0.
            self.GeneEffortsVal = self.TempGeneEfforts[Mask]
            self.GeneEffortsPos = np.nonzero(Mask)[0]
            del self.TempGeneEfforts
        else:
            assert(self.ImposedDof is None)
            self.GeneEffortsVal = np.zeros(1)
            self.GeneEffortsPos = np.zeros(1, dtype=int)
        
        self.TempGeneEfforts = "Finalized"

    def GetDofNumbering(self):
        """Returns the matrix DOFs numbering object."""
        return self.DofNumbering

    def GetImposedDof(self):
        """Returns the matrix boundary conditions object."""
        return self.ImposedDof

    def GetType(self):
        """Returns the matrix type."""
        return self.Type
    
    def GetSize(self):
        """Returns the size of the matrix."""
        return self.Size

    def GetGeneEffPart(self):
        """
        Returns the contribution of the matrix to the generalized efforts
        vector. A tuple of two arrays containing the non-zero values and their
        indices is returned.
        
        """
        assert(self.TempGeneEfforts == "Finalized")
        return self.GeneEffortsPos, self.GeneEffortsVal

    def GetDofsSubMat(self, GlobalNumbring, StorageType=None):
        """
        Returns a submatrix whose DOFs (rows/cols) are stored in the 1D np.array
        GlobalNumbring, rows and cols are permuted to respect DOFs order within
        the array GlobalNumbring. The parameter StorageType is used to specify the 
        storage type that must be used. Only two options are implemented for now :

            - "Dense"  : a 2D array is returned.
            - "Sparse" : a scipy sparse matrix is returned (bsr_matrix)

        """
        assert(self.IsAssembled and StorageType in ["Dense", "Sparse"])
        assert(isinstance(GlobalNumbring, np.ndarray) and GlobalNumbring.ndim == 1)
        if StorageType == "Dense":
            return self.__GetDenseSubMat__(GlobalNumbring)
        else:
            return self.__GetSparseSubMat__(GlobalNumbring)

    def __GetDenseSubMat__(self, GlobalNumbring):
        """
        It should not be used (use GetDofsSubMat instead). It is only used
        within the functions GetDofsSubMat(...) under strict assumptions.

        """

        if self.StorageType == "Symmetric":
            return GetSymAsDense(self.Value)[GlobalNumbring[:,None], GlobalNumbring]
        elif self.StorageType == "NonSymmetric":
            return self.Value[GlobalNumbring[:,None], GlobalNumbring]
        else:
            tup = ()
            for RowIdx in GlobalNumbring:
                tup += self.Value.getrow(RowIdx).toarray()[0,GlobalNumbring],
            return np.vstack(tup)

    def __GetSparseSubMat__(self, GlobalNumbring):
        """
        It should not be used (use GetDofsSubMat instead). It is only used
        within the functions GetDofsSubMat(...) under strict assumptions.

        """

        if self.StorageType == "Symmetric":
            SubMat = GetSymAsDense(self.Value)[GlobalNumbring[:,None], GlobalNumbring]
            return bsr_matrix(SubMat)
        elif self.StorageType == "NonSymmetric":
            SubMat = self.Value[GlobalNumbring[:,None], GlobalNumbring]
            return bsr_matrix(SubMat)
        else:
            tup = ()
            for RowIdx in GlobalNumbring:
                tup += self.Value.getrow(RowIdx),
            result = vstack(tup, format="bsr")
            tup = ()
            for RowIdx in GlobalNumbring:
                tup += result.getcol(RowIdx),
            return hstack(tup, format="bsr")

    def GetDofsNumbring(self, **kwargs):
        """
        Returns an np.array that contains the global numbering of Dofs 
        passed by kwargs.

        Parameters
        ----------
            kwargs : 'dict'
                keys are nodes names (ex Node0, Node175, ...) and their
                corresponding values are desired DOFs names.

                            node_name = (dof_1, dof_2)

                Examples (3D model) :
            
                    - Node0 = ("DX", "DY", "DZ")
            
                    - Node13 = ["DZ", "DY"]
            
                    - Node41 = ("DY",)

        Return
        ------
            Returns a 1D np.ndarray that contains global numbering of 
            passed DOFs. The DOFs order in kwargs is respected.

        """
        return self.DofNumbering.GetDofsNumbring(**kwargs)
    
    def GetDerivative(self, Parameter, IntegType="Complete", StorageType="Sparse"):
        """Returns the derivative of the matrix with respect to Parameter"""
        StudyModel = self.DofNumbering.GetModel()
        MatName = self.GetType() + "Matrix"
        ImposedDof = self.ImposedDof
        Behavior = self.Behavior
        KwArgs = {}

        KwArgs["DerivIntegType"] = IntegType
        KwArgs["StoreElementry"] = False
        KwArgs["StorageType"] = StorageType
        KwArgs["Derivative"] = Parameter
        KwArgs["ImposedDof"] = ImposedDof
        KwArgs[MatName] = True

        return assembly.Assemble(StudyModel, Behavior, **KwArgs)

def GetRowStartIdx(RowIdx, Size):
    assert(0 <= RowIdx < Size)
    return RowIdx * (2 * Size - RowIdx + 1) // 2

def GetTriPartSize(n : int) -> int:
    """
    Returns the number of elements of the triangular upper/lower
    part of a squared matrix whose number of rows/cols is n.

    Nb : It's the inverse of GetFullPartSize()

    """
    return n * (n + 1) // 2

def GetFullPartSize(a : int) -> int:
    """
    Returns the number of rows/cols of a squared matrix whose
    triangular upper/lower part contains a elements. If there
    is no such a matrix, the value -1 is returned.

    Nb : It's the inverse of GetTriPartSize().

    """
    n = int(pow(2 * a + 1 / 4, 1 / 2) - 1 / 2)
    return n if GetTriPartSize(n) == a else -1

def GetSymAsDense(TriUpPart):
    """
    Returns a symmetrix matrix whose triangular upper part
    is stored within the 1D array TriUpPart.

    """
    assert(isinstance(TriUpPart, np.ndarray))
    assert(TriUpPart.ndim == 1)
    Size = GetFullPartSize(TriUpPart.size)
    result = np.empty((Size, Size))
    RowIdxs, ColIdxs  = np.triu_indices(Size)
    result[RowIdxs, ColIdxs] = TriUpPart
    result[ColIdxs, RowIdxs] = TriUpPart
    return result