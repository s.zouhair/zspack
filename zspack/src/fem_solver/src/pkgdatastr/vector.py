"""
This module implements the vector class, and several
utility functions.

"""

import numpy as np

from ..pkgbndary import boundary
from ..pkgresult import Results
from .. import model

class Vector:
    """
    Vector class.

    Parameters
    ----------
        DofNumbering : 'DofNumbering object'
            The DOF's numbering object.

        ImposedDof : 'BoundCond object'
            The boundary conditions object.

        ValuesBuff : 'np.ndarray', optional
            If specified, it's used as the vector's values array (no
            allocation). Available only for DofsField vectors.

        VectorType : 'str',optional
            The vector type. Two values are possible :

                - "GeneEfforts" : for a generalized efforts vector.
                  The parameter ValuesBuff shouldn't be specified
                  because GeneEfforts vectors are supposed to be 
                  assembled.

                - "DofsField"   : for a displacement field vector.
                  The parameter ValuesBuff must be specified 
                  because DofsField vectors aren't supposed to be 
                  asssembled (ex : the displacement vector in linear 
                  elasticity is computed by resolving KU = F).
        
        ElmntsVects : 'bool', optional
            If True, elementry vectors computed during assembly 
            process are stored. Available only for GeneEfforts
            vectors since DofsField aren't assembled.

    Attributes
    ----------
        DofNumbering : 'DofNumbering object'
            The DOF's numbering object of the vector.

        ImposedDof : 'BoundCond object'
            The boundary conditions object of the vector.

        Type : 'str'
            The vector type.

        Size : 'int'
            The DOF's number of the vector.
        
        Value : 'np.ndarray'
            The vector's values array.

        IsAssembled : 'bool'
            Determines whether the value array is assembled or not 
            yet. Available only for GeneEfforts vectors.

        StoreElmntryVects : 'bool'
            Determines whether elementary vectors are stored or 
            not. Available only for GeneEfforts vectors.

        ElemntryVectsVals : 'np.ndarray'
            An array where elementary vectors of some elements are stored.
            Only vectors of elements that have non-zero elementary vectors
            are stored. This array is one dimensional, and available only 
            for GeneEfforts vectors.

        ElemntryVectsPtr : 'np.ndarray'
            A 2d array with whape (ElmntsNbr, 2). Each row correspond to an
            element, if the element doesn't have a stored vector within the
            array ElemntryVectsVals, its both columns are -1, otherwise, the 
            first ans second columns are respectively the start and stop indices
            within ElemntryVectsVals where the elementary vector is stored. It 
            is available only for GeneEfforts vectors.


        ElemntryVectsCurrIdx : 'int'
            An integer that kepps track of the position within ElemntryVectsVals
            where the next elementary vector will be stored during a FE assembly
            process. It is available only for GeneEfforts vectors.

    """

    def __init__(self, DofNumbering, ImposedDof, VectorType=None, ValuesBuff=None, ElmntsVects=None):
        assert(isinstance(DofNumbering, model.DofNumbering))
        assert(VectorType in ["GeneEfforts", "DofsField"])
        assert(DofNumbering.UpdatingFinalized)
        if ImposedDof is not None:
            assert(isinstance(ImposedDof, boundary.BoundCond))
            assert(ImposedDof.GetDofNumbering() is DofNumbering)
        elif DofNumbering.GetImposedDofNbr() != 0:
            raise ValueError("The object DofNumbering contains "
                "imposed DOFs, therefore the parameter "
                "ImposedDof must be specified")

        self.DofNumbering = DofNumbering
        self.ImposedDof = ImposedDof
        self.Type = VectorType
        self.Size = DofNumbering.GetFreeDofNbr()

        if VectorType == "DofsField":
            assert(ElmntsVects is None)
            if ValuesBuff is None:
                raise ValueError("ValuesBuff must be specified."
                    " DofsField vectors aren't assembled.")
            self.Value = np.asarray(ValuesBuff)
            assert(self.Value.shape == (self.Size,))
        else:
            assert(ElmntsVects in [True, False])
            if ValuesBuff is not None:
                raise ValueError("ValuesBuff shouldn't be specified."
                    " GeneEfforts vectors are assembled.")
            self.Value = np.zeros(self.Size)
            self.IsAssembled = False
            # Objects related to elementary vectors
            self.StoreElmntryVects = ElmntsVects
            self.ElemntryVectsVals = None
            self.ElemntryVectsPtr  = None
            self.ElemntryVectsCurrIdx = None
            self.InitElmntsVectsBuff()

    def __iadd__(self, other):
        if isinstance(other, np.ndarray):
            self.Value += other
        else: raise NotImplementedError
        return self

    def InitElmntsVectsBuff(self):
        """
        Initialize elementary vectors objects used for 
        GeneEfforts vectors.

        """
        assert(self.Type == "GeneEfforts")
        StudyModel = self.DofNumbering.GetModel()
        StudyMesh  = StudyModel.GetModelMesh()
        self.ElemntryVectsPtr = - np.ones((StudyMesh.GetNumberof("Elmnts"), 2), dtype=int)
        self.ElemntryVectsCurrIdx = 0

    def DelElmntsVectsBuff(self):
        """
        Delete elementary vectors objects used for 
        GeneEfforts vectors.

        """
        assert(self.Type == "GeneEfforts")
        del self.ElemntryVectsVals
        self.ElemntryVectsVals = None
        del self.ElemntryVectsPtr
        self.ElemntryVectsPtr  = None
        self.ElemntryVectsCurrIdx = 0

    def SetElmntryVectVal(self, ElmntryVect, ElmntIdx):
        """
        Store the elementary vector for the element number
        ElmntIdx. If the element ElmntIdx have already a 
        stored vector, it's updated.

        """
        assert(0 <= ElmntIdx < self.DofNumbering.GetElmntsNbr())
        assert(self.Type == "GeneEfforts" and not self.IsAssembled)
        assert(ElmntryVect.ndim == 1)
        if not self.HaveStoredVect(ElmntIdx):
            self.CreateElemntryVect(ElmntIdx)
        StartIdx, EndIdx = self.ElemntryVectsPtr[ElmntIdx]
        self.ElemntryVectsVals[StartIdx:EndIdx] += ElmntryVect

    def GetElmntryVectVal(self, ElmntIdx, ImpDofs=None):
        """
        Returns the elementry vector of the element ElmntIdx. The
        parameter ImpDofs determines whether imposed DOFs must be 
        eliminated or not (False means that they must be eliminated).

        """
        assert(0 <= ElmntIdx < self.DofNumbering.GetElmntsNbr())
        if self.Type == "DofsField":
            return self.__GetElmntryVectDofsField__(ElmntIdx, ImpDofs=ImpDofs)
        elif self.IsAssembled and not self.StoreElmntryVects:
            raise ValueError("Elementary vectors aren't stored.")
        else:
            return self.__GetElmntryVectGenEfrt__(ElmntIdx, ImpDofs=ImpDofs)

    def __GetElmntryVectDofsField__(self, ElmntIdx, ImpDofs=None):
        """
        Returns the elementry vector of the element ElmntIdx. The
        parameter ImpDofs determines whether imposed DOFs must be 
        eliminated or not (False means that they must be eliminated).
        This function is specific to DofsField vectors. It shouldn't 
        be used out of the vector class.

        """
        assert(self.Type == "DofsField")
        assert(isinstance(ImpDofs, bool))

        DofNumbering = self.DofNumbering

        if self.ImposedDof is None or not DofNumbering.HasImposedDof(ElmntIdx=ElmntIdx):
            GlobalNumbering = DofNumbering.GetElmntNumbring(ElmntIdx, HasImposedDof=False)
            result = self.Value[GlobalNumbering]
        elif ImpDofs:
            FreeDofsNum, ImpoDofsNum = DofNumbering.GetElmntNumbring(ElmntIdx, HasImposedDof=True)
            FreeDofsGlobalNumbering, FreeDofsLocalNumbering  = FreeDofsNum
            ImpValuePosition, ImpDofsLocalNumbering = ImpoDofsNum
            VectSize = DofNumbering.GetElmntDofsNbr(ElmntIdx)
            result = np.empty((VectSize,), dtype=self.Value.dtype)
            result[FreeDofsLocalNumbering] = self.Value[FreeDofsGlobalNumbering]
            result[ImpDofsLocalNumbering] = self.ImposedDof.GetImpDofsVals(ImpValuePosition)
        else:
            FreeDofsNum, _ = DofNumbering.GetElmntNumbring(ElmntIdx, HasImposedDof=True)
            FreeDofsGlobalNumbering, _  = FreeDofsNum
            result = self.Value[FreeDofsGlobalNumbering]

        return result

    def __GetElmntryVectGenEfrt__(self, ElmntIdx, ImpDofs=None):
        """
        Returns the elementry vector of the element ElmntIdx. The
        parameter ImpDofs determines whether imposed DOFs must be 
        eliminated or not (False means that they must be eliminated).
        This function is specific to GeneEfforts vectors. It shouldn't 
        be used out of the vector class.

        """
        assert(self.Type == "GeneEfforts")
        assert(isinstance(ImpDofs, bool))

        DofNumbering = self.DofNumbering

        if ImpDofs or not DofNumbering.HasImposedDof(ElmntIdx=ElmntIdx):
            if self.HaveStoredVect(ElmntIdx):
                start, end = self.ElemntryVectsPtr[ElmntIdx]
                ElmntVect = self.ElemntryVectsVals[start:end]
            else:
                dtype = self.ElemntryVectsVals.dtype
                VectSize = self.DofNumbering.GetElmntDofsNbr(ElmntIdx)
                ElmntVect = np.zeros((VectSize,), dtype=dtype)
        else:
            FreeDofsNum, _ = DofNumbering.GetElmntNumbring(ElmntIdx, HasImposedDof=True)
            _, FreeDofsLocalNumbering  = FreeDofsNum
            if self.HaveStoredVect(ElmntIdx):
                start, end = self.ElemntryVectsPtr[ElmntIdx]
                ElmntVect = self.ElemntryVectsVals[start:end][FreeDofsLocalNumbering]
            else:
                dtype = self.ElemntryVectsVals.dtype
                VectSize = FreeDofsLocalNumbering.size
                ElmntVect = np.zeros((VectSize,), dtype=dtype)

        return ElmntVect
        
    
    def CreateElemntryVect(self, ElmntIdx):
        """
        Creats an elementary vector for the element ElmntIdx
        which don't have yet a stored vector.

        """
        assert(0 <= ElmntIdx < self.DofNumbering.GetElmntsNbr())
        assert(self.Type == "GeneEfforts" and not self.IsAssembled)
        assert(not self.HaveStoredVect(ElmntIdx))
        VectSize = self.DofNumbering.GetElmntDofsNbr(ElmntIdx)
        TempVect = np.zeros((VectSize,), dtype=float)
        if self.ElemntryVectsVals is None:
            self.ElemntryVectsVals = TempVect
        else:
            self.ElemntryVectsVals = np.append(self.ElemntryVectsVals, TempVect)
        self.ElemntryVectsPtr[ElmntIdx, 0] = self.ElemntryVectsCurrIdx
        self.ElemntryVectsPtr[ElmntIdx, 1] = self.ElemntryVectsCurrIdx + VectSize
        self.ElemntryVectsCurrIdx += VectSize

    def HaveStoredVect(self, ElmntIdx):
        """
        Returns a bool that determines whether the element ElmntIdx
        have already a stored elementary vector.

        """
        assert(0 <= ElmntIdx < self.DofNumbering.GetElmntsNbr())
        assert(self.Type == "GeneEfforts")
        if self.ElemntryVectsPtr[ElmntIdx, 0] == -1:
            assert(self.ElemntryVectsPtr[ElmntIdx, 1] == -1)
            return False
        else:
            assert(self.ElemntryVectsPtr[ElmntIdx, 1] != -1)
            return True

    def Assemble(self):
        assert(self.Type == "GeneEfforts" and not self.IsAssembled)
        ElmntList = np.nonzero(self.ElemntryVectsPtr[:,0] >= 0)[0]
        DofNumbering = self.DofNumbering
        for ElmntIdx in ElmntList:
            ElmntVect = self.__GetElmntryVectGenEfrt__(ElmntIdx, ImpDofs=True)
            if DofNumbering.HasImposedDof(ElmntIdx=ElmntIdx):
                FreeDofsNum, _ = DofNumbering.GetElmntNumbring(ElmntIdx, HasImposedDof=True)
                FreeDofsGlobalNumbering, FreeDofsLocalNumbering  = FreeDofsNum
                FreeDofsSubVector    = ElmntVect[FreeDofsLocalNumbering]
                self.SetValues(FreeDofsSubVector, FreeDofsGlobalNumbering)
            else:
                GlobalNumbering = DofNumbering.GetElmntNumbring(ElmntIdx, HasImposedDof=False)
                self.SetValues(ElmntVect, GlobalNumbering)
        if not self.StoreElmntryVects:
            self.DelElmntsVectsBuff()
        self.IsAssembled = True

    def SetValues(self, Values, Idxs):
        """
        Sets values of a block of the vector. The bloc is defined by
        its elements indices stored in the array Idxs. Their corresponding
        values are stored in the parameter Values.

        """
        assert(isinstance(Idxs, np.ndarray) and Idxs.ndim == 1)
        assert(isinstance(Values, np.ndarray) and Values.ndim == 1)
        assert(Values.size == Idxs.size)
        self.Value[Idxs] += Values

    def GetValues(self, Copy=False):
        """
        Returns the values array of the vector. If the parameter Copy
        is False, a view is returned, otherwise a copy is returned.

        """
        if self.Type == "GeneEfforts":
            assert(self.IsAssembled)

        if Copy: return self.Value.copy()
        else: return self.Value

    def GetDofNumbering(self):
        """Returns the vector DOFs numbering object."""
        return self.DofNumbering

    def GetImposedDof(self):
        """Returns the vector boundary conditions object."""
        return self.ImposedDof

    def GetType(self):
        """Returns the vector type."""
        return self.Type

    def GetAllDofs(self):
        """
        Returns a matrix that contains Dofs values of a model's
        Dofs field (Ex : displacement field - see the example below).

        Example (3D model)
        ------------------
            If the mesh contains 4 nodes, each node correspond to a row of the
            returned matrix, and each column to a DOF {0 : DX, 1 : DY, 2 : DZ}.
            The table below contains an example of the returned matrix.

                                      DX         DY         DZ    
                                 ----------------------------------
                        Node 1 : |   0.00   |   0.00   |   0.00   |
                                 |----------|----------|----------|
                        Node 2 : |   3.45   |   2.15   |   0.03   |
                                 |----------|----------|----------|
                        Node 3 : |   2.75   |   1.22   |   0.27   |
                                 |----------|----------|----------|
                        Node 4 : |   0.00   |   0.00   |   0.00   |
                                 ----------------------------------

        """
        assert(self.Type == "DofsField")
        DofNumbering = self.GetDofNumbering()
        DofOrdering  = DofNumbering.DofOrdering
        FreeDofVal  = self.GetValues()
        dtype = FreeDofVal.dtype
        DofNbrPerNod = DofNumbering.GetDofNbrPerNode()
        TotalDofs  = np.empty(
            DofNumbering.GetTotalDofNbr(), 
            dtype=dtype).reshape(-1,DofNbrPerNod)
        for idx in range(1, DofNbrPerNod + 1): # Ex : [i = 0 => DX / i = 0 => DY / i = 0 => DZ]
            mask = DofOrdering[:,idx] >= 0
            FreeDofIdx = DofOrdering[mask, idx]
            TotalDofs[mask, idx - 1]  = FreeDofVal[FreeDofIdx]
            if self.ImposedDof is not None and self.ImposedDof.ContainsImpDofs():
                ImpdDofIdx = -(DofOrdering[~mask, idx] + 1)
                TotalDofs[~mask, idx - 1] = self.ImposedDof.GetImpDofsVals(ImpdDofIdx)
        return TotalDofs

    def GetDofs(self, GlobalNumbring):
        """
        Returns an 1D np.ndarray that contains values of DOFs whose
        global numbering is stored in the 1D np.ndarray GlobalNumbring.

        """
        assert(self.Type == "DofsField")
        return self.GetValues()[GlobalNumbring]

    def GetDofsNumbring(self, **kwargs):
        """
        Returns an np.array that contains the global numbering of Dofs 
        passed by kwargs.

        Parameters
        ----------
            kwargs : 'dict'
                keys are nodes names (ex Node0, Node175, ...) and their
                corresponding values are desired DOFs names.

                            node_name = (dof_1, dof_2)

                Examples (3D model) :
            
                    - Node0 = ("DX", "DY", "DZ")
            
                    - Node13 = ["DZ", "DY"]
            
                    - Node41 = ("DY",)

        Return
        ------
            Returns a 1D np.ndarray that contains global numbering of 
            passed DOFs. The DOFs order in kwargs is respected.

        """
        return self.DofNumbering.GetDofsNumbring(**kwargs)

    def AddResultToVTK(self, ResObj, **kwargs):
        """
        Saves degree of freedom fields into a Results object.
        Only available for FE vectors of "DofsField" type.

        """
        assert(isinstance(ResObj, Results))
        assert(self.Type == "DofsField")
        if bool(kwargs):
            have_suffix = "suffix" in kwargs
            have_imag   = "imag" in kwargs
            
            if len(kwargs) == 1:
                assert(have_suffix or have_imag)
            elif len(kwargs) == 2:
                assert(have_suffix and have_imag)
            else:
                assert(False)
            
            if have_suffix:
                assert(isinstance(kwargs["suffix"], str))
            
            if have_imag:
                assert(isinstance(kwargs["imag"], bool))

        StudyModel = self.DofNumbering.GetModel()
        assert(ResObj.ContainsModel(StudyModel))
        ResultsBydofs = self.GetAllDofs()
        if ResultsBydofs.dtype == complex:
            self.ARTVTK_CMPLX(StudyModel, ResultsBydofs, ResObj, **kwargs)
        else:
            self.ARTVTK_REAL(StudyModel, ResultsBydofs, ResObj, **kwargs)

    def ARTVTK_CMPLX(self, StudyModel, ResultsBydofs, ResObj, **kwargs):
        """
        Saves degree of freedom fields into a Results object.
        Only available for FE vectors of "DofsField" type
        with a complex data type. It should not be used. 
        It is only used within the function AddResultToVTK(...) 
        under strict assumptions.

        """
        RsltsBydofs_real = ResultsBydofs.real
        RsltsBydofs_imag = ResultsBydofs.imag
        DofsNbr   = StudyModel.GetModelDofNbr()
        DofsNames = StudyModel.GetModelDof()
        for DofIdx in range(DofsNbr):
            save_imag = not ("imag" in kwargs and not kwargs["imag"])
            nend = "" if "suffix" not in kwargs else kwargs["suffix"]
            rend = "" if not save_imag else " (real)"

            real_dof_name = DofsNames[DofIdx] + rend + nend
            ResObj.AddNodesField(StudyModel, \
                RsltsBydofs_real[:,DofIdx], real_dof_name)
            
            if save_imag:
                imag_dof_name = DofsNames[DofIdx] + " (imag)" + nend
                ResObj.AddNodesField(StudyModel, \
                    RsltsBydofs_imag[:,DofIdx], imag_dof_name)

    def ARTVTK_REAL(self, StudyModel, ResultsBydofs, ResObj, **kwargs):
        """
        Saves degree of freedom fields into a Results object.
        Only available for FE vectors of "DofsField" type
        with a real data type. It should not be used. It is 
        only used within the function AddResultToVTK(...) 
        under strict assumptions.

        """
        if "imag" in kwargs:
            raise ValueError("Real displacement fields " \
                                "doesn't have an imaginary part !")
        DofsNbr   = StudyModel.GetModelDofNbr()
        DofsNames = StudyModel.GetModelDof()
        for DofIdx in range(DofsNbr):
            nend = "" if "suffix" not in kwargs else kwargs["suffix"]
            
            dof_name = DofsNames[DofIdx] + nend
            ResObj.AddNodesField(StudyModel, \
                ResultsBydofs[:,DofIdx], dof_name)