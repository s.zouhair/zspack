"""
This module implements the result class, and several
utility functions.

"""
from mpi4py import MPI
import numpy as np
import vtk

from vtk.util import numpy_support

from ..pkgfelmnt import fe_cata
from ..pkgutils import GetElmntType, IsParallelStudy, \
                       GetCommWorldSize, GetCommWorldRank
from .. import model

class Results:
    """
    Results class.

    Parameters
    ----------
        StudyModels : 'a list of femsol.model objects'
            The model for which we want to create a Result object.

    Attributes
    ----------
        StudyModels : 'a list of femsol.model objects'
            The underlying model to the Result object.
        
        ModelsNbr : "xxx"
            xxx xxx xxx xxx xxx.
        
        NodesNbr : "xxx"
            xxx xxx xxx xxx xxx.
        
        ElmntsNbr : "xxx"
            xxx xxx xxx xxx xxx.

        UnstructuredGrid : 'vtk.UnstructuredGrid object'
            The vtk object where the mesh and results fields will
            be stored for visualization.
        
        NodesFields : "xxx"
            xxx xxx xxx xxx xxx.
        
        ElmntsFields : "xxx"
            xxx xxx xxx xxx xxx.

    """

    def __init__(self, *StudyModels):
        
        for StudyModel in StudyModels:
            assert(isinstance(StudyModel, model.model))
        
        if IsParallelStudy():
            self.CommRank = GetCommWorldRank()
            self.CommSize = GetCommWorldSize()
        
        self.StudyModels = StudyModels
        self.ModelsNbr = len(StudyModels)

        self.NodesNbr = np.cumsum([0] + [StudyModel.GetModelNodesNbr() \
                                for StudyModel in StudyModels], dtype=int)
        
        self.ElmntsNbr = np.cumsum([0] + [StudyModel.GetModelElmntsNbr() \
                                for StudyModel in StudyModels], dtype=int)
        
        self.UnstructuredGrid = vtk.vtkUnstructuredGrid()
        self.AddNodes()
        self.AddElmnts()

        self.NodesFields = {}
        self.ElmntsFields = {}
    
    def GetModelsNbr(self):
        """Returns the number of models of the result object."""
        return self.ModelsNbr
    
    def GetModelIdx(self, StudyModel):
        """xxx."""
        for ModelIdx, Model in enumerate(self.StudyModels):
            if Model is StudyModel:
                return ModelIdx    
        raise ValueError("The model doesn't belong to the result object. !")
    
    def GetTotalNodesNbr(self):
        """Returns the total number of nodes."""
        return self.NodesNbr[-1]

    def GetTotalElmntsNbr(self):
        """Returns the total number of nodes."""
        return self.ElmntsNbr[-1]
    
    def GetNodesNbr(self, ModelIdx=None):
        """
        Returns the number of nodes of the model ModelIdx. 
        If there is only one model, ModelIdx must be either 
        None or zero.
        
        """
        return self.GetNbrOf(ModelIdx, etype="Nodes")
    
    def GetElmntsNbr(self, ModelIdx=None):
        """
        Returns the number of elements of the model ModelIdx. 
        If there is only one model, ModelIdx must be either 
        None or zero.
        
        """
        return self.GetNbrOf(ModelIdx, etype="Elmnts")

    def GetNbrOf(self, ModelIdx, etype=None):
        assert(etype in ["Nodes", "Elmnts"])
        earray = self.NodesNbr if etype == "Nodes" else self.ElmntsNbr
        if self.GetModelsNbr() == 1:
            assert(ModelIdx is None or ModelIdx == 0)
            result = earray[1]
        else:
            assert(ModelIdx is not None and isinstance(ModelIdx, int))
            assert(ModelIdx < self.GetModelsNbr())
            result = earray[ModelIdx + 1] - earray[ModelIdx]
        return result
    
    def GetNodesShift(self, ModelIdx):
        """
        Returns the index at which starts the numbering of nodes of 
        the model ModelIdx within the vtk unstructured grid object.
        
        """
        return self.GetShiftOf(ModelIdx, etype="Nodes")

    def GetElmntsShift(self, ModelIdx):
        """
        Returns the index at which starts the numbering of elements of 
        the model ModelIdx within the vtk unstructured grid object.
        
        """
        return self.GetShiftOf(ModelIdx, etype="Elmnts")

    def GetShiftOf(self, ModelIdx, etype=None):
        assert(isinstance(ModelIdx, int))
        assert(ModelIdx < self.GetModelsNbr())
        assert(etype in ["Nodes", "Elmnts"])
        earray = self.NodesNbr if etype == "Nodes" else self.ElmntsNbr
        return earray[ModelIdx]

    def GetModel(self, ModelIdx=None):
        """
        Returns the underlying model to the "Results" object.
        If there is only one model, ModelIdx must be either 
        None or zero.
        
        """
        if self.GetModelsNbr() == 1:
            assert(ModelIdx is None or ModelIdx == 0)
            result = self.StudyModels[0]
        else:
            assert(ModelIdx is not None)
            assert(ModelIdx < self.GetModelsNbr())
            result = self.StudyModels[ModelIdx]
        return result
    
    def ContainsModel(self, StudyModel):
        """
        Returns a bool that determines whether or not the 
        model StudyModel belongs to the result object.
        
        """
        try:
            self.GetModelIdx(StudyModel)
            return True
        except ValueError:
            return False

    
    def AddNodes(self):
        """Adds nodes to the unstructured grid vtk object."""
        Nodes = vtk.vtkPoints()
        for ModelIdx in range(self.GetModelsNbr()):
            StudyModel = self.GetModel(ModelIdx=ModelIdx)
            StudyMesh = StudyModel.GetModelMesh()
            NodesCoords = StudyMesh.GetMeshCoords()
            NodesShift = self.GetNodesShift(ModelIdx)
            for NodeIdx, NodeCoords in enumerate(NodesCoords):
                Nodes.InsertPoint(NodeIdx + NodesShift, NodeCoords)
        self.UnstructuredGrid.SetPoints(Nodes)

    def AddElmnts(self):
        """Adds elements to the unstructured grid vtk object."""
        for ModelIdx in range(self.GetModelsNbr()):
            StudyModel = self.GetModel(ModelIdx=ModelIdx)
            StudyMesh = StudyModel.GetModelMesh()
            Connectivity = StudyMesh.GetMeshConnect()
            Dimension = StudyMesh.GetMeshDimension()
            NodesShift = self.GetNodesShift(ModelIdx)
            for ElmntRow in Connectivity:
                ElmntNodesNbr  = ElmntRow[0]
                ElmntNodesList = ElmntRow[1:ElmntNodesNbr+1] + NodesShift
                ElmntVtkType   = GetElmntVtkType(Dimension, ElmntNodesNbr)
                if ElmntVtkType is vtk.VTK_HEXAHEDRON:
                    # For hexahedrons, vtk local numerotation
                    # is different  from the one  used within
                    # our solver
                    ElmntNodesList = ElmntNodesList[[0,1,2,3,4,7,6,5]]
                self.UnstructuredGrid.InsertNextCell(ElmntVtkType, ElmntNodesNbr, ElmntNodesList)

    def CreateField(self, FieldName, etype, dtype):
        """xxx."""
        assert(etype in ["Nodes", "Elmnts"])
        bools_array = np.full(self.GetModelsNbr(), False)
        if etype == "Nodes":
            TotalNodesNbr = self.GetTotalNodesNbr()
            self.NodesFields[FieldName] = \
                (bools_array, np.empty(TotalNodesNbr, dtype=dtype))
        else:
            TotalElmntsNbr = self.GetTotalElmntsNbr()
            self.ElmntsFields[FieldName] = \
                (bools_array, np.empty(TotalElmntsNbr, dtype=dtype))

    def AddNodesField(self, StudyModel, ValuesArray, FieldName, Replace=False):
        assert(isinstance(StudyModel, model.model))
        assert(isinstance(ValuesArray, np.ndarray))
        assert(isinstance(FieldName, str))
        assert(isinstance(Replace, bool))

        ModelIdx = self.GetModelIdx(StudyModel)
        ModelNodesNbr = self.GetNodesNbr(ModelIdx=ModelIdx)
        
        assert(ValuesArray.shape == (ModelNodesNbr,))

        StartIdx = self.GetNodesShift(ModelIdx)
        EndIdx   = StartIdx + ModelNodesNbr

        if FieldName not in self.NodesFields:
            typ = ValuesArray.dtype
            self.CreateField(FieldName, "Nodes", typ)
        elif self.NodesFields[FieldName][0][ModelIdx] and not Replace:
            raise ValueError("Programming error : A second call to" \
                        " AddNodesField without Replace being true !")

        
        self.NodesFields[FieldName][1][StartIdx:EndIdx] = ValuesArray
        self.NodesFields[FieldName][0][ModelIdx] = True

        if np.all(self.NodesFields[FieldName][0]):
            TempVtkArray = numpy_support.numpy_to_vtk( \
                    self.NodesFields[FieldName][1], deep=1)
            TempVtkArray.SetName(FieldName)
            self.UnstructuredGrid.GetPointData().AddArray(TempVtkArray)

    def AddElmntsField(self, StudyModel, ValuesArray, FieldName, Replace=False):
        assert(isinstance(StudyModel, model.model))
        assert(isinstance(ValuesArray, np.ndarray))
        assert(isinstance(FieldName, str))
        assert(isinstance(Replace, bool))

        ModelIdx = self.GetModelIdx(StudyModel)
        ModelElmntsNbr = self.GetElmntsNbr(ModelIdx=ModelIdx)
        
        assert(ValuesArray.shape == (ModelElmntsNbr,))

        StartIdx = self.GetElmntsShift(ModelIdx)
        EndIdx   = StartIdx + ModelElmntsNbr

        if FieldName not in self.ElmntsFields:
            typ = ValuesArray.dtype
            self.CreateField(FieldName, "Elmnts", typ)
        elif self.ElmntsFields[FieldName][0][ModelIdx] and not Replace:
            raise ValueError("Programming error : A second call to \
                        AddElmntsField without Replace being true !")
        
        self.ElmntsFields[FieldName][1][StartIdx:EndIdx] = ValuesArray
        self.ElmntsFields[FieldName][0][ModelIdx] = True
        
        if np.all(self.ElmntsFields[FieldName][0]):
            TempVtkArray = numpy_support.numpy_to_vtk( \
                self.ElmntsFields[FieldName][1], deep=1)
            TempVtkArray.SetName(FieldName)
            self.UnstructuredGrid.GetCellData().AddArray(TempVtkArray)

    def AddResult(self, ResObject, **kwargs):
        """xxx."""
        try:
            ResObject.AddResultToVTK(self, **kwargs)
        except AttributeError:
            ClassName = type(ResObject).__name__
            raise AttributeError(("The class {} doesn't implement"
                " an AddResultToVTK method").format(ClassName))

    def Export(self, Filename, **kwargs):
        """xxx."""
        if bool(kwargs):
            self.__CheckExportKwargs__(**kwargs)
        writer = vtk.vtkUnstructuredGridWriter()
        assert(not Filename.endswith(".vtk"))
        if IsParallelStudy():
            self.__ExportParallelFile__(Filename)
            Filename += "_{}".format(self.CommRank)
        Filename += ".vtk"
        writer.SetFileName(Filename)
        for FieldName, (bools_array, _) in self.NodesFields.items():
            if not np.all(bools_array):
                raise ValueError("The nodes field {} is "
                    "not entirely filled !".format(FieldName))
        for FieldName, (bools_array, _) in self.ElmntsFields.items():
            if not np.all(bools_array):
                raise ValueError("The elements field {} is "
                    "not entirely filled !".format(FieldName))
        writer.SetInputData(self.UnstructuredGrid)
        writer.Write()
    
    def __ExportParallelFile__(self, Filename):
        """xxx"""
        if self.CommRank == 0:
            ResultFile = open(Filename + ".pvtk", "w")
            ResultFile.write('<File version="pvtk-1.0"'
                '\n\t\tdataType="vtkUnstructuredGrid"\n\t'
                '\tnumberOfPieces="{}" >\n'.format(self.CommSize))

            for SdIdx in range(self.CommSize):
                ResultFile.write('\t<Piece fileName="' + \
                    (Filename + "_{}").format(SdIdx) + '.vtk" />\n')

            ResultFile.write("</File>")

            ResultFile.close()
    
    def __CheckExportKwargs__(self, **kwargs):
        """xxx."""
        assert(len(kwargs) == 1)

        if "AddSd" not in kwargs:
            raise ValueError("Invalid keyword arguments !")
        else: assert(isinstance(kwargs["AddSd"], bool))
        
        ModelsNbr = self.GetModelsNbr()
        IsParallel = IsParallelStudy()

        if IsParallel:
            assert(ModelsNbr == 1)
            SdNumber = self.CommSize
        else: SdNumber = ModelsNbr

        if kwargs["AddSd"] and SdNumber == 1:
            print("Warning : there is only one {}, the "
                "keyword 'AddSd' will be ignored".format(
                "subdomain" if IsParallel else "model"))
        elif kwargs["AddSd"]:
            if not IsParallel or self.CommRank == 0:
                scal = np.arange(SdNumber, dtype=int)
                np.random.shuffle(scal)
            else: scal = np.empty(SdNumber, dtype=int)
            if IsParallel:
                MPI.COMM_WORLD.Bcast(scal, root=0)
                ModelObj  = self.GetModel()
                ElmntsNbr = self.GetElmntsNbr()
                ValuesArray = np.ones(ElmntsNbr, dtype=int) * scal[self.CommRank]
                self.AddElmntsField(ModelObj, ValuesArray, "Subdomains")
            else:
                for ModelIdx in range(SdNumber):
                    ModelObj  = self.GetModel(ModelIdx)
                    ElmntsNbr = self.GetElmntsNbr(ModelIdx)
                    ValuesArray = np.ones(ElmntsNbr, dtype=int) * scal[ModelIdx]
                    self.AddElmntsField(ModelObj, ValuesArray, "Subdomains")

def GetElmntVtkType(Dimension, NodesNbr):
    ElmntType = GetElmntType(Dimension, NodesNbr)
    return fe_cata.VtkCellType[ElmntType]