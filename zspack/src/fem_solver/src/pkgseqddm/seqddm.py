"""
This module defines an iterative and direct sequential implementations of the 
primal and dual domain decomposition methods.

"""

import numpy as np
from scipy.sparse import csr_matrix, bsr_matrix
from scipy.linalg import solve, lstsq, pinv
from scipy.sparse.linalg import spsolve, lsqr, aslinearoperator, cg, gmres


from ..model import DofNumbering
from ..pkgutils import GetSubMatrix, SplitVector, SplitMatrix


class seqddm:
    """
    This class defines sequential primal and dual domain decomposition 
    methods.

    Parameters
    ----------
    type : 'str'
        The method's type which can be either "Primal" or "Dual".
    
    blocks_nbr : 'int', optional
        Subdomains matrices are block-structured for some type of problems 
        (e.g. the mCRE localisation step). In this case blocks_nbr is 
        mandatory and it represents the number of blocks per row/column.

        Nb : Blocks are FE matrices, therefore numberings objects are not 
        defined on subdomains matrices but on blocks. blocks_nbr is then 
        used to create numberings of subdomain matrices out of blocks 
        numberings.
    
    intrfc_nodes_nbr : 'int'
        The number of nodes of the global interface. If the dual method is
        used (type == "Dual") and since the dual description of the interface 
        is redundant, a multiple node must be counted once for each pair of 
        subdomains to which it belongs (e.g. a node shared by 4 subdomains is
        counted 6 times).
    
    kwargs : 'dict'
        The list of subdomains and their corresponding objects. The subdomain
        number i (counting starts at 0) is declared as follows :

                      SubDomain_i = (
                                        sd_mat,
                                        sd_vec,
                                        sd_loc_intrfc_nodes,
                                        sd_glb_intrfc_nodes,
                                        sd_numbering
                                    )
            
            - sd_mat : 'np.ndarray, bsr_matrix, csr_matrix'
                  The subdomain matrix. It can be either a dense matrix 
                  (np.ndarray) or a sparse one (bsr_matrix, csr_matrix).

            - sd_vec : 'np.ndarray'
                  The righthand side vector of the subdomain's local problem. 
                  It must be one-dimensional and its shape must matches the 
                  shape of the subdomain matrix.

            - sd_loc_intrfc_nodes : 'np.ndarray'
                  The local numbering of subdomain nodes that belongs to the
                  interface (redundant if Type == "Dual").

            - sd_glb_intrfc_nodes : 'np.ndarray'
                  The numbering of subdomain nodes that belong to the 
                  interface whithin the global interface nodes.

            - sd_numbering : 'DofNumbering'
                  The numbering object of the subdomain.

    Attributes
    ----------
    sd_nbr : 'int'
        The number of subdomains.

    type : 'str'
        The method's type ("Primal" or "Dual").

    dtype : 'data-type'
        The data type of the problem (float or complex).
    
    blocks_nbr : 'int'
        The blocks number per row/column of subdomains matrices. Equal to 1 
        if the matrices aren't block-structured.
    
    intrfc_nodes_nbr : 'int'
        The number of nodes of the global interface. Multiple nodes are 
        counted more than once if type == "Dual" (see intrfc_nodes_nbr in
        Parameters section above for more details).

    intrfc_dofs_nbr : 'int'
        The number of degrees of freedom of the interface problem.

    loc_matrices : 'list'
        A list that contains the matrices of the subdomains. The matrix of
        a subdomain i is the i-th item of the list.

    loc_vectors : 'list'
        A list that contains the rhs vectors of the subdomains. The vector of
        a subdomain i is the i-th item of the list.

    numberings : 'list'
        A list that contains the numbering objects of the subdomains. The 
        numbering of a subdomain i is the i-th item of the list.

    intrfc_mapping : 'list'
        A list that contains the local/global mapping of subdomains
        interfaces. The mapping of a subdomain i is the i-th item of the 
        list and it is stored as a tuple of 2 or 3 arrays depending
        on the method's type (primal or dual).

        type == "Primal" :

                       (loc_intrfc_dofs, glb_intrfc_nodes)

            - loc_intrfc_dofs : an array that contains the degrees of 
              freedom of the subdomain that belongs to the interface.

            - glb_intrfc_nodes : an array that contains the degrees 
              of freedom of the global interface that correspond to 
              those of the subdomain interface. Its size is equal to 
              that of loc_intrfc_dofs.

        type == "Dual" :

               (loc_intrfc_dofs, dual_primal_map, glb_intrfc_nodes)

            - loc_intrfc_dofs : an array that contains the degrees of 
              freedom of the subdomain that belongs to the primal 
              interface. It means that Dofs of cross nodes (multiple
              nodes) aren't redundant within this array. This array 
              is only used to extract degrees of freedom of the 
              geometric interface of the subdomain, their projection 
              on the dual interface is performed using the array 
              dual_primal_map presented below.

            - dual_primal_map : an array that contains a mapping between 
              dual degrees of freedom and their corresponding primal 
              degrees of freedom. The i-th dual Dof of the subdomain 
              correspond to the primal Dof number dual_primal_map[i] of
              the subdomain.

            - glb_intrfc_nodes : an array that contains the degrees 
              of freedom of the global dual interface that correspond 
              to those of the subdomain dual interface. Its size is 
              equal to that of dual_primal_map

    multiplicity : 'list'
        A list that contains the geometric multiplicity of degrees of 
        freedom of the subdomains. The multiplicity of a subdomain i 
        is the i-th item of the list and it's stored as an array.

    dual_dofs_signs : 'list'
        A list that contains the signs of dual degrees of freedom of
        the subdomains (the dual assembly operator is signed, that's 
        why this parameter is needed). The signs of a subdomain i are
        the i-th item of the list and they are stored as an array.
    
    intrfc_pb_res : 'np.ndarray'
        The solution vector of the interface problem. Available only after
        a call to the method SolveSeqDDM(...), otherwise it's None.
    
    """
 
    def __init__(self, **kwargs):
        if "type" not in kwargs:
            raise ValueError('The "type=***" argument is \
                        required to create a new seqddm object')
        elif kwargs["type"] not in ["Dual", "Primal"]:
            raise ValueError('the keyword "type" value must \
                                    be either "Primal or "Dual"')
        else:
            self.type = kwargs["type"]
            del kwargs["type"]
        
        if "blocks_nbr" not in kwargs:
            self.blocks_nbr = 1
        else:
            self.blocks_nbr = int(kwargs["blocks_nbr"])
            del kwargs["blocks_nbr"]

        if "intrfc_nodes_nbr" not in kwargs:
            raise ValueError('The "intrfc_nodes_nbr=***" argument is \
                            required to create a new seqddm object')
        else:
            self.intrfc_nodes_nbr = int(kwargs["intrfc_nodes_nbr"])
            del kwargs["intrfc_nodes_nbr"]
        
        self.intrfc_dofs_nbr = None
        self.loc_matrices    = None
        self.loc_vectors     = None
        self.intrfc_mapping  = None
        self.numberings      = None
        self.multiplicity    = None
        self.intrfc_pb_res   = None
        self.dtype = None
        self.sd_nbr = len(kwargs)

        if self.type == "Dual":
            self.dual_dofs_signs = []

        self.__InitArgs__(**kwargs)

        self.__CreateIntrfcNmbrg__()

    def __InitArgs__(self, **kwargs):
        """Initializes some arguments."""
        check_list = [False for _ in range(len(kwargs))]
        
        self.loc_matrices   = [[] for _ in range(len(kwargs))]
        self.loc_vectors    = [[] for _ in range(len(kwargs))]
        self.intrfc_mapping = [[] for _ in range(len(kwargs))]
        self.numberings     = [[] for _ in range(len(kwargs))]

        for sd_name, sd_objcts in kwargs.items():
            assert(sd_name.startswith("SubDomain_"))
            try:
                sd_idx = int(sd_name[10:])
            except ValueError:
                raise ValueError("Invalid keyword argument : \
                    {}".format(sd_name))
            else:
                check_list[sd_idx] = not check_list[sd_idx]
            assert(len(sd_objcts) == 5)
            self.__AddNCheckSdArgs__(sd_idx, *sd_objcts)
        
        if not all(check_list):
            raise ValueError("There is a repeated subdomain name !")
        
        self.loc_matrices   = [objct[0] for objct in self.loc_matrices]
        self.loc_vectors    = [objct[0] for objct in self.loc_vectors]
        self.intrfc_mapping = [objct[0] for objct in self.intrfc_mapping]
        self.numberings     = [objct[0] for objct in self.numberings]

        self.dtype = self.loc_matrices[0].dtype
        for sd_mat in self.loc_matrices[1:]:
            assert(sd_mat.dtype == self.dtype)
        
        self.InitMultiplicity()

    def __AddNCheckSdArgs__(self, sd_idx, loc_mat, loc_vect, 
                            loc_intrfc_nodes, glbl_intrfc_nodes, numbering):
        """Checks and stores subdomains."""
        assert(isinstance(numbering, DofNumbering))
        assert(isinstance(loc_mat, (np.ndarray, bsr_matrix, csr_matrix)))
        assert(isinstance(loc_vect, np.ndarray))
        assert(isinstance(loc_intrfc_nodes, np.ndarray))
        assert(isinstance(glbl_intrfc_nodes, np.ndarray))
        assert(loc_vect.ndim == 1 and loc_intrfc_nodes.ndim == 1 \
                                        and glbl_intrfc_nodes.ndim == 1)

        local_free_dofs_nbr  = numbering.GetFreeDofNbr() * self.blocks_nbr
        local_nodes_nbr = numbering.GetNodesNbr()

        assert(np.all(loc_intrfc_nodes  < local_nodes_nbr))
        assert(np.all(glbl_intrfc_nodes < self.intrfc_nodes_nbr))

        assert(loc_mat.shape == (local_free_dofs_nbr, local_free_dofs_nbr))
        assert(loc_vect.size == local_free_dofs_nbr)
        assert(loc_intrfc_nodes.size == glbl_intrfc_nodes.size)

        self.loc_matrices[sd_idx].append(loc_mat)
        self.loc_vectors[sd_idx].append(loc_vect)
        self.intrfc_mapping[sd_idx].append((loc_intrfc_nodes, glbl_intrfc_nodes))
        self.numberings[sd_idx].append(numbering)

    def GetMType(self):
        """Returns the method's type, either "Primal or "Dual"."""
        return self.type

    def GetSdNbr(self):
        """Returns the number of subdomains."""
        return self.sd_nbr

    def GetSdLocMat(self, sd_idx):
        """Returns the matrix of subdomain sd_idx."""
        assert(self.IsValidSdIdx(sd_idx))
        assert(self.loc_matrices is not None)
        return self.loc_matrices[sd_idx]

    def GetSdLocVec(self, sd_idx):
        """Returns the rhs of subdomain sd_idx."""
        assert(self.IsValidSdIdx(sd_idx))
        assert(self.loc_vectors is not None)
        return self.loc_vectors[sd_idx]
    
    def GetPbDataType(self):
        """Returns the data type of the problem (float or complex)."""
        return self.dtype

    def GetSdIntrfcMapping(self, sd_idx):
        """Returns the interface mapping of subdomain sd_idx."""
        assert(self.IsValidSdIdx(sd_idx))
        assert(self.intrfc_mapping is not None)
        return self.intrfc_mapping[sd_idx]

    def GetSdNumbering(self, sd_idx):
        """Returns the numbering object of subdomain sd_idx."""
        assert(self.IsValidSdIdx(sd_idx))
        assert(self.numberings is not None)
        return self.numberings[sd_idx]
    
    def GetLocPbSize(self, sd_idx):
        """Returns the size of the local problem of subdomain sd_idx."""
        assert(self.IsValidSdIdx(sd_idx))
        sd_freeDofsNbr = self.GetSdNumbering(sd_idx).GetFreeDofNbr()
        return sd_freeDofsNbr * self.blocks_nbr
    
    def GetIntrfcPbSize(self):
        """Returns the interface problem size."""
        return self.intrfc_dofs_nbr

    def GetSdMultiplicity(self, sd_idx):
        """
        Returns the geometric multiplicity of the interface 
        Dofs of subdomain sd_idx.
        
        """
        assert(self.IsValidSdIdx(sd_idx))
        assert(self.multiplicity is not None)
        return self.multiplicity[sd_idx]
    
    def GetSdLocDofsNmbrg(self, sd_idx, dofs_type=None):
        """
        Returns the local numbering of internal/interface degrees of 
        freedom of subdomain sd_idx. The choice of Dofs type is made
        using the parameter dofs_type which must be either "internal"
        or "interface".
        
        """
        assert(self.IsValidSdIdx(sd_idx))
        assert(isinstance(dofs_type, str) \
                                and dofs_type in ["internal", "interface"])
        sd_locDofsNmbrg = self.GetSdIntrfcMapping(sd_idx)[0]
        if dofs_type == "interface":
            return sd_locDofsNmbrg
        else:
            vec_size = self.GetLocPbSize(sd_idx)
            temp_vec = np.full(vec_size, True)
            temp_vec[sd_locDofsNmbrg] = False
            return np.nonzero(temp_vec)[0]

    def GetSdGlbDofsNmbrg(self, sd_idx):
        """
        Returns the numbering of interface degrees of freedom of the 
        subdomain sd_idx within the global interface numbering.
        
        """
        assert(self.IsValidSdIdx(sd_idx))
        idx = 1 if self.type == "Primal" else 2
        return self.GetSdIntrfcMapping(sd_idx)[idx]
    
    def GetPrimalDualMap(self, sd_idx):
        """
        Return the mapping from dual interface Dofs (with redundancies) 
        to primal interface Dofs (without redundancies) of the subdomain 
        sd_idx.
        
        """
        assert(self.type == "Dual")
        return self.GetSdIntrfcMapping(sd_idx)[1]

    
    def GetDualDofsSigns(self, sd_idx):
        """Returns the signs of dual interface Dofs."""
        assert(self.IsValidSdIdx(sd_idx))
        assert(self.type == "Dual")
        return self.dual_dofs_signs[sd_idx]

    
    def IsValidSdIdx(self, sd_idx):
        """Checks whether sd_idx is a valid subdomain number."""
        return isinstance(sd_idx, int) and sd_idx < self.sd_nbr
    
    def InitMultiplicity(self):
        """
        Computes and initializes the multiplicity of interface nodes for 
        all subdomains.

        Nb : Please note that the multiplicity account only for nodes, but 
        it's updated to account for Dofs once the numbering of interface 
        Dofs is created and the number of Dofs of each interface node is 
        known.
        
        """
        self.multiplicity = []
        func_call = "self.CM_{}()".format(self.GetMType())
        return eval(func_call)

    def CM_Primal(self):
        """
        Computes the multiplicity of primal degrees of freedom for all 
        subdomains. It mustn't be used, it is only used in the function
        InitMultiplicity() under strict assumptions.
        
        """
        assert(self.type == "Primal")
        temp_vect = np.zeros(self.intrfc_nodes_nbr, dtype=int)
        glbl_idxs = [self.GetSdIntrfcMapping(sd_idx)[1] \
                                            for sd_idx in range(self.sd_nbr)]

        for sd_idx in range(self.sd_nbr):
            temp_vect[glbl_idxs[sd_idx]] += 1
        
        for sd_idx in range(self.sd_nbr):
            sd_multiplicity = temp_vect[glbl_idxs[sd_idx]]
            self.multiplicity.append(sd_multiplicity)

    def CM_Dual(self):
        """
        Computes the multiplicity of dual degrees of freedom for all 
        subdomains. It mustn't be used, it is only used in the function
        InitMultiplicity() under strict assumptions.
        
        """
        assert(self.type == "Dual")
        for sd_idx in range(self.sd_nbr):
            local_nodes_nbr = self.GetSdNumbering(sd_idx).GetNodesNbr()
            temp_vect = np.zeros(local_nodes_nbr, dtype=int)
            loc_indices = self.GetSdIntrfcMapping(sd_idx)[0]
            np.add.at(temp_vect, loc_indices, 1)
            sd_multiplicity = temp_vect[loc_indices] + 1
            self.multiplicity.append(sd_multiplicity)

    def __CreateIntrfcNmbrg__(self):
        """
        Creates the global interface numbering of degrees of freedom 
        out of the local/global mappings of interface nodes of subdomains.
        It computes also the number of Dofs of the global interface (the 
        size of the interface problem) and updates the multiplicity that 
        account only for interface nodes before.
        
        """
        temp_vec = np.zeros(self.intrfc_nodes_nbr + 1, dtype=int)
        local_nodes_dofs_nbr  = []
        local_dofs_numbering  = []
        global_dofs_numbering = []

        for sd_idx in range(self.sd_nbr):
            loc_indices, glbl_indices = self.GetSdIntrfcMapping(sd_idx)
            sd_numbering = self.GetSdNumbering(sd_idx)

            sd_intrfcDofsNmbrg = sd_numbering.GetNodesNumbring(loc_indices)
            local_dofs_numbering.append(sd_intrfcDofsNmbrg)
            
            sd_freeDofsNbr = sd_numbering.GetNodesFreeDofNbr(loc_indices)
            local_nodes_dofs_nbr.append(sd_freeDofsNbr)

            temp_vec[glbl_indices + 1] = sd_freeDofsNbr

        temp_intrfc_dofs_nbr = np.sum(temp_vec)
        temp_vec = np.cumsum(temp_vec, out=temp_vec)

        for sd_idx in range(self.sd_nbr):
            _, glbl_indices = self.GetSdIntrfcMapping(sd_idx)
            arrays_list = ()
            for node_idx in glbl_indices:
                start_idx, stop_idx = temp_vec[node_idx : node_idx + 2]
                arrays_list += np.arange(start_idx, stop_idx, dtype=int),
            global_dofs_numbering.append(np.concatenate(arrays_list))

        temp_multiplicity = [np.repeat(multiplicity, freeDofsNbr) \
                            for multiplicity, freeDofsNbr in \
                            zip(self.multiplicity, local_nodes_dofs_nbr)]
        
        if self.blocks_nbr > 1:
            self.intrfc_dofs_nbr = self.blocks_nbr * temp_intrfc_dofs_nbr
            
            self.multiplicity = [np.tile(sd_mltplcty, self.blocks_nbr) \
                                        for sd_mltplcty in temp_multiplicity]

            glb_stride_vec = np.arange(self.blocks_nbr, dtype=int) \
                                                       * temp_intrfc_dofs_nbr
            
            self.intrfc_mapping = []
            for sd_idx in range(self.sd_nbr):
                sd_freeDofsNbr = self.GetSdNumbering(sd_idx).GetFreeDofNbr()
                loc_stride_vec = np.arange(self.blocks_nbr, dtype=int) \
                                                             * sd_freeDofsNbr

                temp_loc_indices = local_dofs_numbering[sd_idx]
                sd_loc_indices   = (loc_stride_vec[:, None] \
                                                + temp_loc_indices).flatten()

                temp_glb_indices = global_dofs_numbering[sd_idx]
                sd_glb_indices   = (glb_stride_vec[:, None] \
                                                + temp_glb_indices).flatten()

                if self.type == "Dual":
                    sd_loc_indices = np.unique(sd_loc_indices, \
                                                         return_inverse=True)

                    self.intrfc_mapping.append((*sd_loc_indices, \
                                                             sd_glb_indices))
                else:
                    self.intrfc_mapping.append((sd_loc_indices, \
                                                             sd_glb_indices))

        else:
            self.intrfc_dofs_nbr = temp_intrfc_dofs_nbr
            self.multiplicity   = temp_multiplicity
            self.intrfc_mapping = []

            for sd_idx in range(self.sd_nbr):
                sd_loc_indices = local_dofs_numbering[sd_idx]
                sd_glb_indices = global_dofs_numbering[sd_idx]
                
                if self.type == "Dual":
                    sd_loc_indices = np.unique(sd_loc_indices, \
                                                         return_inverse=True)
                
                    self.intrfc_mapping.append((*sd_loc_indices, \
                                                             sd_glb_indices))
                else:
                    self.intrfc_mapping.append((sd_loc_indices, \
                                                             sd_glb_indices))
        
        if self.type == "Dual":
            self.__CreateDualDofsSigns__()

    def __CreateDualDofsSigns__(self):
        """Creates the signs of dual degrees of freedom of the interface."""
        intrfc_pb_size = self.GetIntrfcPbSize()
        rand_vec = np.random.choice([-1,1], intrfc_pb_size)
        for sd_idx in range(self.sd_nbr):
            glb_dof_nmbrng = self.GetSdGlbDofsNmbrg(sd_idx)
            sd_dofs_signs = rand_vec[glb_dof_nmbrng]
            self.dual_dofs_signs.append(sd_dofs_signs)
            rand_vec[glb_dof_nmbrng] *= -1

    def GetSubSdMatrix(self, sd_idx, rows_type=None, cols_type=None):
        """
        Returns a sub-block of the subdomain sd_idx matrix. A subdomain 
        degrees of freedom can be splitted into interface ones (noted b)
        and internal ones (noted i). The subdomain matrix can then be 
        viewed as a 2x2 block matrix :

                                | Mat_ii    Mat_ib |
                          Mat = |                  |
                                | Mat_bi    Mat_bb |
        
        The sub-block to be returned is chosen using the parameters 
        rows_type and cols_type which allows selecting respectively the
        rows block and columns block. These two parameters can take only
        two values, "internal" and 'interface".

        """
        assert(self.IsValidSdIdx(sd_idx))
        assert(isinstance(rows_type, str) \
                                and rows_type in ["internal", "interface"])
        assert(isinstance(cols_type, str) \
                                and cols_type in ["internal", "interface"])

        rows_indices = self.GetSdLocDofsNmbrg(sd_idx, dofs_type=rows_type)
        cols_indices = self.GetSdLocDofsNmbrg(sd_idx, dofs_type=cols_type)

        sd_matrix = self.GetSdLocMat(sd_idx)

        return GetSubMatrix(sd_matrix, RowsIdx=rows_indices, 
                                                        ColsIdx=cols_indices)

    def GetSubSdVector(self, sd_idx, dofs_type=None):
        """
        Returns a sub-block of the subdomain sd_idx vector. A subdomain 
        degrees of freedom can be splitted into interface ones (noted b)
        and internal ones (noted i). The subdomain vector can then be 
        viewed as a 2 blocks vector :

                                    | Vec_i |
                              Vec = |       |
                                    | Vec_b |

        The sub-block to be returned is chosen using the parameter dofs_type 
        which can take only two values, "internal" and 'interface".

        """
        assert(self.IsValidSdIdx(sd_idx))
        assert(isinstance(dofs_type, str) \
                                and dofs_type in ["internal", "interface"])
        
        dofs_indices = self.GetSdLocDofsNmbrg(sd_idx, dofs_type=dofs_type)
        sd_vector = self.GetSdLocVec(sd_idx)

        return sd_vector[dofs_indices]

    def SplitSdVector(self, sd_idx):
        """
        Split the subdomain sd_idx vector into two blocks (interface Dofs 
        block and internal Dofs block) and returns them as a tuple of two 
        arrays. A subdomain degrees of freedom can be splitted into interface 
        ones (noted b) and internal ones (noted i). The subdomain vector can 
        then be viewed as a 2 blocks vector :

                                    | Vec_i |
                              Vec = |       |
                                    | Vec_b |
        
        The returned tuple is :

                                (Vec_i, Vec_b)

        """
        assert(self.IsValidSdIdx(sd_idx))

        internal_dofs  = self.GetSdLocDofsNmbrg(sd_idx, dofs_type="internal")
        interface_dofs = self.GetSdLocDofsNmbrg(sd_idx, dofs_type="interface")

        sd_vector = self.GetSdLocVec(sd_idx)

        return SplitVector(sd_vector, internal_dofs, interface_dofs)

    def SplitSdMatrix(self, sd_idx):
        """
        Split the subdomain sd_idx Matrix into four blocks and returns them 
        as a 2x2 list. A subdomain degrees of freedom can be splitted into 
        interface ones (noted b) and internal ones (noted i). The subdomain 
        matrix can then be viewed as a 2x2 block matrix :

                                | Mat_ii    Mat_ib |
                          Mat = |                  |
                                | Mat_bi    Mat_bb |

        The returned list is :

                                  [[Mat_ii, Mat_ib],
                                   [Mat_bi, Mat_bb]]
        
        """
        assert(self.IsValidSdIdx(sd_idx))

        internal_dofs  = self.GetSdLocDofsNmbrg(sd_idx, dofs_type="internal")
        interface_dofs = self.GetSdLocDofsNmbrg(sd_idx, dofs_type="interface")

        sd_matrix = self.GetSdLocMat(sd_idx)

        return SplitMatrix(sd_matrix, internal_dofs, interface_dofs)

    def GetGlbSchurCmplmnt(self, as_linop=False):
        """
        Returns the global schur complement of the interface problem. The
        parameter as_linop is used to choose whether the global schur 
        complement should be explicitly computed or returned only as a 
        linear operator.

            as_linop == False : The global schur complement is returned as
            a dense matrix which means that a direct method will be used to 
            solve the interface problem.

            as_linop == True : The global schur complement is returned as a 
            linear operator which means that an iterative method will be used
            to solve the interface problem.
        
        """
        assert(isinstance(as_linop, bool))
        func_call = "self.__GGLBSC_{}__(as_linop)".format(self.GetMType())
        return eval(func_call)

    def __GGLBSC_Primal__(self, as_linop):
        """
        Returns the primal global schur complement. It mustn't be used, 
        it is only used in the function GetGlbSchurCmplmnt() under strict 
        assumptions.

        Nb : the parameter as_linop is explained in GetGlbSchurCmplmnt().
        
        """
        return self.__GGLBSCP_LO__() if as_linop \
                                             else self.__GGLBSCP_NLO__()

    def __GGLBSCP_LO__(self):
        """
        Returns the primal global schur complement as a linear operator. It 
        mustn't be used, it is only used in the function __GGLBSC_Primal__() 
        under strict assumptions.
        
        """
        return aslinearoperator(GLB_SC_LinOp(self))

    def __GGLBSCP_NLO__(self):
        """
        Returns the primal global schur complement as a dense matrix. It 
        mustn't be used, it is only used in the function __GGLBSC_Primal__() 
        under strict assumptions.
        
        """
        pb_size  = self.GetIntrfcPbSize()
        pb_dtype = self.GetPbDataType()
        result = np.zeros((pb_size, pb_size), pb_dtype)
        for sd_idx in range(self.sd_nbr):
            sd_psc = self.GetSdSchurCmplmnt(sd_idx, mtype="Primal")
            glb_dofs = self.GetSdGlbDofsNmbrg(sd_idx)
            result[glb_dofs[:, None], glb_dofs] += sd_psc
        return result

    def __GGLBSC_Dual__(self, as_linop):
        """
        Returns the dual global schur complement. It mustn't be used, 
        it is only used in the function GetGlbSchurCmplmnt() under strict 
        assumptions.

        Nb : the parameter as_linop is explained in GetGlbSchurCmplmnt().
        
        """
        return self.__GGLBSCD_LO__() if as_linop \
                                             else self.__GGLBSCD_NLO__()

    def __GGLBSCD_LO__(self):
        """
        Returns the dual global schur complement as a linear operator. It 
        mustn't be used, it is only used in the function __GGLBSC_Dual__() 
        under strict assumptions.
        
        """
        return aslinearoperator(GLB_SC_LinOp(self))

    def __GGLBSCD_NLO__(self):
        """
        Returns the dual global schur complement as a dense matrix. It 
        mustn't be used, it is only used in the function __GGLBSC_Dual__() 
        under strict assumptions.
        
        """
        pb_size  = self.GetIntrfcPbSize()
        pb_dtype = self.GetPbDataType()
        result = np.zeros((pb_size, pb_size), pb_dtype)
        for sd_idx in range(self.sd_nbr):
            sd_dsc = self.GetSdSchurCmplmnt(sd_idx, mtype="Dual")
            glb_dofs = self.GetSdGlbDofsNmbrg(sd_idx)
            primal_dual_map = self.GetPrimalDualMap(sd_idx)
            dofs_signs = self.GetDualDofsSigns(sd_idx)
            result[glb_dofs[:, None], glb_dofs] += dofs_signs[:, None] * \
               sd_dsc[primal_dual_map[:, None], primal_dual_map] * dofs_signs
        return result

    def GetSdSchurCmplmnt(self, sd_idx, mtype=None, as_linop=False):
        """
        Returns the schur complement of the subdomain sd_idx. The schur
        complement type is selected using the parameter mtype which can
        be either "Primal" or "Dual". The parameter as_linop is used to 
        choose whether the schur complement should be explicitly computed 
        or returned only as a linear operator.

            as_linop == False : The sudomain schur complement is returned 
            as a dense matrix.

            as_linop == True : The subdomain schur complement is returned 
            as a linear operator.
        
        """
        assert(self.IsValidSdIdx(sd_idx))
        assert(mtype in ["Primal", "Dual"])
        assert(isinstance(as_linop, bool))
        func_call = "self.__GSdSC_{}__(sd_idx, as_linop)".format(mtype)
        return eval(func_call)
    
    def __GSdSC_Primal__(self, sd_idx, as_linop):
        """
        Returns the primal schur complement of the subdomain sd_idx. It 
        mustn't be used, it is only used in the function GetSdSchurCmplmnt() 
        under strict assumptions.

        Nb : the parameter as_linop is explained in GetSdSchurCmplmnt().
        
        """
        return self.__GSdSCP_LO__(sd_idx) if as_linop \
                                             else self.__GSdSCP_NLO__(sd_idx)

    def __GSdSCP_LO__(self, sd_idx):
        """
        Returns the primal schur complement of the subdomain sd_idx as a 
        linear operator. It mustn't be used, it is only used in the function 
        __GSdSC_Primal__() under strict assumptions.
        
        """
        i_rows_blk, b_rows_blk = self.SplitSdMatrix(sd_idx)
        sd_mat_ii, sd_mat_ib = i_rows_blk
        sd_mat_bi, sd_mat_bb = b_rows_blk

        PSC_Object = PSDSC_LinOp(sd_mat_ii, sd_mat_ib, sd_mat_bi, sd_mat_bb)

        return aslinearoperator(PSC_Object)

    def __GSdSCP_NLO__(self, sd_idx):
        """
        Returns the primal schur complement of the subdomain sd_idx as a 
        dense matrix. It mustn't be used, it is only used in the function 
        __GSdSC_Primal__() under strict assumptions.
        
        """
        i_rows_blk, b_rows_blk = self.SplitSdMatrix(sd_idx)
        sd_matrix = self.GetSdLocMat(sd_idx)

        sd_mat_ii, sd_mat_ib = i_rows_blk
        sd_mat_bi, sd_mat_bb = b_rows_blk

        if not isinstance(sd_matrix, np.ndarray):
            sd_mat_ii = sd_mat_ii.toarray()
            sd_mat_ib = sd_mat_ib.toarray()
            sd_mat_bi = sd_mat_bi.toarray()
            sd_mat_bb = sd_mat_bb.toarray()

        temp_mat = solve(sd_mat_ii, sd_mat_ib, \
                        overwrite_a=True, overwrite_b=True)

        return sd_mat_bb - np.matmul(sd_mat_bi, temp_mat)

    def __GSdSC_Dual__(self, sd_idx, as_linop):
        """
        Returns the dual schur complement of the subdomain sd_idx. It 
        mustn't be used, it is only used in the function GetSdSchurCmplmnt() 
        under strict assumptions.

        Nb : the parameter as_linop is explained in GetSdSchurCmplmnt().
        
        """
        return self.__GSdSCD_LO__(sd_idx) if as_linop \
                                             else self.__GSdSCD_NLO__(sd_idx)

    def __GSdSCD_LO__(self, sd_idx):
        """
        Returns the dual schur complement of the subdomain sd_idx as a 
        linear operator. It mustn't be used, it is only used in the function 
        __GSdSC_Dual__() under strict assumptions.
        
        """
        dofs_indices = self.GetSdLocDofsNmbrg(sd_idx, dofs_type="interface")
        sd_matrix    = self.GetSdLocMat(sd_idx)
        DSC_Object   = DSDSC_LinOp(sd_matrix, dofs_indices)

        return aslinearoperator(DSC_Object)

    def __GSdSCD_NLO__(self, sd_idx):
        """
        Returns the dual schur complement of the subdomain sd_idx as a 
        dense matrix. It mustn't be used, it is only used in the function 
        __GSdSC_Dual__() under strict assumptions.
        
        """
        dofs_indices = self.GetSdLocDofsNmbrg(sd_idx, dofs_type="interface")
        sd_matrix = self.GetSdLocMat(sd_idx)

        if not isinstance(sd_matrix, np.ndarray):
            sd_matrix = sd_matrix.toarray()

        result = GetSubMatrix(pinv(sd_matrix), RowsIdx=dofs_indices)

        return result

    def GetGlbRHS(self):
        """Returns the global right hand side of the interface problem."""
        func_call = "self.__GGRHS_{}__()".format(self.GetMType())
        return eval(func_call)
    
    def __GGRHS_Primal__(self):
        """
        Returns the primal global right hand side of the interface 
        problem. It mustn't be used, it is only used in the function 
        GetGlbRHS() under strict assumptions.
        
        """
        typ = self.GetPbDataType()
        pb_size = self.GetIntrfcPbSize()
        result = np.zeros(pb_size, dtype=typ)
        for sd_idx in range(self.sd_nbr):
            sd_rhs = self.GetSdRightHandSide(sd_idx)
            glb_dofs = self.GetSdGlbDofsNmbrg(sd_idx)
            result[glb_dofs] += sd_rhs
        return result

    def __GGRHS_Dual__(self):
        """
        Returns the dual global right hand side of the interface 
        problem. It mustn't be used, it is only used in the function 
        GetGlbRHS(...) under strict assumptions.
        
        """
        typ = self.GetPbDataType()
        pb_size = self.GetIntrfcPbSize()
        result = np.zeros(pb_size, dtype=typ)
        for sd_idx in range(self.sd_nbr):
            sd_rhs = self.GetSdRightHandSide(sd_idx)
            glb_dofs = self.GetSdGlbDofsNmbrg(sd_idx)
            dofs_signs = self.GetDualDofsSigns(sd_idx)
            primal_dual_map = self.GetPrimalDualMap(sd_idx)
            result[glb_dofs] += sd_rhs[primal_dual_map] * dofs_signs
        return - result

    def GetSdRightHandSide(self, sd_idx):
        """Returns the right hand side of the subdomain sd_idx."""
        assert(self.IsValidSdIdx(sd_idx))
        func_call = "self.__GSdRHS_{}__(sd_idx)".format(self.GetMType())
        return eval(func_call)
    
    def __GSdRHS_Primal__(self, sd_idx):
        """
        Returns the primal right hand side of the subdomain sd_idx. 
        It mustn't be used, it is only used in the function 
        GetSdRightHandSide(...) under strict assumptions.
        
        """
        i_vec, b_vec = self.SplitSdVector(sd_idx)

        (sd_mat_ii, _), (sd_mat_bi, _) = self.SplitSdMatrix(sd_idx)

        if isinstance(sd_mat_ii, np.ndarray):
            temp_vec_1 = solve(sd_mat_ii, i_vec, sym_pos=True, \
                                                            overwrite_b=True)
        else:
            temp_vec_1 = spsolve(sd_mat_ii.tocsr(), i_vec)

        return b_vec - sd_mat_bi.dot(temp_vec_1)

    def __GSdRHS_Dual__(self, sd_idx):
        """
        Returns the dual right hand side of the subdomain sd_idx. 
        It mustn't be used, it is only used in the function 
        GetSdRightHandSide(...) under strict assumptions.
        
        """
        dofs_indices = self.GetSdLocDofsNmbrg(sd_idx, dofs_type="interface")
        sd_vector = self.GetSdLocVec(sd_idx)
        sd_matrix = self.GetSdLocMat(sd_idx)

        if isinstance(sd_matrix, np.ndarray):
            temp_vec = lstsq(sd_matrix, sd_vector, overwrite_b=True)[0]
        else:
            temp_vec = lsqr(sd_matrix, sd_vector)[0]

        return temp_vec[dofs_indices]
    
    def GetGlbPrecond(self):
        """
        Returns the preconditioner of the interface problem 
        as a linear operator.
        
        """
        return aslinearoperator(GLB_SC_LinOp(self, is_pcond=True))

    def check_iter_info(self, info):
        """
        Checks the convergence if an iterative method is used 
        to solve the interface problem.
        
        """
        assert(isinstance(info, (int, np.int)))
        if info == 0:
            print("The interface problem is successfully solved !")
        elif info > 0:
            # raise ValueError("Convergence to tolerance not achieved, \
            #                        {} iterations performed".format(info))
            print("Convergence to tolerance not achieved, " \
                       "{} iterations performed".format(info))
        else:
            raise ValueError("Illegal input or breakdown")
    
    def SolveSeqDDM(self, stype="Direct", mtype=None, x0=None, tol=1e-05, \
                                                    maxiter=None, atol=None):
        """
        Solves the interface problem.

        Parameters
        ----------
        stype : 'str'
            The resolution type of the interface problem. 
            It can be either "Direct" or "Iterative". the 
            parameters below are only available if an 
            iterative resolution is chosen.

        mtype : 'str'
            The iterative method to use. Two values are
            possible : "GMRES" (for Generalized minimal 
            residual method) and "CG" (for Conjugate 
            gradient).

        x0 : 'np.ndarray', optional
            Starting guess for the solution.

        tol, atol : 'float', optional
            Tolerances for convergence, 
            norm(residual) <= max(tol*norm(rhs), atol).

        maxiter : 'int'
            Maximum number of iterations. Iteration will 
            stop after maxiter steps even if the specified 
            tolerance has not been achieved.
        
        """
        if self.intrfc_pb_res is not None:
            raise ValueError("The interface problem is already solved !")

        is_direct = stype == "Direct" and mtype is None
        is_iterative = stype == "Iterative" and mtype in ["GMRES", "CG"]
        assert(is_direct or is_iterative)

        mat = self.GetGlbSchurCmplmnt(as_linop=is_iterative)
        rhs = self.GetGlbRHS()

        # print("The norme of the interface problem "\
        #                             "rhs : {}".format(np.linalg.norm(rhs)))

        if is_direct:
            # print("The condition number of the interface "\
            #             "problem operator : {}".format(np.linalg.cond(mat)))
            # return np.linalg.norm(rhs), np.linalg.cond(mat)
            result = solve(mat, rhs, overwrite_a=True, \
                                     overwrite_b=True, assume_a='sym')
        elif mtype == "CG":
            pcond = self.GetGlbPrecond()
            result, info = cg(mat, rhs, x0=x0, tol=tol, maxiter=maxiter, \
                                                        M=pcond, atol=atol)
            self.check_iter_info(info)
        else:
            pcond = self.GetGlbPrecond()
            result, info = gmres(mat, rhs, x0=x0, tol=tol, maxiter=maxiter, \
                                                        M=pcond, atol=atol)
            self.check_iter_info(info)

        self.intrfc_pb_res = result
    
    def GetIntrfcPbRes(self):
        """Returns the solution vector of the interface problem."""
        if self.intrfc_pb_res is None:
            raise ValueError("The interface problem is not solved yet !")
        return self.intrfc_pb_res

    def GetSdResult(self, sd_idx):
        """
        Returns the interface problem solution of the subdomain sd_idx
        as a tuple of two arrays. The first array contains the local
        numbering of the interface Dofs of the subdomain, and the 
        second their corresponding values extracted from the solution
        of the interface problem.

        Nb : The returned interface Dofs aren't redundant if the
        dual method is used. A projection from the dual interface to 
        primal interface is performed in this case.
        
        """
        assert(self.IsValidSdIdx(sd_idx))
        glb_dofs = self.GetSdGlbDofsNmbrg(sd_idx)
        sd_res   = self.GetIntrfcPbRes()[glb_dofs]
        dofs_indices = self.GetSdLocDofsNmbrg(sd_idx, dofs_type="interface")
        if self.GetMType() == "Dual":
            typ = self.GetPbDataType()
            temp_vec = np.zeros(dofs_indices.size, dtype=typ)
            primal_dual_map = self.GetPrimalDualMap(sd_idx)
            dofs_signs = self.GetDualDofsSigns(sd_idx)
            np.add.at(temp_vec, primal_dual_map, dofs_signs * sd_res)
            sd_res = temp_vec
        return dofs_indices, sd_res










class PSDSC_LinOp:
    
    def __init__(self, mat_ii, mat_ib, mat_bi, mat_bb):
        assert(isinstance(mat_ii, (np.ndarray, csr_matrix, bsr_matrix)))
        typ = type(mat_ii)
        for mat in [mat_ib, mat_bi, mat_bb]:
            assert(isinstance(mat, typ))
        
        ii_rows_nbr, ii_cols_nbr = mat_ii.shape
        ib_rows_nbr, ib_cols_nbr = mat_ib.shape
        bi_rows_nbr, bi_cols_nbr = mat_bi.shape
        bb_rows_nbr, bb_cols_nbr = mat_bb.shape

        assert(
            ii_rows_nbr == ii_cols_nbr and \
            bb_rows_nbr == bb_cols_nbr and \
            ii_rows_nbr == ib_rows_nbr and \
            bi_rows_nbr == bb_rows_nbr and \
            ii_cols_nbr == bi_cols_nbr and \
            ib_cols_nbr == bb_cols_nbr
        )

        assert(
            mat_ii.dtype == \
            mat_ib.dtype == \
            mat_bi.dtype == \
            mat_bb.dtype
        )

        self.shape = bb_rows_nbr, bb_cols_nbr
        self.dtype = mat_ii.dtype
        self.storage_type = "Dense" if typ == np.ndarray else "Sparse"

        self.data_ii = mat_ii
        self.data_ib = mat_ib
        self.data_bi = mat_bi
        self.data_bb = mat_bb
    
    def matvec(self, vec):
        """
        Performs the matrix-vector product with the primal 
        schur complement of a subdomain.
        
        """
        assert(isinstance(vec, np.ndarray))
        assert(vec.size == self.shape[1])

        temp_vec_1 = self.data_ib.dot(vec)
        temp_vec_2 = self.data_bb.dot(vec)

        if self.storage_type == "Dense":
            temp_vec_3 = solve(self.data_ii, temp_vec_1, sym_pos=True, \
                                                            overwrite_b=True)
        else:
            temp_vec_3 = spsolve(self.data_ii.tocsr(), temp_vec_1)

        temp_vec_4 = self.data_bi.dot(temp_vec_3)

        return temp_vec_2 - temp_vec_4

class DSDSC_LinOp:
    
    def __init__(self, mat, indices):
        assert(isinstance(mat, (np.ndarray, csr_matrix, bsr_matrix)))
        assert(isinstance(indices, np.ndarray) and indices.ndim == 1)
        mat_rows_nbr, mat_cols_nbr = mat.shape
        assert(mat_rows_nbr == mat_cols_nbr)
        assert(np.all(indices < mat_rows_nbr))

        self.dtype = mat.dtype

        border_size = indices.size
        self.shape = border_size, border_size

        self.extended_vec_size = mat_rows_nbr

        typ = type(mat)
        self.storage_type = "Dense" if typ == np.ndarray else "Sparse"

        self.border_indices = indices
        self.data = mat

    def matvec(self, vec):
        """
        Performs the matrix-vector product with the dual 
        schur complement of a subdomain.
        
        """
        assert(isinstance(vec, np.ndarray))
        assert(vec.size == self.shape[1])

        temp_vec = np.zeros(self.extended_vec_size, dtype=self.dtype)
        temp_vec[self.border_indices] = vec

        if self.storage_type == "Dense":
            temp_vec, residue = lstsq(self.data, temp_vec, overwrite_b=True)[:2]
            assert(np.isclose(residue, 0))
        else:
            temp_vec, istop = lsqr(self.data, temp_vec)[:2]
            # if istop == 1:
            #     print("x is an approximate solution to Ax = b !")
            # else:
            #     print("x approximately solves the least-squares problem !")

        return temp_vec[self.border_indices]

class GLB_SC_LinOp:

    def __init__(self, seq_ddm_objct, is_pcond=False):

        assert(isinstance(seq_ddm_objct, seqddm))
        assert(isinstance(is_pcond, bool))
        pb_size = seq_ddm_objct.GetIntrfcPbSize()
        self.shape = pb_size, pb_size
        self.is_pcond = is_pcond
        self.dtype = seq_ddm_objct.GetPbDataType()
        self.is_primal = seq_ddm_objct.GetMType() == "Primal"
        self.data = seq_ddm_objct

        if self.is_primal and not self.is_pcond:
            self.matvec = self.matvec_primal_oprtr
        elif self.is_primal and self.is_pcond:
            self.matvec = self.matvec_primal_pcond
        elif not self.is_pcond:
            self.matvec = self.matvec_dual_oprtr
        else:
            self.matvec = self.matvec_dual_pcond

    def matvec_primal_oprtr(self, vec):
        """
        Performs the matrix-vector product with the global 
        primal schur complement of the interface problem.
        
        """
        assert(isinstance(vec, np.ndarray))
        assert(vec.size == self.shape[1])
        
        typ = self.dtype
        ddm_objct = self.data
        sd_nbr = ddm_objct.GetSdNbr()
        result = np.zeros(vec.size, dtype=typ)

        for sd_idx in range(sd_nbr):
            sd_psc = ddm_objct.GetSdSchurCmplmnt(sd_idx, mtype="Primal", \
                                                            as_linop=True)
            glb_dofs = ddm_objct.GetSdGlbDofsNmbrg(sd_idx)
            result[glb_dofs] += sd_psc.matvec(vec[glb_dofs])
        
        return result

    def matvec_primal_pcond(self, vec):
        """
        Performs the matrix-vector product with the global 
        Neumann-Neumann preconditioner of the interface problem.
        
        """
        assert(isinstance(vec, np.ndarray))
        assert(vec.size == self.shape[1])
        
        typ = self.dtype
        ddm_objct = self.data
        sd_nbr = ddm_objct.GetSdNbr()
        result = np.zeros(vec.size, dtype=typ)

        for sd_idx in range(sd_nbr):
            sd_dsc = ddm_objct.GetSdSchurCmplmnt(sd_idx, mtype="Dual", \
                                                            as_linop=True)
            glb_dofs = ddm_objct.GetSdGlbDofsNmbrg(sd_idx)
            sd_multiplicity = ddm_objct.GetSdMultiplicity(sd_idx)
            temp_vec = vec[glb_dofs] / sd_multiplicity
            result[glb_dofs] += sd_dsc.matvec(temp_vec) / sd_multiplicity
        
        return result

    def matvec_dual_oprtr(self, vec):
        """
        Performs the matrix-vector product with the global 
        dual schur complement of the interface problem.
        
        """
        assert(isinstance(vec, np.ndarray))
        assert(vec.size == self.shape[1])
        
        typ = self.dtype
        ddm_objct = self.data
        sd_nbr = ddm_objct.GetSdNbr()
        result = np.zeros(vec.size, dtype=typ)

        for sd_idx in range(sd_nbr):
            sd_dsc = ddm_objct.GetSdSchurCmplmnt(sd_idx, mtype="Dual", \
                                                            as_linop=True)
            dofs_signs = ddm_objct.GetDualDofsSigns(sd_idx)
            glb_dofs   = ddm_objct.GetSdGlbDofsNmbrg(sd_idx)
            intrfc_dofs_nbr = sd_dsc.shape[0]
            primal_dual_map = ddm_objct.GetPrimalDualMap(sd_idx)
            temp_vec = np.zeros(intrfc_dofs_nbr, dtype=typ)
            np.add.at(temp_vec, primal_dual_map, vec[glb_dofs] * dofs_signs)
            result[glb_dofs] += sd_dsc.matvec(temp_vec)[primal_dual_map] \
                                                                * dofs_signs
        return result

    def matvec_dual_pcond(self, vec):
        """
        Performs the matrix-vector product with the global 
        Dirichlet preconditioner of the interface problem.
        
        """
        assert(isinstance(vec, np.ndarray))
        assert(vec.size == self.shape[1])
        
        typ = self.dtype
        ddm_objct = self.data
        sd_nbr = ddm_objct.GetSdNbr()
        result = np.zeros(vec.size, dtype=typ)

        for sd_idx in range(sd_nbr):
            sd_psc = ddm_objct.GetSdSchurCmplmnt(sd_idx, mtype="Primal", \
                                                            as_linop=True)
            sd_multiplicity = ddm_objct.GetSdMultiplicity(sd_idx)
            primal_dual_map = ddm_objct.GetPrimalDualMap(sd_idx)
            dofs_signs = ddm_objct.GetDualDofsSigns(sd_idx)
            glb_dofs = ddm_objct.GetSdGlbDofsNmbrg(sd_idx)
            intrfc_dofs_nbr = sd_psc.shape[0]
            temp_vec = np.zeros(intrfc_dofs_nbr, dtype=typ)
            np.add.at(temp_vec, primal_dual_map, \
                            vec[glb_dofs] * dofs_signs / sd_multiplicity )
            result[glb_dofs] += sd_psc.matvec(temp_vec)[primal_dual_map] * \
                                                dofs_signs / sd_multiplicity
        return result
































