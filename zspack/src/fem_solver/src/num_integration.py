"""This module implements numerical integration scheme (Gauss quadrature)."""

import numpy as np
from math import sqrt

# Le dictionnaire ci-dessous regroupe 
GaussianQuad = {}

# Triangle
GaussianQuad["Triangle.P0"] = np.array([[1/2, 1/3, 1/3]])

GaussianQuad["Triangle.P1"] = GaussianQuad["Triangle.P0"]

GaussianQuad["Triangle.P2.V1"] = np.array([[1/6, 1/6, 1/6],
                                           [1/6, 2/3, 1/6],
                                           [1/6, 1/6, 2/3]])

GaussianQuad["Triangle.P2.V2"] = np.array([[1/6, 1/2, 1/2],
                                           [1/6, 0.0, 1/2],
                                           [1/6, 1/2, 0.0]])

GaussianQuad["Triangle.P3"] = np.array([[-27/96, 1/3, 1/3],
                                        [ 25/96, 1/5, 1/5],
                                        [ 25/96, 3/5, 1/5],
                                        [ 25/96, 1/5, 3/5]])
a1  = 0.445948490915965
b1  = 0.091576213509771
P11 = 0.11169079483905
P21 = 0.0549758718227661

GaussianQuad["Triangle.P4"] = np.array([[P21, b1    , b1    ],
                                        [P21, 1-2*b1, b1    ],
                                        [P21, b1    , 1-2*b1],
                                        [P11, a1    , 1-2*a1],
                                        [P11, a1    , a1    ],
                                        [P11, 1-2*a1, a1    ]])

a2  = 0.470142064105115
b2  = 0.101286507323456
P12 = 0.066197076394253
P22 = 0.062969590272413

GaussianQuad["Triangle.P5"] = np.array([[9/80, 1/3   , 1/3   ],
                                        [ P12, a2    , a2    ],
                                        [ P12, 1-2*a2, a2    ],
                                        [ P12, a2    , 1-2*a2],
                                        [ P22, b2    , b2    ],
                                        [ P22, 1-2*b2, b2    ],
                                        [ P22, b2    , 1-2*b2]])

# Quadrangle
GaussianQuad["Quadrangle.P0"] = np.array([[4.0, 0.0, 0.0]])

GaussianQuad["Quadrangle.P1"] = GaussianQuad["Quadrangle.P0"]

GaussianQuad["Quadrangle.P2"] = np.array([[1.0, -1/sqrt(3), -1/sqrt(3)],
                                          [1.0,  1/sqrt(3), -1/sqrt(3)],
                                          [1.0,  1/sqrt(3),  1/sqrt(3)],
                                          [1.0, -1/sqrt(3),  1/sqrt(3)]])

GaussianQuad["Quadrangle.P3"] = GaussianQuad["Quadrangle.P2"]

GaussianQuad["Quadrangle.P4"] = np.array([[25/81, -sqrt(0.6), -sqrt(0.6)],
                                          [25/81,  sqrt(0.6), -sqrt(0.6)],
                                          [25/81,  sqrt(0.6),  sqrt(0.6)],
                                          [25/81, -sqrt(0.6),  sqrt(0.6)],
                                          [40/81,  0.0      , -sqrt(0.6)],
                                          [40/81,  sqrt(0.6),  0.0      ],
                                          [40/81,  0.0      ,  sqrt(0.6)],
                                          [40/81, -sqrt(0.6),  0.0      ],
                                          [64/81,  0.0      ,  0.0      ]])

GaussianQuad["Quadrangle.P5"] = GaussianQuad["Quadrangle.P4"]

# Tétraèdre
GaussianQuad["Tétraèdre.P0"] = np.array([[1/6, 1/4, 1/4, 1/4]])

GaussianQuad["Tétraèdre.P1"] = GaussianQuad["Tétraèdre.P0"]

a = (5 - sqrt(5)) / 20
b = (5 + 3 * sqrt(5)) / 20

GaussianQuad["Tétraèdre.P2"] = np.array([[1/24, a, a, a],
                                         [1/24, a, a, b],
                                         [1/24, a, b, a],
                                         [1/24, b, a, a]])

GaussianQuad["Tétraèdre.P3"] = np.array([[-2/15, 1/4, 1/4, 1/4],
                                         [ 3/40, 1/6, 1/6, 1/6],
                                         [ 3/40, 1/6, 1/6, 1/2],
                                         [ 3/40, 1/6, 1/2, 1/6],
                                         [ 3/40, 1/2, 1/6, 1/6]])

P1 = 8 / 405
P2 = (2665 - 14 * sqrt(15)) / 226800
P3 = (2665 + 14 * sqrt(15)) / 226800
P4 = 5 / 567

b11 = (7 + sqrt(15)) / 34
b22 = (7 - sqrt(15)) / 34
c1 = (13 - 3 * sqrt(15)) / 34
c2 = (13 + 3 * sqrt(15)) / 34
d  = (5 - sqrt(15)) / 20
e  = (5 + sqrt(15)) / 20

GaussianQuad["Tétraèdre.P5"] = np.array([[P1, 1/4, 1/4, 1/4],
                                         [P2, b11, b11, b11],
                                         [P2, b11, b11, c1 ],
                                         [P2, b11, c1 , b11],
                                         [P2, c1 , b11, b11],
                                         [P3, b22, b22, b22],
                                         [P3, b22, b22, c2 ],
                                         [P3, b22, c2 , b22],
                                         [P3, c2 , b22, b22],
                                         [P4, d  , d  , e  ],
                                         [P4, d  , e  , d  ],
                                         [P4, e  , d  , d  ],
                                         [P4, d  , e  , e  ],
                                         [P4, e  , d  , e  ],
                                         [P4, e  , e  , d  ]])


# Hexaèdre
GaussianQuad["Hexaèdre.P0"] = np.array([[8.0, 0.0, 0.0, 0.0]])

GaussianQuad["Hexaèdre.P1"] = GaussianQuad["Hexaèdre.P0"]

GaussianQuad["Hexaèdre.P2"] = np.array([[1.0, -1/sqrt(3), -1/sqrt(3), -1/sqrt(3)],
                                        [1.0, -1/sqrt(3), -1/sqrt(3),  1/sqrt(3)],
                                        [1.0, -1/sqrt(3),  1/sqrt(3), -1/sqrt(3)],
                                        [1.0, -1/sqrt(3),  1/sqrt(3),  1/sqrt(3)],
                                        [1.0,  1/sqrt(3), -1/sqrt(3), -1/sqrt(3)],
                                        [1.0,  1/sqrt(3), -1/sqrt(3),  1/sqrt(3)],
                                        [1.0,  1/sqrt(3),  1/sqrt(3), -1/sqrt(3)],
                                        [1.0,  1/sqrt(3),  1/sqrt(3),  1/sqrt(3)]])

GaussianQuad["Hexaèdre.P3"] = GaussianQuad["Hexaèdre.P2"]

GaussianQuad["Hexaèdre.P4"] = np.array([[125/729, -sqrt(0.6), -sqrt(0.6), -sqrt(0.6)],
                                        [200/729, -sqrt(0.6), -sqrt(0.6),  0.0      ],
                                        [125/729, -sqrt(0.6), -sqrt(0.6),  sqrt(0.6)],
                                        [200/729, -sqrt(0.6),  0.0      , -sqrt(0.6)],
                                        [320/729, -sqrt(0.6),  0.0      ,  0.0      ],
                                        [200/729, -sqrt(0.6),  0.0      ,  sqrt(0.6)],
                                        [125/729, -sqrt(0.6),  sqrt(0.6), -sqrt(0.6)],
                                        [200/729, -sqrt(0.6),  sqrt(0.6),  0.0      ],
                                        [125/729, -sqrt(0.6),  sqrt(0.6),  sqrt(0.6)],
                                        [200/729,  0.0      , -sqrt(0.6), -sqrt(0.6)],
                                        [320/729,  0.0      , -sqrt(0.6),  0.0      ],
                                        [200/729,  0.0      , -sqrt(0.6),  sqrt(0.6)],
                                        [320/729,  0.0      ,  0.0      , -sqrt(0.6)],
                                        [512/729,  0.0      ,  0.0      ,  0.0      ],
                                        [320/729,  0.0      ,  0.0      ,  sqrt(0.6)],
                                        [200/729,  0.0      ,  sqrt(0.6), -sqrt(0.6)],
                                        [320/729,  0.0      ,  sqrt(0.6),  0.0      ],
                                        [200/729,  0.0      ,  sqrt(0.6),  sqrt(0.6)],
                                        [125/729,  sqrt(0.6), -sqrt(0.6), -sqrt(0.6)],
                                        [200/729,  sqrt(0.6), -sqrt(0.6),  0.0      ],
                                        [125/729,  sqrt(0.6), -sqrt(0.6),  sqrt(0.6)],
                                        [200/729,  sqrt(0.6),  0.0      , -sqrt(0.6)],
                                        [320/729,  sqrt(0.6),  0.0      ,  0.0      ],
                                        [200/729,  sqrt(0.6),  0.0      ,  sqrt(0.6)],
                                        [125/729,  sqrt(0.6),  sqrt(0.6), -sqrt(0.6)],
                                        [200/729,  sqrt(0.6),  sqrt(0.6),  0.0      ],
                                        [125/729,  sqrt(0.6),  sqrt(0.6),  sqrt(0.6)]])

GaussianQuad["Hexaèdre.P5"] = GaussianQuad["Hexaèdre.P4"]

GaussianQuad["Hexaèdre.P6"] = GaussianQuad["Hexaèdre.P4"]

