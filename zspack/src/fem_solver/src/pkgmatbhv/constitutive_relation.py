"""This module implements the Material and behavior classes."""

import numpy as np
from functools import reduce
import timeit

from .. import mesh
from . import material_cata

class Material:
    """
    Material class.

    Parameters
    ----------
    kwargs : 'dict'
        A list of keyword arguments that defines a new material. The "type"
        keyword is always required, the remaining keywords depends on the 
        material's type :

            - If type = "isotrope", the required keyword arguments are :

                -> "poisson" : poisson ratio (a scalar).
                -> "young"   : young's modulus (a scalar).
                -> "gho"     : the mass density (a scalar).
                -> "alpha"   : un paramètre pour l'amortissement de Rayleigh.
                -> "beta"   : un paramètre pour l'amortissement de Rayleigh.

        Nb : only isotropic materials are supported for now.


    Attributes
    ----------
    type : 'str'
        The material type.
    
    ... :
        Other attributes that depends on the material type.

    """

    def __init__(self, **kwargs):
        
        if "type" not in kwargs:
            raise ValueError('The "type=***" argument '
                'is required to create a new material')
        elif kwargs["type"] not in material_cata.Material:
            raise ValueError('There is no material of '
                'type %s in the catalog' % kwargs["type"])
        else: self.type = kwargs["type"]
        
        RequiredArgs = reduce((lambda a, b: a + b), 
            material_cata.Material[self.type][0])
        
        for arg in RequiredArgs:
            if arg not in kwargs:
                raise ValueError('The argument %s is '
                    'necessary for the definition of '
                    'an %s material' % (arg, kwargs["type"]))
            else:
                assert(kwargs[arg] is not None)
                self.__setattr__(arg, kwargs[arg])

        if len(kwargs) != len(RequiredArgs) + 1:
            print("There are unnecessary arguments")

    def GetConstRelat(self, Modeling):
        """
        Returns a matrix that connects strain vector to stress vector.
        Nb : the strain (resp. stress) vector is a vector that contains 
        independent components of strain (resp. stress) tensor.
        
        """
        assert(isinstance(Modeling, str))
        ModelingName = self.type + "." + Modeling
        assert(ModelingName in material_cata.ConstRelat)
        if ModelingName == "isotrope.3D":
            lmbda, mu = GetLameCoefs(self)
            constRelat = material_cata.ConstRelat["isotrope.3D"]
            result = np.zeros(constRelat.shape)
            for idx in np.ndindex(result.shape):
                result[idx] = constRelat[idx](lmbda, mu)
            return result
        else: raise ValueError("The modeling "
            "%s is not yet implemented" % Modeling)

    def GetConstRelatDeriv(self, Modeling, Parameter):
        """
        Returns the derivative of the matrix that connects strain vector 
        to stress vector with respect to the parameter Parameter.
        
        """
        assert(isinstance(Modeling, str))
        assert(isinstance(Parameter, str))
        assert(hasattr(self, Parameter))
        ModelingName = self.type + "." + Modeling
        DerivName = ModelingName + "." + Parameter
        assert(ModelingName in material_cata.ConstRelat)
        assert(DerivName in material_cata.ConstRelatDeriv)
        if ModelingName == "isotrope.3D":
            E, v = self.young, self.poisson
            constRelatDeriv = material_cata.ConstRelatDeriv[DerivName]
            result = np.zeros(constRelatDeriv.shape)
            for idx in np.ndindex(result.shape):
                result[idx] = constRelatDeriv[idx](v, E)
            return result
        else: raise ValueError("The modeling "
            "%s is not yet implemented" % Modeling)

    def GetMassParam(self):
        """
        Returns material parameters that defines its mass properties.
        
        Ex : For isotropic materials, it returns :

                            -> gho
        
        """
        return self.GetParam("Mass")

    def GetStiffnessParam(self):
        """
        Returns material parameters that defines its stiffness properties.
        
        Ex : For isotropic materials, it returns :

                        -> (young, poisson)

        """
        return self.GetParam("Stiffness")

    def GetDampingParam(self):
        """
        Returns material parameters that defines its damping properties.
        
        Ex : For isotropic materials, it returns :

                        -> (alpha, beta)
        
        """
        return self.GetParam("Damping")

    def GetParam(self, ParamType):
        """
        It should not be used. It is only used within the functions 
        GetMassParam(...), GetStiffnessParam(...) and GetDampingParam(...)
        under strict assumptions.
        
        """
        assert(ParamType in ["Mass", "Stiffness", "Damping"])
        idx = 0 if ParamType == "Mass" else (1 if ParamType == "Stiffness" else 2)
        result = ()
        for param in material_cata.Material[self.type][0][idx]:
            result = result + (getattr(self,param),)
        return result[0] if len(result) == 1 else result

    def GetType(self):
        """Return the material type"""
        return self.type


class behavior:
    """
    behavior class.

    Parameters
    ----------
    StudyMesh : 'mesh object'
        The underlying mesh to behavior.
    
    MaterialGroups : 'dict'
        A list of {material, elements group} couples that defines the
        material field (each material is assigned to its corresponding
        elements group). The groups used should be a partition of the
        mesh's elements. There is a special group called "All" that
        allows applying a single material to the whole mesh, when used
        no other couples are permitted. Here's how a couple is declared :

                                group_name = material_name

            - group_name    : the name of the elements group to which the
                              material will be assigned (the group should
                              be already defined in the mesh).
            
            - material_name : the name of the material object.

        Examples :
            - group_1 = material_1
            - All     = material_2

    Attributes
    ----------
    mesh : 'mesh object'
        The underlying mesh to behavior.
    
    __Material__ : 'Material object'
        Stores the material's object when a single material is assigned to the
        whole mesh. Otherwise it's "None".
    
    __MaterialSets__ : 'dict'
        "None" if a single material is assigned to the whole mesh, otherwise
        it's a dictionary whose keys are elements groups, and values are 
        their corresponding materials objects.

    """

    def __init__(self, StudyMesh, **MaterialGroups):
        assert(isinstance(StudyMesh, mesh.mesh))
        self.mesh = StudyMesh
        if len(MaterialGroups) == 1:
            assert("All" in MaterialGroups and isinstance(MaterialGroups["All"], Material))
            self.__Material__     = MaterialGroups["All"]
            self.__MaterialSets__ = None
        else:
            GroupList = []
            self.__Material__     = None
            self.__MaterialSets__ = {}
            for group, material in MaterialGroups.items():
                assert(isinstance(material, Material))
                self.__MaterialSets__[group] = material
                GroupList.append(group)
            if not StudyMesh.CheckPartition(*GroupList):
                raise ValueError("The groups {} aren't a partition of the mesh elements".format(GroupList))

    def GetBehaviorGroups(self):
        """
        Returns the list of elements groups to which materials are
        assigned. If a single material is assigned to the whole mesh
        (using the special group "All"), the liste ["All"] is returned.
        
        """
        return ["All"] if self.__Material__ else [group for group in self.__MaterialSets__]

    def GetBehaviorMaterials(self):
        """Returns the list of materials."""
        return [self.__Material__] if self.__Material__ else [material for material in self.__MaterialSets__.values()]

    def GetDisinctGroupsBy(self, ParamType, MergeAndStore=False):
        """
        It should not be used. It is only used within the functions 
        GetStiffnessGroups(...), GetMassGroups(...) and GetDampingGroups(...)
        under strict assumptions.
        
        """
        BehaviourGroups    = self.GetBehaviorGroups()
        BehaviourMaterials = self.GetBehaviorMaterials()
        if len(BehaviourGroups) == 1:
            return [("All",)] , [BehaviourMaterials[0]]
        else:
            BehaviourMaterialsParam = [material.GetParam(ParamType) for material in BehaviourMaterials]
            UniqueMaterialsParam    = list(set(BehaviourMaterialsParam))
            UniqueMaterialsIndex    = [BehaviourMaterialsParam.index(UniqueParam) for UniqueParam in UniqueMaterialsParam]
            UniqueMaterials         = [BehaviourMaterials[idx] for idx in UniqueMaterialsIndex]
            # UniqueMaterialsGroups   = [tuple(group for idx, group in enumerate(BehaviourGroups) if BehaviourMaterialsParam[idx] == UniqueParam) for UniqueParam in UniqueMaterialsParam]
            UniqueMaterialsGroups   = [() for i in UniqueMaterials]
            for idx, group in enumerate(BehaviourGroups):
                GroupMaterialParam = BehaviourMaterialsParam[idx]
                UniqueMaterialsIdx = UniqueMaterialsParam.index(GroupMaterialParam)
                UniqueMaterialsGroups[UniqueMaterialsIdx] += (group,)
            if MergeAndStore:
                UniqueMaterialsGroups   = [(self.mesh.GetGroupsUnion(*GroupsList),) for GroupsList in UniqueMaterialsGroups]
            return UniqueMaterialsGroups, UniqueMaterials

    def GetStiffnessGroups(self, MergeAndStore=False):
        """
        Returns elements groups grouped according to stiffness properties
        of their materials. A list of materials objects that represents
        different stiffnesses of the material field are also returned.
        
        The purpose of this function is to optimize assembly of finite element
        matrices. Althought two elements groups can have different materials,
        if their stiffness properties are similar,  the same constitutive
        relation (in its matrix form) will be used to compute their contributions
        to stiffness matrix. So it's better to consider them as a single group during
        the assembly of this matrix, which reduces splitting operations of elements
        groupes according to elements type.

        Nb : stiffness properties are the material parameters used within stiffness
        matrix calculation.

        Return
        ------
            A tuple of two lists is returned :

                                     (list_1, list_2)

                - list_1 : a list of tuples, each tuple contains the names of elements
                           groups that have the same stiffness properties. If the
                           parameter MergeAndStore is True, groups of each tuple are 
                           replaced by a temporary group resulting from their union 
                           (see mesh.GetGroupsUnion(...) for more details about groups
                           union).

                - list_2 : contains materials objects with different stiffness
                           properties (the order of the first list is respected).
        
        Example
        -------
            Lets consider a mesh whose elements are partitionned into 3 groups,
            each group is assigned a different material. Groups names and their
            corresponding materials objects are given by group_i and material_i,
            with i in range(3). materials are supposed isotropic, their properties
            are presented in the table below.

                          |    material_1    |    material_2    |    material_3    
            --------------|------------------|------------------|------------------
               poisson    |       0.3        |       0.2        |       0.3        
            --------------|------------------|------------------|------------------
               young      |       210        |       200        |       210        
            --------------|------------------|------------------|------------------
               gho        |       23.0       |       23.0       |       25.0       

            Since stiffness properties of material_1 and material_3 are similar, 
            both materials can be used within stiffness matrix calculations of
            group_1 and group_3 elements. So instead of handling these two groups
            differently, they may be considered as a single group (during the
            assembly of stiffness matrix), which also has the advantage of reducing
            the number of groups splitting operations according to elements type
            (splitting a group into n groups of same elements type is less expensive
            than splitting 2 groups into 2n groups). In the case of this example,
            the two returned lists are given below :

                - MergeAndStore = False :
                    
                    -> list_1 = [(group_1, group_3), (group_2,)]
                    -> list_2 = [material_1, material_2]
                
                - MergeAndStore = True :
                    
                    -> list_1 = [(.E.Group.7,), (group_2,)]
                    -> list_2 = [material_1, material_2]
            
            ".E.Group.7" is a temporary elements group resulting from the union of
            group_1 and group_3 (7 is chosen for illustration purposes only).

            Nb : material_3 can be used instead of material_1 in list_2.

        """
        return self.GetDisinctGroupsBy("Stiffness", MergeAndStore)

    def GetMassGroups(self, MergeAndStore=False):
        """
        Returns elements groups grouped according to mass properties
        of their materials. A list of materials objects that represents
        different masses of the material field are also returned (see
        GetStiffnessGroups(..) for more explanations, GetMassGroups(..)
        follows the same logic).
        
        """
        return self.GetDisinctGroupsBy("Mass", MergeAndStore)

    def GetDampingGroups(self, MergeAndStore=False):
        """
        Returns elements groups grouped according to damping properties
        of their materials. A list of materials objects that represents
        different dampings of the material field are also returned (see
        GetStiffnessGroups(..) for more explanations, GetDampingGroups(..)
        follows the same logic).
        
        """
        return self.GetDisinctGroupsBy("Damping", MergeAndStore)

    def GetBehaviorMesh(self):
        """Returns the underlying mesh to the behavior object."""
        return self.mesh

def GetLameCoefs(material):
    """Returns lame coefficients for a given isotropic material."""
    assert(isinstance(material, Material) and material.type == "isotrope")
    E = material.young
    v = material.poisson
    lmbda = E * v / ((1 + v) * (1 - 2 * v))
    mu    = E / (2 * (1 + v))
    return lmbda, mu

if __name__ == "__main__":
    mat1 = Material(type="isotrope", young=3.0, poisson=4.3, gho=3.5)
    mat2 = Material(type="isotrope", young=3.0, poisson=4.3, gho=5.5)
    mat3 = Material(type="isotrope", young=5.0, poisson=6.3, gho=5.5)
    Coordinates  = np.array([[0,0],
                             [1,0],
                             [0.5,0.5],
                             [1,1],
                             [0,1],
                             [2,0],
                             [2,1]],float)
    #print(Coordinates.shape)
    #assert(type(Coordinates) == np.ndarray and Coordinates.shape == (5, 2))
    Connectivity = np.array([[3,0,1,2,-1],
                             [3,1,2,3,-1],
                             [3,2,3,4,-1],
                             [4,1,5,6,4],
                             [3,2,4,0,-1]], int)
    m = mesh.mesh(7,5,2,Coordinates,Connectivity, group1 = ('N', [0,1,6]), group3 = ('N', [2,3]), group5 = ('N', [4,5]), group8=('N', [2,3,4,5]))
    m.AddGroups(group2 = ('E', [1,4]), group4 = ('E', [3]), group6 = ('E', [0,2]), group7 = ('E', [4,0,3,1,2]))

    r = behavior(m, All=mat1)
    t = behavior(m, group2=mat1,group4=mat2,group6=mat3)

    print(r.__Material__)
    print(r.__MaterialSets__)

    print(t.__Material__)
    print(t.__MaterialSets__)

    print(getattr(mat1,"young"))
    print(getattr(mat1,"poisson"))
    print(mat1.GetStiffnessParam())

    print(getattr(mat1,"gho"))
    print(mat1.GetMassParam())
    print(mat1.GetDampingParam())

    print(" ")
    print(" ")

    print(list(zip(*t.GetDisinctGroupsBy("Stiffness", MergeAndStore=True))))
    for grouplist, mat in list(zip(*t.GetDisinctGroupsBy("Stiffness", MergeAndStore=True))):
        print(grouplist,mat.GetParam("Stiffness"))
        print(m.GetGroupItems(grouplist[0], GroupType=None))

    print(" ")
    print(" ")

    print(list(zip(*r.GetDisinctGroupsBy("Stiffness"))))
    for grouplist, mat in list(zip(*r.GetDisinctGroupsBy("Stiffness", MergeAndStore=True))):
        print(grouplist,mat.GetParam("Stiffness"))
        print(m.GetGroupItems(grouplist[0], GroupType="Elmnts"))

    print(" ")
    print(" ")

    print(list(zip(*t.GetDisinctGroupsBy("Mass"))))
    for grouplist, mat in list(zip(*t.GetDisinctGroupsBy("Mass", MergeAndStore=True))):
        print(grouplist,mat.GetParam("Mass"))
        print(m.GetGroupItems(grouplist[0], GroupType="Elmnts"))
    
    r1, r2 = t.GetDisinctGroupsBy("Mass", MergeAndStore=True)
    print(r1)
    print(r2)

    for idx, (j,) in enumerate(r1):
        print(idx, '  ', j)




