"""This module implements the material catalogue."""

import numpy as np
from .materials import isotrop
# Le dictionnaire ci-dessous regroupe les types de matériaux supportés par le code,
# pour chaque type il indique la liste de paramètres du matériau. Cette liste est 
# utilisée par le constructeur de la class material pour vérifier que tt les paramètres 
# ont été fournis.
Material =  {
                "isotrope" :  [
                                    [("gho",), ("young", "poisson",), ("alpha", "beta",)]
                              ]
            }

# Le dictionnaire ci-dessous regroupe les relations de comportement des différents 
# types de matériaux en 3D, 2D ...
ConstRelat = {}

ConstRelat["isotrope.3D"] = isotrop.ConstRelat["3D"]

# Le dictionnaire ci-dessous regroupe les dérivés des relations de comportement des 
# différents types de matériaux en 3D, 2D ...
ConstRelatDeriv = {}

ConstRelatDeriv["isotrope.3D.young"] =  isotrop.ConstRelatDeriv["3D.young"]
ConstRelatDeriv["isotrope.3D.poisson"] =  isotrop.ConstRelatDeriv["3D.poisson"]


if __name__ == "__main__":
    print(ConstRelat["isotrope.3D"])