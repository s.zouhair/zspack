"""This module implements isotropic materials catalogue."""
import numpy as np

ConstRelat = {}
ConstRelatDeriv = {}

# ---------------------------------------------------------------------------
# ------------------------------- 3D Modeling -------------------------------
# ---------------------------------------------------------------------------

CrF1 = lambda lmbda, mu : lmbda + 2 * mu
CrF2 = lambda lmbda, mu : lmbda
CrF3 = lambda lmbda, mu : mu
CrF0 = lambda lmbda, mu : 0

ConstRelat["3D"] = np.array(
    [[CrF1 , CrF2 , CrF2 , CrF0 , CrF0 , CrF0],
     [CrF2 , CrF1 , CrF2 , CrF0 , CrF0 , CrF0],
     [CrF2 , CrF2 , CrF1 , CrF0 , CrF0 , CrF0],
     [CrF0 , CrF0 , CrF0 , CrF3 , CrF0 , CrF0],
     [CrF0 , CrF0 , CrF0 , CrF0 , CrF3 , CrF0],
     [CrF0 , CrF0 , CrF0 , CrF0 , CrF0 , CrF3]]
)

CrdYTF1 = lambda v : 1. / ((1 + v) * (1 - 2 * v))

CrdYF1 = lambda v, E : (1 - v) * CrdYTF1(v)
CrdYF2 = lambda v, E : v * CrdYTF1(v)
CrdYF3 = lambda v, E : (0.5 - v) * CrdYTF1(v)
CrdYF0 = lambda v, E : 0

ConstRelatDeriv["3D.young"] =  np.array(
    [[CrdYF1 , CrdYF2 , CrdYF2 , CrdYF0 , CrdYF0 , CrdYF0],
     [CrdYF2 , CrdYF1 , CrdYF2 , CrdYF0 , CrdYF0 , CrdYF0],
     [CrdYF2 , CrdYF2 , CrdYF1 , CrdYF0 , CrdYF0 , CrdYF0],
     [CrdYF0 , CrdYF0 , CrdYF0 , CrdYF3 , CrdYF0 , CrdYF0],
     [CrdYF0 , CrdYF0 , CrdYF0 , CrdYF0 , CrdYF3 , CrdYF0],
     [CrdYF0 , CrdYF0 , CrdYF0 , CrdYF0 , CrdYF0 , CrdYF3]]
)

CrdPTF1 = lambda v, E : E / ((1 + v) * (1 - 2 * v))
CrdPTF2 = lambda v, E : E * (4 * v + 1) / (((v + 1) ** 2) * ((2 * v - 1) ** 2))

CrdPF1 = lambda v, E : (1 - v) * CrdPTF2(v, E) - CrdPTF1(v, E)
CrdPF2 = lambda v, E : v * CrdPTF2(v, E) + CrdPTF1(v, E)
CrdPF3 = lambda v, E : (0.5 - v) * CrdPTF2(v, E) - CrdPTF1(v, E)
CrdPF0 = lambda v, E : 0

ConstRelatDeriv["3D.poisson"] =  np.array(
    [[CrdPF1 , CrdPF2 , CrdPF2 , CrdPF0 , CrdPF0 , CrdPF0],
     [CrdPF2 , CrdPF1 , CrdPF2 , CrdPF0 , CrdPF0 , CrdPF0],
     [CrdPF2 , CrdPF2 , CrdPF1 , CrdPF0 , CrdPF0 , CrdPF0],
     [CrdPF0 , CrdPF0 , CrdPF0 , CrdPF3 , CrdPF0 , CrdPF0],
     [CrdPF0 , CrdPF0 , CrdPF0 , CrdPF0 , CrdPF3 , CrdPF0],
     [CrdPF0 , CrdPF0 , CrdPF0 , CrdPF0 , CrdPF0 , CrdPF3]]
)

# ---------------------------------------------------------------------------
# ------------------------------ Other Modeling -----------------------------
# ---------------------------------------------------------------------------