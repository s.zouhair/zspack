"""
This module implements some utility functions related to the FE solver.

"""

import petsc4py
from mpi4py import MPI
import sys

__IsParallelStudy__ = False

def init(InitPetsc=False, PetscArgs=None):
    """Initialize PETSc."""
    if MPI.COMM_WORLD.Get_size() > 1:
        global __IsParallelStudy__
        __IsParallelStudy__ = True
    if __IsParallelStudy__ and InitPetsc:
        petsc4py.init(sys.argv if PetscArgs is None else PetscArgs)

def IsParallelStudy():
    """Returns a bool that determines if the current study is parallel"""
    return __IsParallelStudy__

def GetCommWorldRank():
    """Returns the rank within MPI.COMM_WORLD"""
    return MPI.COMM_WORLD.Get_rank()

def GetCommWorldSize():
    """Returns the size of MPI.COMM_WORLD"""
    return MPI.COMM_WORLD.Get_size()

__all__ = ["init", "IsParallelStudy", "GetCommWorldRank",
            "GetCommWorldSize"]