"""
This module implements some utility functions related to materials and behaviors.

"""

from ..pkgmatbhv import material_cata

def GetParamType(Parameter):
    """
    Returns the type of a material parameter. Three 
    values are possible : Mass, Stiffness, Damping.
    
    """
    assert(isinstance(Parameter, str))
    MatsCata = material_cata.Material
    for MatParams in MatsCata.values():
        if Parameter in MatParams[0][0]:
            return "Mass"
        elif Parameter in MatParams[0][1]:
            return "Stiffness"
        elif Parameter in MatParams[0][2]:
            return "Damping"
    raise ValueError("Unknown parameter"
        " : {}".format(Parameter))