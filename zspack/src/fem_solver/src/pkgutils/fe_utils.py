"""
This module implements some utility functions related to FE.

"""

import numpy as np

from .. import num_integration
from ..pkgfelmnt import fe_cata
from ..pkgbndary import efforts_cata

# La fonction ci-dessous renvoie la liste des faces (np.array) de l'élément de type ElmntType (=FEname)
def GetFacesByElmntType(ElmntType):
    # On vérifie si l'élément existe dans le catalogue FEcata
    if ElmntType not in fe_cata.Faces:
        raise ValueError("The element %s isn't implemented" % ElmntType)
    return fe_cata.Faces[ElmntType]

# La fonction ci-dessous renvoie la liste des edges (np.array) de l'élément de type ElmntType (=FEname)
def GetEdgesByElmntType(ElmntType):
    # On vérifie si l'élément existe dans le catalogue FEcata
    if ElmntType not in fe_cata.Edges:
        raise ValueError("The element %s isn't implemented" % ElmntType)
    return fe_cata.Edges[ElmntType]

# La fonction ci-dessous renvoie le nom (type) de face de l'élément de type ElmntType.
def GetFaceNameByElmntType(ElmntType):
    # On vérifie si l'élément existe dans le catalogue FEcata
    if ElmntType not in fe_cata.FaceNameByElmntName:
        raise ValueError("The element %s isn't implemented" % ElmntType)
    return fe_cata.FaceNameByElmntName[ElmntType]

# La fonction ci-dessous renvoie le nom (type) de l'aretede  l'élément de type ElmntType.
def GetEdgeNameByElmntType(ElmntType):
    # On vérifie si l'élément existe dans le catalogue FEcata
    if ElmntType not in fe_cata.EdgeNameByElmntName:
        raise ValueError("The element %s isn't implemented" % ElmntType)
    return fe_cata.EdgeNameByElmntName[ElmntType]

# Cette fonction renvoie le nom de l'élément de dimension ElmntDim, et dont la face contient FaceSize noeud
def GetFaceElmnt(ElmntDim, FaceSize, ReturnElmntSize=True):
    FaceName = "%dD.%dN.F" % (ElmntDim, FaceSize)
    # On vérifie si la face existe dans le catalogue FEcata
    if FaceName not in fe_cata.ElmntNameByFaceName:
        raise ValueError("The %d-dimensional %d nodes face isn't implemented" % (ElmntDim, FaceSize))
    ElmntName = fe_cata.ElmntNameByFaceName[FaceName]
    if ReturnElmntSize:
        return ElmntName, fe_cata.ShapeFcts[ElmntName].shape[0]
    else:
        return ElmntName

def GetModelingDof(Modeling):
    assert(isinstance(Modeling, str))
    # On vérifie si la modélisation demandé existe dans le catalogue FEcata
    if Modeling not in fe_cata.Modeling:
        raise ValueError("There is no modeling called %s" % (Modeling))
    return list(fe_cata.Modeling[Modeling][1])

def GetModelingDofName(Modeling, DofOrder):
    assert(isinstance(Modeling, str))
    assert(isinstance(DofOrder, int))
    DofList = GetModelingDof(Modeling)
    assert(DofOrder < len(DofList))
    return DofList[DofOrder]

def GetModelingDofNbr(Modeling):
    return len(GetModelingDof(Modeling))

def GetModelingDofOrder(Modeling, DofName):
    assert(isinstance(Modeling, str))
    assert(isinstance(DofName, str))
    DofList = GetModelingDof(Modeling)
    if DofName not in DofList:
        raise ValueError("The modeling %s has no DOF called %s" % (Modeling, DofName))
    return DofList.index(DofName)

def GetEffortComponents(EffortType):
    assert(isinstance(EffortType, str))
    if EffortType not in efforts_cata.Efforts:
        raise ValueError("This type of effort ('%s') is not yet implemented" % EffortType)
    return list(efforts_cata.Efforts[EffortType][0])

def GetEffortComponentOrder(EffortType, ComponentName):
    assert(isinstance(EffortType, str))
    assert(isinstance(ComponentName, str))
    ComponentsList = GetEffortComponents(EffortType)
    if ComponentName not in ComponentsList:
        raise ValueError("The effort %s has no component called %s" % (EffortType, ComponentName))
    return ComponentsList.index(ComponentName)

def GetEffortsArraySize(EffortType):
    assert(isinstance(EffortType, str))
    ComponentsList = GetEffortComponents(EffortType)
    return len(ComponentsList)

def GetElmntType(Dimension, NodesNbr):
    return "%dD.%dN" % (Dimension, NodesNbr)

def GetElmntTypeDimAndNodesNbr(ElmntType):
    assert(isinstance(ElmntType, str))
    Dim = int(ElmntType[0])
    NodesNbr = int(ElmntType[3:ElmntType.index('N')])
    return Dim, NodesNbr



# Cette fonction sert à créer un matrice à partir du np.array FctsList qui contient des fonctions,
# un entier appelé Repetition, et un tuple a contenant la valeur des paramètres pour lesquels les 
# fonctions de FctsList seront évalués. Le résultat renvoyé est une matrice sous la forme ci-dessous :
#     _                                                                                            _
#    |   N1(a)    0      0     0    N2(a)    0      0      0     ...   Nm(a)    0      0      0     |
#    |     0    N1(a)    0     0      0    N2(a)    0      0     ...     0    Nm(a)    0      0     |
#    |     0      0     ...    0      0      0     ...     0     ...     0      0     ...     0     |
#    |_    0      0      0   N1(a)    0      0      0    N2(a)   ...     0      0      0    Nm(a)  _|
#
# Le nombre de lignes de la matrice ci-dessous est passé à la fonction par le paramètre Repetition.
def ConmputeN(FctsList, a, Repetition):
    assert(isinstance(FctsList, np.ndarray) and isinstance(FctsList.dtype, object))
    assert(isinstance(a, np.ndarray) and isinstance(Repetition, int))
    if Repetition == 1:
        return np.array([Fct(*a) for Fct in FctsList])
    else:
        DistinctDiag = [np.diag(np.ones(Repetition) * Fct(*a)) for Fct in FctsList]
        return np.concatenate(DistinctDiag, axis=1)

def GetGaussPointsAndWeights(CalcName, IntegOrder):
    assert(IntegOrder in ["Complete", "Reduced"])
    if IntegOrder == "Complete":
        assert(CalcName in fe_cata.CompleteIntegration)
        IntegShemeName = fe_cata.CompleteIntegration[CalcName]
    else:
        assert(CalcName in fe_cata.ReducedIntegration)
        IntegShemeName = fe_cata.ReducedIntegration[CalcName]
    return num_integration.GaussianQuad[IntegShemeName]

if __name__ == "__main__":
    #print(GetFacesSize(3, 8))
    print(GetModelingDof("3D"))
    print(GetModelingDofOrder("3D", "DZ"))
    print(GetModelingDofName("3D", 1))
    print(GetEffortComponents("3D.FacesEfforts"))
    print(GetEffortComponentOrder("3D.FacesEfforts", "Ty"))

    FctsList = np.array([lambda a,b:a+b, lambda a,b:a-b, lambda a,b:a*b])
    print(ConmputeN(FctsList, (1,2), 3))