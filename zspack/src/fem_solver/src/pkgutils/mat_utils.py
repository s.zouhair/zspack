"""
This module implements some utility functions related to matrices.

"""

import numpy as np
from scipy.sparse import bsr_matrix, csr_matrix

def ExpandMatrix(SubMat, GlobalNumbring, MatSize, StorageType=None):
    """
    Returns an expanded version of the matrix SubMat by adding zero 
    rows and columns. The shape of the returned matrix is 
    (MatSize, MatSize). SubMat is inserted into the intersection 
    of rows/cols whose indexes ares stored in GlobalNumbring, note 
    that rows/cols of SubMat are permuted if necessary. The parameter 
    StorageType is used to specify the type of the returned matrix,
    only two options are possible :

        - "Dense"  : a 2D array is returned (The full matrix).
        - "Sparse" : a scipy sparse matrix is returned (bsr_matrix)
    
    NB : rows and columns aren't permuted according to their order in 
         the array GlobalNumbring.

    """
    assert(isinstance(SubMat, np.ndarray) and SubMat.ndim == 2)
    assert(isinstance(GlobalNumbring, np.ndarray) and GlobalNumbring.ndim == 1)
    assert(SubMat.shape[0] == SubMat.shape[1] == GlobalNumbring.size)
    assert(np.all(GlobalNumbring < MatSize) and MatSize > SubMat.shape[0])
    assert(StorageType in ["Dense", "Sparse"])
    if StorageType == "Dense":
        return __ExpandMatrix_Dense__(SubMat, GlobalNumbring, MatSize)
    else:
        return __ExpandMatrix_Sparse__(SubMat, GlobalNumbring, MatSize)

def __ExpandMatrix_Dense__(SubMat, GlobalNumbring, MatSize):
    """
    It should not be used. It is only used within the function
    ExpandMatrix(...) under strict assumptions.

    """
    typ = SubMat.dtype
    MatRes = np.zeros((MatSize,MatSize), dtype=typ)
    MatRes[GlobalNumbring[:,None], GlobalNumbring] = SubMat
    return MatRes

def __ExpandMatrix_Sparse__(SubMat, GlobalNumbring, MatSize):
    """
    It should not be used. It is only used within the function
    ExpandMatrix(...) under strict assumptions.

    """
    typ = SubMat.dtype
    VectSize = GlobalNumbring.size
    row  = GlobalNumbring.repeat(VectSize)
    col  = np.tile(GlobalNumbring, VectSize)
    data = SubMat.flatten()
    # return bsr_matrix((data, (row, col)), shape=(MatSize, MatSize), dtype=float)
    # Apparemment il y'a un bug si on utilise return bsr_matrix(...) 
    # directement au lieu de csr_matrix(...).tobsr()
    return csr_matrix((data, (row, col)), shape=(MatSize, MatSize), dtype=typ).tobsr()

def GetSubMatrix(Matrix, RowsIdx=None, 
    ColsIdx=None, IsFeAsmbly=False, OutFormat=None):
    if IsFeAsmbly:
        ColsIdx = RowsIdx if ColsIdx is None else ColsIdx
        return Matrix[RowsIdx[:,np.newaxis], ColsIdx]
    else: 
        assert(isinstance(Matrix, (np.ndarray, bsr_matrix, csr_matrix)))
        assert(isinstance(RowsIdx, np.ndarray) and RowsIdx.ndim == 1)
        assert(OutFormat in [None, "Dense", "Sparse"])
        
        if ColsIdx is None:
            ColsIdx = RowsIdx
        else:
            assert(isinstance(ColsIdx, np.ndarray) and ColsIdx.ndim == 1)
        
        if isinstance(Matrix, np.ndarray):
            result = Matrix[RowsIdx[:,np.newaxis], ColsIdx]
            is_sparse = OutFormat == "Sparse"
            sp_fun = csr_matrix
        else:
            tup = ()
            for row_idx in RowsIdx:
                tup += Matrix.getrow(row_idx).toarray(),
            result = np.vstack(tup)[:, ColsIdx]
            is_sparse = OutFormat in [None, "Sparse"]
            sp_fun = bsr_matrix if isinstance(Matrix, bsr_matrix) \
                                                            else csr_matrix

        return sp_fun(result) if is_sparse else result

def SplitMatrix(Matrix, *indices_list):
    assert(isinstance(Matrix, (np.ndarray, bsr_matrix, csr_matrix)))
    rows_nbr, cols_nbr = Matrix.shape
    if rows_nbr != cols_nbr:
        raise ValueError("Non-square matrix !")
    rows_blocks = ()
    for indices_set in indices_list:
        assert(isinstance(indices_set, np.ndarray) and indices_set.ndim == 1)
        assert(np.all(indices_set < rows_nbr))
        
        if isinstance(Matrix, np.ndarray):
            rows_blocks += Matrix[indices_set, :],
        else:
            tup = ()
            for row_idx in indices_set:
                tup += Matrix.getrow(row_idx).toarray(),
            rows_blocks += np.vstack(tup),

    sets_nbr = len(indices_list)
    temp_res = [[] for _ in range(sets_nbr)]

    for set_idx in range(sets_nbr):
        for indices_set in indices_list:
            sub_set = rows_blocks[set_idx][:, indices_set]
            temp_res[set_idx].append(sub_set)
    
    del rows_blocks
    
    if not isinstance(Matrix, np.ndarray):
        sp_fun = bsr_matrix if isinstance(Matrix, bsr_matrix) else csr_matrix
        result = [[sp_fun(sub_set) for sub_set in temp_res[set_idx]] 
                                              for set_idx in range(sets_nbr)]
    else:
        result = temp_res

    return result

def GetMatRows(mat, rows_indices, Split=False, SplitOrdering=None,\
                                                         OutFormat=None):
    """
    Extract the rows in rows_indices from the matrix. If Split is true, 
    the block of rows is divided into two blocks of columns, the first 
    contains the columns rows_indices and the second contains the remaining 
    columns. OutFormat is used to specify the desired storage type of the
    output.
    
    """
    assert(isinstance(mat, (np.ndarray, bsr_matrix, csr_matrix)) \
                                                        and mat.ndim == 2)
    assert(isinstance(rows_indices, np.ndarray) and rows_indices.ndim == 1)
    rows_nbr, _ = mat.shape
    assert(np.all(rows_indices < rows_nbr))
    assert(OutFormat in [None, "Dense", "Sparse"])
    assert(isinstance(Split, bool))
    
    if SplitOrdering is not None:
        assert(Split and isinstance(SplitOrdering, np.ndarray))
        assert(SplitOrdering.ndim == 1)
        assert(np.all(SplitOrdering < rows_nbr))

    if isinstance(mat, np.ndarray):
        result = mat[rows_indices]
        is_sparse = OutFormat == "Sparse"
        sp_fun = csr_matrix
    else:
        result = np.vstack([mat.getrow(row_idx).toarray() \
                                            for row_idx in rows_indices])
        is_sparse = OutFormat in [None, "Sparse"]
        sp_fun = bsr_matrix if isinstance(mat, bsr_matrix) else csr_matrix

    if Split:
        if SplitOrdering is not None:
            rows_indices_2 = SplitOrdering
        else:
            temp_vec = np.full(rows_nbr, True)
            temp_vec[rows_indices] = False
            rows_indices_2 = np.nonzero(temp_vec)[0]
        
        temp_res_1 = result[:, rows_indices]
        temp_res_2 = result[:, rows_indices_2]
        
        if is_sparse:
            temp_res_1 = sp_fun(temp_res_1)
            temp_res_2 = sp_fun(temp_res_2)
        
        result = temp_res_1, temp_res_2
    elif is_sparse:
        result = sp_fun(result)

    return result