"""
This module implements some utility functions related to vectors.

"""

import numpy as np

def SplitVector(Vector, *indices_list):
    assert(isinstance(Vector, np.ndarray) and Vector.ndim == 1)
    vec_size = Vector.size
    result   = ()
    for indices_set in indices_list:
        assert(isinstance(indices_set, np.ndarray) and indices_set.ndim == 1)
        assert(np.all(indices_set < vec_size))
        result += Vector[indices_set],
    return result