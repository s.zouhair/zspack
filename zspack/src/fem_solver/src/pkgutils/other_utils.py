"""
This module implements some utility functions.

"""
from functools import reduce
from mpi4py import MPI
from math import pi
import numpy as np
import time

from .sys_utils import GetCommWorldRank, GetCommWorldSize

def GetOmega(Freq):
    """Returns the pulsation related to the frequency Freq"""
    return 2 * pi * float(Freq)

def IsInteger(value):
    """Returns the pulsation related to the frequency Freq"""
    return isinstance(value, (int, np.int32, np.int64))

def PrintAllRanks(ProcInfo, UseHeader=False):
    """Prints informations from all procs sequentially"""
    Rank, Size = GetCommWorldRank(), GetCommWorldSize()
    NotFirst, NotLast = Rank > 0, Rank < Size - 1
    if UseHeader: ToPrint = getToPrintHeader(ProcInfo, Rank)
    else: ToPrint = getToPrintNoHeader(ProcInfo, Rank, Size)
    if NotFirst: MPI.COMM_WORLD.recv(source=Rank-1, tag=1)
    print(ToPrint, flush=True); time.sleep(.1)
    if NotLast: MPI.COMM_WORLD.send(True, dest=Rank+1, tag=1)

def getToPrintHeader(ProcInfo, Rank):
    """Prints informations from all procs sequentially"""
    Top, Mid = 100 * "-" + "\n", 40 * "-"
    Header = Top + Mid + "{:<20}" + Mid + "\n" + Top
    Header = Header.format("Proc {}".format(Rank).center(20))
    return Header + "\n" + str(ProcInfo) + "\n"

def getToPrintNoHeader(ProcInfo, Rank, Size):
    """Prints informations from all procs sequentially"""
    Len = len("Proc {}".format(Size - 1))
    SideStr = "[{}] ".format(
        "Proc {}".format(Rank).center(Len))
    TempList = [SideStr + Line + "\n" for Line \
        in  str(ProcInfo).split("\n") if len(Line) != 0]
    return reduce(lambda x, y: x + y, TempList)[:-1]

__all__ = ["GetOmega", "IsInteger", "PrintAllRanks"]