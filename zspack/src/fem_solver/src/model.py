"""
This module implements the model and DofNumbering classes, and all the 
methods that allow interacting with them.

"""

import numpy as np
import timeit

from .pkgfelmnt import finite_elements, fe_cata
from . import pkgutils as utils
from . import mesh

class model:
    """
    The model class.

    Parameters
    ----------
    StudyMesh : 'femsol.mesh object'
        The underlying mesh to the model.
    
    Modeling : 'str'
        The modeling name. It must be defined in the catalogue, otherwise
        it's not implemented yet. Available modelings are :

            - "3D" : for solid finite elements.

    Attributes
    ----------
    Studymesh : 'femsol.mesh object'
        The model's mesh.
    
    Modeling : 'str'
        The modeling name.
    
    FEList : 'list of FE objects'
        The model finite elements objects list.
    
    """

    def __init__(self, StudyMesh, Modeling):
        assert(isinstance(StudyMesh, mesh.mesh))
        assert(isinstance(Modeling, str))
        # On vérifie si la modélisation demandé existe dans le catalogue FEcata
        if Modeling not in fe_cata.Modeling:
            raise ValueError("There is no modeling called %s" % (Modeling))
        self.Studymesh = StudyMesh
        self.Modeling  = Modeling
        self.FEList    = self.InitModelFiniteElements()

    def GetModelDof(self):
        """
        Returns a list containing the names of the model degrees of freedom.
        
        Example
        -------
            if Modeling == "3D" -> GetModelDof() returns ["DX", "DY", "DZ"]
        
        """
        return utils.GetModelingDof(self.Modeling)
    
    def GetModelDofNbr(self):
        """
        Returns the number of the model's degrees of freedom
        
        Example
        -------
            if Modeling == "3D" -> GetModelDofNbr() returns 3
        
        """
        return utils.GetModelingDofNbr(self.Modeling)
    
    def GetModelDofOrder(self, DofName):
        """
        Returns the index (order) of a given degree of freedom
        
        Example
        -------
            if Modeling == "3D"
                
                -> GetModelDofOrder("DX") returns 0
                
                -> GetModelDofOrder("DY") returns 1
                
                -> GetModelDofOrder("DZ") returns 2
        
        """
        return utils.GetModelingDofOrder(self.Modeling, DofName)

    def GetModelDofName(self, DofOrder):
        """
        Returns the name of the DOF that corresponds to a given index
        (order).
        
        Example
        -------
            if Modeling == "3D"
                
                -> GetModelDofName(0) returns "DX"
                
                -> GetModelDofName(1) returns "DY"
                
                -> GetModelDofName(2) returns "DZ"

        """
        return utils.GetModelingDofName(self.Modeling, DofOrder)
    
    def GetModelDimension(self):
        """Returns the dimension of the model mesh."""
        return self.Studymesh.GetMeshDimension()
    
    def GetModelNodesNbr(self):
        """Returns the nodes number of the model mesh."""
        return self.Studymesh.GetNumberof("Nodes")
    
    def GetModelElmntsNbr(self):
        """Returns elements number of the model mesh."""
        return self.Studymesh.GetNumberof("Elmnts")
    
    def GetModelMesh(self):
        """Returns the underlying mesh to the model."""
        return self.Studymesh

    def GetModeling(self):
        """Returns the modeling name of the model."""
        return self.Modeling

    def GetModelElmntsTypes(self):
        """Returns the elements type(s) of the model mesh."""
        return self.Studymesh.GetElmntsTypeGroups()

    def GetModelFiniteElements(self):
        """Returns the model finite elements objects list."""
        return self.FEList

    def InitModelFiniteElements(self):
        """Used to initialize the model finite elements objects list."""
        ElmntsTypes = self.GetModelElmntsTypes()
        FECara = [utils.GetElmntTypeDimAndNodesNbr(ElmntType) for ElmntType in ElmntsTypes]
        return [finite_elements.FiniteElement(Dim, NodesNbr, self.Modeling) for Dim, NodesNbr in FECara]
        



class DofNumbering:
    """
    The class defining the numbering of the degrees of freedom.

    Parameters
    ----------
    StudyModel : 'femsol.model object'
        The model for which we want to create a DOF numbering.

    Attributes
    ----------
    StudyModel : 'femsol.model object'
        The DofNumbering's model.
    
    FreeDofNbr : 'int'
        The number of free DOFs.
    
    ImposedDofNbr : 'int'
        The number of imposed DOFs.

    DofOrdering : 'np.ndarray'
        A 2D np.ndarray where DOFs numbering is stored, each row 
        correspond to a node. The first element of a row is used to
        determine whether the node contains imposed DOFs, if it's < 0
        the answer is yes and the first element's absolute value correspond
        to the number of its imposed Dofs, otherwise it must be nul. The 
        remaining columns are used to store globale numbering for 
        free DOFs, or indicies where imposed DOFs values are stored in 
        a dedicated array.
    
    UpdatingFinalized : 'bool'
        A boolean that determines whether the Dof numbering updating is
        finalized or not.

    """

    def __init__(self, StudyModel):
        assert(isinstance(StudyModel, model))
        self.StudyModel = StudyModel
        DofNbrPerNode   = StudyModel.GetModelDofNbr()
        ModelNodesNbr   = StudyModel.GetModelMesh().GetNumberof("Nodes")
        self.FreeDofNbr    = ModelNodesNbr * DofNbrPerNode
        self.ImposedDofNbr = 0
        self.DofOrdering = np.zeros((ModelNodesNbr, DofNbrPerNode + 1), dtype=int)
        self.UpdatingFinalized = False

    def UpdateNumbering(self, ImposedNodesIdxs, ImposedDofList, ImposedValuesPos):
        """
        Used to update the DOF numbering whenever a new boundary
        conditions are added (unless the Dof numbering updating is
        finalized).

        """
        if not self.UpdatingFinalized:
            assert(isinstance(ImposedNodesIdxs, np.ndarray))
            assert(isinstance(ImposedValuesPos, np.ndarray))
            assert(isinstance(ImposedDofList  , np.ndarray))
            assert(ImposedDofList.size == ImposedValuesPos.size)
            ImposedDofList = self.__GetRealDofIdxs__(ImposedDofList)
            self.__CheckForOldImposedDof__(ImposedNodesIdxs, ImposedDofList)
            self.DofOrdering[ImposedNodesIdxs[:,None], ImposedDofList] = - (ImposedValuesPos + 1)
            self.DofOrdering[ImposedNodesIdxs[:,None], 0] -= ImposedDofList.size
            self.FreeDofNbr    -= ImposedDofList.size * ImposedNodesIdxs.size
            self.ImposedDofNbr += ImposedDofList.size * ImposedNodesIdxs.size
        else:
            raise ValueError("Updating is already finalized !")

    def __CheckForOldImposedDof__(self, ImposedNodesIdxs, ImposedDofList):
        """
        Checks whether a new boundary conditions tries to impose
        DOFs that are already imposed.
        
        """
        Temp = np.argwhere(self.DofOrdering[ImposedNodesIdxs[:,None], ImposedDofList] != 0)
        if Temp.size:
            print("The following degrees of freedom are already imposed :")
            for row in range(Temp.shape[0]):
                DofOrder = int(ImposedDofList[Temp[row,1]]) - 1
                DofName  = self.StudyModel.GetModelDofName(DofOrder)
                NodeIdx  = ImposedNodesIdxs[Temp[row,0]]
                print("\t- %s component of the node %d." % (DofName, NodeIdx))
            raise ValueError("Please read the above message.")

    def IsUpdatingFinalized(self):
        """
        Used to check whether the DOF's numbering updating is
        finalized (No new boundary conditions are allowed).
        
        """
        return self.UpdatingFinalized

    def FinalizeUpdating(self):
        """Used to finalize DOF numbering updating."""
        if not self.UpdatingFinalized:
            NonImposedDofIdxs = np.nonzero(self.DofOrdering[:,1:] == 0)
            NonImposedDofNbr  = NonImposedDofIdxs[0].size
            assert(NonImposedDofNbr == self.FreeDofNbr)
            np.add(NonImposedDofIdxs[1], 1, out=NonImposedDofIdxs[1])
            self.DofOrdering[NonImposedDofIdxs] = np.arange(NonImposedDofNbr)
            self.UpdatingFinalized = True
        else:
            raise ValueError("Updating is already finalized !")
    
    def __GetRealDofIdxs__(self, ImposedDofList):
        return  np.add(ImposedDofList, 1, out=ImposedDofList)
    
    def GetModel(self):
        """Returns the underlying model."""
        return self.StudyModel
    
    def GetMesh(self):
        """Returns the underlying mesh"""
        return self.StudyModel.GetModelMesh()

    def GetFreeDofNbr(self):
        """Returns the number of free degrees of freedom."""
        assert(self.UpdatingFinalized)
        return self.FreeDofNbr

    def GetImposedDofNbr(self):
        """Returns the number of imposed degrees of freedom."""
        assert(self.UpdatingFinalized)
        return self.ImposedDofNbr

    def GetTotalDofNbr(self):
        """Returns the total number of degrees of freedom."""
        return self.GetFreeDofNbr() + self.GetImposedDofNbr()
    
    def GetDofNbrPerNode(self):
        """Returns degrees of freedom number per node."""
        return self.StudyModel.GetModelDofNbr()

    def GetNodesNbr(self):
        """Returns nodes number of the underlying mesh."""
        return self.StudyModel.GetModelNodesNbr()

    def GetElmntsNbr(self):
        """Returns elements number of the underlying mesh."""
        return self.StudyModel.GetModelElmntsNbr()

    def GetElmntDofsNbr(self, ElmntIdx):
        """
        Returns the total number of degrees of freedom
        of the element number ElmntIdx.

        """
        ElmntNodesNbr = self.GetMesh().GetElmntNodesNbr(ElmntIdx)
        DofsNbrPerNode = self.GetDofNbrPerNode()
        return ElmntNodesNbr * DofsNbrPerNode

    def HasImposedDof(self, ElmntIdx=None, FaceIdx=None):
        """
        Returns a bool that determines whether an element/face
        containes imposed DOFs.
        
        """
        assert((ElmntIdx is not None) ^ (FaceIdx is not None))
        if ElmntIdx is not None:
            NodesList = self.GetMesh().GetElmntNodesList(ElmntIdx)
        else:
            NodesList = self.GetMesh().GetFaceNodesList(FaceIdx)
        return np.any(self.DofOrdering[NodesList, 0] < 0)

    # Cette fonction renvoie la numérotation global des degré de liberté (np.array) pour les éléments 
    # qui comptent aucun ddl imposé. S'il y'a des ddl imposé dans l'élément, la fonction renvoie un tuple 
    # contenant deux autres tuple, et chacun de ces deux tuples contient deux np.arrays. Le premier tuple 
    # concerne les ddl non imposés, le premier np.array de ce tuple contient la numérotation global des ces 
    # ddls, et le deuxième leur numérotation locale. Le deuxième tuple concerne les ddl imposés, son premier 
    # np.array contient la numérotation des ddls imposé dans le vecteur ou les les valeurs de tt les ddls 
    # imposés sont stockés (une sorte de numérotation global des ddls imposés), et le deuxième np.array 
    # contient la numérotation local des ddls imposé. Le mot clé HasImposedDof est ajouter pour permettre de 
    # spécifier si l'élément ElmntIdx contient des ddls imposé si cela est connue au moment de l'appel à 
    # GetElmntNumbring(...), ainsi un appel à HasImposedDof() sera épargné (un peu d'opti qui peut tt de mm 
    # etre inutile)
    def GetElmntNumbring(self, ElmntIdx, HasImposedDof=None):
        """
        Returns an element's numbering.

        Return
        ------
            - If the element doesn't contains imposed DOFs :

                        GetElmntNumbring(...) -> arr1

              A single np.ndarray that contains the element's DOF's 
              global numbering is returned. 

            - If the element contains imposed DOFs :
            
              GetElmntNumbring(...) -> ((arr1, arr2), (arr3, arr4))
        
              A tuple of two tuples is returned, each tuple containes
              two np.ndarrays. The first tuple (arr1, arr2) correspond
              to free DOFs, arr1 and arr2 are respectively the free DOF's
              global numbering, and their corresponding local numbering
              (among the element's DOF).
              
              The second tuple (arr3, arr4) correspond to imposed DOFs, 
              arr4 contains their local numbering, while arr3 contains 
              the positions where their imposed values are stored in a
              dedicated array (See femsol.ImposeDof).

        """
        if HasImposedDof is None:
            HasImposedDof = self.HasImposedDof(ElmntIdx=ElmntIdx)
        ElmntNodesList = self.GetMesh().GetElmntNodesList(ElmntIdx)
        ElmntNodesNumbering = self.DofOrdering[ElmntNodesList, 1:].flatten()
        if not HasImposedDof:
            result = ElmntNodesNumbering
        else:
            mask = ElmntNodesNumbering >= 0
            result = (ElmntNodesNumbering[mask], np.nonzero(mask)[0]),
            mask = ~mask
            result += (-(ElmntNodesNumbering[mask] + 1), np.nonzero(mask)[0]),
        return result

    # Cette fonction renvoie la numérotation globale des ddls non imposés de la face numéro FaceIdx. 
    # Quand la face ne contient aucun ddl imposé, un seul np.array est renvoyé contenant cette 
    # numérotation global. S'il y'a des ddls imposé dans la surface, un tuple de deux np.array est 
    # renvoyé, le premier contient la numérotation globale des ddls non imposés, et le deuxième leur 
    # numérotation local au sein des ddls (imposés ou pas) de la face.
    def GetFaceNumbring(self, FaceIdx, HasImposedDof=None):
        """
        Returns a face's numbering.
        
        Return details
        --------------
            - If the face doesn't contains imposed DOFs :

                        GetFaceNumbring(...) -> arr1

              A single np.ndarray that contains the face's DOFs global
              numbering is returned.

            - If the face contains imposed DOFs :

                    GetFaceNumbring(...) -> (arr1, arr2)

              A tuple is returned, it correspond to free DOFs, arr1 and
              arr2 are respectively the free DOFs global numbering, and
              their corresponding local numbering (among the face's DOF).
        
        N.B : It's not used for now.

        """
        if HasImposedDof is None:
            HasImposedDof = self.HasImposedDof(FaceIdx=FaceIdx)
        FaceNodesList = self.GetMesh().GetFaceNodesList(FaceIdx)
        FaceNodesNumbering = self.DofOrdering[FaceNodesList, 1:].flatten()
        if not HasImposedDof:
            result = FaceNodesNumbering
        else:
            mask = FaceNodesNumbering >= 0
            result = FaceNodesNumbering[mask], np.nonzero(mask)[0]
        return result

    def GetFaceLocalNumbring(self, FaceIdx):
        """
        Returns the element number to which belong the face
        FaceIdx, and the local numbering of the face DOFs
        within the element DOFs.

        """
        StudyMesh = self.GetMesh()
        ElmntIdx = StudyMesh.GetFaceElmnt(FaceIdx)
        ElmntType = StudyMesh.GetElmntType(ElmntIdx)
        FacelocalNbr = StudyMesh.GetFaceLocalNbr(FaceIdx)
        FaceLocalNodes = utils.GetFacesByElmntType(ElmntType)[FacelocalNbr] - 1
        DofsNbrPerNode = self.GetDofNbrPerNode()
        LocalNumbering = (FaceLocalNodes[:,None] * DofsNbrPerNode + np.arange(DofsNbrPerNode)).flatten()
        return ElmntIdx, LocalNumbering
    
    def GetNodesNumbring(self, NodesList):
        """
        Returns the global numbering of free degrees of freedom of 
        nodes whose indices are stored in the array NodesList.

        """
        assert(isinstance(NodesList, np.ndarray) and NodesList.ndim == 1)
        NodesNbr = self.GetNodesNbr()
        assert(np.all(NodesList < NodesNbr))
        NodesNumbering = self.DofOrdering[NodesList, 1:].flatten()
        mask = NodesNumbering >= 0
        return NodesNumbering[mask]
    
    def GetNodesFreeDofNbr(self, NodesList):
        """
        Returns an array that contains the number of free Dofs of 
        Nodes whose indices are stored in the array NodesList. The
        order of nodes in NodesList is used for the output array.
        
        """
        assert(isinstance(NodesList, np.ndarray) and NodesList.ndim == 1)
        NodesNbr = self.GetNodesNbr()
        assert(np.all(NodesList < NodesNbr))
        DofsNbrPerNode = self.GetDofNbrPerNode()
        return self.DofOrdering[NodesList, 0] + DofsNbrPerNode


    def GetDofsNumbring(self, **kwargs):
        """
        Returns an np.array that contains the global numbering of Dofs 
        passed by kwargs.

        Parameters
        ----------
            kwargs : 'dict'
                keys are nodes names (ex Node0, Node175, ...) and their
                corresponding values are desired DOFs names.

                            node_name = (dof_1, dof_2)

                Examples (3D modeling) :
            
                    - Node0 = ("DX", "DY", "DZ")
            
                    - Node13 = ["DZ", "DY"]
            
                    - Node41 = ("DY",)

        Return
        ------
            Returns a 1D np.ndarray that contains global numbering of 
            passed DOFs. The DOFs order in kwargs is respected.

        """
        result = []
        for NodeName, DofsList in kwargs.items():
            assert(isinstance(DofsList, (list, tuple)))
            NodeIdx = self.GetMesh().GetGroupItems(NodeName, GroupType="Nodes")[0]
            TreatedDofs = []
            for DofName in DofsList:
                assert(isinstance(DofName, str))
                DofIdx = self.StudyModel.GetModelDofOrder(DofName)
                DofGlobalNbr = self.DofOrdering[NodeIdx, DofIdx + 1]
                if DofGlobalNbr < 0:
                    raise ValueError(("Degree of freedom {} of node {} is constrained, " +
                                    "thus it don't have a global numbering").format(DofName, int(NodeIdx)))
                elif DofIdx in TreatedDofs:
                    raise ValueError(("Degree of freedom {} of node {} is repeated " +
                                                                "twice.").format(DofName, int(NodeIdx)))
                else:
                    result.append(DofGlobalNbr)
                    TreatedDofs.append(DofIdx)
        return np.array(result, dtype=int)