import numpy as np

from ..pkgfelmnt.finite_elements import (Compute_BE_2D_3D, 
    ComputeElmntJacobian, ComputeFaceJacobian)
from .. import pkgdatastr as datastr

def Assemble_STIFFMAT(StudyModel, Behaviour, DofNumbering, 
    ImposedDof, IntegrationType, StrgType, Derivative, StrElmntry):
    ModelingName    = StudyModel.GetModeling()
    StudyMesh       = StudyModel.GetModelMesh()
    StiffnessMatrix = datastr.Matrix(DofNumbering, ImposedDof, 
        MatrixType="Stiffness", StorageType=StrgType, ElmntsMats=StrElmntry)
    FiniteElmntList = StudyModel.GetModelFiniteElements()
    StiffnessGroups, Materials = Behaviour.GetStiffnessGroups(MergeAndStore=True)
    ConstRelMatrix  = getConstRelMatrix(Materials, ModelingName, Derivative)
    BeRowsNbr       = ConstRelMatrix[0].shape[0]
    for ELmntType in FiniteElmntList:
        ElmntDofNbr   = StudyModel.GetModelDofNbr() * ELmntType.GetNodesNbr()
        LocalStiffMat = np.empty((ElmntDofNbr, ElmntDofNbr), dtype=float)
        ELmntTypeName = "All" if len(FiniteElmntList) == 1 else ELmntType.GetType() 
        GaussWeights, GaussGrads = ELmntType.GetElmntGradAndWghtAtGauss("Stiffness", IntegrationType)
        BE = np.empty((BeRowsNbr, ElmntDofNbr), dtype=float)
        for StiffGroupIdx, (StiffGroupName,) in enumerate(StiffnessGroups):
            GroupsIntersectionElmnts = StudyMesh.GetGroupsIntersection(
                ELmntTypeName, StiffGroupName, Store=False, Type="Elmnts")
            if GroupsIntersectionElmnts is None: continue
            ConstRelat = ConstRelMatrix[StiffGroupIdx]
            for ElmntIdx in GroupsIntersectionElmnts:
                LocalStiffMat[:,:] = 0.0
                for GaussIdx, ShapeFctsGrad in enumerate(GaussGrads):
                    BE, JacobianDet  = Compute_BE_2D_3D(ModelingName, 
                        StudyMesh, ElmntIdx, ShapeFctsGrad, ElmntDofNbr, BE)
                    LocalStiffMat += GaussWeights[GaussIdx] * np.matmul(
                        np.matmul(BE.transpose(), ConstRelat), BE) * JacobianDet
                StiffnessMatrix.SetElmntryMatVal(LocalStiffMat, ElmntIdx)
    StiffnessMatrix.Assemble()
    return StiffnessMatrix

def Assemble_MASSMAT(StudyModel, Behaviour, DofNumbering, 
    ImposedDof, IntegrationType, StrgType, Derivative, StrElmntry):
    StudyMesh       = StudyModel.GetModelMesh()
    MassMatrix = datastr.Matrix(DofNumbering, ImposedDof, 
        MatrixType="Mass", StorageType=StrgType, ElmntsMats=StrElmntry)
    FiniteElmntList = StudyModel.GetModelFiniteElements()
    MassGroups, Materials = Behaviour.GetMassGroups(MergeAndStore=True)
    Masses  = getMassParameter(Materials, Derivative)
    for ELmntType in FiniteElmntList:
        ElmntDofNbr   = StudyModel.GetModelDofNbr() * ELmntType.GetNodesNbr()
        LocalMassMat = np.empty((ElmntDofNbr, ElmntDofNbr), dtype=float)
        ELmntTypeName = "All" if len(FiniteElmntList) == 1 else ELmntType.GetType() 
        GaussWeights, GaussGrads, GaussInterpMats = \
            ELmntType.GetElmntGradAndWghtAtGauss(
            "Mass", IntegrationType, ReturnInterpMats=True)
        for MassGroupIdx, (MassGroupName,) in enumerate(MassGroups):
            GroupsIntersectionElmnts = StudyMesh.GetGroupsIntersection(
                ELmntTypeName, MassGroupName, Store=False, Type="Elmnts")
            if GroupsIntersectionElmnts is None: continue
            MassDensity = Masses[MassGroupIdx]
            for ElmntIdx in GroupsIntersectionElmnts:
                LocalMassMat[:,:] = 0.0
                for GaussIdx, (ShapeFctsGrad, InterpMat) in enumerate(zip(GaussGrads, GaussInterpMats)):
                    JacobianDet = ComputeElmntJacobian(StudyMesh, ElmntIdx, ShapeFctsGrad, OnlyDet=True)
                    LocalMassMat += GaussWeights[GaussIdx] * MassDensity * \
                        np.matmul(InterpMat.transpose(), InterpMat) * JacobianDet
                MassMatrix.SetElmntryMatVal(LocalMassMat, ElmntIdx)
    MassMatrix.Assemble()
    return MassMatrix

def Assemble_DAMPMAT(StudyModel, Behaviour, DofNumbering, ImposedDof, 
    IntegrationType, MassMatrix, StiffMatrix, StrgType, Derivative, StrElmntry):
    if Derivative is not None:
        return getDampMatDerivative(StudyModel, Behaviour, 
            DofNumbering, ImposedDof, IntegrationType, StrgType, Derivative)
    StudyMesh       = StudyModel.GetModelMesh()
    DampingMatrix   = datastr.Matrix(DofNumbering, ImposedDof, 
        MatrixType="Damping", StorageType=StrgType, ElmntsMats=StrElmntry)
    FiniteElmntList = StudyModel.GetModelFiniteElements()
    DampingGroups, Materials = Behaviour.GetDampingGroups(MergeAndStore=True)
    DampingCoeffs  = [Material.GetDampingParam() for Material in Materials]
    for ELmntType in FiniteElmntList:
        ELmntTypeName = "All" if len(FiniteElmntList) == 1 else ELmntType.GetType() 
        for DampingGroupIdx, (DampingGroupName,) in enumerate(DampingGroups):
            GroupsIntersectionElmnts = StudyMesh.GetGroupsIntersection(
                ELmntTypeName, DampingGroupName, Store=False, Type="Elmnts")
            if GroupsIntersectionElmnts is None: continue
            Alpha, Beta = DampingCoeffs[DampingGroupIdx]
            for ElmntIdx in GroupsIntersectionElmnts:
                LocalStifMat = StiffMatrix.GetElmntryMatVal(ElmntIdx, ImpDofs=True, As2DArray=True)
                LocalMassMat = MassMatrix.GetElmntryMatVal(ElmntIdx, ImpDofs=True, As2DArray=True)
                LocalDampMat = Alpha * LocalStifMat + Beta * LocalMassMat
                DampingMatrix.SetElmntryMatVal(LocalDampMat, ElmntIdx)
    DampingMatrix.Assemble()
    return DampingMatrix

def Assemble_GENEEFFORTS(StudyModel, DofNumbering, 
    ImposedDof, ImposedEfforts, IntegrationType, StrElmntry):
    GeneralizedEffort = datastr.Vector(DofNumbering, ImposedDof, 
        VectorType="GeneEfforts", ElmntsVects=StrElmntry)
    if ImposedEfforts is not None:
        if ImposedEfforts.HasFacesEfforts():
            __Assemble_FACESEFFORTS__(StudyModel, 
                ImposedEfforts, GeneralizedEffort, IntegrationType)
        if ImposedEfforts.HasEdgesEfforts():
            __Assemble_EDGESEFFORTS__(StudyModel, 
                ImposedEfforts, GeneralizedEffort, IntegrationType)
        if ImposedEfforts.HasNodesEfforts():
            __Assemble_NODESEFFORTS__(StudyModel, 
                ImposedEfforts, GeneralizedEffort, IntegrationType)
    GeneralizedEffort.Assemble()
    return GeneralizedEffort

def __Assemble_FACESEFFORTS__(StudyModel, ImposedEfforts, GeneralizedEffort, IntegrationType):
    StudyMesh       = StudyModel.GetModelMesh()
    DofNumbering    = GeneralizedEffort.GetDofNumbering()
    FiniteElmntList = StudyModel.GetModelFiniteElements()
    ImpEffortsGroups, EffortsArrays = ImposedEfforts.GetFacesEfforts()
    for ELmntType in FiniteElmntList:
        FaceDofNbr   = StudyModel.GetModelDofNbr() * ELmntType.GetFaceNodesNbr()
        FaceGeneEfrtArray = np.empty(FaceDofNbr, dtype=float)
        TemporaryMatrix    = np.empty((StudyModel.GetModelDofNbr(), FaceDofNbr), dtype=float)
        FaceTypeName = "All" if len(FiniteElmntList) == 1 else ELmntType.GetFacesType()
        GaussWeights, GaussFaceGrads = ELmntType.GetBoundaryGradAndWghtAtGauss(IntegrationType)
        GaussInterpolationMatrix     = ELmntType.GetBoundaryInterpFctAtGauss(IntegrationType)
        assert(TemporaryMatrix.shape == GaussInterpolationMatrix[0].shape)
        for EffortsGroupIdx, EffortsGroupName in enumerate(ImpEffortsGroups):
            GroupsIntersectionFaces = StudyMesh.GetGroupsIntersection(FaceTypeName, EffortsGroupName, Store=False, Type="Faces")
            if GroupsIntersectionFaces is None: continue
            EffortsArray = EffortsArrays[EffortsGroupIdx]
            for FaceIdx in GroupsIntersectionFaces:
                FaceGeneEfrtArray[:] = 0.0
                TemporaryMatrix[:,:]  = 0.0
                for GaussIdx, InterpolationMatrix in enumerate(GaussInterpolationMatrix):
                    FaceJacobian     = ComputeFaceJacobian(StudyMesh, FaceIdx, GaussFaceGrads[GaussIdx])
                    TemporaryMatrix += GaussWeights[GaussIdx] * InterpolationMatrix * FaceJacobian
                FaceGeneEfrtArray = np.matmul(TemporaryMatrix.transpose(), EffortsArray, out=FaceGeneEfrtArray)
                ElmntIdx, FaceLocalNumbring = DofNumbering.GetFaceLocalNumbring(FaceIdx)
                LocalGeneEfrtArrSize = DofNumbering.GetElmntDofsNbr(ElmntIdx)
                LocalGeneEfrtArray = np.zeros((LocalGeneEfrtArrSize,), dtype=float)
                LocalGeneEfrtArray[FaceLocalNumbring] = FaceGeneEfrtArray
                GeneralizedEffort.SetElmntryVectVal(LocalGeneEfrtArray, ElmntIdx)

def __Assemble_EDGESEFFORTS__(StudyModel, ImposedEfforts, GeneralizedEffort, IntegrationType):
    raise NotImplementedError("Not Implemented yet !")

def __Assemble_NODESEFFORTS__(StudyModel, ImposedEfforts, GeneralizedEffort, IntegrationType):
    raise NotImplementedError("Not Implemented yet !")

def getDampMatDerivative(StudyModel, Behaviour, DofNumbering, 
    ImposedDof, IntegrationType, StrgType, Parameter):
    if Parameter not in ["alpha", "beta"]:
        raise ValueError(("{} isn't a "
            "damping parameter").format(Parameter))
    elif Parameter == "alpha":
        FuncToCall = Assemble_STIFFMAT
    else: FuncToCall = Assemble_MASSMAT
    return FuncToCall(StudyModel, Behaviour, DofNumbering, 
        ImposedDof, IntegrationType, StrgType, None, False)

def getConstRelMatrix(Materials, Modeling, Parameter):
    Result = []
    if Parameter is not None:
        for Material in Materials:
            if not hasattr(Material, Parameter):
                raise ValueError(("The parameter {} isn't "
                    "defined for materials of type : {}"
                    "").format(Parameter, Material.GetType()))
            Result.append(Material.GetConstRelatDeriv(Modeling, Parameter))
    else: Result = [Material.GetConstRelat(Modeling) for Material in Materials]
    return Result

def getMassParameter(Materials, Parameter):
    if Parameter not in [None, "gho"]:
        raise ValueError(("{} isn't a mass"
        " parameter").format(Parameter))
    if Parameter is not None:
        return [1. for Material in Materials]
    else: return [Material.GetMassParam() for Material in Materials]