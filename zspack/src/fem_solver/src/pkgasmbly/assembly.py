"""This module implements the assembly function."""

import numpy as np
import timeit

from ..pkgmatbhv import constitutive_relation
from ..pkgbndary import boundary
from .. import model
from . import __3D_2D__

def Assemble(StudyModel, Behaviour  , ImposedDof=None, ImposedEfforts=None, 
             StiffnessMatrix=False  , StiffnessIntegType="Complete",
             MassMatrix=False       , MassIntegType="Complete", 
             GeneralizedEffort=False, GeneEfrtIntegType="Complete",
             DampingMatrix=False    , StorageType="Symmetric",
             Derivative=None        , DerivIntegType="Complete", 
             StoreElementry=False):
    """
    Computes finite elements matrices (stiffness, mass, damping), and
    the generalized efforts vector.

    Parameters
    ----------
    StudyModel : 'model object'
        The model object.
    
    ImposedDof : 'BoundCond object'
        The boundary conditions object.
    
    Behaviour : 'behavior object'
        The behavior object (describes the material field).
    
    ImposedEfforts : 'BoundLoad object'
        The boundary loads object.
    
    StiffnessMatrix : 'bool', optional
        If True, the stiffness matrix is returned (False by default).
    
    StiffnessIntegType : 'str', optional
        The stiffness matrix integration scheme. Two values are possible,
        "complete" (defeult) and "reduced".
    
    MassMatrix : 'bool', optional
        If True, the mass matrix is returned (False by default).
    
    MassIntegType : 'str', optional
        The mass matrix integration scheme. Two values are possible,
        "complete" (defeult) and "reduced".
    
    GeneralizedEffort : 'bool', optional
        If True, the generalized efforts vector is returned (False by default).
    
    GeneEfrtIntegType : 'str', optional
        the generalized efforts vector integration scheme. Two values are
        possible, "complete" (defeult) and "reduced".

    DampingMatrix : 'bool', optional
        If True, the damping matrix is returned (False by default).
    
    StorageType : 'str', optional
        The storage type to use for FE matrices. There three options :

            - "Symmetric" : only the upper triangular part is stored
                            as a one-dimensional array

            - "NonSymmetric" : the whole matrix is stored.

            - "Sparse" : Only non-zero elements are stored.
    
    StoreElementry : 'bool'
        If True, elementary matrices and vectors computed during assembly
        process are also stored.

    Derivative : None or 'srt'
        If not None, The derivative of the FE matrix with respect to 
        the parameter passed by this keyword is computed. In this case 
        only the FE matrix whos derivative must be computed should be asked.

    DerivIntegType : 'str', optional
        The derivative integration scheme. Two values are
        possible, "Complete" (defeult) and "Reduced".

    Return
    ------
        A tuple that contains requested objects by the user is returned (even
        when only one object is requested). Objects order within the function
        declaration is respected.

    """
    assert(isinstance(StudyModel, model.model))
    assert(isinstance(Behaviour, constitutive_relation.behavior))
    
    assert(StiffnessIntegType in ["Complete", "Reduced"])
    assert(MassIntegType in ["Complete", "Reduced"])
    assert(GeneEfrtIntegType in ["Complete", "Reduced"])
    assert(DerivIntegType in ["Complete", "Reduced"])

    if ImposedDof is not None:
        assert(isinstance(ImposedDof, boundary.BoundCond))
        DofNumbering = ImposedDof.GetDofNumbering()
    else: DofNumbering = model.DofNumbering(StudyModel)
    
    if not DofNumbering.IsUpdatingFinalized():
        DofNumbering.FinalizeUpdating()
    
    if ImposedEfforts is not None:
        assert(isinstance(ImposedEfforts, boundary.BoundLoad))
        assert(GeneralizedEffort)
    # elif GeneralizedEffort:
    #     print("Warning: a null generalized efforts vector will be assembled !")
    
    assert(StiffnessMatrix or MassMatrix \
        or DampingMatrix or GeneralizedEffort)
    
    if Derivative is not None:
        assert(isinstance(Derivative, str))
        assert(not (GeneralizedEffort or StoreElementry))
        assert((StiffnessMatrix + MassMatrix + DampingMatrix) == 1)
    
    if Behaviour.GetBehaviorMesh() != StudyModel.GetModelMesh():
        raise ValueError("The model and the behavior are not defined on the same mesh !")
    elif ImposedDof is not None and ImposedDof.GetModel() != StudyModel:
        raise ValueError("Dofs are not imposed on the model provided as a parameter !")
    elif ImposedEfforts is not None and ImposedEfforts.GetModel() != StudyModel:
        raise ValueError("Dofs are not imposed on the model provided as a parameter !")

    result = ()
    
    if StiffnessMatrix:
        if Derivative is not None: StiffnessIntegType = DerivIntegType
        # If the damping matrix will be computed, stiffness elementary 
        # matrices must be stored regardless of the parameter StoreElementry's 
        # value. If this latter is False elementry matrices ares removed once 
        # the damping matrix computed.
        StrElmntryStiff = True if DampingMatrix else StoreElementry
        
        result += AssembleStiffnessMatrix(StudyModel, Behaviour, DofNumbering, 
            ImposedDof, StiffnessIntegType, StorageType, Derivative, StrElmntryStiff),
        
        result[-1].SetBehavior(Behaviour)
    
    if MassMatrix:
        if Derivative is not None: MassIntegType = DerivIntegType
        # If the damping matrix will be computed, mass elementary matrices must 
        # be stored regardless of the parameter StoreElementry's value. If this 
        # latter is False elementry matrices ares removed once the damping matrix 
        # computed.
        StrElmntryMass = True if DampingMatrix else StoreElementry
        
        result += AssembleMassMatrix(StudyModel, Behaviour, DofNumbering, 
            ImposedDof, MassIntegType, StorageType, Derivative, StrElmntryMass),
        
        result[-1].SetBehavior(Behaviour)
    
    if DampingMatrix:
    
        if not StiffnessMatrix and Derivative is None:
            raise ValueError("Damping matrix assembly "
                "requires the assembly of stiffness matrix.")
        elif not MassMatrix and Derivative is None:
            raise ValueError("Damping matrix assembly "
                "requires the assembly of mass matrix.")
        
        StiffMat = None if Derivative is not None else result[0]
        MassMat  = None if Derivative is not None else result[1]

        result += AssembleDampingMatrix(StudyModel, Behaviour, DofNumbering, ImposedDof, 
            DerivIntegType, MassMat, StiffMat, StorageType, Derivative, StoreElementry),
        
        result[-1].SetBehavior(Behaviour)

        if not StoreElementry and Derivative is None:
            # If StoreElementry is False, stiffness elementry matrices 
            # used during damping matrix computation must be removed.
            StiffMat.DelElmntsMatsBuff()
            # If StoreElementry is False, mass elementry matrices used 
            # during damping matrix computation must be removed.
            MassMat.DelElmntsMatsBuff()
    
    if GeneralizedEffort:
        result += AssembleGeneralizedEffort(StudyModel, DofNumbering, 
            ImposedDof, ImposedEfforts, GeneEfrtIntegType, StoreElementry),
    
    if Derivative is not None: assert(len(result) == 1)
    return result if len(result) != 1 else result[0]

def AssembleStiffnessMatrix(StudyModel, Behaviour, DofNumbering, 
    ImposedDof, IntegrationType, StorageType, Derivative, StoreElementry):
    if StudyModel.GetModeling() in ["3D", "2D"]:
        return __3D_2D__.Assemble_STIFFMAT(StudyModel, Behaviour, DofNumbering, 
            ImposedDof, IntegrationType, StorageType, Derivative, StoreElementry)
    else: raise NotImplementedError("Not Implemented yet !")

def AssembleMassMatrix(StudyModel, Behaviour, DofNumbering, 
    ImposedDof, IntegrationType, StorageType, Derivative, StoreElementry):
    if StudyModel.GetModeling() in ["3D", "2D"]:
        return __3D_2D__.Assemble_MASSMAT(StudyModel, Behaviour, DofNumbering, 
            ImposedDof, IntegrationType, StorageType, Derivative, StoreElementry)
    else: raise NotImplementedError("Not Implemented yet !")

def AssembleDampingMatrix(StudyModel, Behaviour, DofNumbering, ImposedDof, 
    IntegrationType, MassMatrix, StiffMatrix, StorageType, Derivative, StoreElementry):
    if StudyModel.GetModeling() in ["3D", "2D"]:
        return __3D_2D__.Assemble_DAMPMAT(StudyModel, Behaviour, DofNumbering, ImposedDof, 
            IntegrationType, MassMatrix, StiffMatrix, StorageType, Derivative, StoreElementry)
    else: raise NotImplementedError("Not Implemented yet !")

def AssembleGeneralizedEffort(StudyModel, DofNumbering, 
    ImposedDof, ImposedEfforts, IntegrationType, StoreElementry):
    if StudyModel.GetModeling() in ["3D", "2D"]:
        return __3D_2D__.Assemble_GENEEFFORTS(StudyModel, DofNumbering, 
            ImposedDof, ImposedEfforts, IntegrationType, StoreElementry)
    else: raise NotImplementedError("Not Implemented yet !")

