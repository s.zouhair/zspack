"""
This module implements the mesh class, and all the methods that allow
interacting with it.

"""

import numpy as np
from functools import reduce
import timeit

from . import pkgutils as utils

class mesh:
    """
    The mesh class.

    Parameters
    ----------
    NodesNbr : 'int'
        The mesh nodes number.
    
    ElmntsNbr : 'int'
        The mesh elements number.
    
    Dimension : 'int'
        The mesh dimension (1, 2 or 3).

    Coordinates : 'np.ndarray'
        A 2D array that contains the nodes coordinates, its shape must be
        equal to (NodesNbr, Dimension). Each row corresponds to a node, 
        and each node is labeled (indexed) by its row number.
    
    Connectivity : 'np.ndarray'
        A 2D array that describes the connectivity of the mesh, its shape
        must be equal to (ElmntsNbr, *). Each row corresponds to an 
        element, and each element is labeled (indexed) by its row number. 
        For a given element, the first column contains its number of nodes,
        the columns that follows contains the indices of these nodes.
    
    Groups : 'dict'
        A list of groups to add to the mesh. Here's how a group is 
        declared :
        
                      group_name = (group_type, items_list)
        
            - group_name : the name of the group, it must not contains 
                           the character "." or start with the string 
                           "Node".
            
            - group_type : the type of the group, a single character.
                           'N' for a nodes group, 'E' for an elements
                           group and 'F' for a faces group (edges groups 
                           are not implemented yet).
            
            - items_list : a list or a tuple that contains the items of
                           the group. For nodes/elements groups, each item
                           is an integer that represents a node/element 
                           (its row index in the Coordinates/Connectivity 
                           array). For faces groups, each face (item) is 
                           described by a tuple containing its nodes.
                           Nb : nodes/elements indices must start at 0.
        
        Examples :
            
            - group_1 = ('N', [73, 41, 3, 55, 24])
            
            - group_2 = ('E', (98, 14, 33, 19, 28))
            
            - group_3 = ('F', [(13, 5, 22), (61, 31, 50), (0, 7, 21)])
    
    Attributes
    ----------
    NodesNbr : 'int'
        The number of nodes.
    
    ElmntsNbr : 'int'
        The number of elements.
    
    Dimension : 'int'
        The mesh dimension.
    
    Coordinates : 'np.ndarray'
        The coordinates array.
    
    Connectivity : 'np.ndarray'
        The connectivity array.
    
    FacesNbr : 'int'
        The number of unique faces added to the mesh through faces groups.
        Nb : only boundary faces are allowed to be added.
    
    Faces : 'np.ndarray'
        The array where faces are stored, its shape is (FacesNbr, 2). Each
        row correspond to a face, and each face is labeled (indexed) by its
        row number. Although faces are added using their nodes list, only 
        two integers are used to store them, the first one is the face 
        element (since a boundary face belongs exactly to one element), and
        the second one is its local numbering among the element's faces.
    
    EdgesNbr : 'int'
        The number of unique edges added to the mesh through edges groups.
        Nb : only boundary edges are allowed to be added. (Not implemented yet)
    
    Edges : 'np.ndarray'
        The array where edges are stored. (Not implemented yet)
    
    __ElmntsType__ : 'str'
        A string used to store the type of elements if the mesh contains only
        one type of elements. Otherwise it's None (the mesh contains elements
        of different types).
        
        Examples :
            
            - "3D.4N" : If the mesh contains only tetrahedral elements with 
                        4 nodes.
            
            - "3D.8N" : If the mesh contains only hexahedral elements with 
                        8 nodes.
    
    __ElmntsTypeGroups__ : 'dict'
        If the mesh contains only one type of elements, __ElmntsTypeGroups__
        is None. Otherwise it's a dictionary whose each key is an element 
        type, and the corresponding value is an array that contains all the 
        elements that matches that type.
        
        Examples :
        
            - __ElmntsTypeGroups__["3D.4N"] = np.array([9, 41, 53, 79, 83]) :
            9, 41, 53, 79 and 83 are row indices in mesh.Coordinates, all 
            corresponding elements are tetrahedrons.
    
    __FacesType__ : 'str'
        A string used to store the type of faces if the mesh contains only
        one type of elements (consequently one type of faces). Otherwise it's 
        None (the mesh contains elements/faces of different types).
        
        Examples :
        
            - "3D.3N.F" : If the mesh contains only tetrahedral elements with 
                          4 nodes (faces contains 3 nodes).
        
            - "3D.4N.F" : If the mesh contains only hexahedral elements with 
                          8 nodes (faces contains 4 nodes).
    
    __FacesTypeGroups__ : 'dict'
        If the mesh contains only one type of faces, __FacesTypeGroups__ 
        is None. Otherwise it's a dictionary whose each key is a face type,
        and the corresponding value is an array that contains all the added 
        faces that matches that type (__FacesTypeGroups__ is updated whenever
        a new face is added to the mesh).
        
        Examples :
        
            - __FacesTypeGroups__["3D.4N.F"] = np.array([10, 5, 17]) : such 
            that 10, 5 and 17 are row indices in mesh.Faces, all corresponding
            faces are quadrangles.
    
    __EdgesType__ : 'str'
        A string used to store the type of edges (since a mesh contains only
        one type of edges, otherwise it's a non conforming mesh).
        
        Examples :
        
            - "3D.2N.E" : If the mesh contains 2 nodes edges.
        
            - "2D.3N.E" : If the mesh contains 3 nodes edges.
    
    NodesGroups : 'dict'
        A dictionary whose each key is a nodes group, and the corresponding 
        value is an array that contains all the nodes that belongs to that 
        group.
        
        Examples :
        
            - NodesGroups["group_1"] = np.array([73, 41, 3, 55, 24]) : such 
            that 73, 41, 3, 55 and 24 are row indices of group_1's nodes in
            mesh.Coordinates.
    
    ElmntsGroups : 'dict'
        A dictionary whose each key is an elements group, and the corresponding 
        value is an array that contains all the elements that belongs to that 
        group.
        
        Examples :
        
            - ElmntsGroups["group_2"] = np.array([98, 14, 33, 19, 28]) : such 
            that 98, 14, 33, 19 and 28 are row indices of group_2's elements 
            in mesh.Connectivity.
    
    FacesGroups : 'dict'
        A dictionary whose each key is an faces group, and the corresponding 
        value is an array that contains all the faces that belongs to that 
        group.
        
        Examples :
        
            - FacesGroups["group_3"] = np.array([0, 1, 2]) : such that
            0, 1 and 2 are row indices in mesh.Faces, where the faces (defined
            by their nodes lists) (13, 5, 22), (61, 31, 50) and (0, 7, 21) were
            inserted.
    
    
    __NodesTempGroups__ : 'dict'
        A temporary dictionary used to store nodes groups resulting from a 
        union or intersection of other nodes groups. keys are of the form :
        
                  ".N.Group.0", ".N.Group.1", ".N.Group.2", ...
    
    __ElmntsTempGroups__ : 'dict'
        A temporary dictionary used to store elements groups resulting from a 
        union or intersection of other elements groups. keys are of the form :
        
                  ".E.Group.0", ".E.Group.1", ".E.Group.2", ...
    
    __FacesTempGroups__ : 'dict'
        A temporary dictionary used to store faces groups resulting from a 
        union or intersection of other faces groups. keys are of the form :
        
                  ".F.Group.0", ".F.Group.1", ".F.Group.2", ...

    """

    def __init__(self, NodesNbr, ElmntsNbr, Dimension, Coordinates, Connectivity, **Groups):
        
        assert(isinstance(NodesNbr, int) and NodesNbr > 0)
        self.NodesNbr  = NodesNbr

        assert(isinstance(ElmntsNbr, int) and ElmntsNbr > 0)
        self.ElmntsNbr = ElmntsNbr

        assert(Dimension in [1,2,3])
        self.Dimension = Dimension

        # Un tableau (vide pour le moment), qui contiendra les faces (des 
        # éléments) ajoutées par l'utilisateur (pour appliquer des efforts
        # surfacique par exemple). Chaque ligne correspond à une face, et 
        # chaque face est décrite par deux entier (donc il y'a deux colonnes
        # dans le tableau). La 1ère colonne contient le numéro de l'élément
        # (numérotation à 0), la 2ème le numéro local de la face.
        self.Faces    = None
        self.FacesNbr = 0

        self.Edges    = None
        self.EdgesNbr = 0

        assert(isinstance(Coordinates, np.ndarray) and Coordinates.shape == (NodesNbr, Dimension))
        assert(isinstance(Connectivity, np.ndarray) and Connectivity.shape[0] == ElmntsNbr)

        self.Coordinates  = Coordinates
        self.Connectivity = Connectivity

        self.__ElmntsType__       = None
        self.__ElmntsTypeGroups__ = None

        self.__FacesType__        = None
        self.__FacesTypeGroups__  = None

        self.__EdgesType__        = None
        
        self.ExtractElmntsTypes()

        self.NodesGroups  = {}
        self.ElmntsGroups = {}
        self.FacesGroups  = {}
        self.AddGroups(**Groups)

        self.__NodesTempGroups__  = None
        self.__ElmntsTempGroups__ = None
        self.__FacesTempGroups__  = None

    def AddGroups(self, **Groups):
        """Allows adding new groups to the mesh"""
        for GroupName, (GroupType, GroupList)  in Groups.items():
            assert(GroupType in ['N', 'E', 'F'])
            if '.' in GroupName:
                raise ValueError("Group name mustn't contain the character '.'")
            elif GroupName in {**self.NodesGroups, **self.ElmntsGroups, **self.FacesGroups}:
                raise ValueError("There is already a group called %s" % GroupName)
            elif GroupName.startswith("Node"):
                raise ValueError("Groups names must not start with 'Node' : %s" % GroupName)
            elif GroupType == 'N':
                self.AddNodesGroup(GroupName, GroupList)
            elif GroupType == 'E':
                self.AddElmntsGroup(GroupName, GroupList)
            else:
                self.AddFacesGroup(GroupName, GroupList)

    def AddNodesGroup(self, GroupName, GroupList):
        """
        Allows adding a new nodes group to the mesh. It should not be
        used (use AddGroups(...) instead). It is only used within the
        function AddGroups(...) under strict assumptions.
        
        """
        TempArray = np.array(GroupList, dtype='int')
        if not TempArray.size:
            # On vérifie que la liste des noeuds du groupe n'est pas vide.
            raise ValueError("The nodes list of the group %s is empty" % GroupName)
        elif np.any(TempArray < 0):
            # On vérifie si la liste des noeuds du groupe contient des éléments négatifs.
            raise ValueError("The nodes list of the group %s contains negative values" % GroupName)
        elif np.amax(TempArray) >= self.NodesNbr:
            # On vérifie que les numéros de noeuds du nouveau groupe
            # ne dépasse pas le nombre de noeuds du maillage
            raise ValueError("Nodes numbers should not exceed %d" % (self.NodesNbr - 1))
        else:
            # On vérifie s'il y a une redondance dans la liste des items (noeuds/éléments) du groupe.
            assert(TempArray.shape == np.unique(TempArray).shape)
        self.NodesGroups[GroupName] = TempArray

    def AddElmntsGroup(self, GroupName, GroupList):
        """
        Allows adding a new elements group to the mesh. It should not be
        used (use AddGroups(...) instead). It is only used within the
        function AddGroups(...) under strict assumptions.
        
        """
        TempArray = np.array(GroupList, dtype='int')
        if not TempArray.size:
            # On vérifie que la liste des éléments du groupe n'est pas vide.
            raise ValueError("The elements list of the group %s is empty" % GroupName)
        elif np.any(TempArray < 0):
            # On vérifie si la liste des éléments du groupe contient des éléments négatifs.
            raise ValueError("The elements list of the group %s contains negative values" % GroupName)
        elif np.amax(TempArray) >= self.ElmntsNbr:
            # On vérifie que les numéros des éléments du nouveau groupe
            # ne dépasse pas le nombre d'éléments du maillage
            raise ValueError("Elements numbers should not exceed %d" % (self.ElmntsNbr - 1))
        else:
            # On vérifie s'il y a une redondance dans la liste des éléments du groupe.
            assert(TempArray.shape == np.unique(TempArray).shape)
        self.ElmntsGroups[GroupName] = TempArray

    def AddFacesGroup(self, GroupName, GroupList):
        """
        Allows adding a new faces group to the mesh. It should not be
        used (use AddGroups(...) instead). It is only used within the
        function AddGroups(...) under strict assumptions.
        
        """
        if not self.HasFaces():
            print("The mesh has no faces, it is only %d-dimensional" % self.Dimension)
        assert(isinstance(GroupList, (list, tuple)))
        if not len(GroupList):
            # On vérifie que la liste des Faces du groupe n'est pas vide.
            raise ValueError("The Faces list of the group %s is empty" % GroupName)
        for l in GroupList:
            assert(isinstance(l, tuple))
        # On regroupe les noeuds de tt les faces dans un seul np.array pour pouvoir 
        # faire des vérifications générales
        TempArray = np.array(reduce(lambda l1, l2: l1 + l2, GroupList), dtype='int')
        if np.any(TempArray < 0):
            # On vérifie si la liste des noeuds des faces du groupe contient des éléments négatifs.
            raise ValueError("The faces nodes's lists of the group %s contains negative values" % GroupName)
        elif np.amax(TempArray) >= self.NodesNbr:
            # On vérifie que les numéros de noeuds des faces du nouveau groupe
            # ne dépasse pas le nombre de noeuds du maillage
            raise ValueError("Nodes numbers should not exceed %d" % (self.NodesNbr - 1))
        else:
            # On vérifie s'il y a une redondance dans la liste des faces du groupe.
            assert(len(GroupList) == len(list(set(GroupList))))
        del TempArray
        self.FacesGroups[GroupName] = np.array([self.__GetFaceNbr__(face) for face in GroupList], dtype='int')
    
    # La fonction ci-dessous permet d'enregistrer une nouvelle face dans les objet du maillage (Faces et 
    # peut etre __FacesTypeGroups__). La face doit etre décrite par la liste des numéros globaux de ses 
    # noeuds (FaceNodesList). Cette fonction s'occupe de vérifier si cette liste de noeuds correspond 
    # bien à une face dans le maillage, elle vérifie aussi s'il s'agit d'une face au bord du maillage, 
    # si ce n'est pas le cas une exception est levée. Si tt se passe bien, GetFaceNbr permet de déterminer 
    # l'indice de l'élément de la face ainsi que la numérotation locale de cette face dans cet l'élément, 
    # ces deux information ainsi que le type de l'élément sont utilisés pour renvoyé un appel à AddFace 
    # qui s'occupe du reste du taff.
    def __GetFaceNbr__(self, FaceNodesList):
        """
        Allows adding new face to the mesh. The face must belong to the 
        boundary of the mesh, otherwise a ValueError exception is raised (It's
        also raised in other circumstances).

        Parameters
        ----------
        FaceNodesList : 'tuple'
            The nodes list of the face.

        Returns
        -------
        integer
            The row index of the face in the mesh.Faces 2D array.

        """

        assert(isinstance(FaceNodesList, tuple))
        FaceSize = len(FaceNodesList)
        if FaceSize != len(set(FaceNodesList)):
            raise ValueError("There is a redundancy in the nodes list of the face {}".format(FaceNodesList))
        ElmntType = utils.GetFaceElmnt(self.Dimension, FaceSize, ReturnElmntSize=False)
        ElmntFacesArray = utils.GetFacesByElmntType(ElmntType)
        if self.__ElmntsType__:
            assert(self.__ElmntsType__ == ElmntType)
            ElmntsIterator = range(self.ElmntsNbr)
        elif ElmntType in self.__ElmntsTypeGroups__:
            ElmntsIterator = self.__ElmntsTypeGroups__[ElmntType]
        else:
            raise ValueError("The mesh doesn't contains faces with %d nodes." % FaceSize)
        AlreadyFoundElmnt = False
        FoundElmntIdx     = -1
        FoundLocalFaceIdx = -1
        for ElmntIdx in ElmntsIterator:
            FaceLocalIdx = self.__GetLocalFaceNbr__(ElmntIdx, ElmntFacesArray, FaceNodesList)
            if FaceLocalIdx:
                if not AlreadyFoundElmnt:
                    AlreadyFoundElmnt = True
                    FoundElmntIdx     = ElmntIdx
                    FoundLocalFaceIdx = FaceLocalIdx - 1
                else:
                    raise ValueError("The face {} does not belong to the boundary of the mesh".format(FaceNodesList))
        if FoundElmntIdx == -1:
            assert(FoundLocalFaceIdx == -1)
            raise ValueError("The face {} does not belong to the mesh".format(FaceNodesList))
        FaceName = utils.GetFaceNameByElmntType(ElmntType)
        return self.__AddFace__(FoundElmntIdx, FoundLocalFaceIdx, FaceName)

    # Cette fonction permet d'enregistrer une nouvelle face das les objets du maillage (Faces et __FacesTypeGroups__), 
    # Pour ce, la face doit etre définie par l'indice de son élément ElmntIdx, ainsi que sa numérotation locale 
    # LocalFaceNbr au seins de cet élément. Le type de la face FaceName (son nom) doit aussi etre fournit. Si la meme 
    # face a déjà été enregistrer dans le maillage, le numéro qui lui a été attribuer durant son premier enregistrement 
    # est renvoyé, sinon la face est rajoutée dans Faces et dans le groupe de faces associé à son type (seulement si le 
    # maillage contient au moins deux type différent de faces Ex : des faces à 3 noeuds et d'autres à 4). Ainsi le numéro 
    # attribué a cette nouvelle face est renvoyé (l'indice de la ligne qui lui correspond dans Faces).
    def __AddFace__(self, ElmntIdx, LocalFaceNbr, FaceName):
        """
        It should not be used. It is only used within the function 
        AddFacesGroup(...) under strict assumptions.
        
        """
        if self.Faces is None:
            self.Faces = np.array([ElmntIdx, LocalFaceNbr], dtype='int').reshape((1,2))
            result = 0
        else:
            ElmntFacesIdx = np.nonzero(self.Faces[:,0]==ElmntIdx)[0]
            if ElmntFacesIdx:
                ElmntFacesLocalIdx = self.Faces[ElmntFacesIdx, 1]
                SameFace = np.nonzero(ElmntFacesLocalIdx == LocalFaceNbr)[0]
                if SameFace:
                    assert(SameFace.size == 1)
                    return ElmntFacesIdx[SameFace[0]]
            NewFace = np.array([ElmntIdx, LocalFaceNbr], dtype='int').reshape((1,2))
            self.Faces = np.concatenate((self.Faces, NewFace), axis=0)
            result = self.Faces.shape[0] - 1
        if self.__FacesType__ is None:
            self.__FacesTypeGroups__[FaceName] = np.append(self.__FacesTypeGroups__[FaceName], result)
        self.FacesNbr += 1
        return result


    # La fonction ci-dessous vérifie si les noueds de la liste FaceNodesList représente 
    # une face de l'élément d'indice ElmntIdx dans le maillage (indexation à zéro) et de 
    # type ElmntType. Si c'est le cas, le numéro local de la face (décalé de 1 "+1") dans 
    # l'élément est renvoyé (Sa ligne dans le np.array FEcata.Faces[ElmntType]). Si la 
    # liste des noeuds FaceNodesList n'est pas une face de l'élément ElmntIdx, la valeur 0 
    # est renvoyée (ainsi elle peut etre utilisée comme booléen).
    def __GetLocalFaceNbr__(self, ElmntIdx, ElmntFacesArray, FaceNodesList):
        """
        It should not be used. It is only used within the function 
        AddFacesGroup(...) under strict assumptions.

        """
        ElmntNodesList = self.GetElmntNodesList(ElmntIdx)
        LocalIdxList = []
        LocalFaceNbr = -1
        for NodeIdx in FaceNodesList:
            NodeLocalIdx = np.nonzero(ElmntNodesList==NodeIdx)[0]
            if NodeLocalIdx.size:
                LocalIdxList.append(NodeLocalIdx[0] + 1)
            else:
                return 0
        LocalIdxList.sort()
        for RowIdx in range(ElmntFacesArray.shape[0]):
            if LocalIdxList == sorted(list(ElmntFacesArray[RowIdx,:])):
                LocalFaceNbr = RowIdx
                break
        assert(LocalFaceNbr != -1)
        return LocalFaceNbr + 1


    def ExtractElmntsTypes(self):
        """
        This function allows to determine the edges type of the mesh and
        set up the variable mesh.__EdgesType__ (since a mesh must contains only
        one edges type otherwise it's not a conforming mesh). 

        It determines also the different types of elements and faces of the 
        mesh. If there is only one type, the variables mesh.__ElmntsType__ and
        mesh.__FacesType__ are respectively set up to the corresponding element
        and face types. Otherwise the two dictionaries mesh.__ElmntsTypeGroups__
        and mesh.__FacesTypeGroups__ are created to store respectively for each 
        type (types are used as keys), the list of elements and faces that 
        shares the same types (as np.ndarray).
        
        """
        # On extrait la première colonne de Connectivity qui contient le nombre de noeuds par élément
        FirstColumn = self.Connectivity[:,0]
        # TempArray contient les différentes tailles/types des éléments du maillage (sans redondance),
        # et Elmntstype indique le type de chaque élément (l'indice dans TempArray)
        TempArray, ElmntsType = np.unique(FirstColumn, return_inverse=True)
        if TempArray.size != 1:
            # Si le maillage contient des éléments avec des nombres de noeuds différents :
            # On construit les noms des différents groupes (par type) d'éléments
            names = [utils.GetElmntType(self.Dimension, i) for i in TempArray]
            # On construit les groupes (avec des listes vides)
            self.__ElmntsTypeGroups__ = {name : [] for name in names}
            # On affecte les éléments a leurs groupes
            for ElmntIdx, TypeIdx in enumerate(ElmntsType):
                self.__ElmntsTypeGroups__[names[TypeIdx]].append(ElmntIdx)
            # On remplace les listes d'éléments par des np.array (pour économiser un peu de mémoire)
            self.__ElmntsTypeGroups__ = {name : np.array(ElmntsList, dtype=int) for name, ElmntsList in self.__ElmntsTypeGroups__.items()}
            if self.HasFaces():
                # Finalement on construit le dictionnaire des type de face (chaque type d'élément a un type de face)
                FacesNames = [utils.GetFaceNameByElmntType(name) for name in names]
                self.__FacesTypeGroups__ = {FaceName : np.array([], dtype='int') for FaceName in FacesNames}
            if self.HasEdges():
                EdgesNames = [utils.GetEdgeNameByElmntType(name) for name in names]
                if len(set(EdgesNames)) > 1:
                    raise ValueError("The mesh does not conform !!")
                self.__EdgesType__ = EdgesNames[0]
        else:
            self.__ElmntsType__ = utils.GetElmntType(self.Dimension, TempArray[0])
            if self.HasFaces():
                self.__FacesType__  = utils.GetFaceNameByElmntType(self.__ElmntsType__)
            if self.HasEdges():
                self.__EdgesType__ = utils.GetEdgeNameByElmntType(self.__ElmntsType__)

        # Cette fonction permet de vérifier si les groupes dans GroupList de type 'Type = Elmnts / Nodes'
        # forment une partition des éléments/noeuds du maillage
    def CheckPartition(self, *GroupList, Type='Elmnts'):
        """
        This function allows to determine whether the groups list 
        GroupList is a partition of the elements/nodes of the mesh. The
        groups type should match the value of the keyword Type.
        
        Nb : Only user defined groups are allowd (Temporary and type 
        groupes aren't)
        
        Returns
        -------
        bool
            True if the groups list GroupList is a partition, False 
            otherwise.

        """
        assert(Type in ['Elmnts', 'Nodes'])
        SizesSum = 0
        GroupsUnion = np.array([])
        Target, SetSize = (self.ElmntsGroups, self.ElmntsNbr) if Type=='Elmnts' else (self.NodesGroups, self.NodesNbr)
        for Group in GroupList:
            assert(isinstance(Group, str))
            if Group not in Target:
                raise ValueError("The mesh doesn't contain any %s group called %s" % (Type, Group))
            else:
                SizesSum += Target[Group].size
                GroupsUnion = np.union1d(GroupsUnion, Target[Group])
        # Pour vérifier que les groupes de GroupList forme une partition des éléments/noeuds du maillage, on 
        # vérifie que la somme de leurs tailles (SizesSum) est égale à la taille de leur union (GroupsUnion.size) 
        # (càd que leurs intersections sont nulles), et égale aussi au nombre d'éléments/noeuds (SetSize).
        return SetSize == SizesSum == GroupsUnion.size

    # Renvoie un np.array (2d) contenant les coordonées des noeuds de l'élément 
    # numéro ElmntIdx (numérotation commence à 0). Chaque ligne correspond à 
    # un noeud, et chaque colonne à la coordonnée suivant une dimenssion. Le paramètre
    # Transp est un booléen qui permet de spécifier si on veut que la transposée 
    # soit renvoyée (il est 'True' par défaut)
    def GetElmntCoord(self, ElmntIdx, Transp=True):
        """
        Returns an array that contains the coordinates of the element
        ElmntIdx. By default (Transp=True) each column correspond to a 
        node, if Transp=False each row correspond to a node. The nodes 
        order is the same as their order in the Connectivity array.
        
        """
        assert(isinstance(Transp, bool))
        # On extrait la liste des noeuds de l'élément
        ElmntNodesList = self.GetElmntNodesList(ElmntIdx)
        # On extrait finalement les coordonnées des noeuds de l'élément
        result = self.GetNodesCoords(ElmntNodesList)
        # On renvoie le résultat tout en tenant compte du paramètre Transp
        return result.transpose() if Transp else result

    # Renvoie le nombre de noeud de l'élément numéro ElmntIdx
    def GetElmntNodesNbr(self, ElmntIdx):
        """Returns the number of nodes of the element ElmntIdx"""
        assert(ElmntIdx < self.ElmntsNbr)
        return self.Connectivity[ElmntIdx, 0]
    
    # Renvoie un np.array contenant les numéros des noeuds de l'élément ElmntIdx
    def GetElmntNodesList(self, ElmntIdx):
        """
        Returns an array tnat contains the nodes list of the element
        ElmntIdx
        
        """
        ElmntNodesNbr = self.GetElmntNodesNbr(ElmntIdx)
        return self.Connectivity[ElmntIdx, 1:ElmntNodesNbr+1]

    # Renvoie les coordonnées des noeuds du np.array NodesList (une ligne par noeud)
    def GetNodesCoords(self, NodesList):
        """
        Returns an array that contains the coordinates of the nodes in the 
        array NodesList, each row correspond to a node. The nodes order is the
        same as their order in the NodesList array.
        
        """
        # On vérifie que NodesList est bien un np.array à 1 dimenssion.
        assert(isinstance(NodesList, np.ndarray) and len(NodesList.shape) == 1)
        return self.Coordinates[NodesList, :]

    def GetMeshCoords(self):
        """
        Returns the mesh coordinates array.
        
        """
        return self.Coordinates
    
    def GetMeshConnect(self):
        """
        Returns the mesh connectivity array.
        
        """
        return self.Connectivity

    # Cette fonction calcul l'union des groupes dans GroupsList, elle renvoie le nom du 
    # nouveau groupe créé pour stocker le résultat de cette opération. Si GroupsList 
    # contient la valeur "All", qui est un groupe particulier contenant tt les noeuds (ou 
    # éléments / faces) du maillage, il est claire que le résultat de l'opération est 
    # aussi "All", dans ce cas il est renvoyé.
    # La list des groupes GroupsList peut contenir tt sorte de groupes, mm les groupes des 
    # types d'éléments ou de faces. Par contre s'il n y'a qu'un type d'élément / face (càd 
    # que tt les éléments/faces du maillage sont du mm type), il ne faut pas mettre le type 
    # d'éléments/faces (qui sont stokés respectivement dans self.__ElmntsType__ et self.__FacesType__)
    # dans la liste GroupsList, mais il faut le remplacer par "All". De toute facons, si cela
    # est fait une assertion interceptera cette erreur.
    def GetGroupsUnion(self, *GroupsList, Store=True):
        """
        Returns the union of the groups of the list GroupsList. The
        type of the groups is determined by the function. Type groups 
        are not allowed only if the mesh contains at least two different
        types of elements/faces. (In other words mesh.__ElmntsType__ and
        mesh.__FacesType__ are not allowed). Single nodes groups (nodes 
        groups that contains only one node, their names are in the form 
        Node* such that * is the node index) are also not allowed.

        Nb : a special group is allowed, its name is "All", it consists 
             of all elements/nodes/faces of the mesh. If used, GroupsList 
             must containes at least one other different group, otherwise 
             the function cannot determine the type of the groups and therefor 
             which type "All" refer to (elements, nodes or faces).
        
        Returns
        -------

            - Store == False : If there is at least one occurence of "All" 
              in GroupsList, an iterator over all the elements/nodes/faces
              of the mesh is returned. The same iterator is returned if the 
              union of the groups in GroupsList containes all the elements/
              nodes/faces of the mesh. Otherwise an array that contains the
              result (elements/nodes/faces list) is returned.
            
            - Store == True  : If there is at least one occurence of "All" 
              in GroupsList, "All" is returned. It's also returned if the 
              union of the groups in GroupsList containes all the elements/
              nodes/faces of the mesh. If GroupsList contains only one group 
              (different than "All"), its name is returned. Otherwise a 
              temporary group is created and its name is returned. The new 
              group is stored in one of the 3 available temporary groups 
              dictionaries according to its type. Its name is used as key, 
              and its value is the resulting array of elements/nodes/faces. 

        """
        if not len(GroupsList):
            raise ValueError("At least one group is required for this operation.")
        # Suppression des redondances
        GroupsList = list(set(GroupsList))
        GroupsType = self.GetGroupsType(*GroupsList)
        assert(GroupsType is not None)
        if "All" not in GroupsList:
            if len(GroupsList) > 1:
                if GroupsType == "Elmnts" and self.__ElmntsType__:
                    assert(self.__ElmntsType__ not in GroupsList)
                elif GroupsType == "Faces" and self.__FacesType__:
                    assert(self.__FacesType__ not in GroupsList)
                elif GroupsType == "Nodes":
                    for GroupName in GroupsList:
                        assert(GroupName[:4] != "Node")
                GroupsItemsList  = [self.GetGroupItems(group, GroupType=GroupsType) for group in GroupsList]
                GroupsUnionItems = reduce(np.union1d, GroupsItemsList)
                if GroupsUnionItems.size != self.GetNumberof(GroupsType):
                    return self.__AddTempGroup__(GroupsUnionItems, GroupsType) if Store else GroupsUnionItems
            else:
                # S'il y a un seul groupe dans GroupsList (différent de "All").
                return GroupsList[0] if Store else self.GetGroupItems(GroupsList[0], GroupType=GroupsType)
        # Si GroupsList contient "All"
        return "All" if Store else self.GetGroupItems("All", GroupType=GroupsType)

    # Cette fonction doit tjr etre appelée sans passer l'argument Type (car il est utilisé seulement 
    # pour passer la valeure de Type calculée durant le premier appel au autres appels recursifs). 
    # Si l'intersection des groupes dans GroupsList est nulle, None est renvoyé
    def GetGroupsIntersection(self, *GroupsList, Store=True, Type=None):
        """
        Returns the intersection of the groups of the list GroupsList.
        The type of the groups is determined by the function (the use of the 
        keyword Type isn't recommended unless you are sure of its value.
        Type is used within the function since it's recursive which allows 
        passing the type computed in the first call to the next calls). Type 
        groups are not allowed only if the mesh contains at least two different 
        types of elements/faces. (In other words mesh.__ElmntsType__ and 
        mesh.__FacesType__ are not allowed). Single nodes groups (nodes 
        groups that contains only one node, their names are in the form 
        Node* such that * is the node index) are also not allowed.

        Nb : a special group is allowed, its name is "All", it consists 
             of all elements/nodes/faces of the mesh. If used, either the
             keyword Type must be specified, or GroupsList must containes 
             at least one other different group, otherwise the function 
             cannot determine the type of the groups and therefor which 
             type "All" refer to (elements, nodes or faces).

        Returns
        -------
            
            - If the intersection is empty, None is returned. the keyword 
              Store is ignored.
            
            - Store == False : If all the groups of the GroupsList are
              "All" and the Type keyword is specified, an iterator over all
              the elements/nodes/faces of the mesh is returned. Otherwise
              an array that contains the result (elements/nodes/faces list)
              is returned.
            
            - Store == True  : If all the groups of GroupsList are "All",
              "All" is returned. If GroupsList contains only one group 
              (different than "All"), its name is returned. Otherwise a 
              temporary group is created and its name is returned. The 
              new group is stored in one of the 3 available temporary 
              groups dictionaries according to its type. Its name is used 
              as key, and its value is the resulting array of elements/
              nodes/faces. 

        """
        if not len(GroupsList):
            raise ValueError("At least one group is required for this operation.")
        # Suppression des redondances
        GroupsList = list(set(GroupsList))
        GroupsType = Type if Type else self.GetGroupsType(*GroupsList)
        assert(GroupsType is not None)
        if "All" not in GroupsList:
            if len(GroupsList) > 1:
                if GroupsType == "Elmnts" and self.__ElmntsType__:
                    assert(self.__ElmntsType__ not in GroupsList)
                elif GroupsType == "Faces" and self.__FacesType__:
                    assert(self.__FacesType__ not in GroupsList)
                elif GroupsType == "Nodes":
                    for GroupName in GroupsList:
                        assert(not GroupName.startswith("Node"))
                GroupsItemsList  = [self.GetGroupItems(group, GroupType=GroupsType) for group in GroupsList]
                GroupsIntersectionItems = reduce(np.intersect1d, GroupsItemsList)
                if GroupsIntersectionItems.size:
                    return self.__AddTempGroup__(GroupsIntersectionItems, GroupsType) if Store else GroupsIntersectionItems
                else:
                    return None
            else:
                # S'il y a un seul groupe dans GroupsList (différent de "All").
                return GroupsList[0] if Store else self.GetGroupItems(GroupsList[0], GroupType=GroupsType)
        elif len(GroupsList) > 1:
            NewGroupsList = list(GroupsList)
            NewGroupsList.remove("All")
            return self.GetGroupsIntersection(*NewGroupsList, Store=Store, Type=GroupsType)
        else:
            return "All" if Store else self.GetGroupItems("All", GroupType=GroupsType)

    def __AddTempGroup__(self, GroupItems, GroupType):
        """Adds a new temporary group and returns its name."""
        assert(isinstance(GroupItems, np.ndarray) and GroupItems.size > 0)
        if GroupType == "Elmnts":
            return self.__AddTempElmntsGroup__(GroupItems)
        elif GroupType == "Nodes":
            return self.__AddTempNodesGroup__(GroupItems)
        else:
            assert(GroupType == "Faces")
            return self.__AddTempFacesGroup__(GroupItems)

    def __AddTempElmntsGroup__(self, GroupItems):
        """Adds a new temporary elements group and returns its name."""
        if self.__ElmntsTempGroups__ is None:
            self.__InitTempElmntsGroups__()
        GroupIdx  = len(self.__ElmntsTempGroups__) + 1
        GroupName = ".E.Group.%d" % GroupIdx
        self.__ElmntsTempGroups__[GroupName]  = GroupItems
        return GroupName


    def __AddTempNodesGroup__(self, GroupItems):
        """Adds a new temporary node group and returns its name."""
        if self.__NodesTempGroups__ is None:
            self.__InitTempNodesGroups__()
        GroupIdx  = len(self.__NodesTempGroups__) + 1
        GroupName = ".N.Group.%d" % GroupIdx
        self.__NodesTempGroups__[GroupName]  = GroupItems
        return GroupName

    def __AddTempFacesGroup__(self, GroupItems):
        """Adds a new temporary faces group and returns its name."""
        if self.__FacesTempGroups__ is None:
            self.__InitTempFacesGroups__()
        GroupIdx  = len(self.__FacesTempGroups__) + 1
        GroupName = ".F.Group.%d" % GroupIdx
        self.__FacesTempGroups__[GroupName]  = GroupItems
        return GroupName

    def __InitTempElmntsGroups__(self):
        """
        Creates the temporary elements group dictionary when the first
        group of that type is created.

        """
        self.__ElmntsTempGroups__ = {}

    def __InitTempNodesGroups__(self):
        """
        Creates the temporary nodes group dictionary when the first
        group of that type is created.

        """
        self.__NodesTempGroups__ = {}

    def __InitTempFacesGroups__(self):
        """
        Creates the temporary faces group dictionary when the first
        group of that type is created.
        
        """
        self.__FacesTempGroups__ = {}

    # La fct ci-dessous renvoie la liste des éléments/noeuds du groupe Group. L'argument GroupType 
    # est optionnel, dans ce cas la fonction se chrage de trouver le type de Group ("Elmnts" ou "Nodes").
    # Attention, si l'argument GroupType est mentionné la fonction ne fait pas de vérification pour 
    # s'assurer que Group fait partie des groupe du maillage de type "GroupType". Donc il ne faut 
    # l'utiliser que lorsqu'on est sur que c'est le bon type de Group et que ce dernier est bien définit
    def GetGroupItems(self, Group, GroupType=None):
        """
        Returns a group items (ex : elements of an element group). The
        keyword GroupType is optional if Group != "All". Otherwise it
        must be :
            
            - "Elmnts" : for elements groups.
            
            - "Nodes"  : for nodes groups.
            
            - "Faces"  : for faces groups.

        Returns
        -------
            - Group == "All" : an iterator over elements/nodes/faces of the
              mesh is returned (according to the keyword GroupType's value).

            - Group == "Node*" : an array with one element is returned in the
              case of single nodes groupes (This type of groups isn't stored 
              in the mesh).
            
            - Group in [mesh.__ElmntsType__, mesh.__FacesType__] : this is 
              possible only when the mesh contains exactly one type of elements/
              faces. In this case an iterator over the elements/faces of the
              mesh is returned (note that this is equivalent to the first case
              with GroupType in ["Elmnts", "Faces"]).
            
            - If Group does not belong to any available dictionary, an exception
              is raised.
            
            - If none of the previous cases is true, an array containing the 
              group's items is extracted from the dictionary that contains 
              Group as a key and it's returned.

        """
        if Group != "All":
            if GroupType is None:
                GroupType = self.GetGroupsType(Group)
            if GroupType == "Elmnts":
                return self.__GetElmntsGroupItems__(Group)
            elif GroupType == "Nodes":
                return self.__GetNodesGroupItems__(Group)
            else:
                assert(GroupType == "Faces")
                return self.__GetFacesGroupItems__(Group)
        else:
            assert(GroupType in ["Elmnts", "Nodes", "Faces"])
            # Je ne sais pas encore quoi faire (renvoyer un itérateur peut etre : range(NodesNbr/ElmntsNbr)
            return range(self.GetNumberof(GroupType))

    # Cette fonction ne doit pas être appelée en dehors de ce module. Cette fonction fait l'hypothèse 
    # que Group fait partie des groupes d'éléments du maillage, donc elle ne le vérifie pas.
    def __GetElmntsGroupItems__(self, Group):
        """
        It should not be used (use GetGroupItems(...) instead). It is
        only used within the function GetGroupItems(...) under strict 
        assumptions.
        
        """
        assert(Group != "All")
        if Group in self.ElmntsGroups:
            return self.ElmntsGroups[Group]
        elif self.__ElmntsType__ and Group == self.__ElmntsType__:
            return range(self.GetNumberof("Elmnts"))
        elif self.__ElmntsTypeGroups__ and Group in self.__ElmntsTypeGroups__:
            return self.__ElmntsTypeGroups__[Group]
        else:
            assert(self.__ElmntsTempGroups__ and Group in self.__ElmntsTempGroups__)
            return self.__ElmntsTempGroups__[Group]

    # Cette fonction ne doit pas être appelée en dehors de ce module. Cette fonction fait l'hypothèse 
    # que Group fait partie des groupes de noeuds du maillage, donc elle ne le vérifie pas.
    def __GetNodesGroupItems__(self, Group):
        """
        It should not be used (use GetGroupItems(...) instead). It is
        only used within the function GetGroupItems(...) under strict 
        assumptions.
        
        """
        assert(Group != "All")
        if Group in self.NodesGroups:
            return self.NodesGroups[Group]
        
        NodeIdx = self.IsSingleNodeGroup(Group)
        if NodeIdx:
            return np.array([NodeIdx - 1], dtype=int)
        else:
            assert(self.__NodesTempGroups__ and Group in self.__NodesTempGroups__)
            return self.__NodesTempGroups__[Group]

    # Cette fonction ne doit pas être appelée en dehors de ce module. Cette fonction fait l'hypothèse 
    # que Group fait partie des groupes de faces du maillage, donc elle ne le vérifie pas.
    def __GetFacesGroupItems__(self, Group):
        """
        It should not be used (use GetGroupItems(...) instead). It is
        only used within the function GetGroupItems(...) under strict 
        assumptions.
        
        """
        assert(Group != "All")
        if Group in self.FacesGroups:
            return self.FacesGroups[Group]
        elif self.__FacesType__ and Group == self.__FacesType__:
            return range(self.GetNumberof("Faces"))
        elif self.__FacesTypeGroups__ and Group in self.__FacesTypeGroups__:
            return self.__FacesTypeGroups__[Group]
        else:
            assert(self.__FacesTempGroups__ and Group in self.__FacesTempGroups__)
            return self.__FacesTempGroups__[Group]

    # La fonction ci-dessous renvoie le type ("Elmnts" ou "Nodes")des groupes de la liste GroupsList. 
    # S'il y a un groupe dans GroupsList qui n'appartient pas au maillage, une exception est envoyée 
    # par la fonction __GetGroupType__(...). Si les groupes de GroupsList n'ont pas le meme type d'éléments, 
    # une exception est renvoyée. Dans le cas particulier ou GroupList = ["All"], la valeur None est 
    # renvoyée, car dans ce programme, le groupe "All" peut etre utiliser pour les éléments comme pour 
    # les groupes. Ce group ("All") contient tout les éléments/Noeuds selon le type es autres groupes 
    # avec qui il est utilisé.
    def GetGroupsType(self, *GroupsList):
        """
        Returns the type of the groups in the list GroupsList. If they 
        don't have the same type or one of them doesn't belong to the mesh
        an exception is raised.

        Nb : This function must be used even if GroupsList contains only 
             one group.

        Returns
        -------

            - If all the groups of GroupsList are "All", None is returned.

            - If all the groups of GroupsList are groups of elements, "Elmnts"
              is returned.

            - If all the groups of GroupsList are groups of nodes, "Nodes"
              is returned.

            - If all the groups of GroupsList are groups of faces, "Faces"
              is returned.

        """
        assert(len(GroupsList) > 0)
        result = None
        for group in GroupsList:
            if group != "All":
                if result is None:
                    result = self.__GetGroupType__(group)
                elif result != self.__GetGroupType__(group):
                    raise ValueError("The groups {} doesn't have the same type".format(GroupsList))
        return result

    # Cette fonction ne doit pas être appelée en dehors de ce module, Il faut utiliser GetGroupsType()
    def __GetGroupType__(self, Group):
        """
        It should not be used (use GetGroupsType(...) instead). It is
        only used within the function GetGroupsType(...) under strict 
        assumptions.
        
        """
        if self.__IsElmntsGroup__(Group):
            return "Elmnts"
        elif self.__IsNodesGroup__(Group):
            return "Nodes"
        elif self.__IsFacesGroup__(Group):
            return "Faces"
        else:
            raise ValueError("The group %s isn't defined in the mesh" % Group)

    def ContainsGroup(self, Group):
        """
        It should not be used (use GetGroupsType(...) instead). It is
        only used within the function GetGroupsType(...) under strict 
        assumptions.
        
        """
        result = self.__IsElmntsGroup__(Group) \
            or self.__IsNodesGroup__(Group) \
            or self.__IsFacesGroup__(Group)
        
        return result

    # Cette fonction ne doit pas être appelée en dehors de ce module.
    def __IsElmntsGroup__(self, Group):
        """
        Returns True. if Group is a group of elements. Otherwise False.
        
        Nb : mesh.__ElmntsType__ is considered as an elements group.
        
        """
        assert(Group is not None and isinstance(Group, str))
        if Group in self.ElmntsGroups or self.__IsElmntsTypeGroup__(Group):
            return True
        elif self.__ElmntsTempGroups__:
            return Group in self.__ElmntsTempGroups__
        else:
            return False

    # Cette fonction ne doit pas être appelée en dehors de ce module.
    def __IsNodesGroup__(self, Group):
        """Returns True. if Group is a group of nodes. Otherwise False."""
        assert(Group is not None and isinstance(Group, str))
        if Group in self.NodesGroups or self.IsSingleNodeGroup(Group):
            return True
        elif self.__NodesTempGroups__:
            return Group in self.__NodesTempGroups__
        else:
            return False

    # Cette fonction ne doit pas être appelée en dehors de ce module.
    def __IsFacesGroup__(self, Group):
        """
        Returns True. if Group is a group of faces. Otherwise False.
        
        Nb : mesh.__FacesType__ is considered as a faces group.
        
        """
        assert(Group is not None and isinstance(Group, str))
        if Group in self.FacesGroups or self.__IsFacesTypeGroup__(Group):
            return True
        elif self.__FacesTempGroups__:
            return Group in self.__FacesTempGroups__
        else:
            return False

    # Cette fonction ne doit pas être appelée en dehors de ce module.
    def __IsElmntsTypeGroup__(self, Group):
        """
        It should not be used (use __IsElmntsGroup__(...) instead). It
        is only used within the function __IsElmntsGroup__(...) under strict 
        assumptions.
        
        """
        if self.__ElmntsType__:
            return Group == self.__ElmntsType__
        else:
            return Group in self.__ElmntsTypeGroups__

    # Cette fonction ne doit pas être appelée en dehors de ce module.
    def __IsFacesTypeGroup__(self, Group):
        """
        It should not be used (use __IsFacesGroup__(...) instead). It
        is only used within the function __IsFacesGroup__(...) under strict
        assumptions.
        
        """
        if self.__FacesType__:
            return Group == self.__FacesType__
        else:
            return Group in self.__FacesTypeGroups__
    
    # Cette Fonction permet de vérifier si le groupe GroupName, est un groupe
    # de noeud (non stocké dans le maillage) contenant un seul neud exactement, 
    # c'est groupe particulier ont des noms qui commencent par Node suivit du 
    # numéro du noeud. Ex: "Node13" est un groupe de noeud contenant le noeud 13.
    def IsSingleNodeGroup(self, GroupName):
        """
        Checks whether GroupName is a single node group. This means 
        that it should be a string of the form "Node*", with * the node's
        index. * must be within the range [0, mesh.NodesNbr - 1].

        Returns
        -------

            - If GroupName isn't a single node group (it doesn't start 
              with "Node"), 0 is returned, so it could be used as a bool.
            
            - If GroupName is a single node group, with the node index
              in the correct range, the index shifted forward by 1 is 
              returned. Otherwise (the index isn't in the correct range)
              an exception is raised.

        """
        if not GroupName.startswith("Node"):
            return 0
        try:
            NodeIdx = int(GroupName[4:])
        except ValueError:
            raise ValueError("Single node groups names must start with " \
                             "'Node' followed by the node number : %s" % GroupName)
        else:
            if NodeIdx >= self.NodesNbr:
                raise ValueError("Nodes numbers should not exceed %d : %s" % (self.NodesNbr - 1, GroupName))
            elif NodeIdx < 0:
                raise ValueError("Nodes numbers should be positive : %s" % GroupName)
            else:
                return NodeIdx + 1

    def GetNumberof(self, GroupType):
        """
        Returns the number of elements/nodes/faces of the mesh, according to
        the parameter GroupType that should be in ["Elmnts", "Nodes", "Faces"]).
        GroupType could also be the name of an elements type group, in that case
        the funtion returns the number of elements within this group (i.e. the 
        number of elements that matches that type).

        Example
        -------

                    mesh.GetNumberof("3D.4N") -> 171

            The mesh contains 171 elements of type tetrahedron.
        
        """
        if GroupType == "Nodes":
            return self.NodesNbr
        elif GroupType == "Elmnts":
            return self.ElmntsNbr
        elif GroupType == "Faces":
            return self.FacesNbr
        elif self.__IsElmntsTypeGroup__(GroupType):
            return self.GetElmntsTypeGroupsize(GroupType)
        else:
            assert(False)

    def GetElmntsTypeGroupsize(self, GroupName):
        """
        Returns the number of elements in the group GroupName.
        GroupName must be an elements type group^.

        """
        assert(self.__IsElmntsTypeGroup__(GroupName))
        if self.__ElmntsType__:
            return self.GetNumberof("Elmnts")
        else:
            self.__ElmntsTypeGroups__[GroupName].size

    def GetMeshDimension(self):
        """Returns the mesh dimension."""
        return self.Dimension

    def GetElmntsTypeGroups(self):
        """
        Returns the type(s) of the elements of the mesh.
        
        Returns
        -------

            - If mesh.__ElmntsType__ is not None (The mesh contains only
              one type of elements), it's returned.

            - If mesh.__ElmntsType__ is None, the mesh contains more than
              one type of elements. These different  types are used as keys
              in the dictionary mesh.__ElmntsTypeGroups__. In ths case all
              of these types are extracted and returned as a tuple.

        """
        if self.__ElmntsType__:
            return self.__ElmntsType__,
        else:
            return tuple(self.__ElmntsTypeGroups__.keys())

    def GetFacesTypeGroups(self):
        """
        Returns the type(s) of the faces of the mesh.
        
        Returns
        -------

            - If mesh.__FacesType__ is not None (The mesh contains only
              one type of faces), it's returned.

            - If mesh.__FacesType__ is None, the mesh contains more than
              one type of faces. These different  types are used as keys
              in the dictionary mesh.__FacesTypeGroups__. In ths case all
              of these types are extracted and returned as a tuple.

        """
        if self.__FacesType__:
            return self.__FacesType__,
        else:
            return tuple(self.__FacesTypeGroups__.keys())

    def HasFaces(self):
        """returns True if the mesh has faces, otherwise False."""
        return self.Dimension == 3

    def HasEdges(self):
        """returns True if the mesh has edges, otherwise False."""
        return self.Dimension in [2, 3]

    def GetElmntType(self, ElmntIdx):
        """Returns the type of the element whose index is ElmntIdx"""
        return utils.GetElmntType(self.Dimension, self.GetElmntNodesNbr(ElmntIdx))

    def GetFaceNodesList(self, FaceIdx):
        """Returns an array tnat contains the nodes list of the face FaceIdx."""
        assert(FaceIdx < self.FacesNbr)
        ElmntIdx, FaceLocalIdx = self.Faces[FaceIdx]
        ElmntType = self.GetElmntType(ElmntIdx)
        FaceNodesLocaleIdxs = utils.GetFacesByElmntType(ElmntType)[FaceLocalIdx] - 1
        return self.GetElmntNodesList(ElmntIdx)[FaceNodesLocaleIdxs]

    # Cette fonction suit la mm logique que GetElmntCoord(...)
    def GetFaceCoord(self, FaceIdx, Transp=True):
        """
        Returns an array that contains the coordinates of the face
        FaceIdx. By default (Transp=True) each column correspond to a 
        node, if Transp=False each row correspond to a node.
        
        """
        assert(isinstance(Transp, bool))
        FaceNodesList = self.GetFaceNodesList(FaceIdx)
        result = self.GetNodesCoords(FaceNodesList)
        return result.transpose() if Transp else result
    
    def GetFaceElmnt(self, FaceIdx):
        """
        Returns the number of the element to which the face number
        FaceIdx belongs to.

        """
        assert(FaceIdx < self.FacesNbr)
        return self.Faces[FaceIdx, 0]
    
    def GetFaceLocalNbr(self, FaceIdx):
        """
        Returns the local number of the face FaceIdx within
        the other faces of the element which it's belong to.

        """
        assert(FaceIdx < self.FacesNbr)
        return self.Faces[FaceIdx, 1]


if __name__ == "__main__":
    Coordinates  = np.array([[0,0],
                             [1,0],
                             [0.5,0.5],
                             [1,1],
                             [0,1],
                             [2,0],
                             [2,1]],float)
    # print(Coordinates.shape)
    # assert(type(Coordinates) == np.ndarray and Coordinates.shape == (5, 2))
    Connectivity = np.array([[3,0,1,2,-1],
                             [3,1,2,3,-1],
                             [3,2,3,4,-1],
                             [4,1,5,6,4],
                             [3,2,4,0,-1]], int)
    m = mesh(7,5,2,Coordinates,Connectivity, group1 = ('N', [0,1,6]), group3 = ('N', [2,3]), group5 = ('N', [4,5]), group8=('N', [2,3,4,5]))
    m.AddGroups(group2 = ('E', [1,4]), group4 = ('E', [3]), group6 = ('E', [0,2]), group7 = ('E', [4,0,3,1,2]))

    print(m.NodesGroups)
    print(m.ElmntsGroups)
    print(m.NodesNbr)
    print(m.ElmntsNbr)

    print(m.__ElmntsTypeGroups__)
    print(m.__ElmntsType__)

    # ElmntsPartition = ['group2','group4','group6']
    ElmntsPartition = ['group7']
    print("The Elmnts partition is :", m.CheckPartition(*ElmntsPartition))
    
    # NodesPartition  = ['group1','group3','group5']
    NodesPartition  = ['group1','group8']
    print("The Nodes  partition is :", m.CheckPartition(*NodesPartition, Type='Nodes'))

    print(m.GetElmntCoord(1))
    print(m.GetElmntCoord(1,Transp=False))
    print(m.GetElmntCoord(3,Transp=False))

    print(m.GetGroupsType("group2","group4","group6", "group7"))
    print(m.GetGroupsType("group1","group3","group5", "group8"))
    print(m.GetGroupsType("All"))
    print(m.GetGroupsType("group2"))
    # print(m.GetGroupsType("xxx"))

    # print(m.Connectivity)
    
    print("Nodes groupes union")
    groupName1 = m.GetGroupsUnion("group1", "group5")
    print(groupName1)
    print(m.GetGroupItems(groupName1, GroupType=None))
    
    print("Nodes groupes intersection")
    groupName2 = m.GetGroupsIntersection("group3", "group8")
    print(groupName2)
    print(m.GetGroupItems(groupName2, GroupType=None))

    print("Elmnts groupes union")
    groupName3 = m.GetGroupsUnion("group2", "group4")
    print(groupName3)
    print(m.GetGroupItems(groupName3, GroupType=None))

    print("Elmnts groupes intersection")
    groupName4 = m.GetGroupsIntersection("All", "group6", "All", "group7")
    print(groupName4)
    print(m.GetGroupItems(groupName4, GroupType=None))

    print('Elmnts groupes intersection ["All", "All"]')
    groupName5 = m.GetGroupsIntersection("All", "All", Store=True, Type="Elmnts")
    print(groupName5)
#    print(m.GetGroupItems(groupName5, GroupType="Elmnts"))

