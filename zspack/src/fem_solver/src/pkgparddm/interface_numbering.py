"""
This module implements the interface numbering class and all 
methods that allow interacting with it.

"""
from petsc4py import PETSc
from mpi4py import MPI
import numpy as np
import time

from .. import model
from ..pkgutils import IsInteger, \
    IsParallelStudy, PrintAllRanks

def CreateInterfaceNumbering(GlbIntrfcNodesNbr, \
    LocIntrfcNodes, GlbIntrfcNodes, LocDofsNumbering, \
    BlocksNbr=1, Type=None, dtype='R'):
    """
    Creates domain decomposition interface numbering.

    Parameters
    ----------
    dtype : 'str'
        The data type. 'R' for real or 'C' for complex.

    GlbIntrfcNodesNbr : 'int'
        The number of nodes of the global interface.

    LocIntrfcNodes : 'np.ndarray'
        The list of subdomain nodes that belongs to the interface. Multiple 
        nodes are redundante (repeated once for every neighbouring subdomain).
        Nodes indices must start at 0.

    GlbIntrfcNodes : 'np.ndarray'
        The list of nodes of the global interface that correspond to the 
        nodes that belongs to the interface of the subdomain. It has the 
        same size.Nodes indices must start at 0.
    
    LocDofsNumbering : 'DofNumbering'
        The numbering object of the subdomain.
    
    BlocksNbr : 'int'
        The number of blocks repetition of degrees of freedom of the 
        interface (1 for all problems except the mcre).

    type : 'str'
        The type of the interface, either "Primal" or "Dual".

    """
    assert(Type in ["Primal", "Dual"])
    if Type == "Dual":
        return FetiInterfaceNumbering(GlbIntrfcNodesNbr, \
            LocIntrfcNodes, GlbIntrfcNodes, LocDofsNumbering, \
            BlocksNbr=BlocksNbr, dtype=dtype)
    else:
        raise ValueError("The parallel " \
            "Primal DDM interface isn't implemented yet")

class FetiInterfaceNumbering:
    """
    The interface numbering class.

    Parameters
    ----------
    dtype : 'str'
        The data type. 'R' for real or 'C' for complex.

    GlbIntrfcNodesNbr : 'int'
        The number of nodes of the global interface.

    LocIntrfcNodes : 'np.ndarray'
        The list of subdomain nodes that belongs to the interface. Multiple 
        nodes are redundante (repeated once for every neighbouring subdomain).
        Nodes indices must start at 0.

    GlbIntrfcNodes : 'np.ndarray'
        The list of nodes of the global interface that correspond to the 
        nodes that belongs to the interface of the subdomain. It has the 
        same size.Nodes indices must start at 0.
    
    LocDofsNumbering : 'DofNumbering'
        The numbering object of the subdomain.
    
    BlocksNbr : 'int'
        The number of blocks repetition of degrees of freedom of the 
        interface (1 for all problems except the mcre).

    Attributes
    ----------
    dtype : 'str'
        The data type. 'R' for real or 'C' for complex.

    BlocksNbr : 'int'
        The number of blocks repetition of degrees of freedom of the 
        interface (1 for all problems except the mcre).
        
    DofsNumbering : 'model.DofsNumbering'
        The DofNumbering object.

    GlbIntrfcDofsNbr : 'int'
        The number of the global interface degrees of freedom (the size
        of the dual interface problem).
    
    SdGlbIntrfcDofs : 'np.ndarray'
        The list of degrees of freedom of the global dual interface that 
        belongs to the sbubdomain local interface.

    SdGlbIntrfcSigns : 'np.ndarray'
        The list of signs {1, -1} of degrees of freedom that belongs to 
        the dual interface of the subdomain.

    LocIntrfcPrimalDofs : 'np.ndarray'
        The list of degrees of freedom of the subdomain that belongs to 
        the interface. DOFs aren't redondante that's why "Primal" is used
        in the attribute name.

    LocIntrfcDualDofs : 'np.ndarray'
        The list of primal degrees of freedom that correspond to the global 
        dual DOFs SdGlbIntrfcDofs. DOFS are indexed using their order in the
        array LocIntrfcPrimalDofs.

    """

    def __init__(self, GlbIntrfcNodesNbr, LocIntrfcNodes, 
        GlbIntrfcNodes, LocDofsNumbering, BlocksNbr=1, dtype='R'):
        
        assert(IsInteger(GlbIntrfcNodesNbr))
        assert(IsInteger(BlocksNbr))
        assert(dtype in ['R', 'C'])

        assert(isinstance(LocIntrfcNodes, np.ndarray))
        assert(isinstance(GlbIntrfcNodes, np.ndarray))
        
        assert(isinstance(LocDofsNumbering, model.DofNumbering))

        if not IsParallelStudy():
            raise ValueError("Interface numbering operator"
                " cannot be used for sequential studies")

        self.dtype = dtype
        self.BlocksNbr = BlocksNbr
        self.DofsNumbering = LocDofsNumbering

        GlbIntrfcData = self.__getGlobalInterfaceData__( \
            GlbIntrfcNodesNbr, LocIntrfcNodes, GlbIntrfcNodes)
        
        self.GlbIntrfcDofsNbr = GlbIntrfcData[0]
        self.SdGlbIntrfcDofs  = GlbIntrfcData[1]
        self.SdGlbIntrfcSigns = GlbIntrfcData[2]

        LocIntrfcData = self.__getLocalInterfaceData__(LocIntrfcNodes)

        self.LocIntrfcPrimalDofs = LocIntrfcData[0]
        self.LocIntrfcDualDofs   = LocIntrfcData[1]

        self.Multiplicity = self.__getMultiplicity__()

    def printInfo(self):
        """xxxx xxxx xxxx xxxx."""
        PrintAllRanks(self.getLocalInfo(), UseHeader=True)

    def getLocalInfo(self):
        """xxxx xxxx xxxx xxxx."""
        ToPrint = "Subdomain - Proc : {}\n".format(self.getCommRank())
        ToPrint += "The global size of the interface : {}\n".format(self.GlbIntrfcDofsNbr)
        ToPrint += "Local primal dofs : {}\n".format(self.LocIntrfcPrimalDofs)
        ToPrint += "Local dual dofs : {}\n".format(self.LocIntrfcDualDofs)
        ToPrint += "Global dofs : {}\n".format(self.SdGlbIntrfcDofs)
        ToPrint += "Interface Dofs Signs : {}\n".format(self.SdGlbIntrfcSigns)
        return ToPrint

    def getCommRank(self):
        """Returns the rank within the communicator."""
        return MPI.COMM_WORLD.Get_rank()

    def getCommSize(self):
        """Returns the rank within the communicator."""
        return MPI.COMM_WORLD.Get_size()
    
    def getBlocksNbr(self):
        """Returns the number of blocks of the problem"""
        return self.BlocksNbr
    
    def getInterfaceType(self):
        """Returns the type of the interface"""
        return "Dual"

    def __getGlobalInterfaceData__(self, \
        GlbIntrfcNodesNbr, LocIntrfcNodes, GlbIntrfcNodes):
        """
        Returns a tuple of 3 objects. The first one is the total number of
        degrees of freedom of the dual interface. The second one is the list
        of the global dual degrees of freedom that belong to the subdomain,
        and the third is the array that contains their signs.
        
        """
        LocIntrfcNodesDofsNbr = \
            self.DofsNumbering.GetNodesFreeDofNbr(LocIntrfcNodes)
        GlbIntrfcNodesDofsNbr = np.zeros(GlbIntrfcNodesNbr + 1, dtype=int)
        GlbIntrfcNodesRanks = np.zeros(GlbIntrfcNodesNbr, dtype=int)
        
        SendData = np.zeros(GlbIntrfcNodesNbr, dtype=int)

        SendData[GlbIntrfcNodes] = LocIntrfcNodesDofsNbr
        MPI.COMM_WORLD.Allreduce(SendData, GlbIntrfcNodesDofsNbr[1:], op=MPI.MAX)
        
        SendData[:] = 0
        SendData[GlbIntrfcNodes] = self.getCommRank()
        MPI.COMM_WORLD.Allreduce(SendData, GlbIntrfcNodesRanks, op=MPI.MAX)

        GlbIntrfcDofsNbr = np.sum(GlbIntrfcNodesDofsNbr)
        GlbIntrfcNodesDofsNbr = \
            np.cumsum(GlbIntrfcNodesDofsNbr, out=GlbIntrfcNodesDofsNbr)

        TempDofsTuple = ()
        TempSignsTuple = ()
        
        for GlbNode in GlbIntrfcNodes:
            start = GlbIntrfcNodesDofsNbr[GlbNode]
            stop = GlbIntrfcNodesDofsNbr[GlbNode + 1]
            TempDofsTuple += np.arange(start, stop, dtype=np.int32),
            TempSignsTuple += np.full(stop - start, \
                (1 if GlbIntrfcNodesRanks[GlbNode] == \
                self.getCommRank() else -1), dtype=np.int32),

        del GlbIntrfcNodesDofsNbr
        del GlbIntrfcNodesRanks
        del SendData

        SdGlbIntrfcDofs = np.concatenate(TempDofsTuple)
        SdGlbIntrfcSigns = np.concatenate(TempSignsTuple)

        if self.BlocksNbr > 1:
            SdGlbIntrfcSigns = np.tile(SdGlbIntrfcSigns, self.BlocksNbr)
            TempArr = np.arange(self.BlocksNbr, dtype=np.int32) * GlbIntrfcDofsNbr
            SdGlbIntrfcDofs = ( TempArr[:, None] + SdGlbIntrfcDofs ).flatten()
            GlbIntrfcDofsNbr *= self.BlocksNbr

        return GlbIntrfcDofsNbr, SdGlbIntrfcDofs, SdGlbIntrfcSigns

    def __getLocalInterfaceData__(self, LocIntrfcNodes):
        """
        Returns a tuple of two objects. The first one is the degrees of 
        freedom of the subdomain that belongs to the primal interface.
        the second objects contains the list of the subdomain dual degrees 
        of freedom with respect to the primal degrees of freedom stored 
        in the object of the tuple.
        
        """
        LocIntrfcNodesDofs = self.DofsNumbering.GetNodesNumbring(LocIntrfcNodes)

        LocIntrfcPrimalDofs, LocIntrfcDualDofs = \
            np.unique(LocIntrfcNodesDofs, return_inverse=True)

        if self.BlocksNbr > 1:
            SdDofsNbr = self.DofsNumbering.GetFreeDofNbr()
            SdIntrfcPrimalDofsNbr = LocIntrfcPrimalDofs.size
            TempArr = np.arange(self.BlocksNbr, dtype=int)
            LocIntrfcPrimalDofs = ( TempArr[:, None] * \
                SdDofsNbr + LocIntrfcPrimalDofs ).flatten()
            LocIntrfcDualDofs = ( TempArr[:, None] * \
                SdIntrfcPrimalDofsNbr + LocIntrfcDualDofs ).flatten()

        return LocIntrfcPrimalDofs, LocIntrfcDualDofs
    
    def __getMultiplicity__(self):
        """Returns the multiplicity of degrees of freedom."""
        PrimalInterfaceSize = self.getLocalPrimalInterfaceSize()
        TempVec = np.zeros(PrimalInterfaceSize, dtype=int)
        np.add.at(TempVec, self.LocIntrfcDualDofs, 1)
        return TempVec + 1
    
    def getLocalDofsNbr(self):
        """Returns the number of local degrees of freedom"""
        return self.DofsNumbering.GetFreeDofNbr() * self.BlocksNbr
    
    def getLocalDofNumbering(self):
        """Returns the local DOFs numbering"""
        return self.DofsNumbering

    def getDataType(self):
        """Returns the data type."""
        return self.dtype

    def getLocalInterfaceDofs(self):
        """
        Returns the local numbering of interface degrees of 
        freedom of the subdomain.
        
        """
        return self.LocIntrfcPrimalDofs

    def getGlobalInterfaceDofs(self):
        """
        Returns the global numbering of interface degrees of 
        freedom of the subdomain.
        
        """
        return self.SdGlbIntrfcDofs

    def getLocalInternalDofs(self):
        """
        Returns the local numbering of internal degrees of 
        freedom of the subdomain.
        
        """
        VecSize = self.getLocalDofsNbr()
        TempVec = np.full(VecSize, True)
        TempVec[self.LocIntrfcPrimalDofs] = False
        return np.nonzero(TempVec)[0]

    def getInterfaceDofsSigns(self):
        """
        Returns the signs of the local interface 
        degrees of freedom of the subdomain.
        
        """
        return self.SdGlbIntrfcSigns
    
    def getDofsMultiplicity(self):
        """
        Returns the multiplicity of interface 
        degrees of freedom of the subdomain.
        
        """
        return self.Multiplicity

    def getGlobalInterfaceSize(self):
        """Returns the size of the global interface."""
        return self.GlbIntrfcDofsNbr

    def getLocalPrimalInterfaceSize(self):
        """Returns the size of the geometric local interface."""
        return self.LocIntrfcPrimalDofs.size

    def getLocalDualInterfaceDofs(self):
        """
        Returns a redundant list of primal interface 
        DOFs that belongs to the local dual interface.
        
        """
        return self.LocIntrfcDualDofs

    def getLocalDualInterfaceSize(self):
        """Returns the size of the local dual interface."""
        return self.LocIntrfcDualDofs.size
    
    def dualToPrimalProjection(self, LocDualArray, ApplyMultiplicity=True):
        """
        Project a vector of local dual interface Dofs on the local 
        primal interface.
        
        """
        assert(isinstance(LocDualArray, np.ndarray))
        assert(isinstance(ApplyMultiplicity, bool))
        PrimalDofsNbr = self.getLocalPrimalInterfaceSize()
        LocPrimalArray = np.zeros(PrimalDofsNbr, \
            dtype=(float if self.dtype == 'R' else complex))
        TempVec = LocDualArray * self.SdGlbIntrfcSigns
        np.add.at(LocPrimalArray, self.LocIntrfcDualDofs, TempVec)
        if ApplyMultiplicity:
            LocPrimalArray /= self.Multiplicity
        return LocPrimalArray

    def primalToDualProjection(self, LocPrimalArray, ApplyMultiplicity=True):
        """
        Project a vector of local primal interface Dofs on the local 
        dual interface.
        
        """
        assert(isinstance(LocPrimalArray, np.ndarray))
        assert(isinstance(ApplyMultiplicity, bool))
        TempVec = LocPrimalArray if not ApplyMultiplicity \
            else LocPrimalArray / self.Multiplicity
        return TempVec[self.LocIntrfcDualDofs] * self.SdGlbIntrfcSigns
    
    def getProjectionMatrix(self):
        """
        Returns the projection matrix from primal to dual subdomain 
        DOFs (The projection matrix is a signed boolean matrix).
        
        """
        PrimalDofsNbr = self.getLocalPrimalInterfaceSize()
        DualDofsNbr = self.getLocalDualInterfaceSize()
        MatSize = DualDofsNbr, PrimalDofsNbr
        
        Result = PETSc.Mat().createAIJ(
            MatSize, comm=MPI.COMM_SELF)
        Result.setPreallocationNNZ(1)
        Result.setUp()
        
        for row in np.arange(DualDofsNbr):
            col = self.LocIntrfcDualDofs[row]
            val = self.SdGlbIntrfcSigns[row]
            Result.setValue(row, col, val)
        
        Result.assemble()
                
        return Result


class BddInterfaceNumbering:
    def __init__(self):
        raise NotImplementedError
