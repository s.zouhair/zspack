"""
This module implements the parallel version of the dual domain decomposition 
method FETI and all functions that allows interacting with it.

"""
from numpy.core.numeric import indices
from petsc4py import PETSc
from slepc4py import SLEPc
from mpi4py import MPI
import numpy as np
import math

from . import feti

class BaseProjector:
    """
    Context of the global projector.

    Parameters
    ----------
    Solver : 'feti.FetiSolver'
        The Feti solver object.

    Attributes
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    """

    def __init__(self, Solver):
        assert(isinstance(Solver, feti.FetiSolver))
        self.FetiSolver = Solver
        self.Verbose = Solver.verbose()

        self.CoarsePbData = self.buildCoarsePbData()
        self.CompleteVersion = self.buildCompleteVersionTest()

        self.CoarsePbComm = self.buildCoarsePbComm()
        self.CoarsePbProcsNbr = self.computeCoarsePbProcsNbr()

        if self.CoarsePbProcsNbr == 0: return

        self.CpProcsSubCommVec = self.buildCpProcsSubCommVec()
        self.CpProcsOwnedVec = self.buildCpProcsOwnedVec()
        self.VecsScatter = self.buildVecsScatter()

        if self.isCoarsePbProc():
            assert(self.CpProcsOwnedVec.getOwnershipRange() \
                == self.CpProcsSubCommVec.getOwnershipRange())
        
        self.ConstraintsMatrix = self.buildConstraintsMatrix()
        self.CoarsePbMatrix = self.buildCoarsePbMatrix()
        self.CoarsePbKsp = self.buildCoarsePbKsp()
        
        self.TempCpCommVec = None if not self.isCoarsePbProc() \
            else self.ConstraintsMatrix.getVecRight()
        
        self.multTranspose = self.mult

        if self.useCompleteVersion():
            self.mult = self.multComplete
            self.multTranspose = self.multTransposeComplete
            self.CmpltVersionMat = self.getCompleteVersionMat()
            self.TempGlbVec = self.CmpltVersionMat.getVecRight()
        else:
            self.TempCpOwnedVec = None
            self.CmpltVersionMat = None
        
        self.ProjectorMatrix = self.buildProjectorMatrix()

        self.InitialGuess = self.buildInitialGuess()

    def createProj(self):
        """Returns a created instance of the projector"""
        return self if self.CoarsePbProcsNbr != 0 else None

    def getCommRank(self):
        """Returns the rank within the communicator."""
        return MPI.COMM_WORLD.Get_rank()

    def getCommSize(self):
        """Returns the rank within the communicator."""
        return MPI.COMM_WORLD.Get_size()

    def verbose(self):
        """
        Returns a bool that determines 
        whether to print infos or not
        
        """
        return self.Verbose

    def printInfo(self):
        """Prints infos about the coarse space"""
        Condition = self.verbose() and \
            self.isCoarsePbProc() and \
            self.CoarsePbComm.Get_rank() == 0
        if not Condition: return None
        CpType = self.getCoarseSpaceType()
        VecsNbr = self.TempCpCommVec.getSize()
        print("The number of vectors of {} coarse"
            " space \t: {}".format(CpType, VecsNbr))

    def isCoarsePbProc(self):
        """
        Checks whether a proc participate to the 
        coarse problem or not.
        
        """
        return self.CoarsePbData is not None

    def getCpVecNbr(self):
        """Return the number of vectors of the coarse problem"""
        raise NotImplementedError

    def buildCompleteVersionTest(self):
        """
        Returns a bool that checks whether to use the 
        complete version of the projector or not
        
        """
        raise NotImplementedError

    def useCompleteVersion(self):
        """
        Returns a bool that checks whether to use the 
        complete version of the projector or not
        
        """
        return self.CompleteVersion

    def getCompleteVersionMat(self):
        """
        Returns the matrix used in the complete 
        version of the projector (not the 
        projector matrix)
        
        """
        raise NotImplementedError

    def getCoarseSpaceType(self):
        """Returns the type of the coarse space"""
        return None

    def getCoarsePbProcsNbr(self):
        """
        Returns the number of subdomains that 
        participate to the coarse problem.
        
        """
        return self.CoarsePbProcsNbr

    def getCoarsePbLocSize(self):
        """
        Returns the number of vectors which the 
        subdomain contribute to the coarse problem.
        
        """
        raise NotImplementedError

    def getProjectorMatrix(self):
        """Returns the matrix representing the projector"""
        return self.ProjectorMatrix

    def getInitialGuess(self):
        """Returns the initial guess of the projector"""
        return self.InitialGuess

    def computeCoarsePbProcsNbr(self):
        """
        Computes the number of subdomains that 
        participate to the coarse problem.
        
        """
        CoarsePbProcsNbr = MPI.COMM_WORLD.allreduce(
            int(self.CoarsePbData is not None), op=MPI.SUM)
        return CoarsePbProcsNbr
    
    def buildCoarsePbComm(self):
        """Builds the coarse problem communicator."""
        CommWorldRank = MPI.COMM_WORLD.Get_rank()
        SubComm = MPI.COMM_WORLD.Split(
            self.isCoarsePbProc(), CommWorldRank)
        return None if not self.isCoarsePbProc() else SubComm

    def buildCoarsePbData(self):
        """Builds the coarse space data."""
        return None

    def buildConstraintsMatrix(self):
        """Builds and returns the constraints matrix."""
        if not self.isCoarsePbProc(): return None
        else: raise NotImplementedError

    def buildCoarsePbMatrix(self):
        """Builds the coarse problem matrix"""
        if not self.useCompleteVersion(): 
            return self.buildCPMatIncomplete()
        else: return self.buildCpMatComplete()

    def buildCPMatIncomplete(self):
        """Builds the coarse problem matrix : C^t * C."""
        if not self.isCoarsePbProc(): return None
        G = self.ConstraintsMatrix
        GlobalRowsNbr, _ = G.getSize()
        I = PETSc.Mat().createAIJ(
            (GlobalRowsNbr, GlobalRowsNbr), 
            nnz=1, comm=self.CoarsePbComm)
        DiagVec = I.getVecLeft()
        DiagVec.set(1.)
        I.setDiagonal(DiagVec)
        I.assemble()
        CpMat = I.PtAP(G)
        return CpMat

    def buildCpMatComplete(self, A=None):
        """Builds the coarse problem matrix : C^t * A * C."""
        if A is None: A = self.getCompleteVersionMat()
        G = self.ConstraintsMatrix
        GlbComm = A.getComm().tompi4py()
        SubComm = None if G is None else G.getComm().tompi4py()

        if G is not None:
            TempMat = PETSc.Mat().createAIJ(
                G.getSizes(), comm=self.CoarsePbComm)
            TempMat.setUp()
            _, ColsNbr = G.getSize()
            start, _ = G.getOwnershipRange()
            assert(GlbComm.Get_size() >= SubComm.Get_size())
        else: ColsNbr = 0
        
        ColsNbr = GlbComm.allreduce(ColsNbr, op=MPI.MAX)
        CpCommVec = self.CpProcsSubCommVec
        CpOwnedVec = self.CpProcsOwnedVec

        GlbVecToMult, GlbVecMultRes = A.getVecs()

        assert(GlbVecToMult.getOwnershipRange() \
            == GlbVecMultRes.getOwnershipRange())

        if not self.isCoarsePbProc():
            for col in range(ColsNbr):
                self.VecsScatter.scatter(CpOwnedVec, 
                    GlbVecToMult, mode="reverse")
                A.mult(GlbVecToMult, GlbVecMultRes)
                self.VecsScatter.scatter(GlbVecMultRes, 
                    CpOwnedVec, mode="forward")
        else:
            for col in range(ColsNbr):
                G.getColumnVector(col, CpCommVec)
                CpOwnedVec.getArray()[:] = CpCommVec.getArray()[:]
                self.VecsScatter.scatter(CpOwnedVec, 
                    GlbVecToMult, mode="reverse")
                A.mult(GlbVecToMult, GlbVecMultRes)
                self.VecsScatter.scatter(GlbVecMultRes, 
                    CpOwnedVec, mode="forward")
                TempArr = CpOwnedVec.getArray()
                rows = np.nonzero(TempArr)[0].astype(np.int32)
                vals = TempArr[rows]
                TempMat.setValues(rows + start, [col], vals)

            TempMat.assemble()
        
        if not self.isCoarsePbProc(): return None
        else: return TempMat.transposeMatMult(G)

    def buildCoarsePbKsp(self):
        """Builds the coarse problem ksp object."""
        if not self.isCoarsePbProc(): return None
        CoarsePbKsp = PETSc.KSP()
        CoarsePbKsp.create(comm=self.CoarsePbComm)
        CoarsePbKsp.setOperators(self.CoarsePbMatrix)
        CoarsePbKsp.setType(PETSc.KSP.Type.PREONLY)
        CoarsePbPc = CoarsePbKsp.getPC()
        CoarsePbPc.setType(PETSc.PC.Type.LU)
        CoarsePbPc.setFactorSolverType(
            PETSc.Mat.SolverType.MUMPS)
        CoarsePbPc.setFactorSetUpSolverType()
        CoarsePbKsp.setUp()
        return CoarsePbKsp

    def buildProjectorMatrix(self):
        """Builds the petsc matrix representing the projector."""
        Numbering = self.FetiSolver.InterfaceNumbering
        MatSize = Numbering.getGlobalInterfaceSize()
        ProjectorMatrix = PETSc.Mat().createPython( \
            size=MatSize, comm=MPI.COMM_WORLD)
        ProjectorMatrix.setPythonContext(self)
        ProjectorMatrix.setUp()
        return ProjectorMatrix

    def buildCpProcsSubCommVec(self):
        """
        Builds a parallel vector on the subcommunicator
        of the coarse problem procs with the same size 
        as the interface problem.
        
        """
        if not self.isCoarsePbProc(): return None
        Numbering = self.FetiSolver.getInterfaceNumbering()
        GlobalSize = Numbering.getGlobalInterfaceSize()
        CpProcsSubCommVec = PETSc.Vec().createMPI(
            (None, GlobalSize), comm=self.CoarsePbComm)
        return CpProcsSubCommVec

    def buildCpProcsOwnedVec(self):
        """
        Builds a parrallel vector on MPI.COMM_WORLD 
        but only owned by coarse problem procs. It's
        size is equal to the interface problem size.
        
        """
        LocalSize = 0 if not self.isCoarsePbProc() \
            else self.CpProcsSubCommVec.getLocalSize()
        Numbering = self.FetiSolver.getInterfaceNumbering()
        GlobalSize = Numbering.getGlobalInterfaceSize()
        CpProcsOwnedVec = PETSc.Vec().createMPI(
            (LocalSize, GlobalSize), comm=MPI.COMM_WORLD)
        return CpProcsOwnedVec

    def buildVecsScatter(self):
        """
        Builds a scatter between two MPI.COMM_WORLD 
        vectors, one is owned by all procs and the 
        other only by the coarse problem procs
        
        """
        IndexSetSize = self.CpProcsOwnedVec.getLocalSize()
        IndexSetFirst = self.CpProcsOwnedVec.getOwnershipRange()[0]
        IndexSet = PETSc.IS().createStride(IndexSetSize, 
            first=IndexSetFirst, step=1, comm=MPI.COMM_WORLD)
        GlobalVecExample = PETSc.Vec().createMPI( \
            self.CpProcsOwnedVec.getSize(), comm=MPI.COMM_WORLD)
        Scatter = PETSc.Scatter().create(GlobalVecExample, 
            IndexSet, self.CpProcsOwnedVec, IndexSet)
        return Scatter

    def buildInitialGuess(self):
        """Builds the initial guess of the projector"""
        raise NotImplementedError

    def mult(self, mat, PetscVecToMult, PetscMultRes):
        """
        Apply the simplified version of the projector 
        to a vector : P = I - G * (G^t * G)^(-1) * G^t
        
        """
        self.VecsScatter.scatter(PetscVecToMult, 
            self.CpProcsOwnedVec, mode="forward")
        if self.isCoarsePbProc():
            self.CpProcsSubCommVec.getArray()[:] = \
                self.CpProcsOwnedVec.getArray()[:]
            self.ConstraintsMatrix.multTranspose(
                self.CpProcsSubCommVec, self.TempCpCommVec)
            self.CoarsePbKsp.solve(
                self.TempCpCommVec, self.TempCpCommVec)
            self.ConstraintsMatrix.mult(
                self.TempCpCommVec, self.CpProcsSubCommVec)
            self.CpProcsOwnedVec.getArray()[:] -= \
                self.CpProcsSubCommVec.getArray()[:]
        self.VecsScatter.scatter(self.CpProcsOwnedVec, 
            PetscMultRes, mode="reverse")

    def multComplete(self, mat, PetscVecToMult, PetscMultRes):
        """
        The mult method of the complete version 
        of the projector
        
        """
        raise NotImplementedError

    def multTransposeComplete(self, mat, PetscVecToMult, PetscMultRes):
        """
        The multTranspose method of the complete 
        version of the projector
        
        """
        raise NotImplementedError

    def mult_1(self, mat, PetscVecToMult, PetscMultRes):
        """
        Apply the matrix vector product related to the 
        matrix : P = I - A * G * (G^t * A * G)^(-1) * G^t
        
        """
        self.VecsScatter.scatter(PetscVecToMult, 
            self.CpProcsOwnedVec, mode="forward")
        if self.isCoarsePbProc():
            self.CpProcsSubCommVec.getArray()[:] = \
                self.CpProcsOwnedVec.getArray()[:]
            self.ConstraintsMatrix.multTranspose(
                self.CpProcsSubCommVec, self.TempCpCommVec)
            self.CoarsePbKsp.solve(
                self.TempCpCommVec, self.TempCpCommVec)
            self.ConstraintsMatrix.mult(
                self.TempCpCommVec, self.CpProcsSubCommVec)
            self.CpProcsOwnedVec.getArray()[:] = \
                - self.CpProcsSubCommVec.getArray()[:]
        self.VecsScatter.scatter(self.CpProcsOwnedVec, 
            self.TempGlbVec, mode="reverse")
        self.CmpltVersionMat.multAdd( \
            self.TempGlbVec, PetscVecToMult, PetscMultRes)
    
    def mult_2(self, mat, PetscVecToMult, PetscMultRes):
        """
        Apply the matrix vector product related to the 
        matrix : P = I - G * (G^t * A * G)^(-1) * G^t * A
        
        """
        self.CmpltVersionMat.mult(
            PetscVecToMult, self.TempGlbVec)
        self.VecsScatter.scatter(self.TempGlbVec, 
            self.CpProcsOwnedVec, mode="forward")
        if self.isCoarsePbProc():
            self.CpProcsSubCommVec.getArray()[:] = \
                self.CpProcsOwnedVec.getArray()[:]
        self.VecsScatter.scatter(PetscVecToMult, 
            self.CpProcsOwnedVec, mode="forward")
        if self.isCoarsePbProc():
            self.ConstraintsMatrix.multTranspose(
                self.CpProcsSubCommVec, self.TempCpCommVec)
            self.CoarsePbKsp.solve(
                self.TempCpCommVec, self.TempCpCommVec)
            self.ConstraintsMatrix.mult(
                self.TempCpCommVec, self.CpProcsSubCommVec)
            self.CpProcsOwnedVec.getArray()[:] -= \
                self.CpProcsSubCommVec.getArray()[:]
        self.VecsScatter.scatter(self.CpProcsOwnedVec, 
            PetscMultRes, mode="reverse")

    def project(self, VecToProject):
        """Apply the projector to a vector."""
        ProjectedVec = VecToProject.duplicate()
        self.mult(None, VecToProject, ProjectedVec)
        return ProjectedVec

    def projectTranspose(self, VecToProject):
        """Apply the projector to a vector."""
        ProjectedVec = VecToProject.duplicate()
        self.multTranspose(None, VecToProject, ProjectedVec)
        return ProjectedVec

    def getResidual(self, SolVec=None):
        """
        Returns the current residual. Used to 
        extract the initial residual in geneo 
        coarse problem and the laste residual 
        in kernel coarse problem.
        
        """
        Solver = self.FetiSolver
        DualSchur = Solver.getDualSchurMatrix()
        UnprojectedRhs = Solver.getFetiRhs(projected=False)
        if SolVec is None:
            SolVec = Solver.getInterfacePbSolution()
        else: assert(isinstance(SolVec, PETSc.Vec))
        return UnprojectedRhs - DualSchur *  SolVec

    def testProjector(self):
        """Tests the projector."""
        raise NotImplementedError

class KernelProjector(BaseProjector):
    """
    Context of the projector of kernel-based coarse problem.

    Parameters
    ----------
    Solver : 'feti.FetiSolver'
        The Feti solver object.

    Attributes
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    """

    def __init__(self, Solver):
        self.multComplete = self.mult_1
        self.multTransposeComplete = self.mult_2
        BaseProjector.__init__(self, Solver)

    def getCoarseSpaceType(self):
        """Returns the type of the coarse space"""
        return "kernel"

    def getCoarsePbLocSize(self):
        """
        Returns the number of vectors which the 
        subdomain contribute to the coarse problem.
        
        """
        if not self.isCoarsePbProc(): return 0
        else: return len(self.CoarsePbData)

    def buildCompleteVersionTest(self):
        """
        Returns a bool that checks whether to use the 
        complete version of the projector or not
        
        """
        return self.FetiSolver.getCoarseSpaceType() == "geneo"

    def getCompleteVersionMat(self):
        """
        Returns the matrix used in the complete 
        version of the projector (not the 
        projector matrix)
        
        """
        return self.FetiSolver.getGlobalPcMatrix()

    def buildCoarsePbData(self):
        """
        Builds the null space basis of the local matrix. The
        result is returned as a list of PETSc vectors that
        spans the null space.
        
        """
        Solver = self.FetiSolver
        LocalPetscKsp = Solver.getLocalKsp()
        LocalPetscMatrix = Solver.getLocalMatrix(petsc=True)
        NullSpaceBasis = []
        MumpsFactorMat = LocalPetscKsp.getPC().getFactorMatrix()
        NullSpaceSize = MumpsFactorMat.getMumpsInfog(28)

        for idx in range(NullSpaceSize):
            TempVec = LocalPetscMatrix.getVecRight()
            MumpsFactorMat.setMumpsIcntl(25, idx + 1)
            LocalPetscKsp.solve(TempVec, TempVec)
            for OtherVec in NullSpaceBasis[:idx]:
                TempVec -= TempVec.dot(OtherVec) * OtherVec
            TempVec.normalize()
            NullSpaceBasis.append(TempVec)
        
        NullSpace = PETSc.NullSpace().create(
            vectors=NullSpaceBasis, comm=MPI.COMM_SELF)

        assert(NullSpace.test(LocalPetscMatrix))

        MumpsFactorMat.setMumpsIcntl(25, 0)

        return None if len(NullSpaceBasis) == 0 else NullSpaceBasis

    def buildConstraintsMatrix(self):
        """Builds and returns the constraints matrix."""
        if not self.isCoarsePbProc(): return None
        assert(self.isCoarsePbProc())

        Numbering = self.FetiSolver.getInterfaceNumbering()
        GlbDualDofs = Numbering.getGlobalInterfaceDofs()
        PrimalInterfaceDofs = Numbering.getLocalInterfaceDofs()
        LocalDualIntrfcSize = Numbering.getLocalDualInterfaceSize()
        InterfacePbSize = Numbering.getGlobalInterfaceSize()
        
        LocalRowsNbr = self.getCoarsePbLocSize()
        MatSize = ((LocalRowsNbr, None), (None, InterfacePbSize))

        G_t = PETSc.Mat().createAIJ(MatSize, 
            nnz=LocalDualIntrfcSize, comm=self.CoarsePbComm)
        G_t.setUp()

        rstart, rend = G_t.getOwnershipRange()
        assert(rend - rstart == LocalRowsNbr)

        for vec, row in zip(self.CoarsePbData, range(rstart, rend)):
            TempArr = Numbering.primalToDualProjection( \
                vec.getArray()[PrimalInterfaceDofs], ApplyMultiplicity=False)
            cols = np.nonzero(TempArr)[0].astype(np.int32)
            vals = TempArr[cols]
            G_t.setValues([row], GlbDualDofs[cols], vals)
        
        G_t.assemble()

        return G_t.transpose()

    def buildInitialGuess(self):
        """Builds the initial guess of kernel projector"""
        VecSize = self.CpProcsOwnedVec.getSize()
        ReusltVec = PETSc.Vec().createMPI( \
            VecSize, comm=MPI.COMM_WORLD)
        if self.isCoarsePbProc():
            ConstraintsRhs = self.getConstraintsRhs()
            self.CoarsePbKsp.solve(
                ConstraintsRhs, ConstraintsRhs)
            self.ConstraintsMatrix.mult(
                ConstraintsRhs, self.CpProcsSubCommVec)
            self.CpProcsOwnedVec.getArray()[:] = \
                self.CpProcsSubCommVec.getArray()[:]
        if not self.useCompleteVersion():
            self.VecsScatter.scatter(self.CpProcsOwnedVec, 
                ReusltVec, mode="reverse")
        else:
            self.VecsScatter.scatter(self.CpProcsOwnedVec, 
                self.TempGlbVec, mode="reverse")
            self.CmpltVersionMat.mult(
                self.TempGlbVec, ReusltVec)
        return ReusltVec

    def getSchurKernelBasis(self):
        """
        Returns the vectors that spans the kernel 
        of the local primal schur complement
        
        """
        if not self.isCoarsePbProc(): return None
        Numbering = self.FetiSolver.getInterfaceNumbering()
        PrimalDofs = Numbering.getLocalInterfaceDofs()
        PrimalDofsIs = PETSc.IS().createGeneral( \
            PrimalDofs.astype(np.int32), comm=MPI.COMM_SELF)
        result = [vec.copy().getSubVector(PrimalDofsIs) 
            for vec in self.CoarsePbData]
        for vec_idx in range(len(result)):
            for ovec_idx in range(0, vec_idx):
                result[vec_idx] -= result[ovec_idx] * \
                    result[vec_idx].dot(result[ovec_idx])
            result[vec_idx].normalize()
        return result

    def setFetiInitialization(self, InitializationVec):
        """Sets the initialization of FETI."""
        assert(InitializationVec.getOwnershipRange() \
             == self.InitialGuess.getOwnershipRange())
        InitializationVec.getArray()[:] = \
            self.InitialGuess.getArray()[:]

    def getRigidBodyDisplacement(self):
        """Returns the rigid body displacement of the subdomain"""
        Coeffs = self.getConstraintsLagrange()
        if Coeffs is None: return np.array([])
        assert(Coeffs.size == self.getCoarsePbLocSize())
        result = self.CoarsePbData[0].duplicate()
        result.zeroEntries()
        result.maxpy(Coeffs, self.CoarsePbData)
        return result.getArray()

    def getConstraintsLagrange(self):
        """xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx"""
        residual = self.getResidual()
        if self.useCompleteVersion():
            residual = self.CmpltVersionMat(residual)
        self.VecsScatter.scatter(residual, 
            self.CpProcsOwnedVec, mode="forward")
        if self.isCoarsePbProc():
            LocalCpSize = self.getCoarsePbLocSize()
            TempVec = PETSc.Vec().createMPI(
                (LocalCpSize, None), comm=self.CoarsePbComm)
            self.CpProcsSubCommVec.getArray()[:] = \
                self.CpProcsOwnedVec.getArray()[:]
            self.ConstraintsMatrix.multTranspose(
                self.CpProcsSubCommVec, TempVec)
            self.CoarsePbKsp.solve(TempVec, TempVec)
            return TempVec.getArray()
        else: return None

    def getConstraintsRhs(self):
        """
        Returns a vector on the coarse problem 
        subcommunicator that represents the 
        constraint right-hand side
        
        """
        if not self.isCoarsePbProc(): return None
        LocalCpSize = self.getCoarsePbLocSize()
        ConstraintsRhs = PETSc.Vec().createMPI(
            (LocalCpSize, None), comm=self.CoarsePbComm)
        LocalRhs = self.FetiSolver.getLocalRhs()
        ConstraintsRhs.getArray()[:] = \
            np.array([-vec.getArray().dot(LocalRhs) 
            for vec in self.CoarsePbData], dtype=float)[:]
        return ConstraintsRhs

    def testProjector(self):
        """Tests the projector."""
        Rank = MPI.COMM_WORLD.Get_rank()
        VecSize = self.CpProcsOwnedVec.getSize()
        UnitVec = PETSc.Vec().createMPI( \
            VecSize, comm=MPI.COMM_WORLD)
        if self.isCoarsePbProc():
            SubRank = self.CoarsePbComm.Get_rank()
            TempVec = self.ConstraintsMatrix.getVecRight()
            if SubRank == 0:
                result = - np.ones(VecSize, dtype=float)
        for i in range(VecSize):
            UnitVec.zeroEntries()
            if Rank == 0 : UnitVec.setValue(i, 1.)
            UnitVec.assemble()
            ProjectedVec = self.project(UnitVec)
            self.VecsScatter.scatter(ProjectedVec, 
                self.CpProcsOwnedVec, mode="forward")
            if self.isCoarsePbProc():
                self.CpProcsSubCommVec.getArray()[:] = \
                    self.CpProcsOwnedVec.getArray()[:]
                self.ConstraintsMatrix.multTranspose(
                    self.CpProcsSubCommVec, TempVec)
                VecNorme = TempVec.norm(norm_type=PETSc.NormType.INFINITY)
                if SubRank == 0: result[i] = VecNorme
        if self.isCoarsePbProc() and SubRank == 0:
            CpType = self.getCoarseSpaceType()
            print((CpType + " : max_ij (G^T * P * I)[i, j]\t: "
                "{}").format(np.abs(result).max()))

class GeneoProjector(BaseProjector):
    """
    Context of the projector of geneo-based coarse problem.

    Parameters
    ----------
    Solver : 'feti.FetiSolver'
        The Feti solver object.

    Attributes
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    """

    def __init__(self, Solver):
        if Solver.getPrecondType() != "dirichlet":
            self.multComplete = self.mult_2
            self.multTransposeComplete = self.mult_1
            BaseProjector.__init__(self, Solver)
        else: raise ValueError("Geneo coarse problem isn't "
                "implemented for dirichlet preconditionner")

    def getCoarseSpaceType(self):
        """Returns the type of the coarse space"""
        return "geneo"

    def getCoarsePbLocSize(self):
        """
        Returns the number of vectors which the 
        subdomain contribute to the coarse problem.
        
        """
        if not self.isCoarsePbProc(): return 0
        else: return len(self.CoarsePbData[0])

    def buildCompleteVersionTest(self):
        """
        Returns a bool that checks whether to use the 
        complete version of the projector or not
        
        """
        return True

    def getCompleteVersionMat(self):
        """
        Returns the matrix used in the complete 
        version of the projector (not the 
        projector matrix)
        
        """
        return self.FetiSolver.getDualSchurMatrix()
    
    def getThreshold(self):
        """
        Returns the threshold used to select 
        eigenvectors of geneo coarse problem
        
        """
        return self.FetiSolver.getGeneoThreshold()

    def buildCoarsePbData(self):
        """Builds the coarse space data."""
        S = self.getLeftHandSideMatrix()
        P = self.getRightHandSideMatrix()
        Tau = self.getThreshold()
        PbSize, _ = S.getSize()
        EigVecsNbr = min(PbSize // 10, 10)
        V0 = self.getDeflationSpace()
        eps = SLEPc.EPS().create(comm=MPI.COMM_SELF)
        eps.setDimensions(nev=EigVecsNbr)
        eps.setProblemType(SLEPc.EPS.ProblemType.GHIEP)
        eps.setOperators(S, P)
        
        # eps.setWhichEigenpairs(SLEPc.EPS.Which.SMALLEST_MAGNITUDE)
        
        eps.setWhichEigenpairs(SLEPc.EPS.Which.TARGET_REAL)
        eps.setTarget(0.)
        
        if V0 is not None:
            eps.setDeflationSpace(V0)
        
        # KspCtx = KspContexte(self)
        # ksp = PETSc.KSP().createPython(context=KspCtx, comm=MPI.COMM_SELF)
        # ST = eps.getST()
        # ST.setType("sinvert")
        # ST.setKSP(None)
        # op = ST.getOperator()
        # op.setType(PETSc.Mat.Type.PYTHON)
        # op.setPythonContext(KspContexte(self))
        # print("The type of st operator is : {}".format(op.getType()))
        # ST.restoreOperator(op)

        eps.solve()

        EigenValues = []
        EigenVectors = []

        # self.testEigenVector(eps, S, P)

        for idx in range(eps.getConverged()):
            EigVal = eps.getEigenvalue(idx)
            EigVec = P.getVecLeft()
            eps.getEigenvector(idx, EigVec)
            if abs(EigVal) < Tau:
                EigenValues.append(EigVal)
                EigenVectors.append(EigVec)
            else: break
        
        return None if not EigenValues else [EigenValues, EigenVectors]

    def testEigenVector(self, eps, S, P):
        """tests computed eigenvectors"""
        ToPrint = "{} eigenvector converged for {}".format(
            eps.getConverged(), MPI.COMM_WORLD.Get_rank())
        for num in range(eps.getConverged()):
            EigVal = eps.getEigenvalue(num)
            EigVec = P.getVecLeft()
            eps.getEigenvector(num,EigVec)
            rayleigh = EigVec.dot(S(EigVec)) / EigVec.dot(P(EigVec))
            error = int(math.log10(abs(rayleigh - \
                EigVal.real) / min(rayleigh, EigVal.real)))
            ToPrint += ("\n\teigenvalue number {} = {} (error"
                " of log order {})").format(num, EigVal, error)
        print(ToPrint)

    def buildConstraintsMatrix(self):
        """Builds and returns the constraints matrix."""
        Solver = self.FetiSolver
        Numbering = Solver.getInterfaceNumbering()
        GlbDualDofs = Numbering.getGlobalInterfaceDofs()
        
        PcGlbMatIs = Solver.getGlobalPcMatrix()
        KernelProjMat = Solver.getProjectorMatrix("kernel")
        
        GlbTempVec_r, GlbTempVec_l = PcGlbMatIs.createVecs()
        
        LocCpSizes = self.getLocCpSizes()
        MyRank = MPI.COMM_WORLD.Get_rank()
        VecsList = self.CoarsePbData[1] \
            if self.isCoarsePbProc() else None
        
        if self.isCoarsePbProc():
            GlbColsNbr = LocCpSizes.sum()
            LocRowsNbr, _ = self.CpProcsOwnedVec.getSizes()
            MatSize = ((LocRowsNbr, None), (None, GlbColsNbr))
            G = PETSc.Mat().createAIJ(MatSize, comm=self.CoarsePbComm)
            G.setUp()
            assert(G.getSize()[0] == Numbering.getGlobalInterfaceSize())
            start, stop = G.getOwnershipRange()
            Indices = np.arange(start, stop, dtype=np.int32)

        col = 0
        
        for Rank in range(LocCpSizes.size):
            VecsNbr = LocCpSizes[Rank]
            if VecsNbr == 0: continue
            for idx in range(VecsNbr):
                GlbTempVec_r.zeroEntries()
                if Rank == MyRank:
                    vec = VecsList[idx]
                    NpVec = Numbering.primalToDualProjection(
                        vec.getArray(), ApplyMultiplicity=False)
                    GlbTempVec_r.setValues(GlbDualDofs, NpVec)
                GlbTempVec_r.assemble()
                if KernelProjMat is not None:
                    PcGlbMatIs.mult(GlbTempVec_r, GlbTempVec_l)
                    KernelProjMat.mult(GlbTempVec_l, GlbTempVec_r)
                self.VecsScatter.scatter(GlbTempVec_r, 
                    self.CpProcsOwnedVec, mode="forward")
                if self.isCoarsePbProc():
                    TempArr = self.CpProcsOwnedVec.getArray()
                    rows = np.nonzero(TempArr)[0].astype(np.int32)
                    vals = TempArr[rows]
                    G.setValues(Indices[rows], [col], vals)
                col += 1
        
        if self.isCoarsePbProc(): G.assemble()
        return G if self.isCoarsePbProc() else None

    def getLocCpSizes(self):
        """
        Returns a vector that contains the 
        number of eigenvectors for each proc
        
        """
        MySize = np.array([self.getCoarsePbLocSize()], dtype=np.int32)
        AllSizes = np.zeros(MPI.COMM_WORLD.Get_size(), dtype=np.int32)
        MPI.COMM_WORLD.Allgather([MySize, MPI.INT], [AllSizes, MPI.INT])
        MyRank = MPI.COMM_WORLD.Get_rank()
        assert(AllSizes[MyRank] == MySize)
        return AllSizes
    
    def getRightHandSideMatrix(self):
        """
        Returns the left hand side matrix of the 
        generelized eigenvalue problem
        
        """
        Numbering = self.FetiSolver.getInterfaceNumbering()
        PrimalToDualMat = Numbering.getProjectionMatrix()

        LocGlbMap = self.FetiSolver.getLocGlbMap()
        MapIndices = PETSc.IS().createGeneral( \
            LocGlbMap.getIndices(), comm=MPI.COMM_SELF)

        GlbPrecondMatrix = self.FetiSolver.getGlobalPcMatrix()
        assert(GlbPrecondMatrix.getType() == "mpiaij")

        LocMat = GlbPrecondMatrix.createSubMatrices(MapIndices)[0]
        return LocMat.PtAP(PrimalToDualMat)
    
    def getLeftHandSideMatrix(self):
        """
        Returns the right hand side matrix of the 
        generelized eigenvalue problem
        
        """
        return self.FetiSolver.getLocPriSchurComp()
    
    def getDeflationSpace(self):
        """
        Returns the deflation space to use in 
        the generelized eigenproblem solver 
        (it contains the kernel of the primal 
        local schur complement)
        
        """
        KernelProjCtx = self.FetiSolver.getProjectorContext()
        return None if KernelProjCtx is None \
            else KernelProjCtx.getSchurKernelBasis()

    def buildInitialGuess(self):
        """Builds the initial guess of geneo projector"""
        VecSize = self.CpProcsOwnedVec.getSize()
        ReusltVec = PETSc.Vec().createMPI( \
            VecSize, comm=MPI.COMM_WORLD)
        FirstResidual = self.getFirstResidual()
        self.VecsScatter.scatter(FirstResidual, 
            self.CpProcsOwnedVec, mode="forward")
        if self.isCoarsePbProc():
            self.CpProcsSubCommVec.getArray()[:] = \
                self.CpProcsOwnedVec.getArray()[:]
            self.ConstraintsMatrix.multTranspose(
                self.CpProcsSubCommVec, self.TempCpCommVec)
            self.CoarsePbKsp.solve(
                self.TempCpCommVec, self.TempCpCommVec)
            self.ConstraintsMatrix.mult(
                self.TempCpCommVec, self.CpProcsSubCommVec)
            self.CpProcsOwnedVec.getArray()[:] = \
                self.CpProcsSubCommVec.getArray()[:]
        self.VecsScatter.scatter(self.CpProcsOwnedVec, 
            ReusltVec, mode="reverse")
        return ReusltVec

    def getFirstResidual(self):
        """Returns the initial residual"""
        Solver = self.FetiSolver
        KernelProjCtx = Solver.getProjectorContext()
        if KernelProjCtx is not None:
            result = self.getResidual(SolVec= \
                KernelProjCtx.getInitialGuess())
        else: result = Solver.getFetiRhs(projected=False)
        return result

    def testProjector(self):
        """Tests the projector."""
        Rank = MPI.COMM_WORLD.Get_rank()
        if self.isCoarsePbProc():
            _, VecSize = self.ConstraintsMatrix.getSize()
            UnitVec = self.ConstraintsMatrix.getVecRight()
            SubRank = self.CoarsePbComm.Get_rank()
        else: VecSize = 0
        VecSize = MPI.COMM_WORLD.allreduce(
            VecSize, op=MPI.MAX)
        TempVec = PETSc.Vec().createMPI( \
            self.CpProcsOwnedVec.getSize(), 
            comm=MPI.COMM_WORLD)
        if Rank == 0: result = - np.ones(VecSize, dtype=float)
        for i in range(VecSize):
            if self.isCoarsePbProc(): 
                UnitVec.zeroEntries()
                if SubRank == 0: UnitVec.setValue(i, 1.)
                UnitVec.assemble()
                self.ConstraintsMatrix.mult(
                    UnitVec, self.CpProcsSubCommVec)
                self.CpProcsOwnedVec.getArray()[:] = \
                    self.CpProcsSubCommVec.getArray()[:]
            self.VecsScatter.scatter(self.CpProcsOwnedVec, 
                TempVec, mode="reverse")
            ProjectedVec = self.project(TempVec)
            VecNorme = ProjectedVec.norm(
                norm_type=PETSc.NormType.INFINITY)
            if Rank == 0: result[i] = VecNorme
        if Rank == 0:
            CpType = self.getCoarseSpaceType()
            print((CpType + " : max_ij (P * C * I)[i, j]\t: "
                "{}").format(np.abs(result).max()))

# class KspContexte:
#     def __init__(self, GeneoProjCtx):
#         FetiSolver = GeneoProjCtx.FetiSolver
#         self.LocalPetscKsp = FetiSolver.getLocalKsp()
#         Numbering = FetiSolver.getInterfaceNumbering()
#         self.PrimalDofs = Numbering.getLocalInterfaceDofs()
#         self.TempVec = self.LocalPetscKsp.mat_op.createVecLeft()
    
    # def solve(self, ksp, b, x):
    #     self.TempVec.zeroEntries()
    #     self.TempVec.getArray()[self.PrimalDofs] = b.copy().getArray()[:]
    #     self.LocalPetscKsp.solve(self.TempVec, self.TempVec)
    #     x.getArray()[:] = self.TempVec.getArray()[self.PrimalDofs]

    # def mult(self, mat, x, y):
    #     self.TempVec.zeroEntries()
    #     self.TempVec.getArray()[self.PrimalDofs] = x.copy().getArray()[:]
    #     self.LocalPetscKsp.solve(self.TempVec, self.TempVec)
    #     y.getArray()[:] = self.TempVec.getArray()[self.PrimalDofs]
