"""
This module implements the parallel version of the dual domain decomposition 
method FETI and all functions that allows interacting with it.

"""
from scipy.sparse import csr_matrix, bsr_matrix
from scipy.sparse.linalg import spsolve
from petsc4py import PETSc
from mpi4py import MPI
import numpy as np
import os

from ..interface_numbering import FetiInterfaceNumbering
from ...pkgutils import SplitMatrix, GetSubMatrix, IsParallelStudy
from ... import pkgdatastr as datastr
from .projector import KernelProjector, GeneoProjector

class FetiSolver:
    """
    Feti solver.

    Parameters
    ----------
    dtype : 'str'
        The data type. 'R' for real or 'C' for complex.

    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    Attributes
    ----------
    dtype : 'str'
        The data type. 'R' for real or 'C' for complex.

    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    """

    def __init__(self, InterfaceNumbering, LocalMatrix, 
        LocalRhs, PrecondType=None, CoarseSpace=None,
        GeneoThreshold=0.55, Info=False):
        
        assert(isinstance(InterfaceNumbering, FetiInterfaceNumbering))
        assert(isinstance(LocalMatrix, (np.ndarray, bsr_matrix, csr_matrix)))
        assert(isinstance(LocalRhs, np.ndarray))
        
        assert(PrecondType in ["dirichlet", 
            "lumped", "superlumped", "jacobi", None])
        assert(CoarseSpace in ["kernel", "geneo", None])
        assert(isinstance(Info, bool))

        if not IsParallelStudy():
            raise ValueError("Feti solver "
                "cannot be used for sequential studies")

        self.dtype = 'R' if LocalMatrix.dtype == float else 'C'
        assert(self.dtype == InterfaceNumbering.getDataType())

        self.PrecondType = "dirichlet" if \
            PrecondType is None else PrecondType
        
        self.Verbose = Info
        self.CoarseSpaceType = CoarseSpace

        self.InterfaceNumbering = InterfaceNumbering

        self.LocalRhs = LocalRhs
        self.LocalPetscRhs = self.buildLocalPetscRhs(LocalRhs)
        
        self.LocalMatrix = LocalMatrix
        self.LocalPetscMatrix = self.buildLocalPetscMatrix(LocalMatrix)
        
        self.LocalPetscKsp = self.buildLocalPetscKsp()

        self.LocPriSchurComp = None if not ( \
            self.PrecondType == "dirichlet" or CoarseSpace == \
            "geneo") else self.buildLocPriSchurComp()

        Temptuple = self.buildLGMAPAndVecs()
        self.LocGlbMap = Temptuple[0]
        self.IntrfcPbGlbSol = Temptuple[1]
        self.IntrfcPbLocSol = Temptuple[2]
        self.GlbToLocScatter = Temptuple[3]

        self.DualSchurMatrix = self.buildDualSchurMatrix()
        self.GlabalPcMatrix = self.buildGlobalPcMatrix()

        self.GlobalKsp = PETSc.KSP().create()

        self.KernelProjectorCtx = None if self.CoarseSpaceType \
            is None else KernelProjector(self).createProj()

        self.GeneoThreshold = GeneoThreshold        

        self.GeneoProjectorCtx = None if self.CoarseSpaceType \
            != "geneo" else GeneoProjector(self).createProj()
        
        self.updateCpType()

        if self.verbose(): self.printInfo()

    def getCommRank(self):
        """Returns the rank within the communicator."""
        return MPI.COMM_WORLD.Get_rank()

    def getCommSize(self):
        """Returns the rank within the communicator."""
        return MPI.COMM_WORLD.Get_size()

    def verbose(self):
        """
        Returns a bool that determines 
        whether to print infos or not
        
        """
        return self.Verbose

    def printInfo(self):
        """Prints infos about the solver context"""
        if self.getCommRank() == 0:
            MatSize = self.InterfaceNumbering.getGlobalInterfaceSize()
            GlbPcMatType = self.GlabalPcMatrix.getType()
            print("The interface problem size "
                "\t\t\t: {}".format(MatSize))
            print("The preconditionner type \t\t\t: {} ({})"
                "".format(self.PrecondType, GlbPcMatType))
            print("The coarse problem type \t\t\t: {}"
                "".format(self.CoarseSpaceType))
        MPI.COMM_WORLD.barrier()
        if self.kernelCoarsePbExists():
            self.KernelProjectorCtx.printInfo()
        MPI.COMM_WORLD.barrier()
        if self.geneoCoarsePbExists():
            self.GeneoProjectorCtx.printInfo()

    def updateCpType(self):
        """Updates the coarse space type"""
        if self.geneoCoarsePbExists():
            self.CoarseSpaceType = "geneo"
        elif self.kernelCoarsePbExists():
            self.CoarseSpaceType = "kernel"
        else: self.CoarseSpaceType = None

    def updateOperators(self, LocalMatrix=None, LocalRhs=None):
        """updates local operators for next resolutions"""
        if LocalRhs is not None:
            self.LocalRhs = LocalRhs
            self.LocalPetscRhs.destroy()
            self.LocalPetscRhs = self.buildLocalPetscRhs(LocalRhs)

        if LocalMatrix is not None:
            self.LocalMatrix = LocalMatrix
            self.LocalPetscMatrix.destroy()
            self.LocalPetscMatrix = self.buildLocalPetscMatrix(LocalMatrix)

            self.LocalPetscKsp.destroy()
            self.LocalPetscKsp = self.buildLocalPetscKsp()

            if self.LocPriSchurComp is not None:
                self.LocPriSchurComp.destroy()
                self.LocPriSchurComp = self.buildLocPriSchurComp()

        self.DualSchurMatrix.destroy()
        self.DualSchurMatrix = self.buildDualSchurMatrix()

        self.GlabalPcMatrix.destroy()
        self.GlabalPcMatrix = self.buildGlobalPcMatrix()

        self.GlobalKsp.destroy()
        self.GlobalKsp = PETSc.KSP().create()

        self.IntrfcPbGlbSol.zeroEntries()
        self.IntrfcPbLocSol.zeroEntries()

        if self.KernelProjectorCtx is not None:
            self.KernelProjectorCtx = \
                KernelProjector(self).createProj()

        if self.GeneoProjectorCtx is not None:
            self.GeneoProjectorCtx = \
                GeneoProjector(self).createProj()
        
        self.updateCpType()

    def getPrecondType(self):
        """Returns the preconditionner type"""
        return self.PrecondType

    def getCoarseSpaceType(self):
        """Returns the type of the coarse space."""
        return self.CoarseSpaceType

    def getInterfaceNumbering(self):
        """Returns the dual interface numbering object."""
        return self.InterfaceNumbering
    
    def getLocalMatrix(self, petsc=False):
        """Returns the local matrix."""
        assert(isinstance(petsc, bool))
        return self.LocalPetscMatrix \
            if petsc else self.LocalMatrix

    def getLocalRhs(self, petsc=False):
        """Returns the local matrix."""
        assert(isinstance(petsc, bool))
        return self.LocalPetscRhs \
            if petsc else self.LocalRhs

    def getLocGlbMap(self):
        """Return the local to global mapping"""
        return self.LocGlbMap

    def getLocPriSchurComp(self):
        """
        Returns a petsc matrix that represents
        the local primal schur complement.
        
        """
        return self.LocPriSchurComp

    def getLocalKsp(self):
        """Returns the local petsc Ksp."""
        return self.LocalPetscKsp
    
    def getDataType(self):
        """Returns the data type."""
        return float if self.dtype == 'R' else complex
    
    def getGeneoThreshold(self):
        """
        Returns the threshold used to select 
        eigenvectors of geneo coarse problem
        
        """
        return self.GeneoThreshold

    def kernelCoarsePbExists(self):
        """
        Returns a bool that determines whether 
        the natural coarse problem existe or not.
        
        """
        return self.KernelProjectorCtx is not None

    def geneoCoarsePbExists(self):
        """
        Returns a bool that determines whether 
        the geneo coarse problem existe or not.
        
        """
        return self.GeneoProjectorCtx is not None

    def coarseProblemExists(self):
        """
        Returns a bool that determines whether 
        the coarse problem existe or not.
        
        """
        return self.kernelCoarsePbExists() \
            or self.geneoCoarsePbExists()
    
    def getProjectorContext(self, ProjType="kernel"):
        """Returns the projector matrix"""
        assert(ProjType in ["kernel", "geneo"])
        if ProjType == "kernel" and self.kernelCoarsePbExists():
            return self.KernelProjectorCtx
        elif ProjType == "geneo" and self.geneoCoarsePbExists():
            return self.GeneoProjectorCtx
        else: return None

    def getProjectorMatrix(self, ProjType="kernel"):
        """Returns the projector matrix"""
        ProjCtx = self.getProjectorContext(ProjType)
        return None if ProjCtx is None \
            else ProjCtx.getProjectorMatrix()
    
    def buildLocalPetscMatrix(self, SpMat):
        """Builds the local petsc matrix."""
        if isinstance(SpMat, bsr_matrix):
            SpMat = SpMat.tocsr()
        elif isinstance(SpMat, np.ndarray):
            SpMat = csr_matrix(SpMat)
        return PETSc.Mat().createAIJ(
            size=SpMat.shape, csr=(SpMat.indptr, 
            SpMat.indices, SpMat.data), comm=MPI.COMM_SELF)

    def buildLocPriSchurComp(self):
        """Builds the local primal schur complement."""
        MatSize = self.InterfaceNumbering.getLocalPrimalInterfaceSize()
        SchurCmplmnt = PETSc.Mat().createPython( \
            size=MatSize, comm=MPI.COMM_SELF)
        SchurCmplmnt.setPythonContext(LocPriSchurCompCtx(self))
        SchurCmplmnt.setUp()
        return SchurCmplmnt

    def buildLocalPetscRhs(self, LocVec):
        """Builds the local petsc rhs vector."""
        return PETSc.Vec().createWithArray(
            LocVec, comm=MPI.COMM_SELF)

    def buildLocalPetscKsp(self):
        """Builds the local petsc ksp."""
        PetscKsp = PETSc.KSP()
        PetscKsp.create(PETSc.COMM_SELF)
        PetscKsp.setOperators(self.LocalPetscMatrix)
        PetscKsp.setType('preonly')
        PetscPc = PetscKsp.getPC()
        PetscPc.setType('lu')
        PetscPc.setFactorSolverType('mumps')
        PetscPc.setFactorSetUpSolverType()

        MumpsFactorMat = PetscPc.getFactorMatrix()
        MumpsFactorMat.setMumpsIcntl(14, 30)
        MumpsFactorMat.setMumpsIcntl(24, 1)
        MumpsFactorMat.setMumpsCntl(3, 1e-6)

        PetscKsp.setUp()

        return PetscKsp

    def buildLGMAPAndVecs(self):
        """
        Returns the local to global mapping, the two PETSc.Vec 
        where respectively the global and local solutions of the 
        interface problem will be stored, and finally the scatter
        object that will be used to extract the local solution 
        vector from the global one.
        
        """
        SdGlbDofs = self.InterfaceNumbering.getGlobalInterfaceDofs()
        GlbVecSize = self.InterfaceNumbering.getGlobalInterfaceSize()
        LocVecSize = self.InterfaceNumbering.getLocalDualInterfaceSize()

        SdGlbDofsIs = PETSc.IS().createGeneral( \
            SdGlbDofs, comm=MPI.COMM_WORLD)
        
        LocGlbMap = PETSc.LGMap().createIS(SdGlbDofsIs)
        
        IntrfcPbGlbSol = PETSc.Vec().createMPI( \
            GlbVecSize, comm=MPI.COMM_WORLD)
        IntrfcPbGlbSol.setLGMap(LocGlbMap)

        IntrfcPbLocSol = PETSc.Vec().createSeq(LocVecSize)

        GlbToLocScatter = PETSc.Scatter().create( \
            IntrfcPbGlbSol, SdGlbDofsIs, IntrfcPbLocSol, None)

        return LocGlbMap, IntrfcPbGlbSol, IntrfcPbLocSol, GlbToLocScatter
    
    def getLocalShellMatrix(self):
        """Returns the local shell matrix."""
        MatSize = self.InterfaceNumbering.getLocalDualInterfaceSize()
        LocShellMat = PETSc.Mat().createPython( \
            size=MatSize, comm=MPI.COMM_SELF)
        LocShellMat.setPythonContext(LocMatCtx(self))
        LocShellMat.setUp()
        return LocShellMat

    def buildDualSchurMatrix(self):
        """Builds the interface problem matrix (PETSc matis)."""
        MatSize = self.InterfaceNumbering.getGlobalInterfaceSize()
        
        DualSchurMatrix = PETSc.Mat().createPython( \
            size=MatSize, comm=MPI.COMM_WORLD)
        DualSchurMatrix.setType(PETSc.Mat.Type.IS)
        DualSchurMatrix.setLGMap(self.LocGlbMap)

        LocMatShell = self.getLocalShellMatrix()
        DualSchurMatrix.setISLocalMat(LocMatShell)
        DualSchurMatrix.assemble()

        return DualSchurMatrix

    def getDualSchurMatrix(self):
        """Returns the interface problem matrix (PETSc matis)."""
        return self.DualSchurMatrix

    def getGlobalKsp(self):
        """Returns the global KSP of the interface problem"""
        return self.GlobalKsp
    
    def getInterfacePbSolution(self):
        """Returns the solution of the dual interface problem"""
        return self.IntrfcPbGlbSol

    def getFetiMatrix(self):
        """
        Sets up the matrix of the interface problem.

        """
        MatSize = self.InterfaceNumbering.getGlobalInterfaceSize()
        FetiMatrix = PETSc.Mat().createPython( \
            size=MatSize, comm=MPI.COMM_WORLD)
        FetiMatrixCtx = GlbMatCtx(self)
        FetiMatrix.setPythonContext(FetiMatrixCtx)
        FetiMatrix.setUp()
        return FetiMatrix

    def setUpPreconditionner(self):
        """
        Sets up the preconditioner of the interface problem.
        
        """
        Pc = self.GlobalKsp.getPC()
        Pc.setType(PETSc.PC.Type.PYTHON)
        Pc.setPythonContext(GlbPcCtx(self))
        Pc.setUp()

    def buildGlobalPcMatrix(self):
        """
        Builds the matrix used as preconditionner
        for the interface problem (PETSc matis).
        
        """
        MatSize = self.InterfaceNumbering.getGlobalInterfaceSize()
        
        GlbPcIs = PETSc.Mat().createPython( \
            size=MatSize, comm=MPI.COMM_WORLD)
        GlbPcIs.setType(PETSc.Mat.Type.IS)
        GlbPcIs.setLGMap(self.LocGlbMap)

        PcLocMat = self.getPcLocalMatrix()
        GlbPcIs.setISLocalMat(PcLocMat)
        GlbPcIs.assemble()

        if self.PrecondType != "dirichlet":
            GlbPcIs.convert(PETSc.Mat.Type.MPIAIJ)

        return GlbPcIs

    def getGlobalPcMatrix(self):
        """
        Returns the matrix used as preconditionner
        for the interface problem (PETSc matis).
        
        """
        return self.GlabalPcMatrix

    def getPcLocalMatrix(self):
        """
        Returns the local matrix used in the assembly of the 
        MATIS matrix that represents the preconditionner.
        
        """
        if self.PrecondType == "dirichlet":
            return self.getDirichletPcLocalMatrix()
        elif self.PrecondType == "lumped":
            return self.getLumpedPcLocalMatrix()
        elif self.PrecondType == "superlumped":
            return self.getSuperlumpedPcLocalMatrix()
        else: return self.getJacobiPcLocalMatrix()
    
    def getDirichletPcLocalMatrix(self):
        """
        Returns the local shell matrix used 
        in the Dirichlet preconditionner.
        
        """
        MatSize = self.InterfaceNumbering.getLocalDualInterfaceSize()
        LocShellPc = PETSc.Mat().createPython( \
            size=MatSize, comm=MPI.COMM_SELF)
        LocShellPc.setPythonContext(LocPcCtx(self))
        LocShellPc.setUp()
        return LocShellPc

    def getLumpedPcLocalMatrix(self):
        """
        Returns the local aij matrix used 
        in the Lumped preconditionner.
        
        """
        Numbering = self.InterfaceNumbering
        Scaling = 1. / Numbering.getDofsMultiplicity()
        DofsSigns = Numbering.getInterfaceDofsSigns()
        PrimalInterfaceDofs = Numbering.getLocalInterfaceDofs()
        DualInterfaceDofs = Numbering.getLocalDualInterfaceDofs()
        
        InterfaceMatrix = GetSubMatrix(self.LocalMatrix, 
            RowsIdx=PrimalInterfaceDofs, OutFormat="Dense")
        
        InterfaceMatrix = Scaling[:, None] * \
            InterfaceMatrix * Scaling
        
        InterfaceMatrix = DofsSigns[:, None] * \
            InterfaceMatrix[DualInterfaceDofs[:, None], \
            DualInterfaceDofs] * DofsSigns
        
        InterfaceMatrix = csr_matrix(InterfaceMatrix)

        return PETSc.Mat().createAIJ(size=InterfaceMatrix.shape, 
            csr=(InterfaceMatrix.indptr, InterfaceMatrix.indices, 
            InterfaceMatrix.data), comm=MPI.COMM_SELF)

    def getSuperlumpedPcLocalMatrix(self):
        """
        Returns the local aij matrix used 
        in the Superlumped preconditionner.
        
        """
        Numbering = self.InterfaceNumbering
        Scaling = 1. / Numbering.getDofsMultiplicity()
        DofsSigns = Numbering.getInterfaceDofsSigns()
        PrimalInterfaceDofs = Numbering.getLocalInterfaceDofs()
        DualInterfaceDofs = Numbering.getLocalDualInterfaceDofs()
        
        InterfaceMatrix = GetSubMatrix(self.LocalMatrix, 
            RowsIdx=PrimalInterfaceDofs, OutFormat="Dense")
        
        InterfaceMatrix = np.diag(np.diagonal(InterfaceMatrix))

        InterfaceMatrix = Scaling[:, None] * \
            InterfaceMatrix * Scaling
        
        InterfaceMatrix = DofsSigns[:, None] * \
            InterfaceMatrix[DualInterfaceDofs[:, None], \
            DualInterfaceDofs] * DofsSigns
        
        InterfaceMatrix = csr_matrix(InterfaceMatrix)

        return PETSc.Mat().createAIJ(size=InterfaceMatrix.shape, 
            csr=(InterfaceMatrix.indptr, InterfaceMatrix.indices, 
            InterfaceMatrix.data), comm=MPI.COMM_SELF)

    def getJacobiPcLocalMatrix(self):
        """
        Returns the local aij matrix used 
        in the Jacobi preconditionner.
        
        """
        Numbering = self.InterfaceNumbering
        BlocksNbr = Numbering.getBlocksNbr()
        if BlocksNbr == 1:
            raise ValueError("Jacobi preconditionner is only "
                "available for block-structured problems")
        Scaling = 1. / Numbering.getDofsMultiplicity()
        DofsSigns = Numbering.getInterfaceDofsSigns()
        PrimalInterfaceDofs = Numbering.getLocalInterfaceDofs()
        DualInterfaceDofs = Numbering.getLocalDualInterfaceDofs()
        BlockSize = PrimalInterfaceDofs.size
        
        InterfaceMatrix = GetSubMatrix(self.LocalMatrix, 
            RowsIdx=PrimalInterfaceDofs, OutFormat="Dense")
        
        for BlockIdx in np.arange(BlocksNbr - 1):
            RowStart = BlockSize * BlockIdx
            RowEnd = RowStart + BlockSize
            InterfaceMatrix[RowStart:RowEnd, RowEnd:] = 0.
            InterfaceMatrix[RowEnd:, RowStart:RowEnd] = 0.
        
        InterfaceMatrix = Scaling[:, None] * \
            InterfaceMatrix * Scaling
        
        InterfaceMatrix = DofsSigns[:, None] * \
            InterfaceMatrix[DualInterfaceDofs[:, None], \
            DualInterfaceDofs] * DofsSigns
        
        InterfaceMatrix = csr_matrix(InterfaceMatrix)

        return PETSc.Mat().createAIJ(size=InterfaceMatrix.shape, 
            csr=(InterfaceMatrix.indptr, InterfaceMatrix.indices, 
            InterfaceMatrix.data), comm=MPI.COMM_SELF)

    def getFetiLocalRhs(self):
        """Returns the interface problem local right-hand side."""
        PrimalInterfaceDofs = \
            self.InterfaceNumbering.getLocalInterfaceDofs()
        
        TempVec = self.LocalPetscRhs.duplicate()
        self.LocalPetscKsp.solve(self.LocalPetscRhs, TempVec)
        TempVec = TempVec.getArray()
        
        LocalRhs = \
            self.InterfaceNumbering.primalToDualProjection( \
            TempVec[PrimalInterfaceDofs], ApplyMultiplicity=False)
        
        return - LocalRhs

    def getFetiRhs(self, projected=True):
        """Returns the interface problem global right-hand side."""
        assert(isinstance(projected, bool))
        VecSize = self.InterfaceNumbering.getGlobalInterfaceSize()
        GlobalRhs = PETSc.Vec().createMPI( \
            VecSize, comm=MPI.COMM_WORLD)
        
        GlobalRhs.zeroEntries()
        GlobalRhs.setLGMap(self.LocGlbMap)
        
        LocDualIntrfcSize = \
            self.InterfaceNumbering.getLocalDualInterfaceSize()
        
        LocalRhs = self.getFetiLocalRhs()
        assert(LocalRhs.size == LocDualIntrfcSize)

        GlobalRhs.setValuesLocal(np.arange(LocDualIntrfcSize, \
            dtype=np.int32), LocalRhs, addv=True)
        
        GlobalRhs.assemble()

        if projected and self.geneoCoarsePbExists():
            GlobalRhs = self.GeneoProjectorCtx.projectTranspose(GlobalRhs)
        if projected and self.kernelCoarsePbExists():
            GlobalRhs = self.KernelProjectorCtx.projectTranspose(GlobalRhs)

        return GlobalRhs
    
    def setNonZeroInitialGuess(self, InitialGuess=None):
        """Sets a non-zero initial guess."""
        if self.kernelCoarsePbExists():
            self.KernelProjectorCtx.setFetiInitialization(self.IntrfcPbGlbSol)
            self.GlobalKsp.setInitialGuessNonzero(True)
        if InitialGuess is not None:
            assert(isinstance(InitialGuess, PETSc.Vec))
            assert(InitialGuess.getOwnershipRange() == \
                self.IntrfcPbGlbSol.getOwnershipRange())
            if not self.coarseProblemExists():
                self.GlobalKsp.setInitialGuessNonzero(True)
                self.IntrfcPbGlbSol.getArray()[:] = \
                    InitialGuess.getArray()[:]
            elif self.kernelCoarsePbExists():
                self.IntrfcPbGlbSol += \
                    self.KernelProjectorCtx.project(InitialGuess)
        self.IntrfcPbGlbSol.assemble()

    def execute(self, mtype=None, x0=None, \
        rtol=None, atol=None, maxiter=None, \
        residual_history=False):
        """Solves the dual interface problem."""
        
        assert(mtype in ["GMRES", "CG", "TFQMR", "MINRES"])

        FetiGlbMatrix = self.getFetiMatrix()
        FetiGlbRhs = self.getFetiRhs()

        self.IntrfcPbGlbSol.zeroEntries()
        
        self.GlobalKsp.setOperators(FetiGlbMatrix)
        self.GlobalKsp.setConvergenceHistory(reset=True)

        if mtype == "MINRES":
            self.GlobalKsp.setType(PETSc.KSP.Type.MINRES)
        elif mtype == "GMRES":
            self.GlobalKsp.setType(PETSc.KSP.Type.GMRES)
        elif mtype == "TFQMR":
            self.GlobalKsp.setType(PETSc.KSP.Type.TFQMR)
        else:
            self.GlobalKsp.setType(PETSc.KSP.Type.CG)
        
        self.setUpPreconditionner()
        self.setNonZeroInitialGuess(InitialGuess=x0)

        self.GlobalKsp.setTolerances(
            rtol=rtol, atol=atol, max_it=maxiter)
        self.GlobalKsp.solve(
            FetiGlbRhs, self.IntrfcPbGlbSol)

        if self.geneoCoarsePbExists():
            self.finalizeGeneoSolution()

        if self.verbose() and self.getCommRank() == 0:
            ConvergedReason = self.GlobalKsp.getConvergedReason()
            if ConvergedReason > 0:
                print("The interface problem converged"
                    " in \t\t:", self.GlobalKsp.its, "iterations.")
            else : print("The interface problem "
                "diverged : {} ({} iterations performed)"
                "".format(ConvergedReason, self.GlobalKsp.its))
        
        if residual_history and self.getCommRank() == 0:
            self.printResidualHistory(self.GlobalKsp)

    def finalizeGeneoSolution(self):
        """
        Finalize the solution in case 
        of geneo coarse problem
        
        """
        GeneoProj = self.GeneoProjectorCtx
        GeneoInitGuess = GeneoProj.getInitialGuess()
        
        if self.kernelCoarsePbExists():
            KernelProj = self.KernelProjectorCtx
            KernelInitGuess = KernelProj.getInitialGuess()       
            self.IntrfcPbGlbSol = KernelInitGuess + \
                GeneoInitGuess + KernelProj.project(
                GeneoProj.project(self.IntrfcPbGlbSol \
                - KernelInitGuess))
        else: self.IntrfcPbGlbSol = GeneoInitGuess + \
                GeneoProj.project(self.IntrfcPbGlbSol)

    def printResidualHistory(self, ksp, iternum=None):
        """Prints the residual history."""
        if self.getCommRank() == 0:
            prefix = "no" if not self.coarseProblemExists() \
                else "geneo" if self.geneoCoarsePbExists() else 'kernel'
            env_variable = os.getenv("ZHRSMR_FETI_ITERATIONS_SAMPLE_NBR")
            if iternum is None and env_variable is not None:
                iternum = int(env_variable)
            iternum = ksp.its if iternum is None \
                or ksp.its < iternum else iternum
            iterations = np.linspace(0, ksp.its - 1, iternum, dtype=int)
            ToPrint = prefix + "_cp_iterations_{} = ".format(self.getCommSize())
            TempStr = np.array2string(iterations, separator=', ', 
                threshold=iterations.size + 1)[1:-1]
            ToPrint += "[\n " + TempStr  + "\n]\n\n"

            ResidualArray = ksp.getConvergenceHistory()[iterations]
            ToPrint += prefix + "_cp_residuals_{}  = ".format(self.getCommSize())
            TempStr = np.array2string(ResidualArray, separator=', ',
                threshold=ResidualArray.size + 1)[1:-1]
            ToPrint += "[\n " + TempStr  + "\n]\n\n"
            
            if self.geneoCoarsePbExists():
                ToPrint += 'axi = getAxis(axis, {})\n'.format(self.getCommSize())
                
                ToPrint += ('axi.plot(kernel_cp_iterations_{0},'
                    ' kernel_cp_residuals_{0}, "-.bo", label="kernel-based '
                    'coarse problem")\n').format(self.getCommSize())
                ToPrint += ('axi.plot(geneo_cp_iterations_{0},'
                    ' geneo_cp_residuals_{0}, "-.go", label="geneo-based '
                    'coarse problem")\n').format(self.getCommSize())
                ToPrint += ('axi.plot(no_cp_iterations_{0}, '
                    'no_cp_residuals_{0}, "-.ro", label="no coarse '
                    'problem")\n').format(self.getCommSize())

                ToPrint += 'axi.set_yscale("log")\n'
                ToPrint += ('axi.set_title("{} subdomains",'
                    ' fontsize=14)\n').format(self.getCommSize())
                ToPrint += 'axi.legend(loc=2, prop={"size": 10})\n'
            
            print(ToPrint)

    def getRigidBodyDisplacement(self):
        """Returns the rigid body displacement of the subdomain"""
        prjctx = self.KernelProjectorCtx
        return np.array([]) if prjctx is None else \
            prjctx.getRigidBodyDisplacement()

    def extractLocalSolution(self):
        """
        Extracts the local solution of the interface 
        problem as a numpy array.
        
        """
        self.GlbToLocScatter.scatter( self.IntrfcPbGlbSol, \
            self.IntrfcPbLocSol, mode='forward')

        DualLocSol = self.IntrfcPbLocSol.getArray()
        PrimalLocSol = \
            self.InterfaceNumbering.dualToPrimalProjection( \
            DualLocSol, ApplyMultiplicity=False)
        
        return PrimalLocSol

    def getLocalSolution(self):
        """Returns the primal dofs of the interface and their values."""
        DofsValues = self.extractLocalSolution()
        DofsIndices = self.InterfaceNumbering.getLocalInterfaceDofs()
        return DofsIndices, DofsValues
    
    def solveLocalProblem(self, ImposedDof=None):
        """
        Solves the local problem after the convergence
        of the interface problem
        
        """
        UpdatedRhs = self.LocalPetscRhs.copy()
        DofsIndices, DofsValues = self.getLocalSolution()
        UpdatedRhs.getArray()[DofsIndices] += DofsValues
        Result = self.LocalPetscRhs.duplicate()
        self.LocalPetscKsp.solve(UpdatedRhs, Result)
        DofNumbering = self.InterfaceNumbering.getLocalDofNumbering()
        LocalDofsVector = datastr.Vector(DofNumbering, ImposedDof, 
            VectorType="DofsField", ValuesBuff=Result.getArray())
        RigidBodyDisp = self.getRigidBodyDisplacement()
        if RigidBodyDisp.size != 0:
            LocalDofsVector += RigidBodyDisp
        if self.verbose():
            self.checkInterfaceDisplacement(LocalDofsVector)
        return LocalDofsVector
    
    def checkInterfaceDisplacement(self, LocalDofsVector):
        """
        Checks the displacement jump between subdomains 
        and returns its maximum along the interface.
        
        """
        LocDualIntrfcSize = \
            self.InterfaceNumbering.getLocalDualInterfaceSize()
        
        PrimalIntrfcDofs = \
            self.InterfaceNumbering.getLocalInterfaceDofs()

        InterfaceDispls = \
            self.InterfaceNumbering.primalToDualProjection(
            np.abs(LocalDofsVector.GetValues()[PrimalIntrfcDofs]), 
            ApplyMultiplicity=False)
        
        TempVec = self.IntrfcPbGlbSol.duplicate()
        TempVec.zeroEntries()
        TempVec.setLGMap(self.LocGlbMap)

        TempVec.setValuesLocal(np.arange(LocDualIntrfcSize, \
            dtype=np.int32), InterfaceDispls, addv=True)

        TempVec.assemble()
        TempVec.getArray()[:] = np.abs(TempVec.getArray())[:]

        _, MaxJump = TempVec.max()

        if self.getCommRank() == 0:
            print("The maximum displacement "
                "jump is \t\t:", abs(MaxJump))

    def testProjector(self):
        """Tests the projector."""
        if self.kernelCoarsePbExists():
            self.KernelProjectorCtx.testProjector()
        if self.geneoCoarsePbExists():
            self.GeneoProjectorCtx.testProjector()

class LocPriSchurCompCtx:
    """
    The context of the local primal schur complement.

    Parameters
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    Attributes
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    """

    def __init__(self, Solver):
        assert(isinstance(Solver, FetiSolver))
        self.Solver = Solver
        SplittedLocMat = self.__GetSplittedMatrix__()
        self.LocMat_ii = SplittedLocMat[0][0].tocsr()
        self.LocMat_ib = SplittedLocMat[0][1]
        self.LocMat_bi = SplittedLocMat[1][0]
        self.LocMat_bb = SplittedLocMat[1][1]

    def __GetSplittedMatrix__(self):
        """Return the splitted local matrix."""
        InterfaceNumbering = self.Solver.getInterfaceNumbering()
        SdInterfaceDofs = InterfaceNumbering.getLocalInterfaceDofs()
        SdInternalDofs = InterfaceNumbering.getLocalInternalDofs()
        LocalMatrix = self.Solver.getLocalMatrix()
        return SplitMatrix(LocalMatrix, SdInternalDofs, SdInterfaceDofs)

    def mult(self, mat, PetscVecToMult, PetscMultRes):
        MultRes = PetscMultRes.getArray()
        VecToMult = PetscVecToMult.getArray(readonly=True)
       
        TempVec1 = self.LocMat_ib.dot(VecToMult)
        TempVec2 = self.LocMat_bb.dot(VecToMult)
        TempVec3 = spsolve(self.LocMat_ii, TempVec1)
        TempVec4 = self.LocMat_bi.dot(TempVec3)

        MultRes[:] = (TempVec2 - TempVec4)[:]

class LocMatCtx:
    """
    The context of the local shell matrix.

    Parameters
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    Attributes
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    """

    def __init__(self, Solver):
        assert(isinstance(Solver, FetiSolver))
        self.LocKsp = Solver.getLocalKsp()
        self.InterfaceNumbering = Solver.getInterfaceNumbering()
        self.PrimalInterfaceDofs = \
            self.InterfaceNumbering.getLocalInterfaceDofs()

        self.TempPetscVec = PETSc.Vec().createSeq(
            self.InterfaceNumbering.getLocalDofsNbr(), 
            comm=MPI.COMM_SELF)

    def mult(self, mat, PetscVecToMult, PetscMultRes):
        MultRes = PetscMultRes.getArray()
        VecToMult = PetscVecToMult.getArray(readonly=True)

        self.TempPetscVec.zeroEntries()
       
        self.TempPetscVec.getArray()[self.PrimalInterfaceDofs] = \
            self.InterfaceNumbering.dualToPrimalProjection( \
            VecToMult, ApplyMultiplicity=False)
        
        self.LocKsp.solve(self.TempPetscVec, self.TempPetscVec)

        MultRes[:] = self.InterfaceNumbering.primalToDualProjection( \
            self.TempPetscVec.getArray()[self.PrimalInterfaceDofs], 
            ApplyMultiplicity=False)[:]

class LocPcCtx:
    """
    The context of the local shell preconditionner.

    Parameters
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    Attributes
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    """

    def __init__(self, Solver):
        assert(isinstance(Solver, FetiSolver))
        self.InterfaceNumbering = Solver.getInterfaceNumbering()
        self.LocPriSchurComp = Solver.getLocPriSchurComp()

        VecSize = self.InterfaceNumbering.getLocalPrimalInterfaceSize()
        self.TempVec = PETSc.Vec().createSeq(VecSize)

    def mult(self, mat, PetscVecToMult, PetscMultRes):
        MultRes = PetscMultRes.getArray()
        VecToMult = PetscVecToMult.getArray(readonly=True)
       
        self.TempVec.getArray()[:] = \
            self.InterfaceNumbering.dualToPrimalProjection( \
            VecToMult, ApplyMultiplicity=True)
        
        self.TempVec = self.LocPriSchurComp(self.TempVec)

        MultRes[:] = \
            self.InterfaceNumbering.primalToDualProjection( \
            self.TempVec.getArray(), ApplyMultiplicity=True)[:]

class GlbPcCtx:
    """
    The context of the global shell preconditionner.

    Parameters
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    Attributes
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    """

    def __init__(self, Solver):
        assert(isinstance(Solver, FetiSolver))
        
        self.PcGlbMatIs = Solver.getGlobalPcMatrix()
        self.KernelProj = Solver.getProjectorMatrix("kernel")
        
        self.TempVec = self.PcGlbMatIs.getVecLeft() \
            if self.KernelProj is not None else None

        self.apply = self.applyNoKernel if \
            self.KernelProj is None else self.applyKernel

    def applyKernel(self, pc, Residual, Result):
        """Apply the interface problem preconditionner."""
        self.PcGlbMatIs.mult(Residual, self.TempVec)
        self.KernelProj.mult(self.TempVec, Result)

    def applyNoKernel(self, pc, Residual, Result):
        """Apply the interface problem preconditionner."""
        self.PcGlbMatIs.mult(Residual, Result)

class GlbMatCtx:
    """
    The context of the global Feti matrix.

    Parameters
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    Attributes
    ----------
    xxxx : 'xxxx'
        xxxx xxxx xxxx xxxx xxxx xxxx xxxx.

    """

    def __init__(self, Solver):
        assert(isinstance(Solver, FetiSolver))

        self.FetiGlbMatIs = Solver.getDualSchurMatrix()
        self.KernelProj = Solver.getProjectorMatrix("kernel")
        self.GeneoProj = Solver.getProjectorMatrix("geneo")

        KernelExists = self.KernelProj is not None
        GeneoExists = self.GeneoProj is not None

        if KernelExists or GeneoExists:
            self.TempVec = self.FetiGlbMatIs.getVecLeft()
        else: self.TempVec = None

        if KernelExists and GeneoExists:
            self.TempVec_1 = self.TempVec.duplicate()
            self.mult = self.multKernelGeneo
        elif KernelExists:
            self.mult = self.multKernel
        elif GeneoExists:
            self.mult = self.multGeneo
        else: self.mult = self.multNoCp

    def multKernel(self, mat, VecToMult, MultRes):
        """Apply the interface problem preconditionner."""
        self.FetiGlbMatIs.mult(VecToMult, self.TempVec)
        self.KernelProj.multTranspose(self.TempVec, MultRes)

    def multGeneo(self, mat, VecToMult, MultRes):
        """Apply the interface problem preconditionner."""
        self.FetiGlbMatIs.mult(VecToMult, self.TempVec)
        self.GeneoProj.multTranspose(self.TempVec, MultRes)

    def multKernelGeneo(self, mat, VecToMult, MultRes):
        """Apply the interface problem preconditionner."""
        self.FetiGlbMatIs.mult(VecToMult, self.TempVec)
        self.GeneoProj.multTranspose(self.TempVec, self.TempVec_1)
        self.KernelProj.multTranspose(self.TempVec_1, MultRes)

    def multNoCp(self, mat, VecToMult, MultRes):
        """Apply the interface problem preconditionner."""
        self.FetiGlbMatIs.mult(VecToMult, MultRes)
