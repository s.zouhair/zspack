"""
This module implements functions that evaluate the modified constitutive relation error 
(mcre) of all formulations (i.e. disp, drkr damp, drkr no damp).

"""

from functools import partial
import numpy as np

from ...pkgutils import GetOmega
from . import dynmcre

def GetCreFunc(Mcre):
    """
    Returns the function that evaluate 
    the constitutive relation error.
    
    """
    assert(isinstance(Mcre, dynmcre.DynMcre))
    ErrType = Mcre.GetErrType()
    if ErrType == "Disp":
        Func = EvalDispCre
    elif not Mcre.DampingExist():
        Func = EvalDrkrNoDampCre
    else: Func = EvalDrkrDampCre
    return partial(Func, Mcre=Mcre)

def EvalDispCre(FreqIdx, Mcre=None):
    """
    Evaluate the constitutive relation 
    error for dissipation based error
    
    """
    raise NotImplementedError

def EvalDrkrDampCre(FreqIdx, Mcre=None):
    """
    Evaluate the constitutive relation 
    error for dissipation based error
    
    """
    raise NotImplementedError

def EvalDrkrNoDampCre(FreqIdx, Mcre=None):
    """
    Evaluate the constitutive relation 
    error for dissipation based error
    
    """
    UV = Mcre.GetField("U - V", FreqIdx)
    K = Mcre.GetFeMatrix("Stiffness")
    M = Mcre.GetFeMatrix("Mass")

    w = GetOmega(Mcre.GetFreq(FreqIdx))
    g = Mcre.GetGamma()

    ym = .5 * pow(w * g, 2) / (1 - g)
    yk = g / 2

    N = yk * K + ym * M

    return np.dot(UV, N.dot(UV))

__all__ = ["GetCreFunc"]