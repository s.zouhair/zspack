"""
This module implements the localization step of the modified constitutive relation 
error (mcre) class in linear dynamics, and all the methods that allow interacting with it.

"""

import numpy as np

from scipy.sparse import bmat
from scipy.linalg import solve
from scipy.sparse.linalg import spsolve

from ... import pkgutils as utils
from ... import pkgdatastr as datastr
from ... import pkgresult as result
from ...pkgparddm import FetiInterfaceNumbering, CreateFetiSolver

class LocDynMcre:
    """
    The mcre localisation step class for linear dynamics problems.

    Parameters
    ----------
        StiffMat : 'datastr.Matrix'
            The stiffness matrix object.

        MassMat : 'datastr.Matrix'
            The mass matrix object.

        DampMat : 'datastr.Matrix'
            The damping matrix object.

        GeneEfrt : 'datastr.Vector'
            The generalized efforts vector.

        FreqsList : 'list or np.ndarray'
            An array or a list that contains the frequencies of the study.

        MesDofs : 'np.ndarray'
            A 1D array that contains the global numbering of measured
            degrees of freedom.

        MesVals : 'np.ndarray'
            A 2D array with shape (len(FreqsList), MesDofs.size), it contains
            the values of measured DOFs. Each row correspond to a frequency
            and DOFs measured values are stored according to DOFs order
            within MesDofs.

        InterfaceNumbering : 'xxx'
            An object used to describe the interface 
            numbering for parallel DDM methods

        ErrType : 'str', optional
            The type of the mcre version to use. Two options are possible :

                - "Drkr" : based on error in Drucker's sense (default).
                - "Disp" : based on dissipation error.

        r : 'float'
            A weighting parameter that expresses the accuracy of the 
            measurements, it must be in [0,1[. Its default value is '0.5'.

        Gamma : 'float'
            A weighting parameter available only for Drucker's error (i.e. 
            ErrType="Drkr"), it must be in [0,1]. Its default value is '0.5'.

        StorageType : 'str'
            The storage type to be used for the MCRE matrix, two options
            are possible :
            
                - "Dense"  : the whole matrix is stored.
                - "Sparse" : only non-zero entries of the matrix are stored
                             as a scipy sparse matrix (bsr_matrix)

    Attributes
    ----------
        StiffMat : 'datastr.Matrix'
            The stiffness matrix object.

        MassMat : 'datastr.Matrix'
            The mass matrix object.

        DampMat : 'datastr.Matrix'
            The damping matrix object.

        GeneEfrt : 'datastr.Vector'
            The generalized efforts vector.

        FreqsList : 'list or np.ndarray'
            An array or a list that contains the frequencies of the study.

        FreqsNbr : 'int'
            The number of frequencies.

        MesDofs : 'np.ndarray'
            A 1D array that contains the global numbering of measured
            degrees of freedom.

        MesVals : 'np.ndarray'
            A 2D array with shape (FreqsNbr, MesDofs.size), it contains
            the values of measured DOFs. Each row correspond to a frequency
            and DOFs measured values are stored according to DOFs order
            within MesDofs.

        StorageType : 'str'
            The storage type, either "Dense" or "Sparse".

        ErrType : 'str'
            The type of the mcre error, either "Drkr" or "Disp".

        r : 'float'
            A weighting parameter that expresses the accuracy of the 
            measurements, it must be in [0,1[. Its default value is '0.5'.

        Gamma : 'float'
            A weighting parameter available only for Drucker's error (i.e. 
            ErrType="Drkr"), it must be in [0,1]. Its default value is '0.5'.

        Gr : 'xxx'
            The norm matrix computed as a projection on measured dofs of a 
            linear combination of stiffness, mass and damping matrices.

        ExpandedGr : 'xxx'
            An extended version of the norm matrix to all Dofs. Rows and 
            columns corresponding to unmeasured Dofs are set to zero.

            Nb : ExpandedGr is the diagonal block of the mcre matrix that 
            corresponds to the measurements.
        
        InterfaceNumbering : 'xxx'
            An object used to describe the interface 
            numbering for parallel DDM methods
        
        CalculatedFreq : 'list'
            A list that stores for each frequency a bool that 
            determines if it's already calculated or not.
        
        ParDDMInitGuess : 'list'
            A list that stores for each frequency the previous
            solution of the interface problem, this solution
            is used as initialization for next localisatuion
            steps.

        ddm_intrfc_size : 'xxx'
            xxx xxx xxx xxx.

        ddm_intrfc_dofs : 'xxx'
            xxx xxx xxx xxx.

        ddm_intrfc_vals : 'xxx'
            xxx xxx xxx xxx.
        
        ddm_context_type : 'xxx'
            xxx xxx xxx xxx.

        ddm_new_mes_dofs : 'xxx'
            xxx xxx xxx xxx.

        Field_U : 'list'
            For a given frequency, the solution vector of the mcre linear 
            system is composed of 2 or 3 blocks depending on the type of 
            error (Drucker or Dissipation). Each block is extracted and 
            stored as an FE vector (datastr.Vector). Field_U is a list 
            containing the U-block of all frequencies. The order of 
            FreqsList is respected

        Field_UV : 'list'
            A list that contains the UV-block of all frequencies. The 
            order of the FreqsList is respected.

        Field_UW : 'list'
            A list that contains the UW-block of all frequencies. The 
            order of the FreqsList is respected.

            Nb : Only available for Drucker error.

        McreResults : 'np.ndarray'
            A 2D or 3D array (depending on the type of error) where error values 
            are stored for all frequencies and elements.
                
                ErrType == "Disp" : A 2D array where each row corresponds to a 
                frequency and each column to an element.
                    
                    Example : 

                        McreResults[f_i, e_j] : the error value of the element 
                        e_j for the frequency f_i.
                
                ErrType == "Drkr" : The Drucker-based error is the sum of two 
                terms, thus for a given frequency there are three erros that 
                must be stored for each element (the first term, the second 
                term and the total error). McreResults is then a 3D array where 
                each row corresponds to a frequency and each column to an error 
                (there are three columns), the third dimension contains the 
                error values for each element.

                    Example : 
                    
                        McreResults[f_i, 0, e_j] : the value of the first error 
                        term (the UV term) of the element e_j for the frequency f_i.

                        McreResults[f_i, 1, e_j] : the value of the second error 
                        term (the UW term) of the element e_j for the frequency f_i.

                        McreResults[f_i, 2, e_j] : the value of the total error 
                        (UV + UW) of the element e_j for the frequency f_i.

    """

    def __init__(self, StiffMat, MassMat, DampMat, GeneEfrt, 
        FreqsList, MesDofs, MesVals, InterfaceNumbering=None, 
        ErrType="Drkr", r=0.5, Gamma=0.5, StorageType=None):
        
        assert(isinstance(StiffMat, datastr.Matrix))
        assert(isinstance(MassMat, datastr.Matrix))
        assert(DampMat is None or isinstance(DampMat, datastr.Matrix))
        assert(isinstance(GeneEfrt, datastr.Vector))

        if StiffMat.GetType() != "Stiffness":
            raise ValueError("The first matrix isn't a stiffness matrix !")
        elif MassMat.GetType() != "Mass":
            raise ValueError("The second matrix isn't a mass matrix !")
        elif DampMat is not None and DampMat.GetType() != "Damping":
            raise ValueError("The third matrix isn't a damping matrix !")
        elif GeneEfrt.GetType() != "GeneEfforts":
            raise ValueError("The vector isn't "
                "a generalized efforts vector !")
        
        if InterfaceNumbering is not None:
            assert(isinstance(InterfaceNumbering, 
                FetiInterfaceNumbering))
        
        self.InterfaceNumbering = InterfaceNumbering
        self.InterfacePbSolver = None
        
        self.OperatorsUpdated = False
        
        if DampMat is None: assert(ErrType == "Drkr")
        if ErrType == "Disp": assert(DampMat is not None)

        self.StiffMat = StiffMat
        self.MassMat  = MassMat
        self.DampMat  = DampMat
        self.GeneEfrt = GeneEfrt
        self.__CheckDofNumbering__()

        assert(StorageType in ["Dense", "Sparse"])
        self.StorageType = StorageType

        if isinstance(FreqsList, np.ndarray):
            self.FreqsList = FreqsList
            self.FreqsNbr  = FreqsList.size
        elif isinstance(FreqsList, list):
            self.FreqsList = np.array(FreqsList, dtype=float)            
            self.FreqsNbr  = len(FreqsList)
        else:
            raise ValueError("Frequency list should"
                " be either a list or an np.ndarray")

        if MesDofs is not None:
            assert(MesVals is not None)
            assert(isinstance(MesDofs, np.ndarray) and MesDofs.ndim == 1)
            assert(isinstance(MesVals, np.ndarray) and \
                MesVals.shape == (self.FreqsNbr, MesDofs.size))
        else:
            assert(MesVals is None)
        
        self.MesDofs  = MesDofs
        self.MesVals = MesVals

        assert(ErrType in ["Drkr", "Disp"])
        self.ErrType = ErrType
        self.BlocksNbr = 2 if ErrType == "Disp" or DampMat is None else 3

        assert(isinstance(r, float) and 0 <= r < 1)
        self.r = r

        assert(isinstance(Gamma, float) and 0 <= Gamma < 1)
        self.Gamma = Gamma
        
        self.ddm_intrfc_size = None
        self.ddm_intrfc_dofs = None
        self.ddm_intrfc_vals = None
        self.ddm_context_type = None
        self.ddm_new_mes_dofs = None

        self.Gr = self.GetGr(1.0, 1.0, 1.0)
        self.ExpandedGr = self.GetExpandedGr()

        self.Field_U  = [None for _ in range(self.FreqsNbr)]
        self.Field_UV = [None for _ in range(self.FreqsNbr)]
        if self.ErrType == "Drkr" and DampMat is not None:
            self.Field_UW = [None for _ in range(self.FreqsNbr)]

        ElmntsNbr = self.GetElmntsNbr()
        if self.ErrType == "Disp" or DampMat is None:
            self.McreResults = np.zeros((self.FreqsNbr, ElmntsNbr))
        else:
            self.McreResults = np.zeros((self.FreqsNbr, 3, ElmntsNbr))
        
        self.CalculatedFreq = [False for _ in FreqsList]
        self.ParDDMInitGuess = [None for _ in FreqsList]

    def __CheckDofNumbering__(self):
        """
        Checks whether the FE matrices and the generalized efforts 
        vector have the same DofNumbering object.

        """
        a = self.StiffMat.GetDofNumbering()
        b = self.MassMat.GetDofNumbering()
        c = b if not self.DampingExist() \
            else self.DampMat.GetDofNumbering()
        d = self.GeneEfrt.GetDofNumbering()
        if not (a is b is c is d):
            raise ValueError("FE matrices and the"
                " generalized efforts vector are not"
                " defined on the same Dofs numbering")

    def DampingExist(self):
        """Returns a bool that determines if damping exist or not."""
        return self.DampMat is not None

    def GetFreq(self, FreqIdx):
        """Returns the frequency number FreqIdx (counting starts at 0)."""
        assert(0 <= FreqIdx < self.FreqsNbr)
        return self.FreqsList[FreqIdx]

    def GetOmega(self, FreqIdx):
        """Returns the pulsation related to the frequency number FreqIdx."""
        return utils.GetOmega(self.GetFreq(FreqIdx))

    def GetFreqMesVals(self, FreqIdx):
        """Returns the measured values for the frequency number FreqIdx."""
        assert(0 <= FreqIdx < self.FreqsNbr and self.SdContainsMeasures())
        return self.MesVals[FreqIdx]

    def GetDofNumbering(self):
        """Returns the underlying DofNumbering object."""
        return self.GeneEfrt.GetDofNumbering()

    def GetImposedDof(self):
        """Returns the underlying Boundary conditions object."""
        return self.GeneEfrt.GetImposedDof()
    
    def GetElmntsNbr(self):
        """Returns the number of elements of the underlying mesh."""
        return self.GetDofNumbering().GetElmntsNbr()
    
    def GetMcreMatBlocksNbr(self):
        """Returns the number of blocks per row/column of the mCRE matrix."""
        return self.BlocksNbr
    
    def useParDDM(self):
        """Checks whether to use a parallel DDM method"""
        return self.InterfaceNumbering is not None

    def UpdateFeOperators(self, StiffMat, MassMat, DampMat, GeneEfrt):
        """Updates Fe operators"""
        
        if self.DampingExist():
            self.DampMat  = DampMat
        else: assert(DampMat is None)
        
        self.StiffMat = StiffMat
        self.MassMat  = MassMat
        self.GeneEfrt = GeneEfrt
        self.__CheckDofNumbering__()
        
        self.Gr = self.GetGr(1.0, 1.0, 1.0)
        self.ExpandedGr = self.GetExpandedGr()

        self.OperatorsUpdated = True
        self.CalculatedFreq[:] = self.FreqsNbr * [False]

    def IsDDMContext(self):
        """Checks if the problem is defined within a DDM context."""
        a = self.ddm_intrfc_size is not None
        b = self.ddm_intrfc_dofs is not None
        c = self.ddm_intrfc_vals is not None
        d = self.ddm_context_type is not None
        e = self.ddm_new_mes_dofs is not None

        assert(a == b == c == d)
        if a and self.ddm_context_type == "Primal":
            assert(e or not self.SdContainsMeasures())
        else: assert(not e)

        return a
    
    def SdContainsMeasures(self):
        """
        Checks within a DDM context if the the subdomain related 
        to the mCRE object contains experimental measures.
        
        """
        return self.MesDofs is not None

    def AlreadyCalculated(self, FreqIdx):
        """
        Checks whether the mCRE localisation step is already performed
        for the frequency FreqIdx.
        
        """
        return self.CalculatedFreq[FreqIdx]

    def GetGr(self, StiffCoeff, MassCoeff, DampCoeff):
        """
        Returns the norm matrix Gr related to experimental measurements.
        The returned matrix is dense since it's very small compared to 
        other FE matrices.
        
        """
        assert(np.isscalar(StiffCoeff))
        assert(np.isscalar(MassCoeff))
        result = None
        if self.SdContainsMeasures():
            Kr = self.StiffMat.GetDofsSubMat(
                self.MesDofs, StorageType="Dense")
            Mr = self.MassMat.GetDofsSubMat(
                self.MesDofs, StorageType="Dense")
            result = StiffCoeff * Kr + MassCoeff * Mr
            if self.DampingExist():
                Cr = self.DampMat.GetDofsSubMat(
                    self.MesDofs, StorageType="Dense")
                result += float(DampCoeff) * Cr
        return result

    def GetExpandedGr(self):
        """
        Returns an expanded version of the norm matrix Gr. It appears within 
        the blocks of the MCRE matrix for the two types of error (i.e. Drucker 
        and dissipation).
        
        """
        result = None

        if self.SdContainsMeasures():
            if self.IsDDMContext() and self.GetDDMType() == "Primal":
                dofs_array = self.ddm_new_mes_dofs
                mat_size = self.StiffMat.Size - self.ddm_intrfc_size
            else:
                dofs_array = self.MesDofs
                mat_size = self.StiffMat.Size

            result =  utils.ExpandMatrix(self.Gr, dofs_array, 
                mat_size, StorageType=self.StorageType)
        
        return result
    
    def CalcDynMcre(self, FreqIdx=None, CalcErr=True, ExtraBC=None, ExtraLoad=None):
        """
        The main function of this class. It is responsible for 
        executing the localization step and saving the results after 
        computing the error for all elements. 

        Parameters
        ----------
        
            FreqIdx : 'None, int', optional
                It Allows to select the frequencies for which the 
                calculation will be performed. If FreqIdx is None 
                all frequencies are selected, otherwise a single 
                frequency is selected and its number should be 
                passed in FreqIdx.
            
            CalcErr : 'bool', optional
                If True, the error is computed for all elements
            
            ExtraBC : 'tuple, list', optional
                A tuple / list that contains additional degrees of 
                freedom to be imposed and their corresponding values

                                (dofs_indices, dofs_vals)
            
                    - dofs_indices : an array containing additional 
                      degrees of freedom to be imposed.

                    - dofs_indices : an array containing the values 
                      of additional degrees of freedom to be imposed.

            ExtraLoad : 'tuple or list', optional
                A tuple / list containing additional nodal forces to 
                be applied and their corresponding values.

                                (dofs_indices, dofs_vals)
                
                    - dofs_indices : an array containing degrees of 
                      freedom where the additional nodal forces will 
                      be applied.

                    - dofs_indices : an array containing the values 
                      of additional nodal forces to be applied.
        
        Nb : After calling this function, results can be visualized 
        using paraview by calling the method AddResult(...) on the class.
        
        """
        NoExtraBC = ExtraBC is None
        NoExtraLoad = ExtraLoad is None

        if self.useParDDM() and NoExtraBC and NoExtraLoad:
            DofsIndices, DofsValues = self.SolveInterfacePb(FreqIdx=FreqIdx)
            if self.InterfaceNumbering.getInterfaceType() == "Dual":
                ExtraBC, ExtraLoad = None, (DofsIndices, DofsValues)
            else: ExtraBC, ExtraLoad = (DofsIndices, DofsValues), None
            return self.CalcDynMcre(FreqIdx=FreqIdx, CalcErr=CalcErr, 
                ExtraBC=ExtraBC, ExtraLoad=ExtraLoad)

        if self.IsDDMContext():
            assert(FreqIdx is not None)
            assert(NoExtraBC != NoExtraLoad)
        
        if FreqIdx is None:
            assert(NoExtraBC and NoExtraLoad)
            freqs_iterator = range(self.FreqsNbr)
        else:
            assert(isinstance(FreqIdx, int))
            assert(0 <= FreqIdx < self.FreqsNbr)
            freqs_iterator = [FreqIdx]
            if not NoExtraBC:
                self.AddExtra(FreqIdx, ExtraBC, "Primal")
            elif not NoExtraLoad:
                self.AddExtra(FreqIdx, ExtraLoad, "Dual")

        for freq_idx in freqs_iterator:
            SolVect = self.GetMcreLsSol(freq_idx)
            self.SaveMcreLsSol(SolVect, freq_idx)
            self.CalculatedFreq[freq_idx] = True

        if CalcErr: self.SaveMcreResults(FreqIdx=FreqIdx)

    def SolveInterfacePb(self, FreqIdx=None):
        """
        Solves the interface problem when 
        parallel DDM methods are used
        
        """
        Numbering = self.InterfaceNumbering
        InterfacePbType = Numbering.getInterfaceType()

        self.ResetIntrfcPbSol(FreqIdx)

        CreateSolver = self.InterfacePbSolver is None
        OpsUpdated = self.OperatorsUpdated

        if CreateSolver or OpsUpdated:
            LocMcreMat = self.GetMcreMat(FreqIdx)
            LocMcreVec = self.GetMcreVec(FreqIdx)
        else: LocMcreMat, LocMcreVec = None, None
        
        if CreateSolver:
            if InterfacePbType == "Dual":
                self.InterfacePbSolver = CreateFetiSolver(
                    Numbering, LocMcreMat, LocMcreVec, 
                    PrecondType='jacobi', Info=False)
            else: raise NotImplementedError
        else: 
            self.InterfacePbSolver.updateOperators(
                LocMcreMat, LocMcreVec)

        self.OperatorsUpdated = False

        x0 = self.GetDDMInitGuess(FreqIdx)
        self.InterfacePbSolver.execute(mtype="GMRES", x0=x0)
        Result = self.InterfacePbSolver.getLocalSolution()
        self.SetDDMInitGuess(self.InterfacePbSolver, FreqIdx)
        
        return Result

    def SetDDMInitGuess(self, Solver, FreqIdx):
        """Sets a initial Guess for next DDM solver executions"""
        self.ParDDMInitGuess[FreqIdx] = \
            Solver.getInterfacePbSolution().copy()

    def GetDDMInitGuess(self, FreqIdx):
        """Returns a initial Guess"""
        return self.ParDDMInitGuess[FreqIdx]
    
    def ResetIntrfcPbSol(self, FreqIdx):
        """Resets the interface problem solution of FreqIdx"""
        if self.ddm_intrfc_vals is None: return
        else: self.ddm_intrfc_vals[FreqIdx] = None

    def AddExtra(self, FreqIdx, Extra, ExtraType):
        assert(isinstance(Extra, (tuple, list)) and len(Extra) == 2)
        indices, vals = Extra
        assert(isinstance(indices, np.ndarray) and indices.ndim == 1)
        assert(isinstance(vals, np.ndarray) and vals.ndim == 1)
        assert(indices.size == vals.size)
        free_dofs_nbr = self.GetDofNumbering().GetFreeDofNbr()
        assert(np.all(indices < free_dofs_nbr * self.GetMcreMatBlocksNbr()))

        if self.IsDDMContext():
            assert(indices.size == self.ddm_intrfc_size * self.GetMcreMatBlocksNbr())
            assert(np.all(self.ddm_intrfc_dofs == indices[:self.ddm_intrfc_size]))
            assert(self.ddm_context_type == ExtraType)
        else: self.InitDDMContext(Extra, ExtraType)
        
        self.ddm_intrfc_vals[FreqIdx] = vals
    
    def InitDDMContext(self, Extra, ExtraType):
        """Initialize the DDM context."""
        indices, vals = Extra
        self.ddm_context_type = ExtraType
        self.ddm_intrfc_size  = indices.size // self.GetMcreMatBlocksNbr()
        self.ddm_intrfc_vals = [None for _ in range(self.FreqsNbr)]
        self.ddm_intrfc_dofs = indices[:self.ddm_intrfc_size]

        free_dofs_nbr = self.GetDofNumbering().GetFreeDofNbr()
        temp_vec_1 = np.arange(self.GetMcreMatBlocksNbr()) * free_dofs_nbr
        temp_vec_2 = (temp_vec_1[:, None] + self.ddm_intrfc_dofs).flatten()
        assert(np.all(indices == temp_vec_2))

        if ExtraType == "Primal" and self.SdContainsMeasures():
            mes_dofs_nbr = self.MesDofs.size
            self.ddm_new_mes_dofs = np.zeros(mes_dofs_nbr, dtype=int)
            for mes_idx, mes_dof in enumerate(self.MesDofs):
                mes_shift = np.count_nonzero(self.ddm_intrfc_dofs < mes_dof)
                self.ddm_new_mes_dofs[mes_idx] = mes_dof - mes_shift
            self.ExpandedGr = self.GetExpandedGr()
    
    def GetDDMType(self):
        """Returns the DDM type which is either "Primal" or "Dual"."""
        assert(self.IsDDMContext())
        return self.ddm_context_type
    
    def GetDDMDofs(self, DofsType=None):
        """
        Returns degrees of freedom in a domain decomposition 
        context. The parameter DofsType is used to select 
        whether internals or interface degrees of freedom 
        should be returned.
        
        """
        assert(self.IsDDMContext())
        assert(DofsType in ["internal", "interface"])
        temp_dofs = self.ddm_intrfc_dofs

        if DofsType == "internal":
            total_dofs_nbr = self.GetDofNumbering().GetFreeDofNbr()
            temp_vec = np.full(total_dofs_nbr, True)
            temp_vec[temp_dofs] = False
            temp_dofs = np.nonzero(temp_vec)[0]
        
        return temp_dofs
    
    def GetDDMVals(self, FreqIdx, FieldName=None):
        """xxx."""
        assert(self.IsDDMContext())
        assert(FieldName in ['U', 'U - V', 'U - W'])
        if self.ddm_intrfc_vals[FreqIdx] is None: return 0.
        size = self.ddm_intrfc_size
        if FieldName == 'U':
            start = (2 if self.ErrType == "Drkr" \
                and self.DampingExist() else 1) * size
        elif FieldName == 'U - W':
            assert(self.ErrType == "Drkr")
            start = size
        else: start = 0
        
        return self.ddm_intrfc_vals[FreqIdx][start:start+size]

    """
    def GetDDMVals(self, FreqIdx, FieldName=None):
        "xxx."
        assert(self.IsDDMContext())
        if self.GetDDMType() == "Primal":
            assert(FieldName in ['U', 'U - V', 'U - W'])
            size = self.ddm_intrfc_size
            if FieldName == 'U':
                start = (2 if self.ErrType == "Drkr" else 1) * size
            elif FieldName == 'U - V':
                start = 0
            else:
                assert(self.ErrType == "Drkr")
                start = size
            result = self.ddm_intrfc_vals[FreqIdx][start:start+size]
        else:
            assert(FieldName is None)
            result = self.ddm_intrfc_vals[FreqIdx]
        
        return result
    """

    def SaveMcreResults(self, FreqIdx=None):
        """
        Calculates and saves Cre for all elements. The error values 
        are stored within the attribute McreResults. If FreqIdx is 
        None all frequencies are selected, otherwise a single frequency 
        is selected and its number should be passed in FreqIdx.
        
        """
        if self.ErrType == "Drkr":
            self.__SMR_Drucker__(FreqIdx)
        else: self.__SMR_Dissipation__(FreqIdx)
    
    def __SMR_Drucker__(self, FreqIdx=None):
        """
        Calculates and saves the Drucker based Cre for all elements.
        
        """
        if self.DampingExist():
            self.__SMR_Drucker_Damp__(FreqIdx)
        else: self.__SMR_Drucker_NoDamp__(FreqIdx)

    def __SMR_Drucker_Damp__(self, FreqIdx=None):
        """
        Calculates and saves the Drucker based Cre for all elements. 
        For a given frequency, each element has three error values 
        (see __CalcElmntryErr_Drucker_Damp__(...)) stored in three separates 
        arrays (see self.McreResults). If FreqIdx is None all frequencies 
        are selected, otherwise a single frequency is selected and its 
        number should be passed in FreqIdx.
        
        """
        assert(self.DampingExist())
        if FreqIdx is None:
            freqs_iterator = range(self.FreqsNbr)
        else:
            assert(0 <= FreqIdx < self.FreqsNbr)
            freqs_iterator = [FreqIdx]
        ElmntsNbr = self.GetElmntsNbr()
        for ElmntIdx in range(ElmntsNbr):
            ElmntryStiff = self.StiffMat.GetElmntryMatVal(\
                        ElmntIdx, ImpDofs=False, As2DArray=True)
            ElmntryMass  = self.MassMat.GetElmntryMatVal(\
                        ElmntIdx, ImpDofs=False, As2DArray=True)
            ElmntryDamp  = self.DampMat.GetElmntryMatVal(\
                        ElmntIdx, ImpDofs=False, As2DArray=True)
            for freq_idx in freqs_iterator:
                ErrUV, ErrUW, ErrTT = self.__CalcElmntryErr_Drucker_Damp__(\
                    freq_idx, ElmntIdx, ElmntryStiff, ElmntryMass, ElmntryDamp)
                self.McreResults[freq_idx, 0, ElmntIdx] = ErrUV
                self.McreResults[freq_idx, 1, ElmntIdx] = ErrUW
                self.McreResults[freq_idx, 2, ElmntIdx] = ErrTT

    def __SMR_Drucker_NoDamp__(self, FreqIdx=None):
        """
        This function should not be used. It is only used within the 
        function __SMR_Drucker__(...) under strict assumptions.

        """
        assert(not self.DampingExist())
        if FreqIdx is None:
            freqs_iterator = range(self.FreqsNbr)
        else:
            assert(0 <= FreqIdx < self.FreqsNbr)
            freqs_iterator = [FreqIdx]
        ElmntsNbr = self.GetElmntsNbr()
        for ElmntIdx in range(ElmntsNbr):
            ElmntryStiff = self.StiffMat.GetElmntryMatVal(\
                        ElmntIdx, ImpDofs=False, As2DArray=True)
            ElmntryMass  = self.MassMat.GetElmntryMatVal(\
                        ElmntIdx, ImpDofs=False, As2DArray=True)
            for freq_idx in freqs_iterator:
                Err = self.__CalcElmntryErr_Drucker_NoDamp__(\
                    freq_idx, ElmntIdx, ElmntryStiff, ElmntryMass)
                self.McreResults[freq_idx, ElmntIdx] = Err

    def __SMR_Dissipation__(self, FreqIdx=None):
        """
        Calculates and saves the dissipation based Cre for all elements. 
        For a given frequency, each element has one error value (see 
        __CalcElmntryErr_Dissipation__(...)) stored in a dedicated array 
        (see self.McreResults). If FreqIdx is None all frequencies are 
        selected, otherwise a single frequency is selected and its number 
        should be passed in FreqIdx.
        
        """
        if FreqIdx is None:
            freqs_iterator = range(self.FreqsNbr)
        else:
            assert(0 <= FreqIdx < self.FreqsNbr)
            freqs_iterator = [FreqIdx]
        ElmntsNbr = self.GetElmntsNbr()
        for ElmntIdx in range(ElmntsNbr):
            ElmntryDamp  = self.DampMat.GetElmntryMatVal(ElmntIdx, \
                                        ImpDofs=False, As2DArray=True)
            for freq_idx in freqs_iterator:
                Err = self.__CalcElmntryErr_Dissipation__(freq_idx, \
                                                ElmntIdx, ElmntryDamp)
                self.McreResults[freq_idx, ElmntIdx] = Err

    def GetMcreLsSol(self, FreqIdx):
        """
        Solves the Mcre linear system (localization step). Direct sparse 
        or dense solvers are used depending on the Mcre matrix's storage type.
        
        """
        SysMat = self.AssembleMcreMat(FreqIdx)
        SysRhs = self.AssembleMcreRhs(FreqIdx)
        if self.StorageType == "Dense":
            return solve(SysMat, SysRhs, overwrite_a=True, \
                overwrite_b=True, check_finite=True)
        else:
            return spsolve(SysMat.tocsr(), SysRhs, use_umfpack=True)
    
    def GetMcreMat(self, FreqIdx=None):
        """
        Assembles and returns the mCRE matrix for the frequency FreqIdx. If 
        there is only one frequency value, FreqIdx can be None, otherwise 
        it must be specified.
        
        """
        if FreqIdx is not None:
            assert(0 <= FreqIdx < self.FreqsNbr)
            freq_idx = FreqIdx
        else: freq_idx = 0
        return self.AssembleMcreMat(freq_idx)

    def GetMcreVec(self, FreqIdx=None):
        """
        Assembles and returns the mCRE righthand side for the frequency 
        FreqIdx. If there is only one frequency value, FreqIdx can be 
        None, otherwise it must be specified.
        
        """
        if FreqIdx is not None:
            assert(0 <= FreqIdx < self.FreqsNbr)
            freq_idx = FreqIdx
        else: freq_idx = 0
        return self.AssembleMcreRhs(freq_idx)

    def AssembleMcreMat(self, FreqIdx):
        """
        This function returns the assembled MCRE matrix for the frequency
        number FreqIdx. The returned matrix is either dense (2D np.ndarray)
        or sparse (scipy.sparse.bsr_matrix) depending on the value of the 
        attribute self.StorageType.

        """
        if self.ErrType == "Drkr":
            MatBlocks = self.__Assemble_Mat_Drucker__(FreqIdx)
        else:
            MatBlocks = self.__Assemble_Mat_Dissipation__(FreqIdx)
        
        if self.StorageType == "Dense":
            McreMat = np.block(MatBlocks)
        else:
            McreMat = bmat(MatBlocks, format="bsr")
        
        return McreMat

    def __Assemble_Mat_Drucker__(self, FreqIdx):
        """
        Assembles the MCRE matrix for frequency number FreqIdx, in case 
        of the error in Drucker's sense. It should not be used. It is 
        only used within the function AssembleMcreMat(...) under strict 
        assumptions.
        
        """
        if self.DampingExist():
            return self.__Assemble_Mat_Drucker_Damp__(FreqIdx)
        else:
            return self.__Assemble_Mat_Drucker_NoDamp__(FreqIdx)

    def __Assemble_Mat_Drucker_Damp__(self, FreqIdx):
        """
        This function should not be used. It is only used within the 
        function __Assemble_Mat_Drucker__(...) under strict assumptions.

        """

        Omg = self.GetOmega(FreqIdx)
        y1  = self.Gamma / 2
        y2  = (1 - self.Gamma) / 2
        T   = 1. / self.GetFreq(FreqIdx)

        OmgSqrd = pow(Omg, 2)
        Rcoef   = self.r / (1 - self.r)

        K  = self.StiffMat.GetValues(StorageType=self.StorageType)
        M  = self.MassMat.GetValues(StorageType=self.StorageType)
        C  = self.DampMat.GetValues(StorageType=self.StorageType)

        FeMatSize = self.StiffMat.Size

        if self.IsDDMContext() and self.GetDDMType() == "Primal":
            internal_dofs = self.GetDDMDofs(DofsType="internal")
            K = utils.GetSubMatrix(K, RowsIdx=internal_dofs)
            M = utils.GetSubMatrix(M, RowsIdx=internal_dofs)
            C = utils.GetSubMatrix(C, RowsIdx=internal_dofs)
            FeMatSize = self.StiffMat.Size - self.ddm_intrfc_size

        N11 = y1 * (K + T * OmgSqrd * C)
        N22 = - y2 * OmgSqrd * M
        N12 = y2 * (K + 1j * Omg * C)
        N13 = None if self.StorageType == "Sparse" \
                    else np.zeros((FeMatSize, FeMatSize))
        N33 = Rcoef * self.ExpandedGr if self.SdContainsMeasures() else N13
        N23 = - y2 * (K + 1j * Omg * C - OmgSqrd * M)

        McreDrck = [[N11, N12, N13],
                    [N12, N22, N23],
                    [N13, N23, N33]]
        
        return McreDrck

    def __Assemble_Mat_Drucker_NoDamp__(self, FreqIdx):
        """
        This function should not be used. It is only used within the 
        function __Assemble_Mat_Drucker__(...) under strict assumptions.

        """

        Omg = self.GetOmega(FreqIdx)
        y1  = self.Gamma / 2.
        y2  = self.Gamma / (1. - self.Gamma)

        OmgSqrd = pow(Omg, 2)
        Rcoef   = self.r / (1. - self.r)

        K  = self.StiffMat.GetValues(StorageType=self.StorageType)
        M  = self.MassMat.GetValues(StorageType=self.StorageType)

        FeMatSize = self.StiffMat.Size

        if self.IsDDMContext() and self.GetDDMType() == "Primal":
            internal_dofs = self.GetDDMDofs(DofsType="internal")
            K = utils.GetSubMatrix(K, RowsIdx=internal_dofs)
            M = utils.GetSubMatrix(M, RowsIdx=internal_dofs)
            FeMatSize = self.StiffMat.Size - self.ddm_intrfc_size

        N11 = - y1 * (K + (OmgSqrd * y2) * M)
        N12 = y1 * (K - OmgSqrd * M)
        N21 = N12
        N22 = Rcoef * self.ExpandedGr if self.SdContainsMeasures() \
            else None if self.StorageType == "Sparse" \
                else np.zeros((FeMatSize, FeMatSize))

        # McreDrck = [[N11, N12],
        #             [N21, N22]]

        McreDrck = [[N21, N22],
                    [N11, N12]]
        
        return McreDrck

    def __Assemble_Mat_Dissipation__(self, FreqIdx):
        """
        Assembles the MCRE matrix for frequency number FreqIdx, in case 
        of dissipation error. It should not be used. It is only used 
        within the function AssembleMcreMat(...) under strict 
        assumptions.
        
        """

        Omg = self.GetOmega(FreqIdx)
        T   = 1. / self.GetFreq(FreqIdx)
        y1  = - 1j * T * Omg / 4

        OmgSqrd = pow(Omg, 2)
        Rcoef   = self.r / (1 - self.r)

        K = self.StiffMat.GetValues(StorageType=self.StorageType)
        M = self.MassMat.GetValues(StorageType=self.StorageType)
        C = self.DampMat.GetValues(StorageType=self.StorageType)

        FeMatSize = self.StiffMat.Size

        if self.IsDDMContext() and self.GetDDMType() == "Primal":
            internal_dofs = self.GetDDMDofs(DofsType="internal")
            K = utils.GetSubMatrix(K, RowsIdx=internal_dofs)
            M = utils.GetSubMatrix(M, RowsIdx=internal_dofs)
            C = utils.GetSubMatrix(C, RowsIdx=internal_dofs)
            FeMatSize = self.StiffMat.Size - self.ddm_intrfc_size

        TempMat = K + 1j * Omg * C - OmgSqrd * M

        N11 = y1 * TempMat
        N21 = - 1j * Omg * C
        N22 = TempMat
        N12 = Rcoef * self.ExpandedGr if self.SdContainsMeasures() \
                            else (None if self.StorageType == "Sparse" \
                                    else np.zeros((FeMatSize, FeMatSize)))

        McreDisp = [[N11, N12],
                    [N21, N22]]
        
        return McreDisp

    def AssembleMcreRhs(self, FreqIdx):
        """
        This function returns the right hand side vector of the MCRE 
        problem for the frequency number FreqIdx.

        """
        if self.ErrType == "Drkr":
            McreRhs = self.__Assemble_Rhs_Drucker__(FreqIdx)
        else:
            McreRhs = self.__Assemble_Rhs_Dissipation__(FreqIdx)
        
        return McreRhs

    def __Assemble_Rhs_Drucker__(self, FreqIdx):
        """
        Assembles the MCRE rhs vector for frequency number FreqIdx, in 
        case of the error in Drucker's sense. It should not be used. It 
        is only used within the function AssembleMcreRhs(...) under strict 
        assumptions.

        """
        if self.DampingExist():
            return self.__Assemble_Rhs_Drucker_Damp__(FreqIdx)
        else:
            return self.__Assemble_Rhs_Drucker_NoDamp__(FreqIdx)

    def __Assemble_Rhs_Drucker_Damp__(self, FreqIdx):
        """
        This function should not be used. It is only used within the 
        function __Assemble_Rhs_Drucker__(...) under strict assumptions.

        """
        y1    = - (1 - self.Gamma) / 2
        Rcoef = self.r / (1 - self.r)

        if self.IsDDMContext() and self.GetDDMType() == "Primal":
            FeVectSize = self.GeneEfrt.Size - self.ddm_intrfc_size
            mes_dofs = self.ddm_new_mes_dofs
        else:
            FeVectSize = self.GeneEfrt.Size
            mes_dofs = self.MesDofs
        
        McreRhs   = np.zeros(3 * FeVectSize, dtype=complex)
        
        TempVect1 = y1 * self.__GetUpdateGeneEfrt__(FreqIdx)
        McreRhs[FeVectSize:2*FeVectSize]  = TempVect1

        if self.SdContainsMeasures():
            TempVect2 = Rcoef * np.matmul(self.Gr, self.GetFreqMesVals(FreqIdx))
            McreRhs[2*FeVectSize + mes_dofs] = TempVect2

        if self.IsDDMContext() and self.GetDDMType() == "Dual":
            interface_dofs = self.GetDDMDofs(DofsType="interface")
            McreRhs[interface_dofs] += \
                self.GetDDMVals(FreqIdx, FieldName='U - V')
            McreRhs[FeVectSize + interface_dofs] += \
                self.GetDDMVals(FreqIdx, FieldName='U - W')
            McreRhs[2*FeVectSize + interface_dofs] += \
                self.GetDDMVals(FreqIdx, FieldName='U')

        return McreRhs

    def __Assemble_Rhs_Drucker_NoDamp__(self, FreqIdx):
        """
        This function should not be used. It is only used within the 
        function __Assemble_Rhs_Drucker__(...) under strict assumptions.

        """
        y1    = self.Gamma / 2.
        Rcoef = self.r / (1 - self.r)

        if self.IsDDMContext() and self.GetDDMType() == "Primal":
            FeVectSize = self.GeneEfrt.Size - self.ddm_intrfc_size
            mes_dofs = self.ddm_new_mes_dofs
        else:
            FeVectSize = self.GeneEfrt.Size
            mes_dofs = self.MesDofs
        
        McreRhs   = np.zeros(2 * FeVectSize, dtype=float)
        
        TempVect1 = y1 * self.__GetUpdateGeneEfrt__(FreqIdx)
        # McreRhs[:FeVectSize]  = TempVect1
        McreRhs[FeVectSize:]  = TempVect1

        if self.SdContainsMeasures():
            TempVect2 = Rcoef * np.matmul(self.Gr, 
                self.GetFreqMesVals(FreqIdx)).real
            # McreRhs[FeVectSize + mes_dofs] = TempVect2
            McreRhs[mes_dofs] = TempVect2

        if self.IsDDMContext() and self.GetDDMType() == "Dual":
            interface_dofs = self.GetDDMDofs(DofsType="interface")
            McreRhs[interface_dofs] += \
                self.GetDDMVals(FreqIdx, FieldName='U - V')
            McreRhs[FeVectSize + interface_dofs] += \
                self.GetDDMVals(FreqIdx, FieldName='U')

        return McreRhs

    def __Assemble_Rhs_Dissipation__(self, FreqIdx):
        """
        Assembles the MCRE rhs vector for frequency number FreqIdx, in 
        case of dissipation error. It should not be used. It is only 
        used within the function AssembleMcreRhs(...) under strict 
        assumptions.
        
        """
        Rcoef = self.r / (1 - self.r)

        if self.IsDDMContext() and self.GetDDMType() == "Primal":
            FeVectSize = self.GeneEfrt.Size - self.ddm_intrfc_size
            mes_dofs = self.ddm_new_mes_dofs
        else:
            FeVectSize = self.GeneEfrt.Size
            mes_dofs = self.MesDofs

        McreRhs   = np.zeros(2 * FeVectSize, dtype=complex)

        TempVect1 = self.__GetUpdateGeneEfrt__(FreqIdx)
        McreRhs[FeVectSize:]  = TempVect1

        if self.SdContainsMeasures():
            TempVect2 = Rcoef * np.matmul(self.Gr, self.GetFreqMesVals(FreqIdx))
            McreRhs[mes_dofs] = TempVect2

        if self.IsDDMContext() and self.GetDDMType() == "Dual":
            interface_dofs = self.GetDDMDofs(DofsType="interface")
            McreRhs[interface_dofs] += \
                self.GetDDMVals(FreqIdx, FieldName='U - V')
            McreRhs[FeVectSize + interface_dofs] += \
                self.GetDDMVals(FreqIdx, FieldName='U')

        return McreRhs

    def SaveMcreLsSol(self, SolVect, FreqIdx):
        """
        Saves the solution of the Mcre linear systeme (localization 
        step). The solution vector is composed of two/three blocks
        depending on the error type used (i.e. Drucker/dissipation).
        
        """
        if self.ErrType == "Drkr":
            self.__SMLS_Drucker__(SolVect, FreqIdx)
        else:
            self.__SMLS_Dissipation__(SolVect, FreqIdx)

    def __SMLS_Drucker__(self, SolVect, FreqIdx):
        """
        The solution vector of Drucker based Mcre is composed of 
        three blocks (U, U-V and U-W). Each block is extracted and
        saved as a FE vector into its corresponding attribute (i.e.
        Field_U, Field_UV, Field_UW). This function should not be 
        used. It is only used within SaveMcreLsSol(...) under strict 
        assumptions.
        
        """
        if self.DampingExist():
            self.__SMLS_Drucker_Damp__(SolVect, FreqIdx)
        else:
            self.__SMLS_Drucker_NoDamp__(SolVect, FreqIdx)

    def __SMLS_Drucker_Damp__(self, SolVect, FreqIdx):
        """
        This function should not be used. It is only used within the 
        function __SMLS_Drucker__(...) under strict assumptions.

        """
        DofNumbering = self.GetDofNumbering()
        ImposedDof   = self.GetImposedDof()
        
        if self.IsDDMContext() and self.GetDDMType() == "Primal":
            interface_dofs = self.GetDDMDofs(DofsType="interface")
            internal_dofs = self.GetDDMDofs(DofsType="internal")
            fe_size = DofNumbering.GetFreeDofNbr()
            FeVectSize   = self.GeneEfrt.Size - self.ddm_intrfc_size
            
            uv_buff = np.zeros(fe_size, dtype=complex)
            uv_buff[internal_dofs]  = SolVect[0:FeVectSize]
            uv_buff[interface_dofs] = self.GetDDMVals(FreqIdx, FieldName='U - V')
            
            uw_buff = np.zeros(fe_size, dtype=complex)
            uw_buff[internal_dofs]  = SolVect[FeVectSize:2*FeVectSize]
            uw_buff[interface_dofs] = self.GetDDMVals(FreqIdx, FieldName='U - W')
            
            u_buff  = np.zeros(fe_size, dtype=complex)
            u_buff[internal_dofs]  = SolVect[2*FeVectSize:]
            u_buff[interface_dofs] = self.GetDDMVals(FreqIdx, FieldName='U')
        else:
            FeVectSize   = self.GeneEfrt.Size
            uv_buff = SolVect[0:FeVectSize]
            uw_buff = SolVect[FeVectSize:2*FeVectSize]
            u_buff  = SolVect[2*FeVectSize:]

        UV = datastr.Vector(DofNumbering, ImposedDof, \
            VectorType="DofsField", ValuesBuff=uv_buff)
        UW = datastr.Vector(DofNumbering, ImposedDof, \
            VectorType="DofsField", ValuesBuff=uw_buff)
        U  = datastr.Vector(DofNumbering, ImposedDof, \
            VectorType="DofsField", ValuesBuff=u_buff)

        self.Field_U[FreqIdx]  = U
        self.Field_UV[FreqIdx] = UV
        self.Field_UW[FreqIdx] = UW

    def __SMLS_Drucker_NoDamp__(self, SolVect, FreqIdx):
        """
        This function should not be used. It is only used within the 
        function __SMLS_Drucker__(...) under strict assumptions.

        """
        DofNumbering = self.GetDofNumbering()
        ImposedDof   = self.GetImposedDof()
        
        if self.IsDDMContext() and self.GetDDMType() == "Primal":
            interface_dofs = self.GetDDMDofs(DofsType="interface")
            internal_dofs = self.GetDDMDofs(DofsType="internal")
            fe_size = DofNumbering.GetFreeDofNbr()
            FeVectSize   = self.GeneEfrt.Size - self.ddm_intrfc_size
            
            uv_buff = np.zeros(fe_size, dtype=float)
            uv_buff[internal_dofs]  = SolVect[0:FeVectSize]
            uv_buff[interface_dofs] = self.GetDDMVals(FreqIdx, FieldName='U - V')
            
            u_buff  = np.zeros(fe_size, dtype=float)
            u_buff[internal_dofs]  = SolVect[FeVectSize:]
            u_buff[interface_dofs] = self.GetDDMVals(FreqIdx, FieldName='U')
        else:
            FeVectSize   = self.GeneEfrt.Size
            uv_buff = SolVect[0:FeVectSize]
            u_buff  = SolVect[FeVectSize:]

        UV = datastr.Vector(DofNumbering, ImposedDof, \
            VectorType="DofsField", ValuesBuff=uv_buff)
        U  = datastr.Vector(DofNumbering, ImposedDof, \
            VectorType="DofsField", ValuesBuff=u_buff)

        self.Field_U[FreqIdx]  = U
        self.Field_UV[FreqIdx] = UV

    def __SMLS_Dissipation__(self, SolVect, FreqIdx):
        """
        The solution vector of Dissipation based Mcre is composed 
        of too blocks (U and U-V). Each block is extracted and
        saved as a FE vector into its corresponding attribute (i.e.
        Field_U, Field_UV). This function should not be used. It is
        only used within SaveMcreLsSol(...) under strict assumptions.
        
        """
        DofNumbering = self.GetDofNumbering()
        ImposedDof   = self.GetImposedDof()

        if self.IsDDMContext() and self.GetDDMType() == "Primal":
            interface_dofs = self.GetDDMDofs(DofsType="interface")
            internal_dofs = self.GetDDMDofs(DofsType="internal")
            fe_size = DofNumbering.GetFreeDofNbr()
            FeVectSize   = self.GeneEfrt.Size - self.ddm_intrfc_size
            
            uv_buff = np.zeros(fe_size, dtype=complex)
            uv_buff[internal_dofs]  = SolVect[0:FeVectSize]
            uv_buff[interface_dofs] = self.GetDDMVals(FreqIdx, FieldName='U - V')
            
            u_buff  = np.zeros(fe_size, dtype=complex)
            u_buff[internal_dofs]  = SolVect[FeVectSize:]
            u_buff[interface_dofs] = self.GetDDMVals(FreqIdx, FieldName='U')
        else:
            FeVectSize   = self.GeneEfrt.Size
            uv_buff = SolVect[0:FeVectSize]
            u_buff  = SolVect[FeVectSize:]

        UV = datastr.Vector(DofNumbering, ImposedDof, \
            VectorType="DofsField", ValuesBuff=uv_buff)
        U  = datastr.Vector(DofNumbering, ImposedDof, \
            VectorType="DofsField", ValuesBuff=u_buff)

        self.Field_U[FreqIdx]  = U
        self.Field_UV[FreqIdx] = UV
    
    def __GetUpdateGeneEfrt__(self, FreqIdx):
        """
        Returns the generelized efforts (i.e. F) vector after adding the 
        contribution of imposed DOFS.
        
        """
        if self.DampingExist():
            UpdatedGeneEfrt = self.GeneEfrt.GetValues().astype(complex)
        else:
            UpdatedGeneEfrt = self.GeneEfrt.GetValues().astype(float)

        Omega   = self.GetOmega(FreqIdx)
        OmgSqrd = pow(Omega, 2)

        StiffGeP, StiffGeV = self.StiffMat.GetGeneEffPart()
        UpdatedGeneEfrt[StiffGeP] += StiffGeV
        
        MassGeP, MassGeV   = self.MassMat.GetGeneEffPart()
        UpdatedGeneEfrt[MassGeP] += - OmgSqrd * MassGeV
        
        if self.DampingExist():
            DampGeP, DampGeV   = self.DampMat.GetGeneEffPart()
            UpdatedGeneEfrt[DampGeP] += 1j * Omega * DampGeV

        if self.IsDDMContext() and self.GetDDMType() == "Primal":
            internal_dofs  = self.GetDDMDofs(DofsType="internal")
            interface_dofs = self.GetDDMDofs(DofsType="interface")
            interface_dofs_vals = self.GetDDMVals(FreqIdx, FieldName="U")

            K  = self.StiffMat.GetValues(StorageType=self.StorageType)
            K_ib = utils.GetSubMatrix(K, RowsIdx=internal_dofs, \
                                                ColsIdx=interface_dofs)
            del K

            M  = self.MassMat.GetValues(StorageType=self.StorageType)
            M_ib = utils.GetSubMatrix(M, RowsIdx=internal_dofs, \
                                                ColsIdx=interface_dofs)
            del M

            UpdatedGeneEfrt = UpdatedGeneEfrt[internal_dofs] - \
                (K_ib - OmgSqrd * M_ib).dot(interface_dofs_vals)
            
            if self.DampingExist():
                C  = self.DampMat.GetValues(StorageType=self.StorageType)
                C_ib = utils.GetSubMatrix(C, RowsIdx=internal_dofs, \
                                                    ColsIdx=interface_dofs)
                del C

                UpdatedGeneEfrt -= (1j * Omega) * C_ib.dot(interface_dofs_vals)
        
        return UpdatedGeneEfrt
    
    def GetField(self, FieldName, FreqIdx):
        """
        Returns the FE vector of frequency 
        FreqIdx whose name is FieldName. 'L'
        is for Lagrange multipliers
        
        """
        assert(0 <= FreqIdx < self.FreqsNbr)

        if FieldName == "U":
            return self.Field_U[FreqIdx]
        elif FieldName == "U - V":
            return self.Field_UV[FreqIdx]
        elif FieldName == "U - W":
            assert(self.ErrType == "Drkr")
            if not self.DampingExist():
                UV = self.GetField('U - V', FreqIdx).GetValues()
                DofNumbering = self.GetDofNumbering()
                ImposedDof   = self.GetImposedDof()
                y = - self.Gamma / (1 - self.Gamma)
                return datastr.Vector(DofNumbering, ImposedDof,
                    VectorType="DofsField", ValuesBuff=y*UV)
            else: return self.Field_UW[FreqIdx]
        elif FieldName not in ['V', 'W', 'L']:
            raise ValueError("Unknown field "
                "name : {}".format(FieldName))
        else: return self.GetSpecialField(FieldName, FreqIdx)
    
    def GetSpecialField(self, FieldName, FreqIdx):
        """
        Returns the FE vector of frequency 
        FreqIdx whose name is FieldName.
        
        """
        DofNumbering = self.GetDofNumbering()
        ImposedDof   = self.GetImposedDof()
        
        if FieldName == 'V':
            Field1 = self.GetField('U - V', FreqIdx).GetValues()
            Field2 = self.GetField('U', FreqIdx).GetValues()
            Result = Field2 - Field1
        elif FieldName == 'W':
            Field1 = self.GetField('U - W', FreqIdx).GetValues()
            Field2 = self.GetField('U', FreqIdx).GetValues()
            Result = Field2 - Field1
        elif self.ErrType == "Drkr" and not self.DampingExist():
            Field = self.GetField('U - W', FreqIdx).GetValues()
            Result = (self.Gamma - 1) * Field
        else: raise NotImplementedError
        
        return datastr.Vector(DofNumbering, ImposedDof,
            VectorType="DofsField", ValuesBuff=Result)

    def GetElmntryField(self, FieldName, FreqIdx, ElmntIdx):
        """
        Returns the elementry field vector of the element number
        ElmntIdx. The element vector is extracted from the solution 
        of the Mcre linear system of frequency number FreqIdx. Since 
        the solution vector is composed of 2/3 blocks depending on 
        the error type, the parameter FieldName is used to select the 
        block from where the elementry vector will be extracted.
        
        """
        assert(0 <= FreqIdx < self.FreqsNbr)
        return self.GetField(FieldName, \
            FreqIdx).GetElmntryVectVal(ElmntIdx, ImpDofs=False)

    def __CalcElmntryErr_Drucker_Damp__(self, FreqIdx, ElmntIdx, \
                            ElmntryStiff, ElmntryMass, ElmntryDamp):
        """
        Returns the Drucker based CRE error (Mcre without the 
        measurements term) of element ElmntIdx evaluated for 
        the frequency FreqIdx. Since when damping exist the Drucker 
        Cre error is composed of two terms, a tuple is returned :

                        (term_1, term_2, term_3)

            - term_1 : modulus of the first term of the error.
            - term_2 : modulus of the second term of the error.
            - term_3 : modulus of the total error (1st + 2nd terms).

        This function should not be used. It is only used within 
        __SMR_Drucker_Damp__(...) under strict assumptions.

        """
        DrukErr1 = self.__CFEED1_Damp__(FreqIdx, \
            ElmntIdx, ElmntryStiff, ElmntryDamp)
        DrukErr2 = self.__CFEED2_Damp__(FreqIdx, ElmntIdx, ElmntryMass)
        TotalErr = DrukErr1 + DrukErr2

        return abs(DrukErr1), abs(DrukErr2), abs(TotalErr)

    def __CalcElmntryErr_Drucker_NoDamp__(self, \
            FreqIdx, ElmntIdx, ElmntryStiff, ElmntryMass):
        """
        Returns the NoDamping-Drucker based CRE error (Mcre without 
        the measurements term) of element ElmntIdx evaluated for 
        the frequency FreqIdx. The returned value is the modulus 
        of the error. This function should not be used. It is 
        only used within __SMR_Drucker_NoDamp__(...) under strict 
        assumptions.

        """
        DrukErr1 = self.__CFEED1_NoDamp__(
            FreqIdx, ElmntIdx, ElmntryStiff)
        DrukErr2 = self.__CFEED2_NoDamp__(
            FreqIdx, ElmntIdx, ElmntryMass)
        TotalErr = DrukErr1 + DrukErr2

        return abs(TotalErr)

    def __CFEED1_Damp__(self, FreqIdx, ElmntIdx, ElmntryStiff, ElmntryDamp):
        """
        Returns the first term of Drucker based Cre error. This 
        function should not be used. It is only used within 
        __CalcElmntryErr_Drucker_Damp__(...) under strict assumptions.
        
        """
        y1 = self.Gamma / 2
        T  = 1. / self.GetFreq(FreqIdx)
        OmgSqrd = pow(self.GetOmega(FreqIdx), 2)

        Mat1 = ElmntryStiff + T * OmgSqrd * ElmntryDamp
        UV   = self.GetElmntryField("U - V", FreqIdx, ElmntIdx)
        TempVect = np.matmul(Mat1, UV)
        return y1 * np.vdot(UV, TempVect)

    def __CFEED1_NoDamp__(self, FreqIdx, ElmntIdx, ElmntryStiff):
        """
        Returns the first term of Drucker based Cre error. This 
        function should not be used. It is only used within 
        __CalcElmntryErr_Drucker_NoDamp__(...) under strict assumptions.
        
        """
        y1 = self.Gamma / 2
        UV   = self.GetElmntryField("U - V", FreqIdx, ElmntIdx)
        TempVect = np.matmul(ElmntryStiff, UV)
        return y1 * np.vdot(UV, TempVect)

    def __CFEED2_Damp__(self, FreqIdx, ElmntIdx, ElmntryMass):
        """
        Returns the second term of Drucker based Cre error. This 
        function should not be used. It is only used within 
        __CalcElmntryErr_Drucker_Damp__(...) under strict assumptions.
        
        """
        y2 = (1 - self.Gamma) / 2
        OmgSqrd = pow(self.GetOmega(FreqIdx), 2)

        UW = self.GetElmntryField("U - W", FreqIdx, ElmntIdx)
        TempVect = np.matmul(ElmntryMass, UW)

        return y2 * OmgSqrd * np.vdot(UW, TempVect)

    def __CFEED2_NoDamp__(self, FreqIdx, ElmntIdx, ElmntryMass):
        """
        Returns the second term of Drucker based Cre error. This 
        function should not be used. It is only used within 
        __CalcElmntryErr_Drucker_Damp__(...) under strict assumptions.
        
        """
        y1 = (1 - self.Gamma) / 2
        y2 = - self.Gamma / (1. - self.Gamma)
        OmgSqrd = pow(self.GetOmega(FreqIdx), 2)

        UV = self.GetElmntryField("U - V", FreqIdx, ElmntIdx)
        UW = y2 * UV
        TempVect = np.matmul(ElmntryMass, UW)

        return y1 * OmgSqrd * np.vdot(UW, TempVect)

    def __CalcElmntryErr_Dissipation__(self, FreqIdx, ElmntIdx, ElmntryDamp):
        """
        Returns the Dissipation based CRE error (Mcre without 
        the measurements term) of element ElmntIdx evaluated for 
        the frequency FreqIdx. The returned value is the modulus 
        of the error. This function should not be used. It is 
        only used within __SMR_Dissipation__(...) under strict 
        assumptions.

        """
        T  = 1. / self.GetFreq(FreqIdx)
        OmgSqrd = pow(self.GetOmega(FreqIdx), 2)

        UV   = self.GetElmntryField("U - V", FreqIdx, ElmntIdx)
        TempVect = np.matmul(ElmntryDamp, UV)

        return abs((T * OmgSqrd / 4) * np.vdot(UV, TempVect))

    def AddResultToVTK(self, ResObj, **kwargs):
        """
        Saves the Mcre results into a "result.Results" object. 
        The object can be used after to visualize the results
        in paraview.

        Parameters
        ----------
            ResObj : 'result.Results'
                The Results object.
            
            kwargs : 'dict'
                An optional dictionary that contains keywords
                used to change the results saved by default. 
                It thus allows the user to control the results 
                to be saved. It can contains the keywords :

                AddFields :
                    If used, it allows to save the fields 'U',
                    'UV' and 'UW' into the Results object.

                    Examples :
                        - AddFields=("Field_U")
                        - AddFields=("Field_UW", "Field_UV")
                        - AddFields=("Field_U", "Field_UV", "Field_UW")
                
                AddErrors : 
                    This keyword is only available for Drucker's
                    based error. If used, it allows to save the 
                    errors related to fields 'UV' and 'UW' into 
                    the Results object.

                    Examples :
                        - AddFields=("Err_UW")
                        - AddFields=("Err_UV")
                        - AddFields=("Err_UV", "Err_UW")

        """
        assert(isinstance(ResObj, result.Results))
        if self.ErrType == "Drkr":
            self.__ARTVTK_Drucker__(ResObj, **kwargs)
        else:
            self.__ARTVTK_Dissipation__(ResObj, **kwargs)

    def __ARTVTK_Drucker__(self, ResObj, **kwargs):
        """
        This function should not be used. It is only used 
        within AddResultToVTK(...) under strict assumptions.
        
        """
        if self.DampingExist():
            self.__ARTVTK_Drucker_Damp__(ResObj, **kwargs)
        else:
            self.__ARTVTK_Drucker_NoDamp__(ResObj, **kwargs)

    def __ARTVTK_Drucker_Damp__(self, ResObj, **kwargs):
        """
        This function should not be used. It is only used 
        within __ARTVTK_Drucker__(...) under strict assumptions.
        
        """
        StudyModel = self.GetDofNumbering().GetModel()
        freqs_iterator = [freq_idx for freq_idx in range(self.FreqsNbr) \
                                        if self.AlreadyCalculated(freq_idx)]
        freqs_nbr = len(freqs_iterator)

        add_fields = False
        add_errors = False

        if bool(kwargs):
            add_fields = "AddFields" in kwargs
            add_errors = "AddErrors" in kwargs

            if len(kwargs) == 1:
                assert(add_fields or add_errors)
            elif len(kwargs) == 2:
                assert(add_fields and add_errors)
            else:
                assert(False)

            if add_fields:
                assert(isinstance(kwargs["AddFields"], (tuple, list)))
                if len(set(kwargs["AddFields"])) != len(kwargs["AddFields"]):
                    raise ValueError("Redundant field name (AddFields) !")
                else:
                    for field_name in kwargs["AddFields"]:
                        assert(field_name in ["Field_U", "Field_UV", "Field_UW"])
            
            if add_errors:
                assert(isinstance(kwargs["AddErrors"], (tuple, list)))
                if len(set(kwargs["AddErrors"])) != len(kwargs["AddErrors"]):
                    raise ValueError("Redundant error name (AddErrors) !")
                else:
                    for error_name in kwargs["AddErrors"]:
                        assert(error_name in ["Err_UV", "Err_UW"])

        is_err_uv = add_errors and "Err_UV" in kwargs["AddErrors"]
        is_err_uw = add_errors and "Err_UW" in kwargs["AddErrors"]

        is_field_u  = add_fields and "Field_U"  in kwargs["AddFields"]
        is_field_uv = add_fields and "Field_UV" in kwargs["AddFields"]
        is_field_uw = add_fields and "Field_UW" in kwargs["AddFields"]

        if freqs_nbr > 1:
            if is_err_uv:
                TotalNameUV = "ErrUV - (Total)"
                TotalArayUV = np.zeros(self.GetElmntsNbr())
            if is_err_uw:
                TotalNameUW = "ErrUW - (Total)"
                TotalArayUW = np.zeros(self.GetElmntsNbr())
            TotalNameTT = "ErrTT - (Total)"
            TotalArayTT = np.zeros(self.GetElmntsNbr())

        for FreqIdx in freqs_iterator:
            str_suffix = " - (%.2f Hz)" % self.GetFreq(FreqIdx)

            if is_err_uv:
                FreqNameUV = "Err_UV" + str_suffix
                FreqArayUV = self.McreResults[FreqIdx, 0]
                ResObj.AddElmntsField(StudyModel, FreqArayUV, FreqNameUV)
            
            if is_err_uw:
                FreqNameUW = "Err_UW" + str_suffix
                FreqArayUW = self.McreResults[FreqIdx, 1]
                ResObj.AddElmntsField(StudyModel, FreqArayUW, FreqNameUW)

            FreqNameTT = "Err" + str_suffix
            FreqArayTT = self.McreResults[FreqIdx, 2]
            ResObj.AddElmntsField(StudyModel, FreqArayTT, FreqNameTT)
            
            if freqs_nbr > 1:
                if is_err_uv:
                    TotalArayUV += FreqArayUV
                if is_err_uw:
                    TotalArayUW += FreqArayUW
                TotalArayTT += FreqArayTT
            
            if is_field_u:
                self.GetField("U", FreqIdx).AddResultToVTK(\
                            ResObj, suffix=" (U)" + str_suffix, imag=False)

            if is_field_uv:
                self.GetField("U - V", FreqIdx).AddResultToVTK(\
                            ResObj, suffix=" (U-V)" + str_suffix, imag=False)

            if is_field_uw:
                self.GetField("U - W", FreqIdx).AddResultToVTK(\
                            ResObj, suffix=" (U-W)" + str_suffix, imag=False)
        
        if freqs_nbr > 1:
            if is_err_uv:
                ResObj.AddElmntsField(StudyModel, TotalArayUV, TotalNameUV)
            if is_err_uw:
                ResObj.AddElmntsField(StudyModel, TotalArayUW, TotalNameUW)
            ResObj.AddElmntsField(StudyModel, TotalArayTT, TotalNameTT)

    def __ARTVTK_Drucker_NoDamp__(self, ResObj, **kwargs):
        """
        This function should not be used. It is only used 
        within __ARTVTK_Drucker__(...) under strict assumptions.
        
        """
        StudyModel = self.GetDofNumbering().GetModel()
        freqs_iterator = [freq_idx for freq_idx in range(self.FreqsNbr) \
                                        if self.AlreadyCalculated(freq_idx)]
        freqs_nbr = len(freqs_iterator)

        add_fields = False

        if bool(kwargs):
            add_fields = "AddFields" in kwargs

            if not add_fields:
                raise ValueError("The keyword 'AddFields' is the only possible one !")
            else:
                assert(isinstance(kwargs["AddFields"], (tuple, list)))

            if len(set(kwargs["AddFields"])) != len(kwargs["AddFields"]):
                raise ValueError("Redundant field name (AddFields) !")
            elif len(kwargs) != 1:
                raise ValueError("Unknown keyword !")

            for field_name in kwargs["AddFields"]:
                assert(field_name in ["Field_U", "Field_UV"])

        is_field_u  = add_fields and "Field_U"  in kwargs["AddFields"]
        is_field_uv = add_fields and "Field_UV" in kwargs["AddFields"]

        if freqs_nbr > 1:
            TotalName = "Err (Total)"
            TotalAray = np.zeros(self.GetElmntsNbr())

        for FreqIdx in freqs_iterator:
            str_suffix = " - (%.2f Hz)" % self.GetFreq(FreqIdx)
            FreqName = "Err" + str_suffix
            FreqAray = self.McreResults[FreqIdx]
            ResObj.AddElmntsField(StudyModel, FreqAray, FreqName)
            if freqs_nbr > 1:
                TotalAray += FreqAray

            if is_field_u:
                self.GetField("U", FreqIdx).AddResultToVTK(\
                            ResObj, suffix=" (U)" + str_suffix)

            if is_field_uv:
                self.GetField("U - V", FreqIdx).AddResultToVTK(\
                            ResObj, suffix=" (U-V)" + str_suffix)
    
        if freqs_nbr > 1:
            ResObj.AddElmntsField(StudyModel, TotalAray, TotalName)

    def __ARTVTK_Dissipation__(self, ResObj, **kwargs):
        """
        This function should not be used. It is only used 
        within AddResultToVTK(...) under strict assumptions.
        
        """
        StudyModel = self.GetDofNumbering().GetModel()
        freqs_iterator = [freq_idx for freq_idx in range(self.FreqsNbr) \
                                        if self.AlreadyCalculated(freq_idx)]
        freqs_nbr = len(freqs_iterator)

        add_fields = False

        if bool(kwargs):
            add_fields = "AddFields" in kwargs
            
            if not add_fields:
                raise ValueError("The keyword 'AddFields' is the only possible one !")
            else:
                assert(isinstance(kwargs["AddFields"], (tuple, list)))

            if len(set(kwargs["AddFields"])) != len(kwargs["AddFields"]):
                raise ValueError("Redundant field name (AddFields) !")
            elif len(kwargs) != 1:
                raise ValueError("Unknown keyword !")
            
            for field_name in kwargs["AddFields"]:
                assert(field_name in ["Field_U", "Field_UV"])

        is_field_u  = add_fields and "Field_U"  in kwargs["AddFields"]
        is_field_uv = add_fields and "Field_UV" in kwargs["AddFields"]

        if freqs_nbr > 1:
            TotalName = "Err (Total)"
            TotalAray = np.zeros(self.GetElmntsNbr())

        for FreqIdx in freqs_iterator:
            str_suffix = " - (%.2f Hz)" % self.GetFreq(FreqIdx)
            FreqName = "Err" + str_suffix
            FreqAray = self.McreResults[FreqIdx]
            ResObj.AddElmntsField(StudyModel, FreqAray, FreqName)
            if freqs_nbr > 1:
                TotalAray += FreqAray

            if is_field_u:
                self.GetField("U", FreqIdx).AddResultToVTK(\
                            ResObj, suffix=" (U)" + str_suffix, imag=False)

            if is_field_uv:
                self.GetField("U - V", FreqIdx).AddResultToVTK(\
                            ResObj, suffix=" (U-V)" + str_suffix, imag=False)
    
        if freqs_nbr > 1:
            ResObj.AddElmntsField(StudyModel, TotalAray, TotalName)



