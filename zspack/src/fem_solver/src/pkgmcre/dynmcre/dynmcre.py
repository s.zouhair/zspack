"""
This module implements the modified constitutive relation error (mcre) class in 
linear dynamics, and all the methods that allow interacting with it.

"""

from scipy.optimize import minimize, fmin_bfgs
from inspect import signature
from functools import reduce
from mpi4py import MPI
import numpy as np

from ... import pkgdatastr as datastr
from ...pkgutils import IsParallelStudy, \
    GetParamType, IsInteger, PrintAllRanks

from . import CreateLocDynMcre
from .gradmcre import *
from .evalmcre import *

class DynMcre:
    """
    The mcre localisation step class for linear dynamics problems.

    Parameters
    ----------
        xxxx : 'xxxx'
            xxxx xxxx xxxx xxxx.

    Attributes
    ----------
        xxxx : 'xxxx'
            xxxx xxxx xxxx xxxx.

        ModelParams : 'dict'
            The model's parameters and their initial values.
            For parallel studies, if the value is None for
            a given proc, that means that the parameter isn't 
            local and belongs to another set of procs. The
            gradient of the local error with respect to this
            parameter is then zero.

            Example : toto = ("young", 210)

    """

    def __init__(self, ModelFunc, FreqsList, MesDofs, MesVals, r=0.5,
        InterfaceNumbering=None, ErrType="Drkr", StorageType="Sparse",
        FreqsScale=None, ApproxGrad=True, Gamma=0.5, **ModelParams):

        self.ModelFunc = ModelFunc
        self.ModelParams = {}

        assert(isinstance(ApproxGrad, bool))
        self.ApproxGrad = ApproxGrad
        self.GradStep = {"young" : 1, "poisson" : 1e-8}
        self.GetFeMatDer = self.GetFeMatDerApprox \
            if ApproxGrad else self.GetFeMatDerAnalyt

        if len(ModelParams) == 0 and not self.IsParallel():
            raise ValueError("Model parameters are mandatory")
        
        if self.IsParallel():
            self.LocModelParams = {p : v for p, v in 
                ModelParams.items() if v is not None}
            ModelParams = self.GetParModelParams(
                self.LocModelParams)
        else: self.LocModelParams = None

        if not self.ApproxGrad:
            self.CheckParamsRedundancy(self.LocModelParams \
                if self.IsParallel() else ModelParams)

        self.ParamsNbr = len(ModelParams)
        self.ParamsValues = np.zeros((1, self.ParamsNbr))
        self.CurrParams = np.zeros(self.ParamsNbr)

        for idx, (key, (pname, value)) in enumerate(ModelParams.items()):
            self.ModelParams[key] = (pname, GetParamType(pname), idx)
            self.ParamsValues[0, idx] = value
            self.CurrParams[idx] = value

        self.InterfaceNumbering = InterfaceNumbering
        self.StorageType = StorageType
        self.FreqsNbr = len(FreqsList)
        self.FreqsList = FreqsList
        self.ErrType = ErrType

        if FreqsScale is not None:
            assert(isinstance(FreqsScale, np.ndarray))
            assert(FreqsScale.ndim == 1 and \
                FreqsScale.size == self.FreqsNbr)
            self.FreqsScale = FreqsScale
        else: self.FreqsScale = np.ones(self.FreqsNbr, dtype=float)
        
        self.MesDofs = MesDofs
        self.MesVals = MesVals
        
        self.Gamma = Gamma
        self.r = r

        ParamsDict = self.LocModelParams if \
            self.IsParallel() else ModelParams
        
        self.CheckModelFunction()
        LocMcreArgs = self.GetLocMcreArgs({p : v 
            for p, (_, v) in ParamsDict.items()})
        
        self.GeneEfrt = LocMcreArgs["GeneEfforts"]
        self.StiffMat = LocMcreArgs["Stiffness"]
        self.DampMat = LocMcreArgs["Damping"]
        self.MassMat = LocMcreArgs["Mass"]

        self.CheckModelParams()

        self.LocMcreObj = CreateLocDynMcre(
            self.StiffMat, self.MassMat  , self.DampMat, 
            self.GeneEfrt, self.FreqsList, self.MesDofs, 
            self.MesVals , self.InterfaceNumbering, 
            self.ErrType , self.r, self.Gamma, 
            self.StorageType)
        
        self.CreFunc = GetCreFunc(self)
        self.CreGrad = {
            "Stiffness" : GetStiffCreGrad(self),
            "Mass" : GetMassCreGrad(self),
            "Damping" : GetDampCreGrad(self)
        }

        # To evaluate the initial value of the error
        self.LocalizeDynMcre(OpsUpdated=False)
        self.OldCreVal = self.EvaluateCreFunc(self.CurrParams)

        self.NewCreVal = self.OldCreVal
        self.FuncCalls, self.GradCalls = 0, 0
        self.UsingBFGS = False
        self.OldParamsIdx = 0

    def IsParallel(self):
        """Checks if the object is parallel"""
        return IsParallelStudy()

    def DampingExist(self):
        """Checks whether damping exist or not."""
        return self.DampMat is not None
    
    def GetErrType(self):
        """Returns the mCRE formulation type"""
        return self.ErrType

    def GetFrqsNbr(self):
        """Returns the number of frequencies"""
        return self.FreqsNbr
    
    def GetMethod(self):
        """Retruns the method used for minimisation"""
        return "BFGS" if self.UsingBFGS else "Gradient Descent"
    
    def GetGradEvalType(self):
        """Returns the gradient evaluation type"""
        return "Approximation" if \
            self.ApproxGrad else "Analytical"

    def GetFreq(self, FreqIdx):
        """Returns the frequency FreqIdx"""
        return self.FreqsList[FreqIdx]

    def GetMcreLocObj(self):
        """Returns the localisation step object"""
        return self.LocMcreObj

    def GetGamma(self):
        """Return the parameter Gamma"""
        return self.Gamma

    def GetCurrentParams(self):
        """
        Returns an array containing the current 
        values of model's parameters
        
        """
        return self.CurrParams

    def IsLocalParam(self, ParamName):
        """Checks if ParamName is a local parameter"""
        return True if not self.IsParallel() \
            else ParamName in self.LocModelParams

    def useParDDM(self):
        """Checks whether to use a parallel DDM method"""
        return self.InterfaceNumbering is not None

    def GetField(self, FieldName, FreqIdx):
        """Returns the field FieldName for frequency FreqIdx"""
        Result = self.LocMcreObj.GetField(FieldName, FreqIdx)
        return Result.GetValues()

    def GetParModelParams(self, ModelParams):
        """Returns the parameters of a parralel model"""
        GlbP = reduce(lambda x, y : {**x, **y}, 
            MPI.COMM_WORLD.allgather(ModelParams))
        for LocP, V in ModelParams.items():
            if GlbP[LocP] != V: MPI.COMM_WORLD.Abort(1)
        return GlbP

    def LocalParamsChanged(self, Params):
        """Checks if a subdomain have parameters"""
        if not self.IsParallel(): return True
        Curr = self.GetCurrentParams()
        return not all([Curr[val[2]] == Params[val[2]] 
            for key, val in self.ModelParams.items() 
            if self.IsLocalParam(key)])

    def CheckParamsRedundancy(self, LocModelParams):
        """
        Checks that the model parameters aren't redundant 
        if the mCRE gradient will be evaluated analytically.
        For example, there shouldn't be two Young or Poisson 
        parameters per subdomain (or the entire domain in 
        the sequential case).

        """
        Cyellow = '\033[33m'
        Cend = '\033[0m'
        FoundParams = []

        #   Si le gradient va etre évaluer analytiquement,
        #   il faut que les paramètres locaux ne soient pas 
        #   redondants (il n'y a pas deux module de young ou
        #   de poisson par exemple).
        # 
        #   Penser peut etre plus tard à donner pour chaque 
        #   paramètre local le groupe d'élément concerné. 
        #   Cela rendra l'évaluation analytic du gradient 
        #   toujours possible.

        for Type, _ in LocModelParams.values():
            if Type in FoundParams: 
                raise ValueError("Two {} parameters "
                    "encountered. The gradient "
                    "cannot be evaluated analytically,"
                    " please switch to an approximate"
                    " evaluation.".format(Type))
            else: FoundParams.append(Type)
        
        if MPI.COMM_WORLD.Get_rank() == 0:
            ToPrint = ("Warning : You are using an "
                "analytical evaluation of the gradient"
                ".\n => If there is any inhomogeneous "
                "parameters{} you might get wrong "
                "results !!!").format((" within a "
                "subdomain,") if self.IsParallel() else ",")
            print(Cyellow + ToPrint + Cend)

    def CheckModelParams(self):
        """Checks the model parameters"""
        if not self.DampingExist():
            assert(self.ErrType != "Disp")
            for val in self.ModelParams.values():
                assert(val[1] != "Damping")
        elif self.ErrType == "Disp":
            for val in self.ModelParams.values():
                assert(val[1] not in ["Stiffness", "Mass"])

    def CheckModelFunction(self):
        """Checks the model's function"""
        if not callable(self.ModelFunc):
            raise ValueError("The first argument "
                "must be a callable fuinction")
        ModFuncSig = str(signature(self.ModelFunc))
        ParamsDict = self.LocModelParams if \
            self.IsParallel() else self.ModelParams
        for Param in ParamsDict.keys():
            if Param not in ModFuncSig:
                raise ValueError("The model function"
                    f" must accept the keyword {Param}")
        if self.ApproxGrad and "Only" not in ModFuncSig:
            raise ValueError("The model function"
                " must accept the keyword Only")

    def GetLocMcreArgs(self, ModelParams, Only=None):
        """"""
        assert(Only in [None, "Stiffness", "Mass", "Damping"])

        if Only is None: PramsDict = ModelParams
        else: PramsDict = {"Only" : Only, **ModelParams}
        
        FeOperators = self.ModelFunc(**PramsDict)
        
        if Only is not None: FeOperators = [FeOperators]
        else: assert(isinstance(FeOperators, (tuple, list)))
        
        McreArgs = {}
        
        for FeOp in FeOperators:
            IsMatrix = isinstance(FeOp, datastr.Matrix)
            IsVector = isinstance(FeOp, datastr.Vector)
            if not (IsMatrix or IsVector):
                raise ValueError("The model function must "
                    "return only FE matrices and vectors")
            elif FeOp.GetType() in McreArgs:
                raise ValueError("the model function returned"
                    " two {} objects".format(FeOp.GetType()))
            McreArgs[FeOp.GetType()] = FeOp
        
        if Only is None:
            if "GeneEfforts" not in McreArgs:
                raise ValueError("The model function must "
                    "return the generelized efforts vector")
            elif "Stiffness" not in McreArgs:
                raise ValueError("The model function "
                    "must return the stiffness matrix")
            elif "Mass" not in McreArgs:
                raise ValueError("The model function"
                    " must return the mass matrix")
            elif "Damping" not in McreArgs:
                McreArgs["Damping"] = None
        elif Only not in McreArgs:
            raise ValueError("Expecting "
                "a {} matrix !".format(Only))
        elif len(McreArgs) != 1:
            raise ValueError("Expecting only"
                "a {} matrix !".format(Only))

        return McreArgs

    def GetFeMatrix(self, MatType, GetValues=True):
        """
        Returns a FE matrix. MatType can be either 
        "Mass", "Stiffness" or "Damping".
        
        """
        assert(MatType in [ "Mass", 
            "Damping", "Stiffness"])
        if MatType == "Stiffness":
            Target = self.StiffMat
        elif MatType == "Damping":
            Target = self.DampMat
        else: Target = self.MassMat
        return Target if not GetValues else \
            Target.GetValues(self.StorageType)

    def GetFeMatDerAnalyt(self, Params, ParamName):
        """
        Returns a FE matrix. MatType can be either 
        "Mass", "Stiffness" or "Damping". If Derivative
        is not None, the derivative of the FE matrix 
        with respect to the parameter passed with this 
        keyword is returned
        
        """
        assert(np.all(Params == self.CurrParams))
        ParamType = self.ModelParams[ParamName][0]
        MatrixType = self.ModelParams[ParamName][1]
        FeDerMatrix  = self.GetFeMatrix(MatrixType, 
            GetValues=False).GetDerivative(ParamType)
        return FeDerMatrix.GetValues(self.StorageType)

    def GetFeMatDerApprox(self, Params, ParamName):
        """
        Returns a FE matrix. MatType can be either 
        "Mass", "Stiffness" or "Damping". If Derivative
        is not None, the derivative of the FE matrix 
        with respect to the parameter passed with this 
        keyword is returned
        
        """
        assert(np.all(Params == self.CurrParams))
        Type, Only, _ = self.ModelParams[ParamName]
        ParamsDict = {key : Params[val[2]] 
            for key, val in self.ModelParams.items() 
            if self.IsLocalParam(key)}
        ParamsDict[ParamName] += self.GradStep[Type]
        FirstMat  = self.GetFeMatrix(Only)
        SecondMat = self.GetLocMcreArgs(ParamsDict, 
            Only=Only)[Only].GetValues(self.StorageType)
        MultFactor = 1. / self.GradStep[Type]
        return (SecondMat - FirstMat) * MultFactor

    def UpdateFeOperators(self, Params):
        """Updates FE operators"""
        Updated = self.LocalParamsChanged(Params)
        if Updated:
            ParamsDict = {key : Params[val[2]] 
                for key, val in self.ModelParams.items() 
                if self.IsLocalParam(key)}
            LocMcreArgs = self.GetLocMcreArgs(ParamsDict)
            self.GeneEfrt = LocMcreArgs["GeneEfforts"]
            self.StiffMat = LocMcreArgs["Stiffness"]
            self.DampMat = LocMcreArgs["Damping"]
            self.MassMat = LocMcreArgs["Mass"]
        self.CurrParams[:] = Params[:]
        if self.UsingBFGS: self.LocalizeDynMcre(Updated)

    def EvaluateCreFunc(self, x):
        """Evaluate the constitutive relation error"""
        CurrParams = self.GetCurrentParams()
        NextParams = np.array(x, dtype=float)
        if not np.all(CurrParams == NextParams):            
            self.UpdateFeOperators(NextParams)
        ErrVal = .0j
        for FreqIdx in range(self.FreqsNbr):
            Scale = self.FreqsScale[FreqIdx]
            ErrVal += Scale * self.CreFunc(FreqIdx)
        return ErrVal.real if not self.IsParallel() else \
            MPI.COMM_WORLD.allreduce(ErrVal.real, op=MPI.SUM)

    def EvaluateCreGrad(self, x):
        """Evaluate the constitutive relation error gradient"""
        CurrParams = self.GetCurrentParams()
        NextParams = np.array(x, dtype=float)
        if not np.all(CurrParams == NextParams):
            self.UpdateFeOperators(NextParams)
        CreGrad = np.zeros(self.ParamsNbr, dtype=float)
        for FreqIdx in range(self.FreqsNbr):
            Scale = self.FreqsScale[FreqIdx]
            CreGrad += Scale * self.GetFreqCreGrad(x, FreqIdx)
        if self.IsParallel(): MPI.COMM_WORLD.Allreduce(
            np.array(CreGrad), CreGrad, op=MPI.SUM)
        return CreGrad if CreGrad.size > 1 else CreGrad[0]

    def GetFreqCreGrad(self, Params, FreqIdx):
        """
        Returns the function that evaluate 
        the constitutive relation error.
        
        """
        CreGrad = np.zeros(self.ParamsNbr, dtype=float)
        for Key, (_, Type, Idx) in self.ModelParams.items(): 
            CreGrad[Idx] = .0 if not self.IsLocalParam(Key) \
                else self.CreGrad[Type](FreqIdx, Params, Key)
        return CreGrad
    
    def optimize(self, method=None, maxiter=None, 
        graderr=1e-10, glberr=None, verbose=False, steps=None):
        """
        The main function of the mCRE-based parameters 
        identification for structural dynamics.
        
        """
        assert(method in ["BFGS", "GRAD-DSCT"])
        assert(isinstance(glberr, float))
        assert(isinstance(verbose, bool))
        assert(IsInteger(maxiter))

        self.OldParamsIdx = self.ParamsValues.shape[0] - 1

        if method == "BFGS": OptimizeFunc = self.OptimizeBFGS
        else: OptimizeFunc = self.OptimizeGRAD

        ItersNbr = OptimizeFunc(maxiter=maxiter, 
            graderr=graderr, glberr=glberr, steps=steps)

        if verbose: self.PrintInfos(ItersNbr)

    def OptimizeGRAD(self, maxiter=None, 
        graderr=1e-10, glberr=None, steps=None):
        """Called if gradient descent method is used."""

        steps = self.GetParParamsSteps(steps)

        if steps is None:
            raise ValueError("The keyword steps is mandatory"
                " for gradient descent minimization.")
        else: assert(isinstance(steps, dict))

        CurrIter, GradCalls, CreCalls = 0, 0, 0
        steps = self.GetParamsSteps(**steps)
        X = self.ParamsValues[-1]

        if steps.size != X.size:
            raise ValueError("{} steps provided for {} "
                "parameter(s)".format(steps.size, X.size))
        
        while self.Continue(CurrIter, maxiter, glberr):
            if CurrIter != 0: self.LocalizeDynMcre(True)
            GradCalls += 1; CurrIter += 1
            CreGrad = self.EvaluateCreGrad(X)
            if graderr >= abs(np.amax(CreGrad)): break
            X = X - CreGrad * steps
            self.AddNewParams(X)
            CreCalls = CreCalls + 1
            self.NewCreVal = self.EvaluateCreFunc(X)

        self.FuncCalls = CreCalls
        self.GradCalls = GradCalls

        return CurrIter

    def Continue(self, CurrIter, MaxIter, GlbErr):
        """Checks the condition to perform next iterations"""
        return CurrIter < MaxIter and self.NewCreVal > GlbErr

    def GetParamsSteps(self, **steps):
        """Returns a vector that contains parameters steps"""
        assert(len(steps) == self.ParamsNbr)
        result = np.zeros(self.ParamsNbr, dtype=float)
        for p, v in self.ModelParams.items():
            if p in steps: result[v[2]] = steps[p]
            else: raise ValueError("missing "
                "parameter step : {}".format(p))
        return result

    def GetParParamsSteps(self, Steps):
        """Returns the parameters steps of a parralel model"""
        if not self.IsParallel(): return Steps
        elif Steps is not None: assert(isinstance(Steps, dict))
        TempList = MPI.COMM_WORLD.allgather(Steps)
        if all([e is None for e in TempList]): return None
        GlbS = reduce(lambda x, y : {**x, **y},
            [e for e in TempList if e is not None])
        if Steps is not None:
            for p, v in Steps.items():
                if GlbS[p] != v: MPI.COMM_WORLD.Abort(1)
        return GlbS

    def OptimizeBFGS(self, maxiter=None, 
        graderr=1e-10, glberr=None, steps=None):
        """Called if BFGS method is used."""
        
        if steps is not None:
            raise ValueError("The keyword steps is only"
                " used for gradient descent minimization.")

        CreFunc = self.EvaluateCreFunc
        CreGrad = self.EvaluateCreGrad
        Save = self.AddNewParams
        
        self.UsingBFGS = True

        X0 = self.ParamsValues[-1]
        
        MinRes = fmin_bfgs(CreFunc, X0, gtol=graderr, 
            disp=False, fprime=CreGrad, maxiter=maxiter, 
            full_output=True, retall=True, callback=Save)
        
        self.NewCreVal = MinRes[1]
        self.FuncCalls = MinRes[4]
        self.GradCalls = MinRes[5]

        return len(MinRes[7]) - 1

    # def OptimizeBFGS(self, maxiter=None, 
    #     graderr=1e-10, glberr=None, steps=None):
    #     """Called if BFGS method is used."""
        
    #     if steps is not None:
    #         raise ValueError("The keyword steps is only"
    #             " used for gradient descent minimization.")

    #     CreFunc = self.EvaluateCreFunc
    #     CreGrad = self.EvaluateCreGrad
    
    #     self.UsingBFGS = True

    #     X0 = self.ParamsValues[-1]

    #     bounds_v = [
    #         (190., 230.),
    #         (0.00, 0.49),
    #         (190., 230.),
    #         (0.00, 0.49),
    #         (190., 230.),
    #         (0.00, 0.49),
    #         (190., 230.),
    #         (0.00, 0.49),
    #         (190., 230.),
    #         (0.00, 0.49),
    #     ]

    #     # bounds_v = [
    #     #     (None, None),
    #     #     (0.00, 0.49),
    #     #     (None, None),
    #     #     (0.00, 0.49),
    #     #     (None, None),
    #     #     (0.00, 0.49),
    #     #     (None, None),
    #     #     (0.00, 0.49),
    #     #     (None, None),
    #     #     (0.00, 0.49),
    #     # ]

    #     # bounds_E = [
    #     #     (190., 230.),
    #     #     (None, None),
    #     #     (190., 230.),
    #     #     (None, None),
    #     #     (190., 230.),
    #     #     (None, None),
    #     #     (190., 230.),
    #     #     (None, None),
    #     #     (190., 230.),
    #     #     (None, None),
    #     # ]
    #     #
    #     # Il faut rajouter la fonction callback : AddNewParams
    #     
    #     MinRes = minimize(CreFunc, X0, method='L-BFGS-B', 
    #         jac=CreGrad, bounds=bounds_v, options={'maxcor' : 100,
    #         'gtol': graderr, 'maxiter': maxiter, 'maxls': 100})

    #     if MPI.COMM_WORLD.Get_rank() == 0:
    #         print(MinRes.message)

    #     NewParamsArray = np.array(MinRes.x)[None,:]
    #     self.ParamsValues = np.concatenate((
    #         self.ParamsValues, NewParamsArray))
        
    #     self.NewCreVal = MinRes.fun
    #     self.FuncCalls = MinRes.nfev
    #     self.GradCalls = MinRes.njev

    #     return MinRes.nit

    def AddNewParams(self, X):
        """Saves the new parameters after each iteration"""
        NewParamsArray = np.array(X)[None,:]
        self.ParamsValues = np.concatenate((
            self.ParamsValues, NewParamsArray))

    def LocalizeDynMcre(self, OpsUpdated=False):
        """Performs the localization step of the mCRE"""
        if OpsUpdated:
            self.LocMcreObj.UpdateFeOperators(
                self.StiffMat, self.MassMat, 
                self.DampMat, self.GeneEfrt)
        if self.useParDDM():
            for FreqIdx in range(self.FreqsNbr):
                self.LocMcreObj.CalcDynMcre(
                    FreqIdx=FreqIdx, CalcErr=False)
        else: self.LocMcreObj.CalcDynMcre(CalcErr=False)
    
    def PrintInfos(self, ItersNbr):
        """Prints some informations after the mCRE minimization"""
        if not self.useParDDM(): print(self.getLocalInfo(ItersNbr))
        else: PrintAllRanks(self.getLocalInfo(ItersNbr), UseHeader=True)

    def getLocalInfo(self, ItersNbr):
        """Prints some informations after the mCRE minimization"""
        MinMethod = self.GetMethod()
        GradEvalType = self.GetGradEvalType()
        ToPrint  = "{:<30} : {}\n".format(
            "Minimisation method", MinMethod)
        ToPrint += "{:<30} : {}\n".format("Gradient "
            "evaluation method", GradEvalType)
        ToPrint += "{:<30} : {}\n".format(
            "Iterations number", ItersNbr)
        ToPrint += "{:<30} : {}\n".format(
            "Initial error", self.OldCreVal)
        ToPrint += "{:<30} : {}\n".format(
            "Finale error", self.NewCreVal)
        ToPrint += "{:<30} : {}\n".format(
            "mCRE evaluations", self.FuncCalls)
        ToPrint += "{:<30} : {}\n".format(
            "Gradient evaluations", self.GradCalls)
        ToPrint += "New model's parameters :\n"
        ToPrint += ("\t{:<15} {:<15} {:<15} {:<15} "
            "{:<15}\n").format("Parameter","Name",
            "Type","Old value","New value")
        ParamsItems = self.ModelParams.items()
        oidx = self.OldParamsIdx
        for Param, (pName, pType, idx) in ParamsItems:
            ToPrint += ("\t{:<15} {:<15} {:<15} "
                "{:<15} {:<15}\n").format(Param, pName, 
                pType, str(self.ParamsValues[oidx][idx])[:15],
                str(self.ParamsValues[-1][idx])[:15])
        return ToPrint
    
    def getParamHistory(self, Parameter):
        """Returns the evolution of a parameter"""
        if Parameter not in self.ModelParams:
            raise ValueError("Unknown parameter"
                " : {}".format(Parameter))
        ParamIdx = self.ModelParams[Parameter][2]
        return np.array(self.ParamsValues[:, ParamIdx])

    def getErrorAndGrad(self, Param, Min, Max, Num):
        """
        Returns the error and the grad for all 
        values of param form max to min
        
        """
        self.UsingBFGS = True
        ParamIdx = self.ModelParams[Param][2]
        Values = np.linspace(Min, Max, num=Num)
        Error = np.zeros(Values.size, dtype=float)
        Grad = np.zeros(Values.size, dtype=float)
        X = np.array(self.CurrParams)
        for Idx, Value in enumerate(Values):
            X[ParamIdx] = Value
            Error[Idx] = self.EvaluateCreFunc(X)
            Grad[Idx] = self.EvaluateCreGrad(X)[ParamIdx]
        return Values, Error, Grad



        

    