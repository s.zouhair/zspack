"""
This module implements the modified constitutive relation error (mcre) gradients of all
formulations (i.e. disp, drkr damp, drkr no damp) with respect to all parameters types
(i.e. stiffness, mass, damping)

"""

from functools import partial
import numpy as np

from ...pkgutils import GetOmega
from . import dynmcre

def GetStiffCreGrad(Mcre):
    """
    Returns the function that evaluate 
    the constitutive relation error.
    
    """
    assert(isinstance(Mcre, dynmcre.DynMcre))
    ErrType = Mcre.GetErrType()
    if ErrType == "Disp": return None
    if not Mcre.DampingExist():
        Func = StiffGradDrkrNoDamp
    else: Func = StiffGradDrkrDamp
    return partial(Func, Mcre=Mcre)

def StiffGradDrkrNoDamp(FreqIdx, Params, ParamName, Mcre=None):
    """
    Returns the gradient with respect to 
    stiffness parameter of the mCRE drucker 
    formulation without damping
    
    """
    UV = Mcre.GetField("U - V", FreqIdx)
    V = Mcre.GetField("V", FreqIdx)
    L = Mcre.GetField("L", FreqIdx)
    Kp = Mcre.GetFeMatDer(Params, ParamName)
    x = np.dot(UV, Kp.dot(UV))
    y = np.dot(L, Kp.dot(V))
    alpha = Mcre.GetGamma() * .5
    result = alpha * x + y
    return result

def StiffGradDrkrDamp(FreqIdx, Params, ParamName, Mcre=None):
    """
    Returns the gradient with respect to 
    stiffness parameter of the mCRE drucker 
    formulation with damping
    
    """
    raise NotImplementedError

def GetMassCreGrad(Mcre):
    """
    Returns the function that evaluate 
    the constitutive relation error.
    
    """
    assert(isinstance(Mcre, dynmcre.DynMcre))
    ErrType = Mcre.GetErrType()
    if ErrType == "Disp": return None
    if not Mcre.DampingExist():
        Func = MassGradDrkrNoDamp
    else: Func = MassGradDrkrDamp
    return partial(Func, Mcre=Mcre)

def MassGradDrkrNoDamp(FreqIdx, Params, ParamName, Mcre=None):
    """
    Returns the gradient with respect to 
    mass parameter of the mCRE drucker 
    formulation without damping
    
    """
    UV = Mcre.GetField("U - V", FreqIdx)
    W = Mcre.GetField("W", FreqIdx)
    L = Mcre.GetField("L", FreqIdx)
    Mp = Mcre.GetFeMatDer(Params, ParamName)
    x = np.dot(UV, Mp.dot(UV))
    y = np.dot(L, Mp.dot(W))
    w = GetOmega(Mcre.GetFreq(FreqIdx))
    g = Mcre.GetGamma()
    alpha = .5 * pow(w * g, 2) / (1 - g)
    beta = - pow(w, 2)
    result = alpha * x + beta * y
    return result

def MassGradDrkrDamp(FreqIdx, Params, ParamName, Mcre=None):
    """
    Returns the gradient with respect to 
    mass parameter of the mCRE drucker 
    formulation with damping
    
    """
    raise NotImplementedError

def GetDampCreGrad(Mcre):
    """
    Returns the function that evaluate 
    the constitutive relation error.
    
    """
    assert(isinstance(Mcre, dynmcre.DynMcre))
    ErrType = Mcre.GetErrType()
    if ErrType == "Disp":
        Func = DampGradDisp
    elif Mcre.DampingExist():
        Func = DampGradDrkrDamp
    else: return None
    return partial(Func, Mcre=Mcre)

def DampGradDrkrDamp(FreqIdx, Params, ParamName, Mcre=None):
    """
    Returns the gradient with respect to 
    damping parameter of the mCRE drucker 
    formulation with damping
    
    """
    raise NotImplementedError

def DampGradDisp(FreqIdx, Params, ParamName, Mcre=None):
    """
    Returns the gradient with respect to 
    damping parameter of the mCRE  
    dissipation formulation.
    
    """
    raise NotImplementedError

__all__ = [
    "GetStiffCreGrad",
    "GetMassCreGrad",
    "GetDampCreGrad"
]