import numpy as np
import random

from zs_pack import femsol, mshgen

# CreateMesh
# CreateModel
# Assemble
# CreateMaterial
# CreateBehaviour
# ImposeDof
# ImposeEfforts
# SolveLE

num   = (5,4,5)
start = (0,0,0)
stop  = (8,9,4)
split = (2,1,2)

maillage, elmnt = mshgen.CreateCuboid(start, stop, num, split, 
                                            grp1=("F","Dx+"), grp2=("F","Dy-"), 
                                            grp3=("N","Dz+"), grp4=("F","Dy+"),
                                            grp5=("N","Dx-"), grp6=("N","Dy-"))

print(elmnt)

print("")

print("Nodes group :")
print(maillage.NodesGroups)

print("")

print("Elmnts group :")
for grpName, grpList in maillage.ElmntsGroups.items():
    print("    %s : " % grpName, grpList)

print("")

print("Faces group :")
print(maillage.FacesGroups)

print("")

for FaceIdx, FaceItems in enumerate(maillage.Faces):
    print("    Face %s : E%d, LN%d" % (FaceIdx, *FaceItems))

print("")
print("")

print("__ElmntsType__ : ", maillage.__ElmntsType__)
print("__ElmntsTypeGroups__ : ", maillage.__ElmntsTypeGroups__)

print("")

print("__FacesType__ : ", maillage.__FacesType__)
print("__FacesTypeGroups__ : ", maillage.__FacesTypeGroups__)

print("")

PartGrpsLst = list(maillage.ElmntsGroups.keys())
print("Is partition : ", maillage.CheckPartition(*PartGrpsLst))
print("Is partition : ", maillage.CheckPartition(*PartGrpsLst[1:]))

print("")

ElmntIdx = 0
print("Elmnt %d coords : " % ElmntIdx, maillage.GetElmntCoord(ElmntIdx, Transp=True))

print("")
print("")

# grp3=("N","Dz+")
# grp5=("N","Dx-")
# grp6=("N","Dy-")

grpname1 = ["grp3", "grp5"]
grpname2 = ["grp5", "grp6"]

newName1 = maillage.GetGroupsIntersection(*grpname1, Store=True)
newName2 = maillage.GetGroupsIntersection(*grpname2, Store=True)

print("newName1 = ", newName1)
print("newName2 = ", newName2)

grpname = [newName1, newName2]

# newName = maillage.GetGroupsIntersection(*grpname, Store=True, Type=None)
newName   = maillage.GetGroupsUnion(*grpname, Store=True)

print("newName = ", newName)

print("The intersection of %s is called %s: " % (grpname, newName), 
            maillage.GetGroupItems(newName, GroupType=None))

print("")
print("")

modele = femsol.CreateModel(maillage, "3D")
print("The model elements type is : ", modele.GetModelElmntsTypes())
print("The Dof number 2 is : ", modele.GetModelDofName(2))
print("The number of Dofs is : ", modele.GetModelDofNbr())

print("")
print("")

material_1 = femsol.CreateMaterial(type="isotrope", young= 5.5, poisson=0.7, gho=2.33)
material_2 = femsol.CreateMaterial(type="isotrope", young= 5.5, poisson=0.7, gho=4.33)
material_3 = femsol.CreateMaterial(type="isotrope", young= 3.5, poisson=0.1, gho=2.33)
material_4 = femsol.CreateMaterial(type="isotrope", young= 3.5, poisson=0.1, gho=4.33)

behavior   = femsol.CreateBehaviour(maillage, GRP_x0_y0_z0=material_1, 
                                              GRP_x0_y0_z1=material_2,
                                              GRP_x1_y0_z0=material_3,
                                              GRP_x1_y0_z1=material_4)

stfgrp1 = ['GRP_x1_y0_z0', 'GRP_x1_y0_z1']
# [ 2  3  6  7 10 11 14 15 18 19 22 23 26 27 30 31 34 35 38 39 42 43 46 47]
stfgrp2 = ['GRP_x0_y0_z0', 'GRP_x0_y0_z1']
# [ 0  1  4  5  8  9 12 13 16 17 20 21 24 25 28 29 32 33 36 37 40 41 44 45]

masgrp1 = ['GRP_x0_y0_z1', 'GRP_x1_y0_z1']
# [24 25 28 29 32 33 36 37 40 41 44 45 26 27 30 31 34 35 38 39 42 43 46 47]
masgrp2 = ['GRP_x0_y0_z0', 'GRP_x1_y0_z0']
# [ 0  1  4  5  8  9 12 13 16 17 20 21 2  3  6  7 10 11 14 15 18 19 22 23]

print("Stiffness :")
stifgrpl, stifmat = behavior.GetStiffnessGroups(MergeAndStore=True)
for (grps,), mats in zip(stifgrpl, stifmat):
    print(grps, " : ", mats.GetStiffnessParam(), " > ", maillage.GetGroupItems(grps))

print("stfgrp1 : ", maillage.GetGroupsUnion(*stfgrp1, Store=False))
print("stfgrp1 : ", maillage.GetGroupsUnion(*stfgrp2, Store=False))

print("")

print("Mass :")
massgrpl, massmat = behavior.GetMassGroups(MergeAndStore=True)
for (grpm,), matm in zip(massgrpl, massmat):
    print(grpm, " : ", matm.GetMassParam()," > ", maillage.GetGroupItems(grpm))

print("masgrp1 : ", maillage.GetGroupsUnion(*masgrp1, Store=False))
print("masgrp2 : ", maillage.GetGroupsUnion(*masgrp2, Store=False))

print("")
print("")

impdof = femsol.ImposeDof( 
                           modele, 
                           grp5=(
                                 ("DZ", 1.5),
                                 ("DY", 3.2),
                                 ("DX", 54.),
                                ),
                           Node4=(
                                 ("DZ", 0.5),
                                 ("DY", 32.2),
#                                 ("DX", 54.),
                                )
                         )

print("The Free Dof Nbr is : ", impdof.DofNumbering.FreeDofNbr)
print("The Impo Dof Nbr is : ", impdof.DofNumbering.ImposedDofNbr)

print("")
print("")

DofIdxs = np.array([ 4, 0,  5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80,
       85, 90, 95])

print(impdof.DofNumbering.DofOrdering[DofIdxs])
print(impdof.ImposedValues)
print(np.sum(impdof.DofNumbering.DofOrdering[:,0] < 0))

print("")
print("")

impeff = femsol.ImposeEfforts(modele)
impeff.AddImpFacesEfforts(
                          grp1=(
                                ("Ty", 3.2),
                                ("Tz", 5.1),
                                ("Tx", 7.4)
                               )
                         )

print("Efforts are imposed : ", impeff.GetFacesEfforts())





stifmatrx_C, genefrts_C = femsol.Assemble(
                                           modele,
                                           impdof,
                                           behavior,
                                           impeff,
                                           StiffnessMatrix=True,
                                           StiffnessIntegType="Complete",
                                           GeneralizedEffort=True,
                                           GeneEfrtIntegType="Complete",
                                         )

print("The det of stifmatrx_C is : ", np.linalg.det(stifmatrx_C.GetDenseVal()))
print("The symetrie of stifmatrx_C is : ", np.allclose(stifmatrx_C.GetDenseVal(), stifmatrx_C.GetDenseVal().T))

stifmatrx_R, genefrts_R = femsol.Assemble(
                                           modele,
                                           impdof,
                                           behavior,
                                           impeff,
                                           StiffnessMatrix=True,
                                           StiffnessIntegType="Reduced",
                                           GeneralizedEffort=True,
                                           GeneEfrtIntegType="Reduced",
                                         )

print("The det of stifmatrx_R is : ", np.linalg.det(stifmatrx_R.GetDenseVal()))
print("The symetrie of stifmatrx_R is : ", np.allclose(stifmatrx_R.GetDenseVal(), stifmatrx_R.GetDenseVal().T))

NormEfrts = np.linalg.norm(genefrts_C.GetValues()) - np.linalg.norm(genefrts_R.GetValues())
val = stifmatrx_R.GetDenseVal()
# for elmnt in np.ndindex(val.shape):
#         print(val[elmnt])

a = np.sum(val != 0)
b = val.size

print("Non null elemnts : %d (%d %%)" % (a, (a / b) * 100))
print("Are stifmatrx_C and stifmatrx_R close : ", np.allclose(stifmatrx_C.GetDenseVal(), stifmatrx_R.GetDenseVal()))
print("Diff between Reduced and Complete GenEfrt is : ", NormEfrts)

dspfield_C = femsol.SolveLE(
                           stifmatrx_C,
                           genefrts_C,
                           CheckFinite=False
                         )

dspfield_R = femsol.SolveLE(
                           stifmatrx_R,
                           genefrts_R,
                           CheckFinite=False
                         )

print("dspfield_C .ALLCLOSE. dspfield_R : ", np.allclose(dspfield_C.GetValues(), dspfield_R.GetValues()))













