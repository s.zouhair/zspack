from zspack import femsol, mshgen
from mpi4py import MPI

#        sd 1              sd 2              sd 3              sd 4
# |-----------------|-----------------|-----------------|-----------------|
# |--------|-----------------|-----------------|-----------------|--------|
#     M1            M2                M3                M4           M5

femsol.Initialize(InitPetsc=True)

assert(MPI.COMM_WORLD.Get_size() == 4)
Repeat=(4,1,1)
LocNum=(5,5,5)
Start=(0,0,0)
Stop=(540,108,216)
GlbNum = (
    (LocNum[0] - 1) * Repeat[0] + 1,
    (LocNum[1] - 1) * Repeat[1] + 1,
    (LocNum[2] - 1) * Repeat[2] + 1
)

# ---------------------------------------------------------------------------
# --------------------- Simulated Experimental Measures ---------------------
# ---------------------------------------------------------------------------

GlbMesh, _ = mshgen.CreateCuboid(
                        start=Start,
                        stop=Stop,
                        num=GlbNum,
                        split=(8,1,1),
                        grp1=("F","Dx+"),
                        grp5=("N","Dx-")
                    )

GlbModel = femsol.CreateModel(GlbMesh, "3D")

ExcMat1 = femsol.CreateMaterial(
                        type="isotrope",
                        young= 220, # GPA
                        poisson=0.2,
                        gho=7.33e-6,
                        alpha=1.,
                        beta=1.
                    )

ExcMat2 = femsol.CreateMaterial(
                        type="isotrope",
                        young= 215, # GPA
                        poisson=0.25,
                        gho=7.33e-6,
                        alpha=1.,
                        beta=1.
                    )

ExcMat3 = femsol.CreateMaterial(
                        type="isotrope",
                        young= 210, # GPA
                        poisson=0.3,
                        gho=7.33e-6,
                        alpha=1.,
                        beta=1.
                    )

ExcMat4 = femsol.CreateMaterial(
                        type="isotrope",
                        young= 205, # GPA
                        poisson=0.35,
                        gho=7.33e-6,
                        alpha=1.,
                        beta=1.
                    )

ExcMat5 = femsol.CreateMaterial(
                        type="isotrope",
                        young= 200, # GPA
                        poisson=0.4,
                        gho=7.33e-6,
                        alpha=1.,
                        beta=1.
                    )

ExcBehavior = femsol.CreateBehavior(
                            GlbMesh,
                            GRP_x0_y0_z0=ExcMat1,
                            GRP_x1_y0_z0=ExcMat2,
                            GRP_x2_y0_z0=ExcMat2,
                            GRP_x3_y0_z0=ExcMat3,
                            GRP_x4_y0_z0=ExcMat3,
                            GRP_x5_y0_z0=ExcMat4,
                            GRP_x6_y0_z0=ExcMat4,
                            GRP_x7_y0_z0=ExcMat5
                        )

GlbImpDof = femsol.CreateBC( 
                    GlbModel,
                    grp5=(
                            ("DZ", 0.),
                            ("DY", 0.),
                            ("DX", 0.)
                        )
                )

GlbImpEff = femsol.CreateLoad(GlbModel)

GlbImpEff.ApplyFaceLoad(
                grp1=(
                        ("Tx", 5.4e-1), # GPA
                        ("Ty", 7.4e-3),
                        ("Tz", 6.3e-3)
                    )
            )

ExcStiff, ExcMass, ExcGeff = femsol.Assemble(
                                        GlbModel,
                                        ExcBehavior,
                                        ImposedDof=GlbImpDof,
                                        ImposedEfforts=GlbImpEff,
                                        StiffnessMatrix=True,
                                        StiffnessIntegType="Complete",
                                        MassMatrix=True,
                                        MassIntegType="Complete",
                                        GeneralizedEffort=True,
                                        GeneEfrtIntegType="Complete",
                                        StorageType="Sparse",
                                        StoreElementry=False
                                    )

LdynRes = femsol.SolveLDYN(
                    StiffMatrix=ExcStiff,
                    MassMatrix=ExcMass,
                    GeneEfforts=ExcGeff,
                    Freq=1.5,
                    StorageType="Sparse"
                )

# ---------------------------------------------------------------------------
# ----------------------------- Parallel Model ------------------------------
# ---------------------------------------------------------------------------

CommSize = MPI.COMM_WORLD.Get_size()
CommRank = MPI.COMM_WORLD.Get_rank()

sd_coord = {
    "x" : CommRank % Repeat[0],
    "y" : (CommRank % (Repeat[0] * Repeat[1])) // Repeat[0],
    "z" : CommRank // (Repeat[0] * Repeat[1])
}

measures_indices = [
    (1, 1),
    (1, 3),
    (2, 2),
    (3, 1),
    (3, 3)
]

SdShift = {
    'x' : sd_coord['x'] * (LocNum[0] - 1),
    'y' : sd_coord['y'] * (LocNum[1] - 1),
    'z' : sd_coord['z'] * (LocNum[2] - 1)
}

def isAtRight():
    return sd_coord['y'] == 0

def isAtLeft():
    return sd_coord['y'] == Repeat[1] - 1

def isAtBottom():
    return sd_coord['z'] == 0

def isAtTop():
    return sd_coord['z'] == Repeat[2] - 1

def haveMeasures():
    return isAtRight() or isAtLeft() \
        or isAtBottom() or isAtTop()

def getNodeIdx(nc, mc):
    return nc[0] + nc[1] * mc[0] \
        + nc[2] * mc[0] * mc[1]

# ---------------------------------------------------------------------------
# ----------------------------- Parallel Model ------------------------------
# ---------------------------------------------------------------------------

LocMesh = mshgen.CreateParallelCuboid(
                                start=Start,
                                stop=Stop,
                                num=LocNum,
                                split=(2,1,1),
                                repeat=Repeat,
                                grp1=("F","Dx+"),
                                grp5=("N","Dx-")
                            )

LocMesh, ElmntsGrps = LocMesh

LocModel = femsol.CreateModel(LocMesh, "3D")

LocImpDof = femsol.CreateBC( 
                        LocModel, 
                        grp5=(
                                ("DZ", 0.),
                                ("DY", 0.),
                                ("DX", 0.)
                            )
                        )

LocImpEff = femsol.CreateLoad(LocModel)

LocImpEff.ApplyFaceLoad(
                grp1=(
                        ("Tx", 5.4e-1), # GPA
                        ("Ty", 7.4e-3),
                        ("Tz", 6.3e-3)
                    )
            )

TempBehavior   = femsol.CreateBehavior(
                                LocMesh,
                                All=ExcMat1
                            )

LocGeff = femsol.Assemble(
                    LocModel,
                    TempBehavior,
                    ImposedDof=LocImpDof,
                    ImposedEfforts=LocImpEff,
                    GeneralizedEffort=True,
                    GeneEfrtIntegType="Complete"
                )

LocDofsNumbering = LocGeff.GetDofNumbering()
del LocGeff, TempBehavior

# ---------------------------------------------------------------------------
# --------------------------- The Model Function ----------------------------
# ---------------------------------------------------------------------------

def MyModel(Young1=None, Poisson1=None, Young2=None, 
    Poisson2=None, Young3=None, Poisson3=None, Young4=None, 
    Poisson4=None, Young5=None, Poisson5=None, Only=None):

    if sd_coord["x"] == 0:
        FirstYoung, FirstPoisson = Young1, Poisson1
        SecondYoung, SecondPoisson = Young2, Poisson2
    elif sd_coord["x"] == 1:
        FirstYoung, FirstPoisson = Young2, Poisson2
        SecondYoung, SecondPoisson = Young3, Poisson3
    elif sd_coord["x"] == 2:
        FirstYoung, FirstPoisson = Young3, Poisson3
        SecondYoung, SecondPoisson = Young4, Poisson4
    elif sd_coord["x"] == 3:
        FirstYoung, FirstPoisson = Young4, Poisson4
        SecondYoung, SecondPoisson = Young5, Poisson5
    else: assert(False)

    WrgMat1 = femsol.CreateMaterial(
                            type="isotrope",
                            young=FirstYoung, # GPA
                            poisson=FirstPoisson,
                            gho=7.33e-6,
                            alpha=1.,
                            beta=1.
                        )

    WrgMat2 = femsol.CreateMaterial(
                            type="isotrope",
                            young=SecondYoung, # GPA
                            poisson=SecondPoisson,
                            gho=7.33e-6,
                            alpha=1.,
                            beta=1.
                        )

    LocBehavior   = femsol.CreateBehavior(
                                    LocMesh,
                                    GRP_x0_y0_z0=WrgMat1,
                                    GRP_x1_y0_z0=WrgMat2,
                                )

    sMat = False if Only == "Mass" else True
    mMat = False if Only == "Stiffness" else True
    gEff = False if Only is not None else True
    impEff = LocImpEff if gEff else None

    result = femsol.Assemble(
                        LocModel,
                        LocBehavior,
                        ImposedDof=LocImpDof,
                        ImposedEfforts=impEff,
                        StiffnessMatrix=sMat,
                        StiffnessIntegType="Complete",
                        MassMatrix=mMat,
                        MassIntegType='Complete',
                        GeneralizedEffort=gEff,
                        GeneEfrtIntegType="Complete",
                        StorageType="Sparse",
                        StoreElementry=True
                    )

    return result

# ---------------------------------------------------------------------------
# -------------------- Extracting Experimental Measures ---------------------
# ---------------------------------------------------------------------------

if haveMeasures():
    GlbMeasuredDofs = {}
    LocMeasuredDofs = {}

    if isAtRight():
        for xc, yc in measures_indices:
            LocNodeCoord = (xc, 0, yc)
            LocNodeName = "Node{}".format(getNodeIdx(LocNodeCoord, LocNum))
            LocMeasuredDofs[LocNodeName] = ('DX', 'DY', 'DZ')

            GlbNodeCoord = (xc + SdShift['x'], 0, yc + SdShift['z'])
            GlbNodeName = "Node{}".format(getNodeIdx(GlbNodeCoord, GlbNum))
            GlbMeasuredDofs[GlbNodeName] = ('DX', 'DY', 'DZ')

    if isAtLeft():
        for xc, yc in measures_indices:
            LocNodeCoord = (xc, LocNum[1] - 1, yc)
            LocNodeName = "Node{}".format(getNodeIdx(LocNodeCoord, LocNum))
            LocMeasuredDofs[LocNodeName] = ('DX', 'DY', 'DZ')

            GlbNodeCoord = (xc + SdShift['x'], GlbNum[1] - 1, yc + SdShift['z'])
            GlbNodeName = "Node{}".format(getNodeIdx(GlbNodeCoord, GlbNum))
            GlbMeasuredDofs[GlbNodeName] = ('DX', 'DY', 'DZ')

    if isAtBottom():
        for xc, yc in measures_indices:
            LocNodeCoord = (xc, yc, 0)
            LocNodeName = "Node{}".format(getNodeIdx(LocNodeCoord, LocNum))
            LocMeasuredDofs[LocNodeName] = ('DX', 'DY', 'DZ')

            GlbNodeCoord = (xc + SdShift['x'], yc + SdShift['y'], 0)
            GlbNodeName = "Node{}".format(getNodeIdx(GlbNodeCoord, GlbNum))
            GlbMeasuredDofs[GlbNodeName] = ('DX', 'DY', 'DZ')

    if isAtTop():
        for xc, yc in measures_indices:
            LocNodeCoord = (xc, yc, LocNum[2] - 1)
            LocNodeName = "Node{}".format(getNodeIdx(LocNodeCoord, LocNum))
            LocMeasuredDofs[LocNodeName] = ('DX', 'DY', 'DZ')

            GlbNodeCoord = (xc + SdShift['x'], yc + SdShift['y'], GlbNum[2] - 1)
            GlbNodeName = "Node{}".format(getNodeIdx(GlbNodeCoord, GlbNum))
            GlbMeasuredDofs[GlbNodeName] = ('DX', 'DY', 'DZ')
    
    GlbMeasNumbering = LdynRes.GetDofsNumbring(**GlbMeasuredDofs)
    LocMeasNumbering = LocDofsNumbering.GetDofsNumbring(**LocMeasuredDofs)
    MeasuresValues = LdynRes.GetDofs(GlbMeasNumbering)[None,:]

else: LocMeasNumbering, MeasuresValues = None, None

# ---------------------------------------------------------------------------
# ------------------------ Creating Dual Interface --------------------------
# ---------------------------------------------------------------------------

IntrfcMap, GlbIntrfcNodesNbr = mshgen.GetParallelInterfaceMapping(
                                                            num=LocNum,
                                                            repeat=Repeat,
                                                            Type="Dual"
                                                        )

LocIntrfcNodes, GlbIntrfcNodes = IntrfcMap

InterfaceNumbering = femsol.CreateInterfaceNumbering(
                                                GlbIntrfcNodesNbr, 
                                                LocIntrfcNodes, 
                                                GlbIntrfcNodes, 
                                                LocDofsNumbering,
                                                BlocksNbr=2,
                                                Type="Dual"
                                            )

# ---------------------------------------------------------------------------
# -------------------------- Creating mCRE Object ---------------------------
# ---------------------------------------------------------------------------


x = sd_coord["x"]

MyParams = {
    "Young1"   : ("young"  , 216.) if x in    [0] else None,    # 220.
    "Poisson1" : ("poisson", 0.17) if x in    [0] else None,    # 0.20
    "Young2"   : ("young"  , 211.) if x in [0, 1] else None,    # 215.
    "Poisson2" : ("poisson", 0.29) if x in [0, 1] else None,    # 0.25
    "Young3"   : ("young"  , 214.) if x in [1, 2] else None,    # 210.
    "Poisson3" : ("poisson", 0.22) if x in [1, 2] else None,    # 0.30
    "Young4"   : ("young"  , 197.) if x in [2, 3] else None,    # 205.
    "Poisson4" : ("poisson", 0.41) if x in [2, 3] else None,    # 0.35
    "Young5"   : ("young"  , 209.) if x in    [3] else None,    # 200.
    "Poisson5" : ("poisson", 0.19) if x in    [3] else None     # 0.40
}

MyRealParams = {
    "Young1"   : 220.,
    "Poisson1" : 0.20,
    "Young2"   : 215.,
    "Poisson2" : 0.25,
    "Young3"   : 210.,
    "Poisson3" : 0.30,
    "Young4"   : 205.,
    "Poisson4" : 0.35,
    "Young5"   : 200.,
    "Poisson5" : 0.40
}

McreObject = femsol.CreateDynMcre(
            MyModel, 
            [1.5], 
            LocMeasNumbering, 
            MeasuresValues,
            InterfaceNumbering=InterfaceNumbering,
            ApproxGrad=True,
            **MyParams
        )

McreObject.optimize(
    method="BFGS",
    maxiter=60,
    glberr=1.e-5,
    graderr=1e-20,
    verbose=True
)

# YoungStep = 0.5
# PoissonStep = 5e-5

# MySteps = {
#     "Young1"   : YoungStep,
#     "Poisson1" : PoissonStep,
#     "Young2"   : YoungStep,
#     "Poisson2" : PoissonStep,
#     "Young3"   : YoungStep,
#     "Poisson3" : PoissonStep,
#     "Young4"   : YoungStep,
#     "Poisson4" : PoissonStep,
#     "Young5"   : YoungStep,
#     "Poisson5" : PoissonStep
# }

# McreObject.optimize(
#     method="GRAD-DSCT",
#     maxiter=55,
#     glberr=1.e-5,
#     steps=MySteps,
#     graderr=1e-20,
#     verbose=True
# )



