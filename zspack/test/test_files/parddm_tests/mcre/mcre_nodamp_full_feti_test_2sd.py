from zspack import femsol, mshgen
from mpi4py import MPI
import numpy as np

#        sd 1              sd 2
# |-----------------|-----------------|
# |-----------------|-----------------|
#          M1                M2

femsol.Initialize(InitPetsc=True)

assert(MPI.COMM_WORLD.Get_size() == 2)
Repeat=(2,1,1)
LocNum=(4,4,4)
Start=(0,0,0)
Stop=(540,108,216)
GlbNum = (
    (LocNum[0] - 1) * Repeat[0] + 1,
    (LocNum[1] - 1) * Repeat[1] + 1,
    (LocNum[2] - 1) * Repeat[2] + 1
)

# ---------------------------------------------------------------------------
# --------------------- Simulated Experimental Measures ---------------------
# ---------------------------------------------------------------------------

GlbMesh = mshgen.CreateCuboid(
                        start=Start,
                        stop=Stop,
                        num=GlbNum,
                        split=None,
                        grp1=("F","Dx+"),
                        grp5=("N","Dx-")
                    )

GlbModel = femsol.CreateModel(GlbMesh, "3D")

ExcMat = femsol.CreateMaterial(
                        type="isotrope",
                        young= 210, # GPA
                        poisson=0.3,
                        gho=7.33e-6,
                        alpha=1.,
                        beta=1.
                    )

ExcBehavior = femsol.CreateBehavior(
                            GlbMesh,
                            All=ExcMat
                        )

GlbImpDof = femsol.CreateBC( 
                    GlbModel,
                    grp5=(
                            ("DZ", 0.),
                            ("DY", 0.),
                            ("DX", 0.)
                        )
                )

GlbImpEff = femsol.CreateLoad(GlbModel)

GlbImpEff.ApplyFaceLoad(
                grp1=(
                        ("Tx", 5.4e-1), # GPA
                        ("Ty", 7.4e-3),
                        ("Tz", 6.3e-3)
                    )
            )

ExcStiff, ExcMass, ExcGeff = femsol.Assemble(
                                        GlbModel,
                                        ExcBehavior,
                                        ImposedDof=GlbImpDof,
                                        ImposedEfforts=GlbImpEff,
                                        StiffnessMatrix=True,
                                        StiffnessIntegType="Complete",
                                        MassMatrix=True,
                                        MassIntegType="Complete",
                                        GeneralizedEffort=True,
                                        GeneEfrtIntegType="Complete",
                                        StorageType="Sparse",
                                        StoreElementry=False
                                    )

LdynRes = femsol.SolveLDYN(
                    StiffMatrix=ExcStiff,
                    MassMatrix=ExcMass,
                    GeneEfforts=ExcGeff,
                    Freq=1.5,
                    StorageType="Sparse"
                )

# ---------------------------------------------------------------------------
# ----------------------------- Parallel Model ------------------------------
# ---------------------------------------------------------------------------

CommSize = MPI.COMM_WORLD.Get_size()
CommRank = MPI.COMM_WORLD.Get_rank()

sd_coord = {
    "x" : CommRank % Repeat[0],
    "y" : (CommRank % (Repeat[0] * Repeat[1])) // Repeat[0],
    "z" : CommRank // (Repeat[0] * Repeat[1])
}

measures_indices = [
    (1, 1),
    (1, 2),
    (2, 1),
    (2, 2)
]

sd_selector = {
    'x' : 1,
    'y' : None,
    'z' : None
}

sd_splitter = {
    'x' : 3,
    'y' : 1,
    'z' : 1
}

error_selector = {
    'x' : 1,
    'y' : None,
    'z' : None
}

SdShift = {
    'x' : sd_coord['x'] * (LocNum[0] - 1),
    'y' : sd_coord['y'] * (LocNum[1] - 1),
    'z' : sd_coord['z'] * (LocNum[2] - 1)
}

def sdHaveError():
    cond_x = True if sd_selector['x'] is None \
        else sd_coord['x'] == sd_selector['x']
    cond_y = True if sd_selector['y'] is None \
        else sd_coord['y'] == sd_selector['y']
    cond_z = True if sd_selector['z'] is None \
        else sd_coord['z'] == sd_selector['z']
    return cond_x and cond_y and cond_z

def getSdSplit():
    return None if not sdHaveError() \
        else tuple(sd_splitter.values())

def grpHasWrgMat(x, y, z):
    cond_x = True if error_selector['x'] \
        is None else x == error_selector['x']
    cond_y = True if error_selector['y'] \
        is None else y == error_selector['y']
    cond_z = True if error_selector['z'] \
        is None else z == error_selector['z']
    return cond_x and cond_y and cond_z

def isAtRight():
    return sd_coord['y'] == 0

def isAtLeft():
    return sd_coord['y'] == Repeat[1] - 1

def isAtBottom():
    return sd_coord['z'] == 0

def isAtTop():
    return sd_coord['z'] == Repeat[2] - 1

def haveMeasures():
    return isAtRight() or isAtLeft() \
        or isAtBottom() or isAtTop()

def getNodeIdx(nc, mc):
    return nc[0] + nc[1] * mc[0] \
        + nc[2] * mc[0] * mc[1]

# ---------------------------------------------------------------------------
# ----------------------------- Parallel Model ------------------------------
# ---------------------------------------------------------------------------

LocMesh = mshgen.CreateParallelCuboid(
                                start=Start,
                                stop=Stop,
                                num=LocNum,
                                split=getSdSplit(),
                                repeat=Repeat,
                                grp1=("F","Dx+"),
                                grp5=("N","Dx-")
                            )

if sdHaveError(): LocMesh, ElmntsGrps = LocMesh

LocModel = femsol.CreateModel(LocMesh, "3D")

LocImpDof = femsol.CreateBC( 
                        LocModel, 
                        grp5=(
                                ("DZ", 0.),
                                ("DY", 0.),
                                ("DX", 0.)
                            )
                        )

LocImpEff = femsol.CreateLoad(LocModel)

LocImpEff.ApplyFaceLoad(
                grp1=(
                        ("Tx", 5.4e-1), # GPA
                        ("Ty", 7.4e-3),
                        ("Tz", 6.3e-3)
                    )
            )

TempBehavior   = femsol.CreateBehavior(
                                LocMesh,
                                All=ExcMat
                            )

LocGeff = femsol.Assemble(
                    LocModel,
                    TempBehavior,
                    ImposedDof=LocImpDof,
                    ImposedEfforts=LocImpEff,
                    GeneralizedEffort=True,
                    GeneEfrtIntegType="Complete"
                )

LocDofsNumbering = LocGeff.GetDofNumbering()
del LocGeff, TempBehavior

# ---------------------------------------------------------------------------
# --------------------------- The Model Function ----------------------------
# ---------------------------------------------------------------------------

def MyModel(MyYoung=None, Only=None):

    if sdHaveError():
        TempMat = femsol.CreateMaterial(
                            type="isotrope",
                            young=MyYoung, # GPA
                            poisson=0.3,
                            gho=7.33e-6,
                            alpha=1.,
                            beta=1.
                        )
    else: TempMat = ExcMat

    LocBehavior   = femsol.CreateBehavior(
                                    LocMesh,
                                    All=TempMat
                                )

    sMat = False if Only == "Mass" else True
    mMat = False if Only == "Stiffness" else True
    gEff = False if Only is not None else True
    impEff = LocImpEff if gEff else None

    result = femsol.Assemble(
                        LocModel,
                        LocBehavior,
                        ImposedDof=LocImpDof,
                        ImposedEfforts=impEff,
                        StiffnessMatrix=sMat,
                        StiffnessIntegType="Complete",
                        MassMatrix=mMat,
                        MassIntegType='Complete',
                        GeneralizedEffort=gEff,
                        GeneEfrtIntegType="Complete",
                        StorageType="Sparse",
                        StoreElementry=True
                    )

    return result

# ---------------------------------------------------------------------------
# -------------------- Extracting Experimental Measures ---------------------
# ---------------------------------------------------------------------------

if haveMeasures():
    GlbMeasuredDofs = {}
    LocMeasuredDofs = {}

    if isAtRight():
        for xc, yc in measures_indices:
            LocNodeCoord = (xc, 0, yc)
            LocNodeName = "Node{}".format(getNodeIdx(LocNodeCoord, LocNum))
            LocMeasuredDofs[LocNodeName] = ('DX', 'DY', 'DZ')

            GlbNodeCoord = (xc + SdShift['x'], 0, yc + SdShift['z'])
            GlbNodeName = "Node{}".format(getNodeIdx(GlbNodeCoord, GlbNum))
            GlbMeasuredDofs[GlbNodeName] = ('DX', 'DY', 'DZ')

    if isAtLeft():
        for xc, yc in measures_indices:
            LocNodeCoord = (xc, LocNum[1] - 1, yc)
            LocNodeName = "Node{}".format(getNodeIdx(LocNodeCoord, LocNum))
            LocMeasuredDofs[LocNodeName] = ('DX', 'DY', 'DZ')

            GlbNodeCoord = (xc + SdShift['x'], GlbNum[1] - 1, yc + SdShift['z'])
            GlbNodeName = "Node{}".format(getNodeIdx(GlbNodeCoord, GlbNum))
            GlbMeasuredDofs[GlbNodeName] = ('DX', 'DY', 'DZ')

    if isAtBottom():
        for xc, yc in measures_indices:
            LocNodeCoord = (xc, yc, 0)
            LocNodeName = "Node{}".format(getNodeIdx(LocNodeCoord, LocNum))
            LocMeasuredDofs[LocNodeName] = ('DX', 'DY', 'DZ')

            GlbNodeCoord = (xc + SdShift['x'], yc + SdShift['y'], 0)
            GlbNodeName = "Node{}".format(getNodeIdx(GlbNodeCoord, GlbNum))
            GlbMeasuredDofs[GlbNodeName] = ('DX', 'DY', 'DZ')

    if isAtTop():
        for xc, yc in measures_indices:
            LocNodeCoord = (xc, yc, LocNum[2] - 1)
            LocNodeName = "Node{}".format(getNodeIdx(LocNodeCoord, LocNum))
            LocMeasuredDofs[LocNodeName] = ('DX', 'DY', 'DZ')

            GlbNodeCoord = (xc + SdShift['x'], yc + SdShift['y'], GlbNum[2] - 1)
            GlbNodeName = "Node{}".format(getNodeIdx(GlbNodeCoord, GlbNum))
            GlbMeasuredDofs[GlbNodeName] = ('DX', 'DY', 'DZ')
    
    GlbMeasNumbering = LdynRes.GetDofsNumbring(**GlbMeasuredDofs)
    LocMeasNumbering = LocDofsNumbering.GetDofsNumbring(**LocMeasuredDofs)
    MeasuresValues = LdynRes.GetDofs(GlbMeasNumbering)[None,:]

else: LocMeasNumbering, MeasuresValues = None, None

# ---------------------------------------------------------------------------
# ------------------------ Creating Dual Interface --------------------------
# ---------------------------------------------------------------------------

IntrfcMap, GlbIntrfcNodesNbr = mshgen.GetParallelInterfaceMapping(
                                                            num=LocNum,
                                                            repeat=Repeat,
                                                            Type="Dual"
                                                        )

LocIntrfcNodes, GlbIntrfcNodes = IntrfcMap

InterfaceNumbering = femsol.CreateInterfaceNumbering(
                                                GlbIntrfcNodesNbr, 
                                                LocIntrfcNodes, 
                                                GlbIntrfcNodes, 
                                                LocDofsNumbering,
                                                BlocksNbr=2,
                                                Type="Dual"
                                            )

# ---------------------------------------------------------------------------
# -------------------------- Creating mCRE Object ---------------------------
# ---------------------------------------------------------------------------

if not sdHaveError():
    MyYugVal, MyStepVal = None, None
else:
    MyYugVal = ("young", 200.)
    MyStepVal = {"MyYoung" : 10}

McreObject = femsol.CreateDynMcre(
            MyModel, 
            [1.5], 
            LocMeasNumbering, 
            MeasuresValues,
            InterfaceNumbering=InterfaceNumbering,
            ApproxGrad=True,
            MyYoung=MyYugVal
        )

McreObject.optimize(
    method="BFGS",
    maxiter=20,
    glberr=1.e-5,
    graderr=1e-20,
    verbose=True
)

# McreObject.optimize(
#     method="GRAD-DSCT",
#     maxiter=20,
#     glberr=1.e-5,
#     steps=MyStepVal,
#     graderr=1e-20,
#     verbose=True
# )