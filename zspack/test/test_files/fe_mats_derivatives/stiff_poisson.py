from zspack import femsol, mshgen, zstest
import numpy as np

dp = 1e-8

maillage = mshgen.CreateCuboid(
                        start=(0,0,0),
                        stop=(800,900,400), # mm
                        num=(7,7,7),
                        split=None,
                        grp1=("F","Dx+"),
                        grp5=("N","Dx-")
                    )

modele = femsol.CreateModel(maillage, "3D")

material_1 = femsol.CreateMaterial(
                        type="isotrope",
                        young=210., # GPA
                        poisson=0.3,
                        gho=2.33,
                        alpha=50,
                        beta=1e-7
                    )

material_2 = femsol.CreateMaterial(
                        type="isotrope",
                        young=210., # GPA
                        poisson=0.3+dp,
                        gho=2.33,
                        alpha=50,
                        beta=1e-7
                    )

behavior_1 = femsol.CreateBehavior(
                        maillage,
                        All=material_1
                    )

behavior_2 = femsol.CreateBehavior(
                        maillage,
                        All=material_2
                    )

impdof = femsol.CreateBC( 
                    modele, 
                    grp5=(
                        ("DZ", 0.),
                        ("DY", 0.),
                        ("DX", 0.)
                        )
                )

StiffnessMat_1 = femsol.Assemble(
                    modele,
                    behavior_1,
                    ImposedDof=impdof,
                    StiffnessMatrix=True,
                    StiffnessIntegType="Complete",
                    StorageType="Sparse",
                    StoreElementry=False
                )

StiffnessMat_2 = femsol.Assemble(
                    modele,
                    behavior_2,
                    ImposedDof=impdof,
                    StiffnessMatrix=True,
                    StiffnessIntegType="Complete",
                    StorageType="Sparse",
                    StoreElementry=False
                )

StiffnessDer = femsol.Assemble(
                    modele,
                    behavior_1,
                    ImposedDof=impdof,
                    StiffnessMatrix=True,
                    DerivIntegType="Complete",
                    StorageType="Sparse",
                    StoreElementry=False,
                    Derivative="poisson"
                )

# StiffnessDer = StiffnessMat_1.GetDerivative("poisson", 
#     IntegType="Complete", StorageType="Sparse")

Mat_1 = StiffnessMat_1.GetValues(StorageType="Dense")
Mat_2 = StiffnessMat_2.GetValues(StorageType="Dense")
Der = StiffnessDer.GetValues(StorageType="Dense")

AbsDiff = (Mat_2 - Mat_1) * (1. / dp) - Der

TestArr = np.ones(AbsDiff.shape, dtype=bool)

for idx in np.ndindex(AbsDiff.shape):
    if -1e-10 < Der[idx] < 1e-10: continue
    else: TestArr[idx] = TestArr[idx] and ((AbsDiff[idx] / Der[idx]) < 1e-5)

print("The max absolute jump is :", np.max(AbsDiff))
print("All relative differences are smaller than 0.001 % :", np.all(TestArr))