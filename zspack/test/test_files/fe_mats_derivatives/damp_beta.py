from zspack import femsol, mshgen, zstest
import numpy as np

maillage = mshgen.CreateCuboid(
                        start=(0,0,0),
                        stop=(800,900,400), # mm
                        num=(7,7,7),
                        split=None,
                        grp1=("F","Dx+"),
                        grp5=("N","Dx-")
                    )

modele = femsol.CreateModel(maillage, "3D")

material = femsol.CreateMaterial(
                        type="isotrope",
                        young=210, # GPA
                        poisson=0.3,
                        gho=2.33,
                        alpha=50,
                        beta=1e-7
                    )

behavior = femsol.CreateBehavior(
                        maillage,
                        All=material
                    )

impdof = femsol.CreateBC( 
                    modele, 
                    grp5=(
                        ("DZ", 0.),
                        ("DY", 0.),
                        ("DX", 0.)
                        )
                )

MassMat = femsol.Assemble(
                    modele,
                    behavior,
                    ImposedDof=impdof,
                    MassMatrix=True,
                    StorageType="Sparse",
                    StoreElementry=False
                )

DampingDer = femsol.Assemble(
                    modele,
                    behavior,
                    ImposedDof=impdof,
                    DampingMatrix=True,
                    StorageType="Sparse",
                    StoreElementry=False,
                    Derivative="beta"
                )

Mat = MassMat.GetValues(StorageType="Dense")
Der = DampingDer.GetValues(StorageType="Dense")

Diff = Mat - Der

print("The max jumping is :", np.max(Diff))