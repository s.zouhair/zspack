from zspack import femsol, mshgen, zstest
import numpy as np

maillage = mshgen.CreateCuboid(
                                start=(0,0,0),
                                stop=(800,900,400), # mm
                                num=(7,7,7),
                                split=None,
                                grp1=("F","Dx+"),
                                grp5=("N","Dx-"),
                              )

modele = femsol.CreateModel(maillage, "3D")

material = femsol.CreateMaterial(
                                  type="isotrope",
                                  young= 210, # GPA
                                  poisson=0.27,
                                  gho=6.87e-6,
                                  alpha=17.514,
                                  beta=0.179
                                )

behavior   = femsol.CreateBehavior(
                                    maillage,
                                    All=material
                                  )

impdof = femsol.CreateBC( 
                          modele, 
                          grp5=(
                                ("DZ", 0.23),
                                # ("DZ", 0.0),
                                ("DY", 0.57),
                                # ("DY", 0.0),
                                ("DX", 0.13)
                                # ("DX", 0.0)
                               )
                        )

impeff = femsol.CreateLoad(modele)

impeff.ApplyFaceLoad(
                      grp1=(
                            ("Tx", 3.4e-4),
                            ("Ty", 9.2e-4),
                            ("Tz", 5.1e-4) # GPA
                           )
                    )

exact_stiff, exact_mass, exact_damp, exact_genefrts = femsol.Assemble(
                                                                        modele,
                                                                        behavior,
                                                                        ImposedDof=impdof,
                                                                        ImposedEfforts=impeff,
                                                                        StiffnessMatrix=True,
                                                                        StiffnessIntegType="Complete",
                                                                        MassMatrix=True,
                                                                        MassIntegType="Complete",
                                                                        GeneralizedEffort=True,
                                                                        GeneEfrtIntegType="Complete",
                                                                        DampingMatrix=True,
                                                                        StorageType="Sparse",
                                                                        StoreElementry=False
                                                                      )

dspfield_C = femsol.SolveLDYN(
                                exact_stiff,
                                exact_mass,
                                exact_damp,
                                exact_genefrts,
                                1.67,
                                StorageType="Sparse"
                              )

# young en GPA -> dimenssions en millimètre
# young en PA  -> dimenssions en mètre

aster_dspls_imag = zstest.GetAsterRes(0, "Linear_Dynamic", FieldPart="imag")
aster_dspls_real = zstest.GetAsterRes(0, "Linear_Dynamic", FieldPart="real")

femsol_dspls_imag = np.imag(dspfield_C.GetAllDofs())
femsol_dspls_real = np.real(dspfield_C.GetAllDofs())

diff_imag = np.empty(femsol_dspls_imag.shape)
diff_real = np.empty(femsol_dspls_real.shape)

for idx, (v1, v2) in enumerate(zip(femsol_dspls_imag, aster_dspls_imag)):
    diff_imag[idx] = v1 - v2
  
for idx, (v1, v2) in enumerate(zip(femsol_dspls_real, aster_dspls_real)):
    diff_real[idx] = v1 - v2

print("{} degrees of freedom\n".format(femsol_dspls_imag.size))
print("Real part :\n")
print("\t- Same result as Aster : ", np.allclose(femsol_dspls_real, aster_dspls_real, atol=1e-06))
print("\t- Error range : {} -> {}.\n".format(np.amin(diff_real), np.amax(diff_real)))
print("Imaginary part :\n")
print("\t- Same result as Aster : ", np.allclose(femsol_dspls_imag, aster_dspls_imag, atol=1e-06))
print("\t- Error range : {} -> {}.\n".format(np.amin(diff_imag), np.amax(diff_imag)))

res = femsol.CreateResults(modele)
res.AddResult(dspfield_C)
res.Export("test_ldyn_0")
