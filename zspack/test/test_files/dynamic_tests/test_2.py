from zspack import femsol, mshgen, zstest
import numpy as np

maillage, elmnt = mshgen.CreateCuboid(
                                        start=(0,0,0),
                                        stop=(720,72,48), # mm
                                        num=(25,13,13),
                                        split=(2,1,1),
                                        grp1=("F","Dx+"),
                                        grp5=("N","Dx-")
                                      )

modele = femsol.CreateModel(maillage, "3D")

material_1 = femsol.CreateMaterial(
                                    type="isotrope",
                                    young= 210, # GPA
                                    poisson=0.3,
                                    gho=7.33e-6,
                                    alpha=1.0,
                                    beta=1.0
                                  )

material_2 = femsol.CreateMaterial(
                                    type="isotrope",
                                    young= 150, # GPA
                                    poisson=0.2,
                                    gho=4.87e-6,
                                    alpha=0.3, # 9.0
                                    beta=9.0  # 0.3
                                  )

behavior   = femsol.CreateBehavior(
                                     maillage,
                                     GRP_x0_y0_z0=material_1, 
                                     GRP_x1_y0_z0=material_2,
                                   )

impdof = femsol.CreateBC( 
                          modele, 
                          grp5=(
                                ("DZ", 0.17),
                                # ("DZ", 0.0),
                                ("DY", 0.79),
                                # ("DY", 0.0),
                                ("DX", 0.25)
                                # ("DX", 0.0)
                               )
                        )

impeff = femsol.CreateLoad(modele)

impeff.ApplyFaceLoad(
                      grp1=(
                            ("Tx", 7.4e-4),
                            ("Ty", 3.2e-4),
                            ("Tz", 5.1e-4) # GPA
                           )
                    )

stiff_matrix, mass_matrix, damp_matrix, genefrts_vector = femsol.Assemble(
                                                                            modele,
                                                                            behavior,
                                                                            ImposedDof=impdof,
                                                                            ImposedEfforts=impeff,
                                                                            StiffnessMatrix=True,
                                                                            StiffnessIntegType="Complete",
                                                                            MassMatrix=True,
                                                                            MassIntegType="Complete",
                                                                            GeneralizedEffort=True,
                                                                            GeneEfrtIntegType="Complete",
                                                                            DampingMatrix=True,
                                                                            StorageType="Sparse",
                                                                            StoreElementry=False
                                                                          )

dspfield_C = femsol.SolveLDYN(
                                stiff_matrix,
                                mass_matrix,
                                damp_matrix,
                                genefrts_vector,
                                1.7,
                                StorageType="Sparse"
                              )

# young en GPA -> dimenssions en millimètre
# young en PA  -> dimenssions en mètre

aster_dspls_imag = zstest.GetAsterRes(2, "Linear_Dynamic", FieldPart="imag")
aster_dspls_real = zstest.GetAsterRes(2, "Linear_Dynamic", FieldPart="real")

femsol_dspls_imag = np.imag(dspfield_C.GetAllDofs())
femsol_dspls_real = np.real(dspfield_C.GetAllDofs())

diff_imag = np.empty(femsol_dspls_imag.shape)
diff_real = np.empty(femsol_dspls_real.shape)

for idx, (v1, v2) in enumerate(zip(femsol_dspls_imag, aster_dspls_imag)):
    diff_imag[idx] = v1 - v2
  
for idx, (v1, v2) in enumerate(zip(femsol_dspls_real, aster_dspls_real)):
    diff_real[idx] = v1 - v2

print("{} degrees of freedom\n".format(femsol_dspls_imag.size))
print("Real part :\n")
print("\t- Same result as Aster : ", np.allclose(femsol_dspls_real, aster_dspls_real, atol=1e-06))
print("\t- Error range : {} -> {}.\n".format(np.amin(diff_real), np.amax(diff_real)))
print("Imaginary part :\n")
print("\t- Same result as Aster : ", np.allclose(femsol_dspls_imag, aster_dspls_imag, atol=1e-06))
print("\t- Error range : {} -> {}.\n".format(np.amin(diff_imag), np.amax(diff_imag)))

res = femsol.CreateResults(modele)
res.AddResult(dspfield_C)
res.Export("test_ldyn_2")


