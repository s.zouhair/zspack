from zspack import femsol, mshgen
import matplotlib.pyplot as plt
import numpy as np
import os

maillage, _ = mshgen.CreateCuboid(
                                    start=(0,0,0),
                                    stop=(720,72,48), # mm
                                    num=(10,4,4),
                                    split=(3,1,1),
                                    grp1=("F","Dx+"),
                                    grp5=("N","Dx-"),
                                  )

modele = femsol.CreateModel(maillage, "3D")

exact_material = femsol.CreateMaterial(
                                        type="isotrope",
                                        young= 210, # GPA
                                        poisson=0.3,
                                        gho=7.33e-6,
                                        alpha=1.,
                                        beta=1.
                                      )

exact_behavior   = femsol.CreateBehavior(
                                          maillage,
                                          All=exact_material
                                        )

impdof = femsol.CreateBC( 
                          modele, 
                          grp5=(
                                ("DZ", 0.),
                                ("DY", 0.),
                                ("DX", 0.)
                               )
                        )

impeff = femsol.CreateLoad(modele)

impeff.ApplyFaceLoad(
                      grp1=(
                            ("Tx", 7.4e-4),
                            ("Ty", 3.2e-4),
                            ("Tz", 5.1e-4) # GPA
                           )
                    )

exact_stiff, exact_mass, exact_genefrts = femsol.Assemble(
                                                    modele,
                                                    exact_behavior,
                                                    ImposedDof=impdof, 
                                                    ImposedEfforts=impeff,
                                                    StiffnessMatrix=True,
                                                    StiffnessIntegType="Complete",
                                                    MassMatrix=True,
                                                    MassIntegType="Complete",
                                                    GeneralizedEffort=True,
                                                    GeneEfrtIntegType="Complete",
                                                    StorageType="Sparse",
                                                    StoreElementry=False
                                                    )

dspfield_C = femsol.SolveLDYN(
                                StiffMatrix=exact_stiff,
                                MassMatrix=exact_mass,
                                GeneEfforts=exact_genefrts,
                                Freq=1.5,
                                StorageType="Sparse"
                              )

def MyModel(MyYoung=None, MyPoisson=None, Only=None):

    wrong_material = femsol.CreateMaterial(
                                    type="isotrope",
                                    young=MyYoung, # GPA
                                    poisson=MyPoisson,
                                    gho=7.33e-6,
                                    alpha=790.,
                                    beta=520.
                                )

    wrong_behavior   = femsol.CreateBehavior(
                                    maillage,
                                    All=wrong_material,
                                )

    sMat = False if Only == "Mass" else True
    mMat = False if Only == "Stiffness" else True
    gEff = False if Only is not None else True
    impEff = impeff if gEff else None

    result = femsol.Assemble(
                        modele,
                        wrong_behavior,
                        ImposedDof=impdof,
                        ImposedEfforts=impEff,
                        StiffnessMatrix=sMat,
                        StiffnessIntegType="Complete",
                        MassMatrix=mMat,
                        MassIntegType="Complete",
                        GeneralizedEffort=gEff,
                        GeneEfrtIntegType="Complete",
                        StorageType="Sparse",
                        StoreElementry=True
                    )

    return result

def nodeidx(nc, mc=(10,4,4)):
    return nc[0] + nc[1] * mc[0] + nc[2] * mc[0] * mc[1]

MeasuredDofs = {
    # Dy-
    "Node{}".format(nodeidx((1, 0, 1))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((5, 0, 2))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((8, 0, 1))) : ("DX", "DY", "DZ"),

    # Dy+
    "Node{}".format(nodeidx((1, 3, 1))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((5, 3, 2)))  : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((8, 3, 1))) : ("DX", "DY", "DZ"),

    # Dx+
    "Node{}".format(nodeidx((9, 1, 2))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((9, 2, 1))) : ("DX", "DY", "DZ"),

    # Dz-
    "Node{}".format(nodeidx((1, 2, 0))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((5, 1, 0))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((8, 2, 0))) : ("DX", "DY", "DZ"),

    # Dz+
    "Node{}".format(nodeidx((1, 2, 3))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((5, 1, 3))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((8, 2, 3))) : ("DX", "DY", "DZ"),
}

MeasuresNumbering = dspfield_C.GetDofsNumbring(**MeasuredDofs)
MeasuresValues    = dspfield_C.GetDofs(MeasuresNumbering)[None,:]

McreObj = femsol.CreateDynMcre(
            MyModel, 
            [1.5], 
            MeasuresNumbering, 
            MeasuresValues,
            ApproxGrad=True,
            MyYoung=("young", 200),
            MyPoisson=("poisson", 0.2)
        )

# McreObj.optimize(
#     method="GRAD-DSCT", 
#     maxiter=100, 
#     glberr=1.e-10,
#     steps={
#         "MyYoung" : 1e6,
#         "MyPoisson" : 1e2
#     },
#     graderr=1e-20,
#     verbose=True
# )

McreObj.optimize(
    method="BFGS", 
    maxiter=100, 
    glberr=1.e-15,
    graderr=1e-20,
    verbose=True
)































