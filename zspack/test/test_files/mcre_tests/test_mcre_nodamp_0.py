from zspack import femsol, mshgen
import numpy as np

maillage, _ = mshgen.CreateCuboid(
                                    start=(0,0,0),
                                    stop=(720,72,48), # mm
                                    num=(26,13,13),
                                    split=(5,1,1),
                                    grp1=("F","Dx+"),
                                    grp5=("N","Dx-"),
                                  )

modele = femsol.CreateModel(maillage, "3D")

exact_material = femsol.CreateMaterial(
                                        type="isotrope",
                                        young= 210, # GPA
                                        poisson=0.3,
                                        gho=7.33e-6,
                                        alpha=1.,
                                        beta=1.
                                      )

wrong_material = femsol.CreateMaterial(
                                        type="isotrope",
                                        young= 150, # GPA
                                        poisson=0.2,
                                        gho=7.33e-6,
                                        alpha=790.,
                                        beta=520.
                                      )


exact_behavior   = femsol.CreateBehavior(
                                          maillage,
                                          All=exact_material
                                        )


wrong_behavior   = femsol.CreateBehavior(
                                          maillage,
                                          GRP_x0_y0_z0=exact_material,
                                          GRP_x1_y0_z0=exact_material,
                                          GRP_x2_y0_z0=wrong_material,
                                          GRP_x3_y0_z0=exact_material,
                                          GRP_x4_y0_z0=exact_material
                                        )

impdof = femsol.CreateBC( 
                          modele, 
                          grp5=(
                                ("DZ", 0.),
                                ("DY", 0.),
                                ("DX", 0.)
                               )
                        )

impeff = femsol.CreateLoad(modele)

impeff.ApplyFaceLoad(
                      grp1=(
                            ("Tx", 7.4e-4),
                            ("Ty", 3.2e-4),
                            ("Tz", 5.1e-4) # GPA
                           )
                    )



exact_stiff, exact_mass, exact_damp, exact_genefrts = femsol.Assemble(
                                                                        modele,
                                                                        exact_behavior,
                                                                        ImposedDof=impdof, 
                                                                        ImposedEfforts=impeff,
                                                                        StiffnessMatrix=True,
                                                                        StiffnessIntegType="Complete",
                                                                        MassMatrix=True,
                                                                        MassIntegType="Complete",
                                                                        GeneralizedEffort=True,
                                                                        GeneEfrtIntegType="Complete",
                                                                        DampingMatrix=True,
                                                                        StorageType="Sparse",
                                                                        StoreElementry=False
                                                                      )


wrong_stiff, wrong_mass, wrong_geneefrts = femsol.Assemble(
                                                                        modele,
                                                                        wrong_behavior,
                                                                        ImposedDof=impdof,
                                                                        ImposedEfforts=impeff,
                                                                        StiffnessMatrix=True,
                                                                        StiffnessIntegType="Complete",
                                                                        MassMatrix=True,
                                                                        MassIntegType="Complete",
                                                                        GeneralizedEffort=True,
                                                                        GeneEfrtIntegType="Complete",
                                                                        StorageType="Sparse",
                                                                        StoreElementry=True
                                                                      )


dspfield_C = femsol.SolveLDYN(
                                exact_stiff,
                                exact_mass,
                                exact_damp,
                                exact_genefrts,
                                1.5,
                                StorageType="Sparse"
                              )

# def nodeidx(nc, mc):
#     return nc[0] + nc[1] * mc[0] + nc[2] * mc[0] * mc[1]

MeasuredDofs = {
    # Dy-
    "Node681"  : ("DX", "DY", "DZ"),
    "Node3056" : ("DX", "DY", "DZ"),
    "Node2049" : ("DX", "DY", "DZ"),

    # Dy+
    "Node3021" : ("DX", "DY", "DZ"),
    "Node666"  : ("DX", "DY", "DZ"),
    "Node2025" : ("DX", "DY", "DZ"),

    # Dx+
    "Node1793" : ("DX", "DY", "DZ"),
    "Node3301" : ("DX", "DY", "DZ"),

    # Dz+
    "Node4112" : ("DX", "DY", "DZ"),
    "Node4274" : ("DX", "DY", "DZ"),
    "Node4208" : ("DX", "DY", "DZ"),
}

MeasuresNumbering = dspfield_C.GetDofsNumbring(**MeasuredDofs)
MeasuresValues    = dspfield_C.GetDofs(MeasuresNumbering)[None,:]

McreObj = femsol.CreateLocDynMcre(
                                wrong_stiff, 
                                wrong_mass,
                                None,
                                wrong_geneefrts,
                                [1.5],
                                MeasuresNumbering,
                                MeasuresValues,
                                ErrType="Drkr",
                                StorageType="Sparse"
                              )

McreObj.CalcDynMcre()

res = femsol.CreateResults(modele)
res.AddResult(
                McreObj, 
                AddFields=(
                             "Field_U",
                          )
             )
res.Export("test_mcre_Drucker_nodamp")































