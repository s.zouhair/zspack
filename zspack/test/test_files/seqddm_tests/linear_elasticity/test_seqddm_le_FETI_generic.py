from zspack import femsol, mshgen
import numpy as np

sd_nbr = (1, 1, 3)
num = (4, 4, 4)
start = (0,  0, 0)
lenght = (200, 200, 200)



repeat = (sd_nbr[2], sd_nbr[1], sd_nbr[0])

material = femsol.CreateMaterial(
                                   type="isotrope",
                                   young= 210, # GPA
                                   poisson=0.3,
                                   gho=2.33,
                                   alpha=50,
                                   beta=1e-7
                                )

InterfaceMapping, IntrfcNodesNbr = mshgen.GetInterfaceMapping(
                                                                num=num,
                                                                repeat=repeat,
                                                                Type="Dual"
                                                             )

for sd_idx in np.ndindex(sd_nbr):
   z_idx, y_idx, x_idx = sd_idx
   sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]

   x_start = start[0] + lenght[0] * x_idx
   y_start = start[1] + lenght[1] * y_idx
   z_start = start[2] + lenght[2] * z_idx

   x_stop = x_start + lenght[0]
   y_stop = y_start + lenght[1]
   z_stop = z_start + lenght[2]

   mesh_str = "maillage_{} = mshgen.CreateCuboid(\
      start=({},{},{}),\
      stop=({},{},{}),\
      num=({},{},{}),\
      split=None".format(sd_glb_idx,\
      x_start, y_start, z_start,\
      x_stop, y_stop, z_stop,\
      num[0], num[1], num[2])

   if x_idx == 0:
      mesh_str += ", grp5=('N','Dx-')"

   if x_idx + 1 == sd_nbr[2]:
      mesh_str += ", grp1=('F','Dx+')"
   
   mesh_str += ")"
   
   exec(mesh_str)

   model_str = "modele_{} = femsol.CreateModel(maillage_{}, '3D')".format(\
                                                   sd_glb_idx, sd_glb_idx)
   
   exec(model_str)

   behav_str = "behavior_{}   = femsol.CreateBehavior( \
                                       maillage_{},    \
                                       All=material   \
                                    )".format(sd_glb_idx, sd_glb_idx)

   exec(behav_str)

   if x_idx == 0:
      impdof_str = "impdof_{} = femsol.CreateBC(      \
                           modele_{},                 \
                           grp5=(                     \
                                   ('DZ', 0.),        \
                                   ('DY', 0.),        \
                                   ('DX', 0.)         \
                                )                     \
                        )".format(sd_glb_idx, sd_glb_idx)
      
      exec(impdof_str)
   
   if x_idx + 1 == sd_nbr[2]:
      impeff_str_0 ="impeff_{} = femsol.CreateLoad(modele_{})".format(\
                                                   sd_glb_idx, sd_glb_idx)
      exec(impeff_str_0)

      impeff_str_1 ="impeff_{}.ApplyFaceLoad(               \
                       grp1=(                               \
                               ('Tx', 7.4),                 \
                               ('Ty', 7.4e-3),              \
                               ('Tz', 6.3e-3)               \
                            )                               \
                    )".format(sd_glb_idx)
      
      exec(impeff_str_1)
   
   asse_str = "stif_mat_{}, gene_eff_{}".format(sd_glb_idx, sd_glb_idx)

   asse_str += " = femsol.Assemble(       \
                               modele_{},  \
                               behavior_{},".format(sd_glb_idx, sd_glb_idx)
   if x_idx == 0:
      asse_str += "  \
                               ImposedDof=impdof_{},".format(sd_glb_idx)
   if x_idx + 1 == sd_nbr[2]:
      asse_str += "  \
                               ImposedEfforts=impeff_{},".format(sd_glb_idx)
   asse_str += "  \
                               StiffnessMatrix=True,           \
                               StiffnessIntegType='Complete',  \
                               GeneralizedEffort=True,         \
                               GeneEfrtIntegType='Complete',"
   asse_str += " \
                               StorageType='Sparse', \
                            )"
   
   exec(asse_str)

   nmbrng_str = "numbering_{} = stif_mat_{}.\
                     GetDofNumbering()".format(sd_glb_idx, sd_glb_idx)
   exec(nmbrng_str)

   stiff_val_str = "stif_mat_val_{} = stif_mat_{}.\
         GetValues(StorageType='Sparse')".format(sd_glb_idx, sd_glb_idx)
   exec(stiff_val_str)

   gene_eff_val_str = "gene_eff_val_{} = gene_eff_{}.\
      GetValues(Copy=True)".format(sd_glb_idx, sd_glb_idx)

   exec(gene_eff_val_str)

   eff_stiff_part_str_0 = "gep_{}, gev_{} = stif_mat_{}.GetGeneEffPart()".format(\
                                          sd_glb_idx, sd_glb_idx, sd_glb_idx)
   exec(eff_stiff_part_str_0)

   eff_stiff_part_str_1 = "gene_eff_val_{}[gep_{}] += gev_{}".format(\
                                          sd_glb_idx, sd_glb_idx, sd_glb_idx)
   exec(eff_stiff_part_str_1)

   intrfc_nmbrg_str = "local_intrfc_indices_{}, global_intrfc_indices_{}   \
            = InterfaceMapping[{}][{}][{}]".format(sd_glb_idx, sd_glb_idx, \
               z_idx, y_idx, x_idx)
   exec(intrfc_nmbrg_str)

le_seqddm_str = "le_seqddm = femsol.CreateSeqDDM(                 \
                                  type='Dual',                  \
                                  intrfc_nodes_nbr=IntrfcNodesNbr"

for sd_idx in np.ndindex(sd_nbr):
   z_idx, y_idx, x_idx = sd_idx
   sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]

   le_seqddm_str += ",   \
                                  SubDomain_{}=( \
                                      stif_mat_val_{}, \
                                      gene_eff_val_{}, \
                                      local_intrfc_indices_{},  \
                                      global_intrfc_indices_{}, \
                                      numbering_{}  \
                                  )".format(sd_glb_idx, sd_glb_idx,  \
                                             sd_glb_idx, sd_glb_idx,  \
                                             sd_glb_idx, sd_glb_idx)

le_seqddm_str += "   \
                               )"

exec(le_seqddm_str)

exec("le_seqddm.SolveSeqDDM()")
# exec("le_seqddm.SolveSeqDDM(stype='Iterative', mtype='CG', tol=1e-5)")

for sd_idx in np.ndindex(sd_nbr):
   z_idx, y_idx, x_idx = sd_idx
   sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]

   sd_res_str = "extra_bc_indices_{}, extra_bc_vals_{} = \
      le_seqddm.GetSdResult({})".format(sd_glb_idx, sd_glb_idx, sd_glb_idx)

   exec(sd_res_str)

   stif_val_del_str = "del stif_mat_val_{}".format(sd_glb_idx)
   exec(stif_val_del_str)

   gene_eff_val_del_str = "del gene_eff_val_{}".format(sd_glb_idx)
   exec(gene_eff_val_del_str)

   local_intrfc_indices_del = "del local_intrfc_indices_{}".format(sd_glb_idx)
   exec(local_intrfc_indices_del)

   global_intrfc_indices_del = "del global_intrfc_indices_{}".format(sd_glb_idx)
   exec(global_intrfc_indices_del)

exec("del le_seqddm")

for sd_idx in np.ndindex(sd_nbr):
   z_idx, y_idx, x_idx = sd_idx
   sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]

   le_solves_str = "dsp_field_{} = femsol.SolveLE(  \
                               StiffMatrix=stif_mat_{}".format(\
                                          sd_glb_idx, sd_glb_idx)
   
   le_solves_str += ",  \
                               GeneEfforts=gene_eff_{}".format(sd_glb_idx)
   
   le_solves_str += ",                                           \
                               StorageType='Sparse',             \
                               ExtraLoad=(                       \
                                            extra_bc_indices_{}, \
                                            extra_bc_vals_{}     \
                                         )                       \
                            )".format(sd_glb_idx, sd_glb_idx)

   exec(le_solves_str)

res_decl_str = "res = femsol.CreateResults(modele_0"

for sd_idx in np.ndindex(sd_nbr):
   z_idx, y_idx, x_idx = sd_idx
   sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]
   if sd_glb_idx != 0:
      res_decl_str += ", modele_{}".format(sd_glb_idx)

res_decl_str += ")"

exec(res_decl_str)

for sd_idx in np.ndindex(sd_nbr):
   z_idx, y_idx, x_idx = sd_idx
   sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]
   res_add_str = "res.AddResult(dsp_field_{})".format(sd_glb_idx)
   exec(res_add_str)

filename = "test_le_seqddm_x{}_y{}_z{}".format(sd_nbr[2], sd_nbr[1], sd_nbr[0])
exec("res.Export(filename)")


















