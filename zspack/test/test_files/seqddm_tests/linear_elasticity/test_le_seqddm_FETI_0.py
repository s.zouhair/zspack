from zspack import femsol, mshgen

maillage_0 = mshgen.CreateCuboid(
                                   start=(0,0,0),
                                   stop=(800,900,400), # mm
                                   num=(7,7,7),
                                   split=None,
                                   grp1=("F","Dx+"),
                                   grp5=("N","Dx-")
                                )

maillage_1 = mshgen.CreateCuboid(
                                   start=(0,0,400),
                                   stop=(800,900,800), # mm
                                   num=(7,7,7),
                                   split=None,
                                   grp1=("F","Dx+"),
                                   grp5=("N","Dx-")
                                )

InterfaceMapping, IntrfcNodesNbr = mshgen.GetInterfaceMapping(
                                                                num=(7,7,7),
                                                                repeat=(1,1,2),
                                                                Type="Dual"
                                                             )

local_intrfc_indices_0, global_intrfc_indices_0 = InterfaceMapping[0][0][0]
local_intrfc_indices_1, global_intrfc_indices_1 = InterfaceMapping[1][0][0]

modele_0 = femsol.CreateModel(maillage_0, "3D")

modele_1 = femsol.CreateModel(maillage_1, "3D")

material = femsol.CreateMaterial(
                                   type="isotrope",
                                   young= 210, # GPA
                                   poisson=0.3,
                                   gho=2.33,
                                   alpha=50,
                                   beta=1e-7
                                )

behavior_0   = femsol.CreateBehavior(
                                       maillage_0,
                                       All=material
                                    )

behavior_1   = femsol.CreateBehavior(
                                       maillage_1,
                                       All=material
                                    )

impdof_0 = femsol.CreateBC( 
                           modele_0,
                           grp5=(
                                   ("DZ", 0.),
                                   ("DY", 0.),
                                   ("DX", 0.)
                                )
                        )

impdof_1 = femsol.CreateBC( 
                           modele_1,
                           grp5=(
                                   ("DZ", 0.),
                                   ("DY", 0.),
                                   ("DX", 0.)
                                )
                        )

impeff_0 = femsol.CreateLoad(modele_0)

impeff_0.ApplyFaceLoad(
                       grp1=(
                               ("Tx", 7.4e-3), # GPA
                            )
                    )

impeff_1 = femsol.CreateLoad(modele_1)

impeff_1.ApplyFaceLoad(
                       grp1=(
                               ("Tx", 7.4e-3), # GPA
                            )
                    )

stif_mat_0, gene_eff_0 = femsol.Assemble(
                                           modele_0,
                                           behavior_0,
                                           ImposedDof=impdof_0,
                                           ImposedEfforts=impeff_0,
                                           StiffnessMatrix=True,
                                           StiffnessIntegType="Complete",
                                           GeneralizedEffort=True,
                                           GeneEfrtIntegType="Complete",
                                           StorageType="Sparse",
                                        )

numbering_0 = stif_mat_0.GetDofNumbering()
stif_mat_val_0 = stif_mat_0.GetValues(StorageType="Sparse")
gene_eff_val_0 = gene_eff_0.GetValues(Copy=True)
gep_0, gev_0 = stif_mat_0.GetGeneEffPart()
gene_eff_val_0[gep_0] += gev_0

stif_mat_1, gene_eff_1 = femsol.Assemble(
                                           modele_1,
                                           behavior_1,
                                           ImposedDof=impdof_1,
                                           ImposedEfforts=impeff_1,
                                           StiffnessMatrix=True,
                                           StiffnessIntegType="Complete",
                                           GeneralizedEffort=True,
                                           GeneEfrtIntegType="Complete",
                                           StorageType="Sparse",
                                        )

numbering_1 = stif_mat_1.GetDofNumbering()
stif_mat_val_1 = stif_mat_1.GetValues(StorageType="Sparse")
gene_eff_val_1 = gene_eff_1.GetValues(Copy=True)
gep_1, gev_1 = stif_mat_1.GetGeneEffPart()
gene_eff_val_1[gep_1] += gev_1

le_seqddm = femsol.CreateSeqDDM(
                                  type="Dual",
                                  intrfc_nodes_nbr=IntrfcNodesNbr,
                                  SubDomain_0=(
                                      stif_mat_val_0,
                                      gene_eff_val_0,
                                      local_intrfc_indices_0,
                                      global_intrfc_indices_0,
                                      numbering_0
                                  ),
                                  SubDomain_1=(
                                      stif_mat_val_1,
                                      gene_eff_val_1,
                                      local_intrfc_indices_1,
                                      global_intrfc_indices_1,
                                      numbering_1
                                  )
                               )

le_seqddm.SolveSeqDDM(stype="Iterative", mtype="CG", tol=1e-5)
# le_seqddm.SolveSeqDDM()

extra_bc_indices_0, extra_bc_vals_0 = le_seqddm.GetSdResult(0)
extra_bc_indices_1, extra_bc_vals_1 = le_seqddm.GetSdResult(1)

del le_seqddm

dsp_field_0 = femsol.SolveLE(
                               StiffMatrix=stif_mat_0,
                               GeneEfforts=gene_eff_0,
                               StorageType="Sparse",
                               ExtraLoad=(
                                            extra_bc_indices_0,
                                            extra_bc_vals_0
                                         )
                            )

dsp_field_1 = femsol.SolveLE(
                               StiffMatrix=stif_mat_1,
                               GeneEfforts=gene_eff_1,
                               StorageType="Sparse",
                               ExtraLoad=(
                                            extra_bc_indices_1,
                                            extra_bc_vals_1
                                         )
                            )


res = femsol.CreateResults(modele_0, modele_1)
res.AddResult(dsp_field_0)
res.AddResult(dsp_field_1)
res.Export("test_le_seqddm_FETI_0_iterative")


















