from zspack import femsol, mshgen

sd_nbr = (20, 20, 1)
num = (4, 4, 4)

repeat = (sd_nbr[2], sd_nbr[1], sd_nbr[0])
lenght = (
  200 * repeat[0], 
  200 * repeat[1], 
  200 * repeat[2]
)

num = (
  (num[0] - 1) * repeat[0] + 1,
  (num[1] - 1) * repeat[1] + 1,
  (num[2] - 1) * repeat[2] + 1
)

maillage = mshgen.CreateCuboid(
                                start=(0,0,0),
                                stop=lenght, # mm
                                num=num,
                                split=None,
                                grp1=("F","Dx+"),
                                grp5=("N","Dx-")
                              )

modele = femsol.CreateModel(maillage, "3D")

material = femsol.CreateMaterial(
                                   type="isotrope",
                                   young= 210, # GPA
                                   poisson=0.3,
                                   gho=2.33,
                                   alpha=50,
                                   beta=1e-7
                                )

behavior   = femsol.CreateBehavior(
                                    maillage,
                                    All=material
                                  )

impdof = femsol.CreateBC( 
                          modele, 
                          grp5=(
                                ("DZ", 0.),
                                ("DY", 0.),
                                ("DX", 0.)
                               )
                        )

impeff = femsol.CreateLoad(modele)

impeff.ApplyFaceLoad(
                      grp1=(
                            ("Tx", 7.4), # GPA
                           )
                    )

stifmatrx_C, massmatrx_C, dampmatrx_C, genefrts_C = femsol.Assemble(
                                                                      modele,
                                                                      behavior,
                                                                      ImposedDof=impdof,
                                                                      ImposedEfforts=impeff,
                                                                      StiffnessMatrix=True,
                                                                      StiffnessIntegType="Complete",
                                                                      MassMatrix=True,
                                                                      MassIntegType="Complete",
                                                                      GeneralizedEffort=True,
                                                                      GeneEfrtIntegType="Complete",
                                                                      DampingMatrix=True,
                                                                      StorageType="Sparse",
                                                                      StoreElementry=False
                                                                   )

dspfield_C = femsol.SolveLE(
                           StiffMatrix=stifmatrx_C,
                           GeneEfforts=genefrts_C,
                           StorageType="Sparse"
                         )


res = femsol.CreateResults(modele)
res.AddResult(dspfield_C)
res.Export("test_le_ref_x{}_y{}_z{}".format(*repeat))

































