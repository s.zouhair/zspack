from zspack import femsol, mshgen

def nodeidx(nc, mc):
    return nc[0] + nc[1] * mc[0] + nc[2] * mc[0] * mc[1]

maillage = mshgen.CreateCuboid(
                                start=(0,0,0),
                                stop=(540,108,216), # mm
                                num=(4,4,7),
                                split=None,
                                grp5=("N","Dx-"),
                                grp1=("F","Dx+")
                              )

modele = femsol.CreateModel(maillage, "3D")

exact_material = femsol.CreateMaterial(
                                        type="isotrope",
                                        young= 210, # GPA
                                        poisson=0.3,
                                        gho=7.33e-6,
                                        alpha=1.,
                                        beta=1.
                                      )

exact_behavior = femsol.CreateBehavior(
                                        maillage,
                                        All=exact_material
                                      )

impdof = femsol.CreateBC( 
                           modele,
                           grp5=(
                                   ("DZ", 0.),
                                   ("DY", 0.),
                                   ("DX", 0.)
                                )
                        )

impeff = femsol.CreateLoad(modele)

impeff.ApplyFaceLoad(
                       grp1=(
                               ("Tx", 5.4e-1), # GPA
                               ("Ty", 7.4e-3),
                               ("Tz", 6.3e-3)
                            )
                    )

exact_stiff, exact_mass, exact_damp, exact_genefrts = femsol.Assemble(
                                                                        modele,
                                                                        exact_behavior,
                                                                        ImposedDof=impdof, 
                                                                        ImposedEfforts=impeff,
                                                                        StiffnessMatrix=True,
                                                                        StiffnessIntegType="Complete",
                                                                        MassMatrix=True,
                                                                        MassIntegType="Complete",
                                                                        GeneralizedEffort=True,
                                                                        GeneEfrtIntegType="Complete",
                                                                        DampingMatrix=True,
                                                                        StorageType="Sparse",
                                                                        StoreElementry=False
                                                                      )

dspfield_C = femsol.SolveLDYN(
                                exact_stiff,
                                exact_mass,
                                exact_damp,
                                exact_genefrts,
                                1.5,
                                StorageType="Sparse"
                              )

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

InterfaceMapping, IntrfcNodesNbr = mshgen.GetInterfaceMapping(
                                                                num=(4,4,4),
                                                                repeat=(1,1,2),
                                                                Type="Primal"
                                                             )

wrong_material = femsol.CreateMaterial(
                                        type="isotrope",
                                        young= 150, # GPA
                                        poisson=0.2,
                                        gho=7.33e-6,
                                        alpha=790.,
                                        beta=520.
                                      )

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

maillage_0, _ = mshgen.CreateCuboid(
                                     start=(0,0,0),
                                     stop=(540,108,108), # mm
                                     num=(4,4,4),
                                     split=(1,1,3),
                                     grp5=("N","Dx-"),
                                     grp1=("F","Dx+")
                                   )

modele_0 = femsol.CreateModel(maillage_0, "3D")

wrong_behavior_0 = femsol.CreateBehavior(
                                          maillage_0,
                                          GRP_x0_y0_z0=exact_material,
                                          GRP_x0_y0_z1=exact_material,
                                          GRP_x0_y0_z2=exact_material
                                        )

impeff_0 = femsol.CreateLoad(modele_0)

impeff_0.ApplyFaceLoad(
                         grp1=(
                                 ("Tx", 5.4e-1), # GPA
                                 ("Ty", 7.4e-3),
                                 ("Tz", 6.3e-3)
                              )
                      )

impdof_0 = femsol.CreateBC( 
                             modele_0,
                             grp5=(
                                     ("DZ", 0.),
                                     ("DY", 0.),
                                     ("DX", 0.)
                                  )
                          )

wrong_stiff_0, wrong_mass_0, wrong_damp_0, wrong_geneefrts_0 = femsol.Assemble(
                                                                                modele_0,
                                                                                wrong_behavior_0,
                                                                                ImposedDof=impdof_0,
                                                                                ImposedEfforts=impeff_0,
                                                                                StiffnessMatrix=True,
                                                                                StiffnessIntegType="Complete",
                                                                                MassMatrix=True,
                                                                                MassIntegType="Complete",
                                                                                GeneralizedEffort=True,
                                                                                GeneEfrtIntegType="Complete",
                                                                                DampingMatrix=True,
                                                                                StorageType="Sparse",
                                                                                StoreElementry=True
                                                                              )

MeasuredDofs_glb_0 = {
    "Node{}".format(nodeidx((1,0,1), (4,4,7))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((2,3,2), (4,4,7))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((2,1,0), (4,4,7))) : ("DX", "DY", "DZ")
}

MeasuredDofs_sd_0 = {
    "Node{}".format(nodeidx((1,0,1), (4,4,4))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((2,3,2), (4,4,4))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((2,1,0), (4,4,4))) : ("DX", "DY", "DZ")
}

MeasuresNumbering_glb_0 = dspfield_C.GetDofsNumbring(**MeasuredDofs_glb_0)
MeasuresNumbering_0 = wrong_stiff_0.GetDofsNumbring(**MeasuredDofs_sd_0)
MeasuresValues_0    = dspfield_C.GetDofs(MeasuresNumbering_glb_0)[None,:]

mcre_obj_0 = femsol.CreateLocDynMcre(
                                   wrong_stiff_0,
                                   wrong_mass_0,
                                   wrong_damp_0,
                                   wrong_geneefrts_0,
                                   [1.5],
                                   MeasuresNumbering_0,
                                   MeasuresValues_0,
                                   ErrType="Drkr",
                                   StorageType="Sparse"
                                 )

numbering_0 = wrong_stiff_0.GetDofNumbering()
mat_val_0 = mcre_obj_0.GetMcreMat()
vec_val_0 = mcre_obj_0.GetMcreVec()

local_intrfc_indices_0, global_intrfc_indices_0 = InterfaceMapping[0][0][0]

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

maillage_1, _ = mshgen.CreateCuboid(
                                     start=(0,0,108),
                                     stop=(540,108,216), # mm
                                     num=(4,4,4),
                                     split=(1,1,3),
                                     grp5=("N","Dx-"),
                                     grp1=("F","Dx+")
                                   )

modele_1 = femsol.CreateModel(maillage_1, "3D")

wrong_behavior_1 = femsol.CreateBehavior(
                                          maillage_1,
                                          GRP_x0_y0_z0=exact_material,
                                          GRP_x0_y0_z1=wrong_material,
                                          GRP_x0_y0_z2=exact_material
                                        )

impdof_1 = femsol.CreateBC( 
                             modele_1,
                             grp5=(
                                     ("DZ", 0.),
                                     ("DY", 0.),
                                     ("DX", 0.)
                                  )
                          )

impeff_1 = femsol.CreateLoad(modele_1)

impeff_1.ApplyFaceLoad(
                         grp1=(
                                 ("Tx", 5.4e-1), # GPA
                                 ("Ty", 7.4e-3),
                                 ("Tz", 6.3e-3)
                              )
                      )

wrong_stiff_1, wrong_mass_1, wrong_damp_1, wrong_geneefrts_1 = femsol.Assemble(
                                                                                modele_1,
                                                                                wrong_behavior_1,
                                                                                ImposedDof=impdof_1,
                                                                                ImposedEfforts=impeff_1,
                                                                                StiffnessMatrix=True,
                                                                                StiffnessIntegType="Complete",
                                                                                MassMatrix=True,
                                                                                MassIntegType="Complete",
                                                                                GeneralizedEffort=True,
                                                                                GeneEfrtIntegType="Complete",
                                                                                DampingMatrix=True,
                                                                                StorageType="Sparse",
                                                                                StoreElementry=True
                                                                              )

MeasuredDofs_glb_1 = {
    "Node{}".format(nodeidx((1,0,4), (4,4,7))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((2,3,5), (4,4,7))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((2,1,3), (4,4,7))) : ("DX", "DY", "DZ")
}

MeasuredDofs_sd_1 = {
    "Node{}".format(nodeidx((1,0,1), (4,4,4))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((2,2,3), (4,4,4))) : ("DX", "DY", "DZ"),
    "Node{}".format(nodeidx((2,3,2), (4,4,4))) : ("DX", "DY", "DZ"),
}

MeasuresNumbering_glb_1 = dspfield_C.GetDofsNumbring(**MeasuredDofs_glb_1)
MeasuresNumbering_1 = wrong_stiff_1.GetDofsNumbring(**MeasuredDofs_sd_1)
MeasuresValues_1    = dspfield_C.GetDofs(MeasuresNumbering_glb_1)[None,:]

mcre_obj_1 = femsol.CreateLocDynMcre(
                                   wrong_stiff_1,
                                   wrong_mass_1,
                                   wrong_damp_1,
                                   wrong_geneefrts_1,
                                   [1.5],
                                   MeasuresNumbering_1,
                                   MeasuresValues_1,
                                   ErrType="Drkr",
                                   StorageType="Sparse"
                                 )

numbering_1 = wrong_stiff_1.GetDofNumbering()
mat_val_1 = mcre_obj_1.GetMcreMat()
vec_val_1 = mcre_obj_1.GetMcreVec()

local_intrfc_indices_1, global_intrfc_indices_1 = InterfaceMapping[1][0][0]

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

mcre_seqddm = femsol.CreateSeqDDM(
                                    type="Primal",
                                    blocks_nbr=3,
                                    intrfc_nodes_nbr=IntrfcNodesNbr,
                                    SubDomain_0=(
                                        mat_val_0,
                                        vec_val_0,
                                        local_intrfc_indices_0,
                                        global_intrfc_indices_0,
                                        numbering_0
                                    ),
                                    SubDomain_1=(
                                        mat_val_1,
                                        vec_val_1,
                                        local_intrfc_indices_1,
                                        global_intrfc_indices_1,
                                        numbering_1
                                    )
                                 )

# mcre_seqddm.SolveSeqDDM(stype="Iterative", mtype="CG", tol=6e-1)

mcre_seqddm.SolveSeqDDM()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

extra_bc_indices_0, extra_bc_vals_0 = mcre_seqddm.GetSdResult(0)

mcre_obj_0.CalcDynMcre(
                        FreqIdx=0,
                        ExtraBC=(
                                  extra_bc_indices_0,
                                  extra_bc_vals_0
                                )
                      )

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

extra_bc_indices_1, extra_bc_vals_1 = mcre_seqddm.GetSdResult(1)

mcre_obj_1.CalcDynMcre(
                        FreqIdx=0,
                        ExtraBC=(
                                   extra_bc_indices_1,
                                   extra_bc_vals_1
                                )
                      )

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - #

res = femsol.CreateResults(modele_0, modele_1)
res.AddResult(mcre_obj_0)
res.AddResult(mcre_obj_1)
res.Export("test_seqddm_mcre_0_direct")
# res.Export("test_seqddm_mcre_0_iterative")


















