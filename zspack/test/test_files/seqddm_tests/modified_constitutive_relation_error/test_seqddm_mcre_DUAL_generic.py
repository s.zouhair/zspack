from zspack import femsol, mshgen
import numpy as np

def nodeidx(nc, mc):
    return nc[0] + nc[1] * mc[0] + nc[2] * mc[0] * mc[1]

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sd_nbr = (2, 2, 1)
num = (13, 3, 3)
start  = (0, 0, 0)
lenght = (240, 15, 10)

measures_indices = [
    (1, 1),
    (2, 2),
    (3, 1),
    (4, 2),
    (5, 1),
    (6, 2),
    (7, 1),
    (9, 1),
    (10, 2),
    (11, 1)
]

# Used to select subdomains where modeling 
# errors will be enforced
sd_selector = {
   'x' : None,
   'y' : None,
   'z' : None
}

# Used to split subdomains where modeling 
# errors are enforced
sd_splitter = {
   'x' : 6,
   'y' : 1,
   'z' : 1
}

# Used to select elements groups where 
# errors will be enforced (only for 
# concerned subdomains)
error_selector = {
   'x' : 2,
   'y' : None,
   'z' : None
}

freq_val = 1.5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

repeat = (sd_nbr[2], sd_nbr[1], sd_nbr[0])

glb_lenght = (
  lenght[0] * repeat[0], 
  lenght[1] * repeat[1], 
  lenght[2] * repeat[2]
)

glb_num = (
  (num[0] - 1) * repeat[0] + 1,
  (num[1] - 1) * repeat[1] + 1,
  (num[2] - 1) * repeat[2] + 1
)

InterfaceMapping, IntrfcNodesNbr = mshgen.GetInterfaceMapping(
                                                                num=num,
                                                                repeat=repeat,
                                                                Type="Dual"
                                                             )

maillage = mshgen.CreateCuboid(
                                start=start,
                                stop=glb_lenght, # mm
                                num=glb_num,
                                split=None,
                                grp5=("N","Dx-"),
                                grp1=("F","Dx+")
                              )

modele = femsol.CreateModel(maillage, "3D")

exact_material = femsol.CreateMaterial(
                                        type="isotrope",
                                        young= 210, # GPA
                                        poisson=0.3,
                                        gho=7.33e-6,
                                        alpha=1.,
                                        beta=1.
                                      )

wrong_material = femsol.CreateMaterial(
                                        type="isotrope",
                                        young= 150, # GPA
                                        poisson=0.2,
                                        gho=7.33e-6,
                                        alpha=790.,
                                        beta=520.
                                      )

exact_behavior = femsol.CreateBehavior(
                                        maillage,
                                        All=exact_material
                                      )

impdof = femsol.CreateBC( 
                           modele,
                           grp5=(
                                   ("DZ", 0.),
                                   ("DY", 0.),
                                   ("DX", 0.)
                                )
                        )

impeff = femsol.CreateLoad(modele)

impeff.ApplyFaceLoad(
                       grp1=(
                               ("Tx", 7.4e-2), # GPA
                               ("Ty", 3.2e-3),
                               ("Tz", 5.1e-3)
                            )
                    )

exact_stiff, exact_mass, exact_damp, exact_genefrts = femsol.Assemble(
                                                                        modele,
                                                                        exact_behavior,
                                                                        ImposedDof=impdof, 
                                                                        ImposedEfforts=impeff,
                                                                        StiffnessMatrix=True,
                                                                        StiffnessIntegType="Complete",
                                                                        MassMatrix=True,
                                                                        MassIntegType="Complete",
                                                                        GeneralizedEffort=True,
                                                                        GeneEfrtIntegType="Complete",
                                                                        DampingMatrix=True,
                                                                        StorageType="Sparse",
                                                                        StoreElementry=False
                                                                      )

dspfield_C = femsol.SolveLDYN(
                                exact_stiff,
                                exact_mass,
                                exact_damp,
                                exact_genefrts,
                                freq_val,
                                StorageType="Sparse"
                              )

for sd_idx in np.ndindex(sd_nbr):
   z_idx, y_idx, x_idx = sd_idx
   sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]

   x_start = start[0] + lenght[0] * x_idx
   y_start = start[1] + lenght[1] * y_idx
   z_start = start[2] + lenght[2] * z_idx

   x_stop = x_start + lenght[0]
   y_stop = y_start + lenght[1]
   z_stop = z_start + lenght[2]

   sd_selector_cond_x = True if sd_selector['x'] is None else x_idx == sd_selector['x']
   sd_selector_cond_y = True if sd_selector['y'] is None else y_idx == sd_selector['y']
   sd_selector_cond_z = True if sd_selector['z'] is None else z_idx == sd_selector['z']

   sd_have_error = sd_selector_cond_x and sd_selector_cond_y and sd_selector_cond_z

   split_str = 'None' if not sd_have_error else '({},{},{})'.format(\
                              sd_splitter['x'], sd_splitter['y'], sd_splitter['z'])
   tup_str = ", _ " if sd_have_error else " "

   mesh_str = "maillage_{}{}= mshgen.CreateCuboid(\
      start=({},{},{}),\
      stop=({},{},{}),\
      num=({},{},{}),\
      split={}".format(sd_glb_idx, tup_str,        \
                       x_start, y_start, z_start,  \
                       x_stop, y_stop, z_stop,     \
                       num[0], num[1], num[2],     \
                       split_str)

   if x_idx == 0:
      mesh_str += ", grp5=('N','Dx-')"

   if x_idx + 1 == sd_nbr[2]:
      mesh_str += ", grp1=('F','Dx+')"
   
   mesh_str += ")"
   
   exec(mesh_str)

   model_str = "modele_{} = femsol.CreateModel(maillage_{}, '3D')".format(\
                                                   sd_glb_idx, sd_glb_idx)
   
   exec(model_str)

   behav_str = "behavior_{}   = femsol.CreateBehavior( \
                                       maillage_{}".format(sd_glb_idx, sd_glb_idx)
   if not sd_have_error:
      behav_str += ", All=exact_material)"
   else:
      for sd_sd_idx in np.ndindex((sd_splitter['z'], sd_splitter['y'], sd_splitter['x'])):
         sd_z_idx, sd_y_idx, sd_x_idx = sd_sd_idx
         sd_sd_selector_cond_x = True if error_selector['x'] is None \
                                          else sd_x_idx == error_selector['x']
         sd_sd_selector_cond_y = True if error_selector['y'] is None \
                                          else sd_y_idx == error_selector['y']
         sd_sd_selector_cond_z = True if error_selector['z'] is None \
                                          else sd_z_idx == error_selector['z']

         sd_sd_have_error = sd_sd_selector_cond_x and sd_sd_selector_cond_y and sd_sd_selector_cond_z

         material_name = "wrong_material" if sd_sd_have_error else "exact_material"
         sd_sd_name = "GRP_x{}_y{}_z{}".format(sd_x_idx, sd_y_idx, sd_z_idx)

         behav_str += ", {}={}".format(sd_sd_name, material_name)
      
      behav_str += ")"


   exec(behav_str)

   if x_idx == 0:
      impdof_str = "impdof_{} = femsol.CreateBC(      \
                           modele_{},                 \
                           grp5=(                     \
                                   ('DZ', 0.),        \
                                   ('DY', 0.),        \
                                   ('DX', 0.)         \
                                )                     \
                        )".format(sd_glb_idx, sd_glb_idx)
      
      exec(impdof_str)
   
   if x_idx + 1 == sd_nbr[2]:
      impeff_str_0 ="impeff_{} = femsol.CreateLoad(modele_{})".format(\
                                                   sd_glb_idx, sd_glb_idx)
      exec(impeff_str_0)

      impeff_str_1 ="impeff_{}.ApplyFaceLoad(               \
                       grp1=(                               \
                               ('Tx', 7.4e-2),              \
                               ('Ty', 3.2e-3),              \
                               ('Tz', 5.1e-3),              \
                            )                               \
                    )".format(sd_glb_idx)
      
      exec(impeff_str_1)
   
   asse_str = "stif_mat_{}, mass_mat_{}, damp_mat_{}, gene_eff_{}".format(\
                                       sd_glb_idx, sd_glb_idx, sd_glb_idx, sd_glb_idx)

   asse_str += " = femsol.Assemble(       \
                               modele_{},  \
                               behavior_{},".format(sd_glb_idx, sd_glb_idx)
   if x_idx == 0:
      asse_str += "  \
                               ImposedDof=impdof_{},".format(sd_glb_idx)
   if x_idx + 1 == sd_nbr[2]:
      asse_str += "  \
                               ImposedEfforts=impeff_{},".format(sd_glb_idx)
   asse_str += "  \
                               StiffnessMatrix=True,           \
                               StiffnessIntegType='Complete',  \
                               MassMatrix=True,                \
                               MassIntegType='Complete',       \
                               DampingMatrix=True,             \
                               GeneralizedEffort=True,         \
                               GeneEfrtIntegType='Complete',   \
                               StorageType='Sparse',           \
                               StoreElementry=True             \
                            )"
   
   exec(asse_str)

   is_right  = y_idx == 0
   is_left   = y_idx == sd_nbr[1] - 1
   is_bottom = z_idx == 0
   is_top    = z_idx == sd_nbr[0] - 1

   have_measures = is_right or is_left or is_bottom or is_top

   if have_measures:
      glb_measures_dict_str = "MeasuredDofs_glb_{} = {{}}".format(sd_glb_idx)
      exec(glb_measures_dict_str)

      sd_measures_dict_str  = "MeasuredDofs_sd_{} = {{}}".format(sd_glb_idx)
      exec(sd_measures_dict_str)

      m_idx_x_shift = x_idx * (num[0] - 1)
      m_idx_y_shift = y_idx * (num[1] - 1)
      m_idx_z_shift = z_idx * (num[2] - 1)

      if is_right:
         for mes_point_coord_1, mes_point_coord_2 in measures_indices:
            sd_node_coord  = (mes_point_coord_1, 0, mes_point_coord_2)
            sd_node_name  = "'Node{}'".format(nodeidx(sd_node_coord, num))
            sd_mes_dict_str = "MeasuredDofs_sd_{}[{}] = ('DX', 'DY', 'DZ')".format(sd_glb_idx, sd_node_name)
            exec(sd_mes_dict_str)

            glb_node_coord = (mes_point_coord_1 + m_idx_x_shift,\
                              0, mes_point_coord_2 + m_idx_z_shift)
            glb_node_name = "'Node{}'".format(nodeidx(glb_node_coord, glb_num))
            glb_mes_dict_str = "MeasuredDofs_glb_{}[{}] = ('DX', 'DY', 'DZ')".format(sd_glb_idx, glb_node_name)
            exec(glb_mes_dict_str)

      if is_left:
         for mes_point_coord_1, mes_point_coord_2 in measures_indices:
            sd_node_coord  = (mes_point_coord_1, num[1] - 1, mes_point_coord_2)
            sd_node_name  = "'Node{}'".format(nodeidx(sd_node_coord, num))
            sd_mes_dict_str = "MeasuredDofs_sd_{}[{}] = ('DX', 'DY', 'DZ')".format(sd_glb_idx, sd_node_name)
            exec(sd_mes_dict_str)

            glb_node_coord = (mes_point_coord_1 + m_idx_x_shift,\
                              glb_num[1] - 1, mes_point_coord_2 + m_idx_z_shift)
            glb_node_name = "'Node{}'".format(nodeidx(glb_node_coord, glb_num))
            glb_mes_dict_str = "MeasuredDofs_glb_{}[{}] = ('DX', 'DY', 'DZ')".format(sd_glb_idx, glb_node_name)
            exec(glb_mes_dict_str)

      if is_bottom:
         for mes_point_coord_1, mes_point_coord_2 in measures_indices:
            sd_node_coord  = (mes_point_coord_1, mes_point_coord_2, 0)
            sd_node_name  = "'Node{}'".format(nodeidx(sd_node_coord, num))
            sd_mes_dict_str = "MeasuredDofs_sd_{}[{}] = ('DX', 'DY', 'DZ')".format(sd_glb_idx, sd_node_name)
            exec(sd_mes_dict_str)

            glb_node_coord = (mes_point_coord_1 + m_idx_x_shift,\
                              mes_point_coord_2 + m_idx_y_shift, 0)
            glb_node_name = "'Node{}'".format(nodeidx(glb_node_coord, glb_num))
            glb_mes_dict_str = "MeasuredDofs_glb_{}[{}] = ('DX', 'DY', 'DZ')".format(sd_glb_idx, glb_node_name)
            exec(glb_mes_dict_str)

      if is_top:
         for mes_point_coord_1, mes_point_coord_2 in measures_indices:
            sd_node_coord  = (mes_point_coord_1, mes_point_coord_2, num[2] - 1)
            sd_node_name  = "'Node{}'".format(nodeidx(sd_node_coord, num))
            sd_mes_dict_str = "MeasuredDofs_sd_{}[{}] = ('DX', 'DY', 'DZ')".format(sd_glb_idx, sd_node_name)
            exec(sd_mes_dict_str)

            glb_node_coord = (mes_point_coord_1 + m_idx_x_shift,\
                              mes_point_coord_2 + m_idx_y_shift, glb_num[2] - 1)
            glb_node_name = "'Node{}'".format(nodeidx(glb_node_coord, glb_num))
            glb_mes_dict_str = "MeasuredDofs_glb_{}[{}] = ('DX', 'DY', 'DZ')".format(sd_glb_idx, glb_node_name)
            exec(glb_mes_dict_str)

      glb_mes_numebring = "MeasuresNumbering_glb_{} = dspfield_C.GetDofsNumbring( \
                                                         **MeasuredDofs_glb_{})".format(sd_glb_idx, sd_glb_idx)
      exec(glb_mes_numebring)

      sd_mes_numebring = "MeasuresNumbering_{} = stif_mat_{}.GetDofsNumbring( \
                                              **MeasuredDofs_sd_{})".format(sd_glb_idx, sd_glb_idx, sd_glb_idx)
      exec(sd_mes_numebring)

      mes_values = "MeasuresValues_{} = dspfield_C.GetDofs( \
                                              MeasuresNumbering_glb_{})[None,:]".format(sd_glb_idx, sd_glb_idx)
      exec(mes_values)

   mcre_str = "mcre_obj_{} = femsol.CreateLocDynMcre(                       \
                                                   stif_mat_{},          \
                                                   mass_mat_{},          \
                                                   damp_mat_{},          \
                                                   gene_eff_{},          \
                                                   [{}],".format(sd_glb_idx, sd_glb_idx,
                                                                 sd_glb_idx, sd_glb_idx,
                                                                 sd_glb_idx, freq_val)

   if have_measures:
      mcre_str += "                                MeasuresNumbering_{}, \
                                                   MeasuresValues_{},".format(sd_glb_idx, sd_glb_idx)
   else:
      mcre_str += "                                None, \
                                                   None,"

   mcre_str += "                                   ErrType='Drkr',       \
                                                   StorageType='Sparse'  \
                                                 )"

   exec(mcre_str)

   numbering_str = "numbering_{} = stif_mat_{}.GetDofNumbering()".format(sd_glb_idx, sd_glb_idx)
   exec(numbering_str)
   
   mat_str = "mat_val_{} = mcre_obj_{}.GetMcreMat()".format(sd_glb_idx, sd_glb_idx)
   exec(mat_str)
   
   vec_str = "vec_val_{} = mcre_obj_{}.GetMcreVec()".format(sd_glb_idx, sd_glb_idx)
   exec(vec_str)

   intrfc_nmbrg_str = "local_intrfc_indices_{}, global_intrfc_indices_{}   \
            = InterfaceMapping[{}][{}][{}]".format(sd_glb_idx, sd_glb_idx, \
               z_idx, y_idx, x_idx)
   exec(intrfc_nmbrg_str)

mcre_seqddm_str = "mcre_seqddm = femsol.CreateSeqDDM(                 \
                                    type='Dual',                    \
                                    blocks_nbr=3,                     \
                                    intrfc_nodes_nbr=IntrfcNodesNbr"

for sd_idx in np.ndindex(sd_nbr):
   z_idx, y_idx, x_idx = sd_idx
   sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]

   mcre_seqddm_str += ",   \
                                      SubDomain_{}=(                \
                                          mat_val_{},               \
                                          vec_val_{},               \
                                          local_intrfc_indices_{},  \
                                          global_intrfc_indices_{}, \
                                          numbering_{}  \
                                      )".format(sd_glb_idx, sd_glb_idx, \
                                               sd_glb_idx, sd_glb_idx,  \
                                               sd_glb_idx, sd_glb_idx)

mcre_seqddm_str += "   \
                                 )"

exec(mcre_seqddm_str)

# exec("mcre_seqddm.SolveSeqDDM()")
exec("mcre_seqddm.SolveSeqDDM(stype='Iterative', mtype='CG', tol=9e-2)")

for sd_idx in np.ndindex(sd_nbr):
   z_idx, y_idx, x_idx = sd_idx
   sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]

   sd_res_str = "extra_bc_indices_{}, extra_bc_vals_{} = \
      mcre_seqddm.GetSdResult({})".format(sd_glb_idx, sd_glb_idx, sd_glb_idx)

   exec(sd_res_str)

exec("del mcre_seqddm")


for sd_idx in np.ndindex(sd_nbr):
   z_idx, y_idx, x_idx = sd_idx
   sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]
   mcre_sol_str = "mcre_obj_{}.CalcDynMcre(                               \
                                          FreqIdx=0,                      \
                                          ExtraLoad=(                     \
                                                     extra_bc_indices_{}, \
                                                     extra_bc_vals_{}     \
                                                  )                       \
                                       )".format(sd_glb_idx, sd_glb_idx, sd_glb_idx)
   exec(mcre_sol_str)

res_decl_str = "res = femsol.CreateResults(modele_0"

for sd_idx in np.ndindex(sd_nbr):
   z_idx, y_idx, x_idx = sd_idx
   sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]
   if sd_glb_idx != 0:
      res_decl_str += ", modele_{}".format(sd_glb_idx)

res_decl_str += ")"

exec(res_decl_str)

for sd_idx in np.ndindex(sd_nbr):
   z_idx, y_idx, x_idx = sd_idx
   sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]
   res_add_str = "res.AddResult(mcre_obj_{})".format(sd_glb_idx)
   exec(res_add_str)

filename = "test_mcre_seqddm_DUAL_x{}_y{}_z{}".format(sd_nbr[2], sd_nbr[1], sd_nbr[0])
exec("res.Export(filename, AddSd=True)")


















