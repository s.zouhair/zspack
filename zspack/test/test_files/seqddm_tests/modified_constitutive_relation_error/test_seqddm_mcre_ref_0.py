from zspack import femsol, mshgen
import numpy as np

def nodeidx(nc, mc):
    return nc[0] + nc[1] * mc[0] + nc[2] * mc[0] * mc[1]
  
def have_wrong_behav(sd_sd_glb_coord, sd_splitter, sd_selector, error_selector):
    sd_idx = (
      sd_sd_glb_coord[0] // sd_splitter['x'],
      sd_sd_glb_coord[1] // sd_splitter['y'],
      sd_sd_glb_coord[2] // sd_splitter['z']
    )

    sd_selector_cond_x = True if sd_selector['x'] is None else sd_idx[0] == sd_selector['x']
    sd_selector_cond_y = True if sd_selector['y'] is None else sd_idx[1] == sd_selector['y']
    sd_selector_cond_z = True if sd_selector['z'] is None else sd_idx[2] == sd_selector['z']

    sd_have_error = sd_selector_cond_x and sd_selector_cond_y and sd_selector_cond_z

    sd_sd_loc_coord = (
        sd_sd_glb_coord[0] % sd_splitter['x'],
        sd_sd_glb_coord[1] % sd_splitter['y'],
        sd_sd_glb_coord[2] % sd_splitter['z']
    )

    sd_sd_selector_cond_x = True if error_selector['x'] is None \
                                              else sd_sd_loc_coord[0] == error_selector['x']
    sd_sd_selector_cond_y = True if error_selector['y'] is None \
                                              else sd_sd_loc_coord[1] == error_selector['y']
    sd_sd_selector_cond_z = True if error_selector['z'] is None \
                                              else sd_sd_loc_coord[2] == error_selector['z']

    sd_sd_have_error = sd_sd_selector_cond_x and sd_sd_selector_cond_y and sd_sd_selector_cond_z

    return sd_have_error and sd_sd_have_error

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sd_nbr = (3, 3, 1)
num = (19, 3, 3)
start  = (0, 0, 0)
lenght = (240, 15, 10)

measures_indices = [
    (1, 1),
    (2, 2),
    (3, 1),
    (4, 2),
    (5, 1),
    (6, 2),
    (7, 1),
    (9, 1),
    (10, 2),
    (11, 1),
    (12, 2),
    (13, 1),
    (14, 2),
    (15, 1),
    (16, 2),
    (17, 1)
]

# Used to select subdomains where modeling 
# errors will be enforced
sd_selector = {
   'x' : None,
   'y' : None,
   'z' : None
}

# Used to split subdomains where modeling 
# errors are enforced
sd_splitter = {
   'x' : 9,
   'y' : 1,
   'z' : 1
}

# Used to select elements groups where 
# errors will be enforced (only for 
# concerned subdomains)
error_selector = {
   'x' : 2,
   'y' : None,
   'z' : None
}

freq_val = 1.5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

repeat = (sd_nbr[2], sd_nbr[1], sd_nbr[0])

glb_lenght = (
    lenght[0] * repeat[0], 
    lenght[1] * repeat[1], 
    lenght[2] * repeat[2]
)

glb_num = (
    (num[0] - 1) * repeat[0] + 1,
    (num[1] - 1) * repeat[1] + 1,
    (num[2] - 1) * repeat[2] + 1
)

glb_split = (
    repeat[0] * sd_splitter['x'],
    repeat[1] * sd_splitter['y'],
    repeat[2] * sd_splitter['z']
)

maillage, _ = mshgen.CreateCuboid(
                                    start=start,
                                    stop=glb_lenght, # mm
                                    num=glb_num,
                                    split=glb_split,
                                    grp5=("N","Dx-"),
                                    grp1=("F","Dx+")
                                  )

modele = femsol.CreateModel(maillage, "3D")

exact_material = femsol.CreateMaterial(
                                        type="isotrope",
                                        young= 210, # GPA
                                        poisson=0.3,
                                        gho=7.33e-6,
                                        alpha=1.,
                                        beta=1.
                                      )

wrong_material = femsol.CreateMaterial(
                                        type="isotrope",
                                        young= 150, # GPA
                                        poisson=0.2,
                                        gho=7.33e-6,
                                        alpha=790.,
                                        beta=520.
                                      )

exact_behavior = femsol.CreateBehavior(
                                        maillage,
                                        All=exact_material
                                      )

elements_dict_wrong = {
  "GRP_x{}_y{}_z{}".format(*elmnt_grp_coord) : wrong_material \
  for elmnt_grp_coord in np.ndindex(glb_split) \
  if have_wrong_behav(elmnt_grp_coord, sd_splitter, sd_selector, error_selector)
}

elements_dict_exact = {
  "GRP_x{}_y{}_z{}".format(*elmnt_grp_coord) : exact_material \
  for elmnt_grp_coord in np.ndindex(glb_split) \
  if not have_wrong_behav(elmnt_grp_coord, sd_splitter, sd_selector, error_selector)
}

wrong_behavior = femsol.CreateBehavior(
                                        maillage,
                                        **{**elements_dict_wrong, **elements_dict_exact}
                                      )

impdof = femsol.CreateBC( 
                           modele,
                           grp5=(
                                   ("DZ", 0.),
                                   ("DY", 0.),
                                   ("DX", 0.)
                                )
                        )

impeff = femsol.CreateLoad(modele)

impeff.ApplyFaceLoad(
                       grp1=(
                               ("Tx", 7.4e-2), # GPA
                               ("Ty", 3.2e-3),
                               ("Tz", 5.1e-3)
                            )
                    )

exact_stiff, exact_mass, exact_damp, exact_genefrts = femsol.Assemble(
                                                                        modele,
                                                                        exact_behavior,
                                                                        ImposedDof=impdof, 
                                                                        ImposedEfforts=impeff,
                                                                        StiffnessMatrix=True,
                                                                        StiffnessIntegType="Complete",
                                                                        MassMatrix=True,
                                                                        MassIntegType="Complete",
                                                                        GeneralizedEffort=True,
                                                                        GeneEfrtIntegType="Complete",
                                                                        DampingMatrix=True,
                                                                        StorageType="Sparse",
                                                                        StoreElementry=False
                                                                      )

wrong_stiff, wrong_mass, wrong_damp, wrong_genefrts = femsol.Assemble(
                                                                        modele,
                                                                        wrong_behavior,
                                                                        ImposedDof=impdof, 
                                                                        ImposedEfforts=impeff,
                                                                        StiffnessMatrix=True,
                                                                        StiffnessIntegType="Complete",
                                                                        MassMatrix=True,
                                                                        MassIntegType="Complete",
                                                                        GeneralizedEffort=True,
                                                                        GeneEfrtIntegType="Complete",
                                                                        DampingMatrix=True,
                                                                        StorageType="Sparse",
                                                                        StoreElementry=True
                                                                      )

dspfield_C = femsol.SolveLDYN(
                                exact_stiff,
                                exact_mass,
                                exact_damp,
                                exact_genefrts,
                                freq_val,
                                StorageType="Sparse"
                              )

MeasuredDofs = {}

for sd_idx in np.ndindex(sd_nbr):
    z_idx, y_idx, x_idx = sd_idx
    sd_glb_idx = x_idx + y_idx * sd_nbr[2] + z_idx * sd_nbr[2] * sd_nbr[1]

    is_right  = y_idx == 0
    is_left   = y_idx == sd_nbr[1] - 1
    is_bottom = z_idx == 0
    is_top    = z_idx == sd_nbr[0] - 1

    have_measures = is_right or is_left or is_bottom or is_top

    if have_measures:
      m_idx_x_shift = x_idx * (num[0] - 1)
      m_idx_y_shift = y_idx * (num[1] - 1)
      m_idx_z_shift = z_idx * (num[2] - 1)

      if is_right:
        for mes_point_coord_1, mes_point_coord_2 in measures_indices:
            glb_node_coord = (mes_point_coord_1 + m_idx_x_shift,\
                              0, mes_point_coord_2 + m_idx_z_shift)
            glb_node_name = "Node{}".format(nodeidx(glb_node_coord, glb_num))
            MeasuredDofs[glb_node_name] = ('DX', 'DY', 'DZ')
          
      if is_left:
        for mes_point_coord_1, mes_point_coord_2 in measures_indices:
            glb_node_coord = (mes_point_coord_1 + m_idx_x_shift,\
                              glb_num[1] - 1, mes_point_coord_2 + m_idx_z_shift)
            glb_node_name = "Node{}".format(nodeidx(glb_node_coord, glb_num))
            MeasuredDofs[glb_node_name] = ('DX', 'DY', 'DZ')

      if is_bottom:
        for mes_point_coord_1, mes_point_coord_2 in measures_indices:
            glb_node_coord = (mes_point_coord_1 + m_idx_x_shift,\
                              mes_point_coord_2 + m_idx_y_shift, 0)
            glb_node_name = "Node{}".format(nodeidx(glb_node_coord, glb_num))
            MeasuredDofs[glb_node_name] = ('DX', 'DY', 'DZ')

      if is_top:
        for mes_point_coord_1, mes_point_coord_2 in measures_indices:
            glb_node_coord = (mes_point_coord_1 + m_idx_x_shift,\
                              mes_point_coord_2 + m_idx_y_shift, glb_num[2] - 1)
            glb_node_name = "Node{}".format(nodeidx(glb_node_coord, glb_num))
            MeasuredDofs[glb_node_name] = ('DX', 'DY', 'DZ')

MeasuresNumbering = dspfield_C.GetDofsNumbring(**MeasuredDofs)
MeasuresValues    = dspfield_C.GetDofs(MeasuresNumbering)[None,:]

McreObj = femsol.CreateLocDynMcre(
                                wrong_stiff, 
                                wrong_mass,
                                wrong_damp,
                                wrong_genefrts,
                                [freq_val],
                                MeasuresNumbering,
                                MeasuresValues,
                                ErrType="Drkr",
                                StorageType="Sparse"
                              )

McreObj.CalcDynMcre()

res = femsol.CreateResults(modele)
res.AddResult(McreObj)
res.Export("test_seqddm_mcre_ref_x{}_y{}_z{}".format(*repeat))






























