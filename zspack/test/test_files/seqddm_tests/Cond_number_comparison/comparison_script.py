from MCRE_SEQDDM import SeqDDM_Mcre
from LE_SEQDDM import SeqDDM_Le

import matplotlib.pyplot as plt
import numpy as np

measures_indices = [
     (1, 1),
     (2, 2),
]

sd_selector = {
   'x' : None,
   'y' : None,
   'z' : None
}

sd_splitter = {
   'x' : 3,
   'y' : 3,
   'z' : 3
}

error_selector = {
   'x' : 1,
   'y' : 1,
   'z' : 1
}

freq_val = 1.5
mtype = "Primal"

test_cases = [
    {
        "sd_nbr" : (int(x_sd_nbr), int(x_sd_nbr), 1),
        "num"    : (4, 4, 4),
        "start"  : (0,  0, 0),
        "lenght" : (200, 200, 200)
    }

    for x_sd_nbr in np.arange(5) + 2
]

tests_nbr = len(test_cases)

subdomains_nbr = np.zeros(tests_nbr, dtype=int)

mcre_rhs_norm = np.zeros(tests_nbr)
mcre_opr_cond = np.zeros(tests_nbr)

le_rhs_norm = np.zeros(tests_nbr)
le_opr_cond = np.zeros(tests_nbr)

for test_idx, test_dict in enumerate(test_cases):
    norm_1, cond_1 = SeqDDM_Mcre(mtype, measures_indices, sd_selector, 
                sd_splitter, error_selector, freq_val, **test_dict)
    
    mcre_rhs_norm[test_idx] = norm_1
    mcre_opr_cond[test_idx] = cond_1

    norm_2, cond_2 = SeqDDM_Le(mtype, **test_dict)

    le_rhs_norm[test_idx] = norm_2
    le_opr_cond[test_idx] = cond_2

    sd_nbr = test_dict["sd_nbr"]

    subdomains_nbr[test_idx] = sd_nbr[0] * sd_nbr[1] * sd_nbr[2]

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)

ax1.plot(subdomains_nbr, mcre_rhs_norm, 'o-', label='mcre')
ax1.plot(subdomains_nbr, le_rhs_norm, 's-',label='linear elasticity')
ax1.set(xlabel='Subdomains number', ylabel='The norm of the righthand side')
ax1.legend(loc="upper right")

ax2.plot(subdomains_nbr, mcre_opr_cond, 'o-', label='mcre')
ax2.plot(subdomains_nbr, le_opr_cond, 's-', label='linear elasticity')
ax2.set(xlabel='Subdomains number', ylabel='The condition number')
ax2.legend(loc="upper right")

ax3.plot(subdomains_nbr, le_rhs_norm / mcre_rhs_norm, 'o-', label='mcre')
ax3.set(xlabel='Subdomains number', ylabel='le_norm / mcre_norm')

ax4.plot(subdomains_nbr, mcre_opr_cond / le_opr_cond, 'o-', label='mcre')
ax4.set(xlabel='Subdomains number', ylabel='mcre_cond / le_cond')

plt.show()