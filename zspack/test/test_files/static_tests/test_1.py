from zspack import femsol, mshgen, zstest
import numpy as np

maillage = mshgen.CreateCuboid(
                                start=(0,0,0),
                                stop=(720,72,48), # mm
                                num=(25,13,13),
                                split=None,
                                grp1=("F","Dx+"),
                                grp5=("N","Dx-"),
                              )

modele = femsol.CreateModel(maillage, "3D")

material = femsol.CreateMaterial(
                                  type="isotrope",
                                  young= 210, # GPA
                                  poisson=0.3,
                                  gho=2.33,
                                  alpha=50,
                                  beta=1e-7
                                )

behavior   = femsol.CreateBehavior(
                                     maillage,
                                     All=material
                                   )

impdof = femsol.CreateBC( 
                          modele, 
                          grp5=(
                                ("DZ", 0.0003),
                                ("DY", 0.0002),
                                ("DX", 0.0001)
                               )
                        )

impeff = femsol.CreateLoad(modele)

impeff.ApplyFaceLoad(
                      grp1=(
                            ("Ty", 3.2e-2),
                            ("Tz", 5.1e-2),
                            ("Tx", 7.4e-2) # GPA
                           )
                    )



stifmatrx_C, massmatrx_C, dampmatrx_C, genefrts_C = femsol.Assemble(
                                                                      modele,
                                                                      behavior,
                                                                      ImposedDof=impdof,
                                                                      ImposedEfforts=impeff,
                                                                      StiffnessMatrix=True,
                                                                      StiffnessIntegType="Complete",
                                                                      MassMatrix=True,
                                                                      MassIntegType="Complete",
                                                                      GeneralizedEffort=True,
                                                                      GeneEfrtIntegType="Complete",
                                                                      DampingMatrix=True,
                                                                      StorageType="Sparse",
                                                                      StoreElementry=False
                                                                   )

# print("The det of stifmatrx_C is : ", np.linalg.det(stifmatrx_C.GetDenseVal()))
# print("The symmetry of stifmatrx_C is : ", np.allclose(stifmatrx_C.GetDenseVal(), stifmatrx_C.GetDenseVal().T))

# print(stifmatrx_C.GetElmntryMatVal(17))
# print(massmatrx_C.GetElmntryMatVal(17))
# print(dampmatrx_C.GetElmntryMatVal(17))

dspfield_C = femsol.SolveLE(
                           StiffMatrix=stifmatrx_C,
                           GeneEfforts=genefrts_C,
                           StorageType="Sparse"
                         )

# Check if the stiffness matrix is defined positive
# L = np.linalg.cholesky(stifmatrx_C.GetValues(StorageType="Dense"))
# M = np.linalg.cholesky(massmatrx_C.GetValues(StorageType="Dense"))
# C = np.linalg.cholesky(dampmatrx_C.GetValues(StorageType="Dense"))

aster_dspls = zstest.GetAsterRes(1, "Linear_Elasticity")


# young en GPA -> dimenssions en millimètre
# young en PA  -> dimenssions en mètre

femsol_dspls = dspfield_C.GetAllDofs()

diff = np.empty(femsol_dspls.shape)

for idx, (v1, v2) in enumerate(zip(femsol_dspls, aster_dspls)):
    diff[idx] = v1 - v2

print("{} degrees of freedom".format(femsol_dspls.size))
print("Same result as Aster : ", np.allclose(femsol_dspls, aster_dspls, atol=1e-06))
print("Error range : {} -> {}.".format(np.amin(diff), np.amax(diff)))


res = femsol.CreateResults(modele)
res.AddResult(dspfield_C)
res.Export("test_le_1")































