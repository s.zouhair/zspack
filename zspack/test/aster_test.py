import csv
import numpy as np
from os import path

from zspack import mshgen

TestsParam = [
    # Test 0 :
    {
        "start" : (0,0,0),
        "stop"  : (800,900,400),
        "num"   : (7,7,7)
    },
    # Test 1 :
    {
        "start" : (0,0,0),
        "stop"  : (720,72,48),
        "num"   : (25,13,13),
    },
    # Test 2 :
    {
        "start" : (0,0,0),
        "stop"  : (720,72,48), # mm
        "num"   : (25,13,13),
    },
]

def GetAsterRes(TestNbr, StudyType, FieldPart=None):

    assert(isinstance(TestNbr, int))
    assert(isinstance(StudyType, str))

    if StudyType == "Linear_Elasticity":
        assert(FieldPart is None)
        FileName = "atest_le_{}.csv".format(TestNbr)
        FamilyNode = 1
    elif StudyType == "Linear_Dynamic":
        assert(FieldPart in ["real", "imag"])
        FileName = "atest_ldyn_{}_{}.csv".format(TestNbr, FieldPart)
        FamilyNode = 0
    else:
        raise NotImplementedError

    FileDir = path.join(path.join(path.abspath(path.dirname(__file__)), "csv_files"), StudyType)
    FilePath = path.join(FileDir, FileName)

    X,  Y,  Z  = [], [], []
    DX, DY, DZ = [], [], []

    with open(FilePath, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        for idx, row in enumerate(spamreader):
            if idx:
                DX.append(float(row[0]))
                DY.append(float(row[1]))
                DZ.append(float(row[2]))
                X.append(float(row[3 + FamilyNode]))
                Y.append(float(row[4 + FamilyNode]))
                Z.append(float(row[5 + FamilyNode]))
        x_coord = np.array(X)
        y_coord = np.array(Y)
        z_coord = np.array(Z)
        del X, Y, Z
        x_dspls = np.array(DX)
        y_dspls = np.array(DY)
        z_dspls = np.array(DZ)
        del DX, DY, DZ

    _, sortedIdxs = mshgen.GetNodesIdxFromCoord(
                                                 **TestsParam[TestNbr],
                                                 x_coord=x_coord,
                                                 y_coord=y_coord,
                                                 z_coord=z_coord,
                                                 srtdIdx=True
                                               )

    aster_dspls = np.concatenate((x_dspls[:,None], y_dspls[:,None],
                                          z_dspls[:,None]), axis=1)[sortedIdxs]
    
    return aster_dspls

if __name__ == "__main__":
    '''
    print("x_coord : {} -> {} .".format(np.amin(X), np.amax(X)))
    print("x_dipls : {} -> {} .".format(np.amin(DX), np.amax(DX)))

    print("")
    print("")

    print("y_coord : {} -> {} .".format(np.amin(Y), np.amax(Y)))
    print("y_dipls : {} -> {} .".format(np.amin(DY), np.amax(DY)))

    print("")
    print("")

    print("z_coord : {} -> {} .".format(np.amin(Z), np.amax(Z)))
    print("z_dipls : {} -> {} .".format(np.amin(DZ), np.amax(DZ)))

    # print('x : ', dspls[sortedIdxs][:7][:,0])
    # print('y : ', dspls[sortedIdxs][6:43:7][:,1])
    # print('z : ', dspls[sortedIdxs][6:295:49][:,2])
    '''


